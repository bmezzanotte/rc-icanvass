//
//  ActivityAnswer.h
//  iCanvass
//
//  Created by Bernardino on 14/02/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ActivityHeader;

@interface ActivityAnswer : NSManagedObject

@property (nonatomic, retain) NSString * questionDefinitionUnid;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * unid;
@property (nonatomic, retain) NSString * value;
@property (nonatomic, retain) ActivityHeader *header;

@end
