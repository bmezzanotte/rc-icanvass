//
//  ActivityAnswer.m
//  iCanvass
//
//  Created by Bernardino on 14/02/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import "ActivityAnswer.h"
#import "ActivityHeader.h"


@implementation ActivityAnswer

@dynamic questionDefinitionUnid;
@dynamic type;
@dynamic unid;
@dynamic value;
@dynamic header;

@end
