//
//  ActivityDetailViewController.h
//  iCanvass
//
//  Created by Bernardino on 27/03/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Activity.h"
#import "Answer.h"
#import "CustomerActivityViewController.h"
#import "PickListViewController.h"
#import "NoteActivityViewController.h"

#import "RDDetailSimpleCell.h"

@interface ActivityDetailViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, CustomerActivityDelegate,PickListDelegate,NoteActivityDelegate, RDDetailCellDelegate, UIActionSheetDelegate, UITextFieldDelegate>{
    
}
@property (weak, nonatomic) IBOutlet UIBarButtonItem *editButton;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) Activity *activity;

- (IBAction)editAction:(id)sender;
@end