//
//  ActivityDetailViewController.m
//  iCanvass
//
//  Created by Bernardino on 27/03/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import "ActivityDetailViewController.h"
#import "DataBaseManager.h"
#import "Utility.h"
#import "Target.h"
#import "TargetQuestion.h"

#define kPickerAnimationDuration    0.40   // duration for the animation to slide the date picker into view
#define kDatePickerTag              99     // view tag identifiying the date picker view

#define kTitleKey       @"label"   // key for obtaining the data source item's title
#define kDateKey        @"date"    // key for obtaining the data source item's date value
#define kValueKey       @"value"
#define kHeightRow      @"heightRow"
#define kNumberOfLines  @"numberOfLines"

// keep track of which rows have date cells
#define kDateStartRow   0
#define kDateEndRow     0

static const float CELL_TABLE_MAX_FRAME = 569.0;

static NSString *kCustomerSegue = @"chooseCustomerSegue";
static NSString *kPickListSegue = @"pickListSegue";
static NSString *kNoteActivitySegue = @"NoteActivityIdentifier";

static NSString *kDateCellID = @"dateCell";     // the cells with date
static NSString *kDatePickerID = @"datePicker"; // the cell containing the date picker
static NSString *kOtherCell = @"otherCell";     // the remaining cells at the end
static NSString *kNumberCell = @"numberCell";     // the remaining cells at the end

@interface ActivityDetailViewController ()
@property (nonatomic, retain) NSString *pickerListValueChoosed;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *saveButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cancelButton;

- (IBAction)saveAction:(id)sender;
- (IBAction)cancelAction:(id)sender;

@property (nonatomic, strong) NSMutableArray *headerDataArray;
@property (nonatomic, strong) NSArray *questionsDataArray;
@property (nonatomic, strong) NSDateFormatter *dateFormatter;

// keep track which indexPath points to the cell with UIDatePicker
@property (nonatomic, strong) NSIndexPath *datePickerIndexPath;
@property (assign) NSInteger pickerCellRowHeight;


@property (nonatomic, strong) NSArray *sections;
@property (nonatomic, strong) NSArray *headerPattern;
@property (nonatomic, strong) DataBaseManager *dbManager;
@property (nonatomic, strong) Utility *util;

@property (nonatomic, strong) NSDictionary *cellClass;
@property (nonatomic, strong) NSDictionary *rowHeight;

@property (nonatomic) bool saveFailure;
@end

@implementation ActivityDetailViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    //hide back button
    self.navigationItem.hidesBackButton = YES;
    [self.navigationItem setTitle:NSLocalizedString(@"Dettaglio Attività", @"Titolo navigationBar nel dettaglio attività")];
    [self.editButton setTitle:NSLocalizedString(@"Note", @"Note")];
    
    NSURL *plistURL = [[NSBundle mainBundle] URLForResource:@"ActivityDetailTableConfiguration" withExtension:@"plist"];
    NSDictionary *plist = [NSDictionary dictionaryWithContentsOfURL:plistURL];
    self.sections = [plist valueForKey:@"sections"];
    self.cellClass = [[self.sections objectAtIndex:1] valueForKey:@"classDictionary"];
    self.rowHeight = [[self.sections objectAtIndex:1] valueForKey:@"rowHeightDictionary"];

    NSLog(@"num sezioni:%lu",(unsigned long)[self.sections count]);

    self.dbManager = [[DataBaseManager alloc] init];
    self.util = [[Utility alloc] init];
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setDateStyle:NSDateFormatterShortStyle];    // show short-style date format
    [self.dateFormatter setTimeStyle:NSDateFormatterNoStyle];

    self.headerPattern = [[self.sections objectAtIndex:0] valueForKey:@"rows"];
    self.saveButton.title = NSLocalizedString(@"Salva", @"Salva");
    
    int statusActivity = [[_activity valueForKey:@"status"] intValue];
    //int statusActivity = 2;
    if (statusActivity <= 1) {
        [self.saveButton setEnabled:YES];
        [self.cancelButton setTitle:NSLocalizedString(@"Annulla", @"Annulla")];
    }else{
        [self.saveButton setEnabled:NO];
        [self.cancelButton setTitle:NSLocalizedString(@"Chiudi", @"Chiudi")];
    }
    
    // obtain the picker view cell's height, works because the cell was pre-defined in our storyboard
    UITableViewCell *pickerViewCellToCheck = [self.tableView dequeueReusableCellWithIdentifier:kDatePickerID];
    self.pickerCellRowHeight = pickerViewCellToCheck.frame.size.height;
    
    // if the local changes while in the background, we need to be notified so we can update the date
    // format in the table view cells
    //
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(localeChanged:)
                                                 name:NSCurrentLocaleDidChangeNotification
                                               object:nil];

    
    // setup Activity data source
    [self prepareDataForDisplay];
}
- (void) prepareDataForDisplay
{
    // setup Activity Header data source
    self.headerDataArray = [[NSMutableArray alloc] init];
    for (NSDictionary *elem in self.headerPattern) {
        if ([[elem valueForKey:@"key"] isEqualToString:@"date"]) {
            NSDate *activityDate = self.activity.date;
            if (activityDate) {
                [self.headerDataArray addObject:[@{@"label":[elem valueForKey:@"label"],@"date":activityDate} mutableCopy]];
            }else{
                [self.headerDataArray addObject:[@{@"label":[elem valueForKey:@"label"],@"date":[NSDate date]} mutableCopy]];
            }
            
        }else{
            if ([self.activity respondsToSelector:(SEL)NSSelectorFromString([elem valueForKey:@"key"])]) {
                NSString *testValue = [self.activity performSelector:(SEL)NSSelectorFromString([elem valueForKey:@"key"])];
                if (testValue) {
                    if ([[self.activity performSelector:(SEL)NSSelectorFromString([elem valueForKey:@"key"])] isKindOfClass:[NSArray class]]) {
                        NSArray *testValueArray = [self.activity performSelector:(SEL)NSSelectorFromString([elem valueForKey:@"key"])];
                        testValue = @"";
                        for (NSManagedObject *customer in testValueArray) {
                            if ([testValue isEqualToString:@""]) {
                                testValue = [testValue stringByAppendingString:[customer valueForKey:@"name"]];
                            }else{
                                testValue = [testValue stringByAppendingString:[NSString stringWithFormat:@", %@",[customer valueForKey:@"name"]]];
                            }
                        }
                        
                        NSLog(@"%@",testValue);
                        [self.headerDataArray addObject:[@{@"label":[elem valueForKey:@"label"],@"value":testValue} mutableCopy]];
                    }else{
                        [self.headerDataArray addObject:[@{@"label":[elem valueForKey:@"label"],@"value":testValue} mutableCopy]];
                    }
                    
                }else{
                    if ([elem valueForKey:@"rowHeight"] != nil) {
                        if ([elem valueForKey:@"numberOfLines"] != nil) {
                            [self.headerDataArray addObject:[@{@"label":[elem valueForKey:@"label"],@"value":[elem valueForKey:@"placeholder"], @"rowHeight":[elem valueForKey:@"rowHeight"],@"numberOfLines":[elem valueForKey:@"numberOfLines"]} mutableCopy]];
                        }else{
                            [self.headerDataArray addObject:[@{@"label":[elem valueForKey:@"label"],@"value":[elem valueForKey:@"placeholder"], @"rowHeight":[elem valueForKey:@"rowHeight"]} mutableCopy]];
                        }
                    }else{
                        if ([elem valueForKey:@"numberOfLines"] != nil) {
                            [self.headerDataArray addObject:[@{@"label":[elem valueForKey:@"label"],@"value":[elem valueForKey:@"placeholder"],@"numberOfLines":[elem valueForKey:@"numberOfLines"]} mutableCopy]];
                        }else{
                            [self.headerDataArray addObject:[@{@"label":[elem valueForKey:@"label"],@"value":[elem valueForKey:@"placeholder"]} mutableCopy]];
                        }
                    }
                }
            }else{
                [self.headerDataArray addObject:[@{@"label":[elem valueForKey:@"label"],@"value":[elem valueForKey:@"placeholder"]} mutableCopy]];
            }
        }
    }
    
    // setup Activity Questions data source
    if (self.activity.targetSubject != nil) {
        Target *myTarget =  [[self.dbManager selectFromTable:@"Target"
                                               withPredicate:[NSPredicate predicateWithFormat:@"subject == %@",self.activity.targetSubject]
                                                     OrderBy:@"subject"
                                                 isAscending:YES]
                                                objectAtIndex:0];
    
        if (! [self.activity.targetUnid isEqualToString:myTarget.subjectUnid] | !self.questionsDataArray) {
            self.activity.targetUnid = myTarget.subjectUnid;
            self.activity.targetAllowMultipleInstances = myTarget.allowMultipleInstances;
            NSSet *myQuestions = myTarget.questions;
            
            NSSortDescriptor *sortDescriptor;
            sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"subject" ascending:YES];
            NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
            NSArray *questionsArray = [[myQuestions allObjects] sortedArrayUsingDescriptors:sortDescriptors];
            
            if (![self.activity.answers count]) {
                //il questionario non è ancora stato inizializzato
                self.activity.answers = [[NSMutableArray alloc] init];
                for (TargetQuestion *myQuestion in questionsArray) {
                    NSLog(@"answer subject: %@", myQuestion.subject);
                    NSLog(@"answer type: %@", myQuestion.type);
                    
                    Answer *myAnswer = [[Answer alloc] init];
                    
                    myAnswer.subject = myQuestion.subject;
                    myAnswer.questionUnid = myQuestion.unid;
                    myAnswer.type = myQuestion.type;
                    myAnswer.required = myQuestion.required;
                    
                    myAnswer.response = @"";
                    
                    [self.activity.answers addObject:myAnswer];
                }
            }else{
                //devo assicurarmi che non sia stata cancellata sul server alcuna domanda del questionario compilato in precedenza
                if ([self.activity.answers count] < [questionsArray count]){
                    for (int i=0; i<[self.activity.answers count]; i++) {
                        NSString *unid1 = [[self.activity.answers objectAtIndex:i] valueForKey:@"questionUnid"];
                        NSString *unid2 = [[questionsArray objectAtIndex:i] valueForKey:@"unid"];
                        if (![unid1 isEqualToString:unid2]) {
                            TargetQuestion *myQuestion = [questionsArray objectAtIndex:i];
                            
                            NSLog(@"answer subject: %@", myQuestion.subject);
                            NSLog(@"answer type: %@", myQuestion.type);
                            
                            Answer *myAnswer = [[Answer alloc] init];
                            
                            myAnswer.subject = myQuestion.subject;
                            myAnswer.questionUnid = myQuestion.unid;
                            myAnswer.type = myQuestion.type;
                            myAnswer.required = myQuestion.required;
                            
                            myAnswer.response = @"";
                            
                            [self.activity.answers insertObject:myAnswer atIndex:i];
                        }
                    }
                }
            }
            self.questionsDataArray = questionsArray;
        }
    }else{
        self.questionsDataArray = nil;
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:NSCurrentLocaleDidChangeNotification
                                                  object:nil];
}


#pragma mark - Locale

/*! Responds to region format or locale changes.
 */
- (void)localeChanged:(NSNotification *)notif
{
    // the user changed the locale (region format) in Settings, so we are notified here to
    // update the date format in the table view cells
    //
    [self.tableView reloadData];
}


#pragma mark - Utilities

/*! Determines if the given indexPath has a cell below it with a UIDatePicker.
 
 @param indexPath The indexPath to check if its cell has a UIDatePicker below it.
 */
- (BOOL)hasPickerForIndexPath:(NSIndexPath *)indexPath
{
    BOOL hasDatePicker = NO;
    
    NSInteger targetedRow = indexPath.row;
    targetedRow++;
    
    UITableViewCell *checkDatePickerCell =
    [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:targetedRow inSection:0]];
    UIDatePicker *checkDatePicker = (UIDatePicker *)[checkDatePickerCell viewWithTag:kDatePickerTag];
    
    hasDatePicker = (checkDatePicker != nil);
    return hasDatePicker;
}

/*! Updates the UIDatePicker's value to match with the date of the cell above it.
 */
- (void)updateDatePicker
{
    if (self.datePickerIndexPath != nil)
    {
        UITableViewCell *associatedDatePickerCell = [self.tableView cellForRowAtIndexPath:self.datePickerIndexPath];
        
        UIDatePicker *targetedDatePicker = (UIDatePicker *)[associatedDatePickerCell viewWithTag:kDatePickerTag];
        if (targetedDatePicker != nil)
        {
            // we found a UIDatePicker in this cell, so update it's date value
            //
            NSDictionary *itemData = self.headerDataArray[self.datePickerIndexPath.row - 1];
            [targetedDatePicker setDate:[itemData valueForKey:kDateKey] animated:NO];
        }
    }
}

/*! Determines if the UITableViewController has a UIDatePicker in any of its cells.
 */
- (BOOL)hasInlineDatePicker
{
    if (self.datePickerIndexPath != nil) {
        NSLog(@"inline date picker in section:%ld at row:%ld",(long)self.datePickerIndexPath.section, (long)self.datePickerIndexPath.row);
    }
    
    return (self.datePickerIndexPath != nil);
}

/*! Determines if the given indexPath points to a cell that contains the UIDatePicker.
 
 @param indexPath The indexPath to check if it represents a cell with the UIDatePicker.
 */
- (BOOL)indexPathHasPicker:(NSIndexPath *)indexPath
{
    return ([self hasInlineDatePicker] && self.datePickerIndexPath.row == indexPath.row);
}

/*! Determines if the given indexPath points to a cell that contains the start/end dates.
 
 @param indexPath The indexPath to check if it represents start/end date cell.
 */
- (BOOL)indexPathHasDate:(NSIndexPath *)indexPath
{
    BOOL hasDate = NO;
    
    if ((indexPath.row == kDateStartRow) ||
        (indexPath.row == kDateEndRow || ([self hasInlineDatePicker] && (indexPath.row == kDateEndRow + 1))))
    {
        hasDate = YES;
    }
    
    return hasDate;
}



#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.sections count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return NSLocalizedString(@"Dettaglio Attività", @"Dettaglio Attività");
    }else{
        return NSLocalizedString(@"Questionario", @"Questionario");
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0){
        return ([self indexPathHasPicker:indexPath] ? self.pickerCellRowHeight : self.tableView.rowHeight);
    }else{
        NSDictionary *itemData;
        itemData = self.questionsDataArray[indexPath.row];
        CGFloat rowH = 44.0;
        NSString *rowHeightString = [self.rowHeight valueForKey:[itemData valueForKey:@"type"]];
        rowH =[rowHeightString floatValue];
        return rowH;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger numRows = 0;
    if (section == 0) {
        numRows = self.headerDataArray.count;
    }else{
        numRows = self.questionsDataArray.count;
    }
    
    if ([self hasInlineDatePicker]){
        if (self.datePickerIndexPath.section == section) {
            // we have a date picker, so allow for it in the number of rows in this section
            return ++numRows;
        }
    }
    
    return numRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    NSString *cellID = nil;
    
    NSDictionary *itemData;
    if (indexPath.section == 0) {
        
        
        if ([self indexPathHasPicker:indexPath])
        {
            // the indexPath is the one containing the inline date picker
            cellID = kDatePickerID;     // the current/opened date picker cell
        }
        /*
        else if ([self indexPathHasDate:indexPath])
        {
            // the indexPath is one that contains the date information
            cellID = kDateCellID;       // the start/end date cells
        }
         */
        else
        {
            cellID = [[self.headerPattern objectAtIndex:indexPath.row] valueForKey:@"cellID"];
        }
        
        cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        
        
        // if we have a date picker open whose cell is above the cell we want to update,
        // then we have one more cell than the model allows
        NSInteger modelRow = indexPath.row;
        if (self.datePickerIndexPath != nil && self.datePickerIndexPath.row < indexPath.row)
        {
            modelRow--;
        }

        
        itemData = self.headerDataArray[modelRow];

        // proceed to configure our cell
        if ([cellID isEqualToString:kDateCellID])
        {
            // we have a date cells, populate label and date field
            cell.textLabel.text = [itemData valueForKey:kTitleKey];
            cell.detailTextLabel.text = [self.dateFormatter stringFromDate:[itemData valueForKey:kDateKey]];
        }
        else if ([cellID isEqualToString:kOtherCell])
        {
            // this cell is a non-date cell, just assign it's text label and value
            cell.textLabel.text = [itemData valueForKey:kTitleKey];
            cell.detailTextLabel.text = [itemData valueForKey:kValueKey];
        }
        int statusActivity = [[_activity valueForKey:@"status"] intValue];
        //int statusActivity = 2;
        if (statusActivity > 1) {
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }else{
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.selectionStyle = UITableViewCellSelectionStyleDefault;
        }
        
        if ([itemData valueForKey:kNumberOfLines] != nil) {
            cell.detailTextLabel.numberOfLines = [[itemData valueForKey:kNumberOfLines]intValue];
        }
        NSLog(@"cellID = %@", cellID);
        return cell;
        
    }else{
        NSString *cellClassName;

        itemData = self.questionsDataArray[indexPath.row];

        cellClassName = [self.cellClass valueForKey:[itemData valueForKey:@"type"]];
        
        
        
        
        //a scopo di test - forzare uso cella EDITABLE TEXT per domande di tipo numerico
//        if ([cellClassName isEqualToString:@"RDDetailNumberCell"]) {
//            cellClassName = @"RDDetailEditableTextCell";
//        }
        
        
        
        
        if (cellClassName == nil) {
            cellClassName = @"RDDetailSimpleCell";
        }
        
        RDDetailSimpleCell *cell = [tableView dequeueReusableCellWithIdentifier:cellClassName];
        if (cell == nil){
            Class cellClass = NSClassFromString(cellClassName);
            cell = [cellClass alloc];
            
            cell = [cell initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:cellClassName];
        }
        
        
        cell.delegate = self;
        cell.label.text = [itemData valueForKey:@"subject"];
        cell.key = [itemData valueForKey:@"unid"];
        
        cell.maxFrame = CELL_TABLE_MAX_FRAME;

        
        cell.value = nil;
        if (!cell.value) {
            if ([[itemData valueForKey:@"type"] isEqualToString:@"Number"]) {
                cell.value = [[self.activity.answers objectAtIndex:indexPath.row] valueForKey:@"response"];
                
            }else{
                if ([self.activity.answers count]) {
                    NSString *response = [[self.activity.answers objectAtIndex:indexPath.row] valueForKey:@"response"];
                    if (![response isEqualToString:@""]) {
                        cell.value = response;
                    }
                }
            }
        }
        

        if ([cellClassName isEqualToString:@"RDDetailPickListCell"]) {
            if (cell.value) {
                [cell setFieldValueWithString:cell.value];
            }else{
                [cell setFieldValueWithString:NSLocalizedString(@"- seleziona risposta -", @"- seleziona risposta -")];
            }
        } else if ([cellClassName isEqualToString:@"RDDetailNumberCell"]) {
            if ([cell.value length] == 0) {
                if ([[itemData valueForKey:@"required"] integerValue] == 1) {
                    [cell setFieldValueWithString:@""];
                    cell.value = nil;
                }else{
                    [cell setFieldValueWithString:@"0"];
                    cell.value = @"0";
                }
            }else{
                [cell setFieldValueWithString:cell.value];
            }

        } else if ([cellClassName isEqualToString:@"RDDetailEditableTextCell"]) {
            if (cell.value) {
                [cell setFieldValueWithString:cell.value];
            }
        } else if ([cellClassName isEqualToString:@"RDDetailYesNoCell"]) {
            
            NSString *valueON = @"1";
            NSString *valueOff = @"";
            
            /*
             //Implementare questo meccanismo, per rendere dinamica la scelta dei valori ON/OFF, usato per il CENSIMeNTO PV?
            if ([itemData valueForKey:@"valueWhenON"]) {
                valueON = [itemData valueForKey:@"valueWhenON"];
            }
            if ([itemData valueForKey:@"valueWhenOFF"]) {
                valueOff = [itemData valueForKey:@"valueWhenOFF"];
            }
             */
            
            cell.controlOnValue = valueON;
            cell.controlOffValue = valueOff;
            
            [cell setSwitchStatus:false];
            if (cell.value) {
                if ([cell.value isKindOfClass:[NSString class]]) {
                    NSString *currValue = (NSString *)cell.value;
                    if ([currValue isEqualToString:valueON]) {
                        [cell setSwitchStatus:true];
                    }
                }
            }
            
        }
        
        
        if ([[itemData valueForKey:@"required"] integerValue] == 1) {
            cell.label.text = [cell.label.text stringByAppendingString:@"*"];
            
            if (self.saveFailure && !cell.value) {
                [cell setRedColorForLabel];
            }
        }

        
        NSLog(@"Row: %ld",(long)indexPath.row);
        NSLog(@"Cell class: %@",cellClassName);
        NSLog(@"cellID = %@", cellID);
        return cell;
    }
    
    
}

/*! Adds or removes a UIDatePicker cell below the given indexPath.
 
 @param indexPath The indexPath to reveal the UIDatePicker.
 */
- (void)toggleDatePickerForSelectedIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView beginUpdates];
    
    NSArray *indexPaths = @[[NSIndexPath indexPathForRow:indexPath.row + 1 inSection:0]];
    
    // check if 'indexPath' has an attached date picker below it
    if ([self hasPickerForIndexPath:indexPath])
    {
        // found a picker below it, so remove it
        [self.tableView deleteRowsAtIndexPaths:indexPaths
                              withRowAnimation:UITableViewRowAnimationFade];
    }
    else
    {
        // didn't find a picker below it, so we should insert it
        [self.tableView insertRowsAtIndexPaths:indexPaths
                              withRowAnimation:UITableViewRowAnimationFade];
    }
    
    [self.tableView endUpdates];
}

/*! Reveals the date picker inline for the given indexPath, called by "didSelectRowAtIndexPath".
 
 @param indexPath The indexPath to reveal the UIDatePicker.
 */
- (void)displayInlineDatePickerForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // display the date picker inline with the table content
    [self.tableView beginUpdates];
    
    BOOL before = NO;   // indicates if the date picker is below "indexPath", help us determine which row to reveal
    if ([self hasInlineDatePicker])
    {
        before = self.datePickerIndexPath.row < indexPath.row;
    }
    
    BOOL sameCellClicked = (self.datePickerIndexPath.row - 1 == indexPath.row);
    
    // remove any date picker cell if it exists
    if ([self hasInlineDatePicker])
    {
        [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:self.datePickerIndexPath.row inSection:0]]
                              withRowAnimation:UITableViewRowAnimationFade];
        self.datePickerIndexPath = nil;
    }
    
    if (!sameCellClicked)
    {
        // hide the old date picker and display the new one
        NSInteger rowToReveal = (before ? indexPath.row - 1 : indexPath.row);
        NSIndexPath *indexPathToReveal = [NSIndexPath indexPathForRow:rowToReveal inSection:0];
        
        [self toggleDatePickerForSelectedIndexPath:indexPathToReveal];
        self.datePickerIndexPath = [NSIndexPath indexPathForRow:indexPathToReveal.row + 1 inSection:0];
    }
    
    // always deselect the row containing the start or end date
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self.tableView endUpdates];
    
    // inform our date picker of the current date to match the current cell
    [self updateDatePicker];
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    int statusActivity = [[_activity valueForKey:@"status"] intValue];
    //int statusActivity = 2;
    if (statusActivity <= 1) {
        if (indexPath.section == 0) {
            if ([cell.reuseIdentifier isEqualToString:@"dateCell"]){
                [self displayInlineDatePickerForRowAtIndexPath:indexPath];
            }else{
                if ([self hasInlineDatePicker]) {
                    NSLog(@"C'è un date picker aperto!");
                    // hide the old date picker
                    NSInteger rowToReveal = self.datePickerIndexPath.row -1;
                    NSIndexPath *indexPathToReveal = [NSIndexPath indexPathForRow:rowToReveal inSection:0];
                    
                    self.datePickerIndexPath = nil;
                    [self toggleDatePickerForSelectedIndexPath:indexPathToReveal];
                    
                    if (rowToReveal < indexPath.row) {
                        indexPath =[NSIndexPath indexPathForRow:indexPath.row -1  inSection:indexPath.section];
                    }
                }
            }
            
            if ([[[self.headerPattern objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"modalPicker"]){
                if (self.activity.isNewActivity || ![[self.headerPattern objectAtIndex:indexPath.row] valueForKey:@"lockWhenSaved"]) {
                    
                    if ([[[self.headerPattern objectAtIndex:indexPath.row]valueForKey:@"label"] isEqualToString:@"Oggetto"]) {
                        NSString *entity = [[self.headerPattern objectAtIndex:indexPath.row] valueForKey:@"sourceEntity"];
                        
                        if (!self.activity.periodCode) {
                            self.activity.date = [self.util getCurrentDate];
                            [self.activity setPeriodCodeWithDate:self.activity.date];
                        }
                        
                        NSString *selectedPeriod = self.activity.periodCode;
                        NSMutableArray *dataArray = [self.dbManager selectPropertyFromTable:entity
                                                                                      Where:[NSString stringWithFormat:@"status != '9' AND (periodCode == '%@' OR periodCode == 'R') AND disabledForInput != '1'",selectedPeriod]
                                                                                    OrderBy:@"subject" isAcending:true
                                                                                forProperty:@"subject"];
                        
                        if ([dataArray count] != 0) {
                            _pickerListValueChoosed = cell.detailTextLabel.text;
                            [self performSegueWithIdentifier:kPickListSegue sender:self];
                        }else{
                            UIAlertView * alert =  [[UIAlertView alloc ] initWithTitle:NSLocalizedString(@"Attenzione!", @"Attenzione!")
                                                                               message:NSLocalizedString(@"Nessun obiettivo registrato per il periodo in corso", @"Nessun obiettivo registrato per il periodo in corso")
                                                                              delegate:nil
                                                                     cancelButtonTitle:@"OK"
                                                                     otherButtonTitles:nil];
                            [alert show];
                        }
                    }else{
                        _pickerListValueChoosed = cell.detailTextLabel.text;
                        [self performSegueWithIdentifier:kPickListSegue sender:self];
                    }
                    //_pickerListValueChoosed = cell.detailTextLabel.text;
                    //[self performSegueWithIdentifier:kPickListSegue sender:self];
                }else{
                    //inserire finestra messaggio per spiegare che non si può modificare "Oggetto" (tipo di attività) su Attività già salvate
                    UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:NSLocalizedString(@"Attenzione!", @"Attenzione!")
                                                                     message:NSLocalizedString(@"Non è possibile modificare l'oggetto di un'attività già salvata", @"Non è possibile modificare l'oggetto di un'attività già salvata")
                                                                    delegate:nil
                                                           cancelButtonTitle:@"OK"
                                                           otherButtonTitles:nil];
                    [alert show];
                    [tableView deselectRowAtIndexPath:indexPath animated:YES];
                }
            } else if ([[[self.headerPattern objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"customerPicker"]) {
                [self performSegueWithIdentifier:kCustomerSegue sender:self];
            }else{
                [tableView deselectRowAtIndexPath:indexPath animated:YES];
            }
        }else{
            if ([cell.reuseIdentifier isEqualToString:@"RDDetailPickListCell"]) {
                
                [self performSegueWithIdentifier:kPickListSegue sender:self];
                _pickerListValueChoosed = cell.detailTextLabel.text;
            }else{
//              [tableView deselectRowAtIndexPath:indexPath animated:YES];
            }
        }
    }
}

/*
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    RDDetailSimpleCell *myCell = (RDDetailSimpleCell *)[self.tableView cellForRowAtIndexPath: indexPath];
    NSLog(@"cella %@",myCell.class);

    if ([myCell respondsToSelector:@selector(setValue:)]) {
        NSLog(@"Cell value = %@", myCell.value);
//        [[self.activity.answers objectAtIndex:indexPath.row] setResponse:myCell.value];
    }

}
*/

#pragma mark - Actions

/*! User chose to change the date by changing the values inside the UIDatePicker.
 
 @param sender The sender for this action: UIDatePicker.
 */
- (IBAction)dateAction:(id)sender
{
    NSIndexPath *targetedCellIndexPath = nil;
    
    if ([self hasInlineDatePicker])
    {
        // inline date picker: update the cell's date "above" the date picker cell
        //
        targetedCellIndexPath = [NSIndexPath indexPathForRow:self.datePickerIndexPath.row - 1 inSection:0];
    }
    else
    {
        // external date picker: update the current "selected" cell's date
        targetedCellIndexPath = [self.tableView indexPathForSelectedRow];
    }
    
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:targetedCellIndexPath];
    UIDatePicker *targetedDatePicker = sender;
    
    // update our data model
    self.activity.date = targetedDatePicker.date;
    [self.activity setPeriodCodeWithDate:self.activity.date];

    if (self.activity.periodCode) {
        NSMutableDictionary *itemData = self.headerDataArray[targetedCellIndexPath.row];
        [itemData setValue:targetedDatePicker.date forKey:@"date"];
        
        // update the cell's date string
        cell.detailTextLabel.text = [self.dateFormatter stringFromDate:targetedDatePicker.date];

    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Errore" message:@"Data non valida" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}


#pragma mark - Several Delegate Methods
- (void)setQuestion:(NSString *)questionUnid withValue:(NSString *)fieldValue {
    NSLog(@"question unid: %@",questionUnid);
    NSLog(@"Field value: %@",fieldValue);
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"questionUnid == %@",questionUnid];
    NSArray *result = [self.activity.answers filteredArrayUsingPredicate:predicate];
    
    [[result objectAtIndex:0] setResponse:fieldValue];
    
    
}

- (void)getListCustomerSelected:(NSMutableArray *)arrayCustomers{

    self.activity.customers = arrayCustomers;
    [self prepareDataForDisplay];
    [self.tableView reloadData];
    
}

-(void) getNoteInput:(NSString *)noteContent {

    self.activity.note = noteContent;
    
}

-(NSString *)getNote {
    
    return self.activity.note;

}

- (void)getSelectedItems:(NSMutableArray *)selectedItems{
    NSIndexPath *currentIndexPath = self.tableView.indexPathForSelectedRow;
    
    if (currentIndexPath.section == 0){
        NSString *currentCellKey = [[self.headerPattern objectAtIndex:currentIndexPath.row] valueForKey:@"key"];
        
        //se ho cambiato il tipo di attività, devo resettare il questionario
        if ([currentCellKey isEqualToString:@"targetSubject"]) {
            if (![self.activity.targetSubject isEqualToString:[selectedItems objectAtIndex:0]]) {
                self.activity.answers = nil;
            }
        }
        
        NSString *currentSelector = [NSString stringWithFormat:@"set%@:",[currentCellKey stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[currentCellKey substringToIndex:1] uppercaseString]]];
        
        if ([self.activity respondsToSelector:(SEL)NSSelectorFromString(currentSelector)]) {
            [self.activity performSelector:(SEL)NSSelectorFromString(currentSelector) withObject:[selectedItems objectAtIndex:0]];
        }
    }else{
        RDDetailSimpleCell *cell = (RDDetailSimpleCell *)[self.tableView cellForRowAtIndexPath:currentIndexPath];
        cell.value = [selectedItems objectAtIndex:0];
        
        [[self.activity.answers objectAtIndex:currentIndexPath.row] setResponse:cell.value];
    }
    
    [self prepareDataForDisplay];
    [self.tableView reloadData];

}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:kNoteActivitySegue]) {
        NoteActivityViewController *destination = segue.destinationViewController;
        destination.activityStatus = [self.activity.status intValue];
        destination.delegate = self;
    }
    if ([[segue identifier] isEqualToString:kCustomerSegue]) {
        UINavigationController *destination = (UINavigationController *)segue.destinationViewController;
        
        CustomerActivityViewController *listCustomerView = [[destination viewControllers]objectAtIndex:0];
        
        listCustomerView.customerSelectedList = [self.activity.customers mutableCopy];
        listCustomerView.delegate = self;
        
    }else if ([[segue identifier] isEqualToString:kPickListSegue]) {
        PickListViewController *destination =  [segue destinationViewController];
        
        int section = (int)[[self.tableView indexPathForSelectedRow] section];
        int index = (int)[[self.tableView indexPathForSelectedRow] row];
        
        if (section == 0) {
            NSString *sourceType = [[self.headerPattern objectAtIndex:index] valueForKey:@"sourceType"];
            if ([sourceType isEqualToString:@"array"]) {
                destination.dataArray = [[[self.headerPattern objectAtIndex:index] valueForKey:@"sourceArray"] mutableCopy];
            }else{
                NSString *entity = [[self.headerPattern objectAtIndex:index] valueForKey:@"sourceEntity"];
                NSString *selectedPeriod = self.activity.periodCode;
                NSMutableArray *dataArray = [self.dbManager selectPropertyFromTable:entity
                                                                              Where:[NSString stringWithFormat:@"status != '9' AND (periodCode == '%@' OR periodCode == 'R') AND disabledForInput != '1'",selectedPeriod]
                                                                            OrderBy:@"subject" isAcending:true
                                                                        forProperty:@"subject"];

                
                destination.dataArray = dataArray;
            }

            destination.pickerTitle = [[self.headerPattern objectAtIndex:index] valueForKey:@"placeholder"];
            
            if ([[self.headerDataArray objectAtIndex:index] valueForKey:@"value"]) {
                destination.selectedItems = [@[[[self.headerDataArray objectAtIndex:index] valueForKey:@"value"]] mutableCopy];
            }
            
        }else{
            NSString *options = [[self.questionsDataArray objectAtIndex:index] valueForKey:@"options"];
            NSArray *optionsArray = [options componentsSeparatedByString: @","];
            destination.dataArray = [optionsArray mutableCopy];
            destination.pickerTitle = @"Scegli risposta";
            
            if ([[self.activity.answers objectAtIndex:index] response]) {
                destination.selectedItems = [@[[[self.activity.answers objectAtIndex:index] response]] mutableCopy];
            }
        }

        
        destination.delegate = self;
        
    }
}

- (IBAction)saveAction:(id)sender {
   
    if (!self.activity.targetUnid) {
        //verifico che sia stato selezionato il tipo di attività
        NSString *alertTitle = NSLocalizedString(@"Errore", @"Errore");
        NSString *alertMessage = NSLocalizedString(@"Selezionare il tipo di attività", @"Selezionare il tipo di attività");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertTitle message:alertMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }else{
        
        NSString *saveAndClose = NSLocalizedString(@"Salva e chiudi", @"ActivityActionSheet0");
        NSString *saveAndNewActivity = NSLocalizedString(@"Salva e apri nuova attività stesso negozio", @"ActivityActionSheet1");
        NSString *saveAndNewShop = NSLocalizedString(@"Salva e apri stessa attività su nuovo negozio", @"ActivityActionSheet2");
        NSString *cancelTitle = NSLocalizedString(@"Annulla", @"ActivityActionSheet3");

        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:nil
                                      delegate:self
                                      cancelButtonTitle:cancelTitle
                                      destructiveButtonTitle:nil
                                      otherButtonTitles:saveAndClose, saveAndNewActivity,saveAndNewShop, nil];

        [actionSheet showInView:self.view];
        
        
        
/*
        int result = [self.activity save];
        
        if (result == 0) {
            //error alert
            NSString *alertTitle = NSLocalizedString(@"Errore", @"Errore");
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertTitle message:self.activity.saveErrorMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }else{
           [self.navigationController popViewControllerAnimated:YES];
        }
 */
    }
}

- (IBAction)cancelAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)editAction:(id)sender {
    [self performSegueWithIdentifier:@"NoteActivityIdentifier" sender:self];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (self.activity.periodCode) {
        [self.tableView reloadData];
    }
}

#pragma mark Action Sheet Delegate Methods
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{

    
    switch (buttonIndex) {
        case 0:
            if ([self.activity save] == 0) {
                //error alert
                NSString *alertTitle = NSLocalizedString(@"Errore", @"Errore");
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertTitle message:self.activity.saveErrorMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }else{
                [self.navigationController popViewControllerAnimated:YES];
            }
            break;
        case 1:
            if ([self.activity save] == 0) {
                //error alert
                NSString *alertTitle = NSLocalizedString(@"Errore", @"Errore");
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertTitle message:self.activity.saveErrorMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }else{
                //aprire nuova attività stesso negozio
                NSDate *currentDate = self.activity.date;
                NSArray *currentShops = self.activity.customers;
                NSString *currentPeriod = self.activity.periodCode;
                
                self.activity = [[Activity alloc] init];
                self.activity.newActivity = YES;
                self.activity.periodCode = currentPeriod;
                self.activity.date = currentDate;
                self.activity.customers = currentShops;
                
                self.activity.unid = nil;
                self.activity.timeSlot = nil;
                self.activity.note = nil;
                self.activity.targetUnid = nil;
                self.activity.targetSubject = nil;
                self.activity.status = nil;
                self.activity.answers = nil;
                
                
                [self prepareDataForDisplay];
                [self.tableView reloadData];
            }
            
            break;
        case 2:
            if ([self.activity save] == 0) {
                //error alert
                NSString *alertTitle = NSLocalizedString(@"Errore", @"Errore");
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertTitle message:self.activity.saveErrorMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }else{
                //aprire stessa attività altro negozio
                //aprire nuova attività stesso negozio
                NSDate *currentDate = self.activity.date;
                NSString *currentTargetUnid = self.activity.targetUnid;
                NSString *currentTargetSubject = self.activity.targetSubject;
                NSString *currentPeriod = self.activity.periodCode;
                
                self.activity = [[Activity alloc] init];
                self.activity.newActivity = YES;
                self.activity.periodCode = currentPeriod;
                self.activity.date = currentDate;
                self.activity.targetSubject = currentTargetSubject;
                self.activity.targetUnid = currentTargetUnid;
                
                self.activity.unid = nil;
                self.activity.timeSlot = nil;
                self.activity.note = nil;
                self.activity.customers = nil;
                self.activity.status = nil;
                self.activity.answers = nil;
                
                self.questionsDataArray = nil;
                
                [self prepareDataForDisplay];
                [self.tableView reloadData];
            }
            
            break;
        default:
            
            break;
    }
}

@end
