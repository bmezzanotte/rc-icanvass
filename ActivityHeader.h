//
//  ActivityHeader.h
//  iCanvass
//
//  Created by Bernardino on 14/02/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ActivityAnswer, Target;

@interface ActivityHeader : NSManagedObject

@property (nonatomic, retain) NSString * canvass;
@property (nonatomic, retain) NSString * category;
@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSString *timeSlot;
@property (nonatomic, retain) NSString * note;
@property (nonatomic, retain) NSString *status;
@property (nonatomic, retain) NSString * unid;
@property (nonatomic, retain) NSSet *answers;
@property (nonatomic, retain) Target *target;
@property (nonatomic, retain) NSSet *customer;
@end

@interface ActivityHeader (CoreDataGeneratedAccessors)

- (void)addAnswersObject:(ActivityAnswer *)value;
- (void)removeAnswersObject:(ActivityAnswer *)value;
- (void)addAnswers:(NSSet *)values;
- (void)removeAnswers:(NSSet *)values;

- (void)addCustomerObject:(NSManagedObject *)value;
- (void)removeCustomerObject:(NSManagedObject *)value;
- (void)addCustomer:(NSSet *)values;
- (void)removeCustomer:(NSSet *)values;

@end
