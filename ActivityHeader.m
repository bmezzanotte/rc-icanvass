//
//  ActivityHeader.m
//  iCanvass
//
//  Created by Bernardino on 14/02/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import "ActivityHeader.h"
#import "ActivityAnswer.h"


@implementation ActivityHeader

@dynamic canvass;
@dynamic category;
@dynamic date;
@dynamic timeSlot;
@dynamic note;
@dynamic status;
@dynamic unid;
@dynamic answers;
@dynamic target;
@dynamic customer;

@end
