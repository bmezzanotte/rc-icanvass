//
//  ActivityListViewController.h
//  iCanvass
//
//  Created by NAeS Lavoro on 24/02/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivityListViewController : UITableViewController<NSFetchedResultsControllerDelegate>

@end
