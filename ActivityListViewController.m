//
//  ActivityListViewController.m
//  iCanvass
//
//  Created by NAeS Lavoro on 24/02/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import "ActivityListViewController.h"
#import "AppDelegate.h"
#import "ActivityHeader.h"
#import "ActivityAnswer.h"
#import "Target.h"
#import "TargetQuestion.h"
//#import "DetailActivityViewController.h"
#import "ActivityDetailViewController.h"
#import "Utility.h"
#import "DataBaseManager.h"

@interface ActivityListViewController ()
@property (nonatomic, strong, readonly) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic,strong) Utility *util;
@property (nonatomic, strong) DataBaseManager *dbManager;
@end

@implementation ActivityListViewController
@synthesize fetchedResultsController = _fetchedResultsController;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.util = [[Utility alloc] init];
    self.dbManager = [[DataBaseManager alloc] init];

    self.navigationItem.title = NSLocalizedString(@"Attività", @"Titolo nella navigation bar");

}

- (void)viewWillAppear:(BOOL)animated
{
    
    
    //temporaneo, per vedere cosa c'è nella tabella ActivityHeader
    AppDelegate *appDelegate =[[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *managedObjectContext = [appDelegate managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"ActivityHeader" inManagedObjectContext:managedObjectContext];
    [request setEntity:entity];
    NSError *error1;
    NSArray *objects = [managedObjectContext executeFetchRequest:request error:&error1];
    //temporaneo, per vedere cosa c'è nella tabella ActivityHeader

    
    
    [NSFetchedResultsController deleteCacheWithName:@"Activity"];
    NSError *error = nil;
    if (![[self fetchedResultsController] performFetch:&error]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Errore caricamento dati", @"Messagio de errore nel caricamento dati") message:
                              [NSString stringWithFormat:NSLocalizedString(@"L'errore è stato: %@, provocando la chiusura.", @"Error was: %@, quitting."),
                               [error localizedDescription]]
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"Aw, Nuts", @"Aw, Nuts")
                                              otherButtonTitles:nil];
        [alert show];
    }
    
    [self.tableView reloadData];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    tableView.sectionIndexColor = [UIColor darkGrayColor];
    //    self.tableView.sectionIndexBackgroundColor = [UIColor whiteColor];
    //    self.tableView.sectionIndexTrackingBackgroundColor = [UIColor lightGrayColor];
    tableView.sectionIndexMinimumDisplayRowCount = 20.0;
    
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id < NSFetchedResultsSectionInfo > sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"ActivityCellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    UILabel *dateActivityLabel = (UILabel *)[cell viewWithTag:1];
    UILabel *targetLabel = (UILabel *)[cell viewWithTag:2];
    UILabel *customersLabel = (UILabel *)[cell viewWithTag:3];
    UILabel *addressCustomerLabel = (UILabel *)[cell viewWithTag:4];
    
    ActivityHeader *activity = [self.fetchedResultsController objectAtIndexPath:indexPath];
    NSSet *activityCustomerSet = activity.customer;
    Target *target = activity.target;
    
    dateActivityLabel.text = [self.util getStringFromDate:activity.date withPattern:@"dd/MM/yyyy"];
    targetLabel.text = target.subject;
    NSLog(@"cella %ld - subject: %@", (long)indexPath.row, targetLabel.text);
    
    if ([activityCustomerSet count] == 1) {
        NSManagedObject *customer = [[activityCustomerSet allObjects]objectAtIndex:0];
        customersLabel.text = [customer valueForKey:@"name"];
        addressCustomerLabel.text = [NSString stringWithFormat:@"%@ - %@",[customer valueForKey:@"city"],[customer valueForKey:@"address"]];
    }else{
        [addressCustomerLabel setAlpha:0.0];
        [customersLabel setFrame:CGRectMake(customersLabel.frame.origin.x, customersLabel.frame.origin.y, customersLabel.frame.size.width, 40)];
        customersLabel.numberOfLines = 0;
        NSArray *listCustomer = [[activityCustomerSet allObjects] valueForKey:@"name"];
        //    customersLabel.text = [[listCustomer objectAtIndex:0] valueForKey:@"name"];
        if (listCustomer.count >= 1) {
            customersLabel.text =[listCustomer componentsJoinedByString:@", "];
        }else{
            customersLabel.text = NSLocalizedString(@"Punto vendita non indicato", @"Punto vendita non indicato");
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    ActivityHeader *activity = [self.fetchedResultsController objectAtIndexPath:indexPath];
    if ([activity.status integerValue] > 1) {
        return NO;
    }else{
        return YES;
    }
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [_dbManager updateItem:[self.fetchedResultsController objectAtIndexPath:indexPath] WithUpdateData:[NSDictionary dictionaryWithObject:@"9" forKey:@"status"]];
        //[tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}



#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"AddNewActivitySegue"]) {
        Activity *newActivity = [[Activity alloc] init];
        newActivity.date = [NSDate date];
        [newActivity setPeriodCodeWithDate:newActivity.date];

        ActivityDetailViewController *destination = [segue destinationViewController];
        destination.activity = newActivity;
        
        destination.activity.newActivity = YES;
    }
    
    if ([[segue identifier] isEqualToString:@"EditActivitySegue"]) {
        ActivityHeader *myActivityHeader = [self.fetchedResultsController objectAtIndexPath:[self.tableView indexPathForSelectedRow]];
        
        Activity *myActivity = [[Activity alloc] init];
        myActivity.unid = myActivityHeader.unid;
        myActivity.date = myActivityHeader.date;
        [myActivity setPeriodCodeWithDate:myActivity.date];
        myActivity.timeSlot = myActivityHeader.timeSlot;
        myActivity.note = myActivityHeader.note;
        myActivity.targetUnid = myActivityHeader.target.subjectUnid;
        myActivity.targetSubject = myActivityHeader.target.subject;
        myActivity.targetAllowMultipleInstances = myActivityHeader.target.allowMultipleInstances;

        myActivity.status = myActivityHeader.status;
        
        NSSet *myCustomers = myActivityHeader.customer;
        NSMutableArray *customerArray = [[NSMutableArray alloc] init];
        for (NSManagedObject *myCustomer in myCustomers) {
            [customerArray addObject:myCustomer];
        }
        myActivity.customers = [customerArray copy];

        //compilazione questionario
        NSSet *myAnswers = myActivityHeader.answers;
//        NSSortDescriptor *sortDescriptor;
//        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"subject" ascending:YES];
//        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        NSArray *answersArray = [myAnswers allObjects];
        
        myActivity.answers = [[NSMutableArray alloc] init];
        for (ActivityAnswer *myAnswer in answersArray) {
            NSLog(@"answer value: %@", myAnswer.value);
            NSLog(@"answer type: %@", myAnswer.type);
            
            Answer *answerObject = [[Answer alloc] init];
            answerObject.questionUnid = myAnswer.questionDefinitionUnid;

            NSArray *myQuestions = [self.dbManager selectFromTable:@"TargetQuestion"
                                                             Where:[NSString stringWithFormat:@"unid == '%@'",myAnswer.questionDefinitionUnid]
                                                           OrderBy:@"subject"
                                                       isAscending:YES];

            if (myQuestions) {
                TargetQuestion *myQuestion =  [myQuestions objectAtIndex:0];

                answerObject.subject = myQuestion.subject ;
                answerObject.required = myQuestion.required;
                answerObject.type = myAnswer.type;
                
                answerObject.response = myAnswer.value;
                
                [myActivity.answers addObject:answerObject];
            }
        }
        
        NSSortDescriptor *sortDescriptor;
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"subject" ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        myActivity.answers = [[myActivity.answers sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
        
        ActivityDetailViewController *destination = [segue destinationViewController];
        destination.activity = myActivity;
        destination.activity.newActivity = NO;
    }
}

#pragma mark - FetchedResultsController Property
- (NSFetchedResultsController *)fetchedResultsController{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = [appDelegate managedObjectContext];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"ActivityHeader" inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    [fetchRequest setFetchBatchSize:20];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status != '9'"];
    [fetchRequest setPredicate:predicate];
    NSString *sectionKey = nil;
    NSSortDescriptor *sortDescriptor1 = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:NO];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor1, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
//    sectionKey = @"nameInitial";
    _fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:managedObjectContext sectionNameKeyPath:nil cacheName:@"Activity"];
    _fetchedResultsController.delegate = self;
    return _fetchedResultsController;
}

#pragma mark - NSFetchedResultsControllerDelegate Methods
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}
- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}
- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id < NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type{
    switch(type) {
        case NSFetchedResultsChangeInsert: [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete: [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}
- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath{
    switch(type) {
        case NSFetchedResultsChangeInsert: [self.tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete: [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeUpdate:
        case NSFetchedResultsChangeMove:
            break;
    }
}

@end
