//
//  SurveysSync.m
//  iRoyalDays
//
//  Created by Bernardino on 19/12/13.
//  Copyright (c) 2013 NAeS Lavoro. All rights reserved.
//

#import "ActivitySyncGet.h"
#import "DataBaseManager.h"
#import "Utility.h"
#import "ActivityHeader.h"

@interface ActivitySyncGet()
@property (nonatomic, strong) Utility *util;

@property (nonatomic, strong) NSString *targetUnid;
@property (nonatomic, strong) NSMutableArray *customers;
@property (nonatomic, strong) NSString *questionDefinitionUnid;
@end

@implementation ActivitySyncGet
@synthesize creaDB = _creaDB, wsResponseData = _wsResponseData, syncSessionStarted = _syncSessionStarted, syncSessionEnded = _syncSessionEnded, util = _util, newHeaders = _newHeaders, updatedHeaders = _updatedHeaders, deletedHeaders = _deletedHeaders, newRows = _newRows, updatedRows = _updatedRows, deletedRows = _deletedRows;
@synthesize syncResultErrorStatus;

- (id)initWithToken:(NSString *)cookieToken{
    if (self = [super init]) {
        _util = [[Utility alloc] init];
        _syncSessionStarted = [_util getCurrentDateAsString];

        NSString *path = [[NSBundle mainBundle] pathForResource:@"ActivityHeader" ofType:@"plist"];
        headerTagToPropertyMapping = [NSDictionary dictionaryWithContentsOfFile:path];
        
        path = [[NSBundle mainBundle] pathForResource:@"ActivityAnswer" ofType:@"plist"];
        itemTagToPropertyMapping = [NSDictionary dictionaryWithContentsOfFile:path];

        itemList = [[NSMutableArray alloc]init];
        _creaDB = [[DataBaseManager alloc]init];
        
        //costruisco array UNID record esistenti in CoreData, da inviare al WS. SIA ATTIVITA' che RIGHI QUESTIONARIO, perché sul server potrebbe essere stato cancellato anche un singolo rigo
        unidArray = [[NSMutableArray alloc] init];
        unidArray = [_creaDB selectFromTable:@"ActivityHeader" Where:nil OrderBy:nil isAscending:NO];
        unidArray = [[unidArray arrayByAddingObjectsFromArray:[_creaDB selectFromTable:@"ActivityAnswer" Where:nil OrderBy:nil isAscending:NO]] mutableCopy];
        
        WebServiceClient *activityGET = [[WebServiceClient alloc] initWithWsName:@"GetActivitiesXML" withMethodName:@"GETACTIVITYRESULT" withTimeStamp:[self timeStampForEntity:@"lastSyncForActivity"] withArrayOfUniversalID:unidArray];

        _wsResponseData = [activityGET getWebServiceResponseWithToken:cookieToken];
        
        NSInteger httpResponseStatusCode = [activityGET.httpResponse statusCode];
        NSLog(@"http response status code: %ld",(long)httpResponseStatusCode);
        if (httpResponseStatusCode != 200) {
            NSLog(@"Errore WS HTTP error: %ld",(long)httpResponseStatusCode);
            //devo interrompere completamente la sincronizzazione e mostrare a video un messaggio d'errore
            self.syncResultErrorStatus = RDSyncErrorStatusServerError;
        }else{
            if (_wsResponseData == nil){
                NSLog(@"Errore WS: ResponseData VUOTO");
                //devo interrompere soltanto la sincronizzazione di questa tabella e, a fine sincronizzazione, mostrare KO in riepilogo
                self.syncResultErrorStatus = RDSyncErrorStatusNullStringResult;
            }else{
                //proseguo con il parsing XML della risposta
                self.syncResultErrorStatus = RDSyncErrorStatusNoError;
            }
        }
    }
    return self;
}

- (void) syncTable {
    _newHeaders = 0;
    _updatedHeaders = 0;
    _deletedHeaders = 0;
    _newRows = 0;
    _updatedRows = 0;
    _deletedRows = 0;
    
    NSXMLParser *parser = [[NSXMLParser alloc]initWithData:_wsResponseData];
    [parser setDelegate:self];
    BOOL success = [parser parse];
    
    if (success == YES) {
        NSLog(@"XML parsing ok");
//        _syncSessionEnded = [_util getCurrentDateAsString];
//        [_creaDB updateOrInsertSettingWithPropertyName:@"lastSyncForActivity" AndPropertyValue:_syncSessionEnded];
    }else{
        NSError *error = [parser parserError];
        NSLog(@"error parsing XML:%@",error);
    }
}

- (NSString *) timeStampForEntity:(NSString *)propertyNameForEntity {
    NSString *timeStamp = [NSString alloc];
    NSArray *settingsCapture = [_creaDB selectFromTable:@"Settings" Where:[NSString stringWithFormat:@"propertyName = '%@'",propertyNameForEntity] OrderBy:nil isAscending:0];
    if ([settingsCapture count]>0){
        timeStamp = [[settingsCapture objectAtIndex:0]  valueForKey:@"propertyValue"];
    }else{
        timeStamp = @"0000/00/00 00.00.00.000";
    }
    
    return timeStamp;
}



- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    if ([elementName isEqualToString:@"faultstring"]) {
        self.syncResultErrorStatus = RDSyncErrorStatusWSThrowFault;
    }else if ([elementName isEqualToString:@"ACTIVITYARRAY"]) {
        headerElements = [[NSMutableDictionary alloc]init];
        self.customers = [[NSMutableArray alloc] init];
        self.targetUnid = @"";
        
        xmlBranch =elementName.mutableCopy;
    }else if ([elementName isEqualToString:@"ITEMINFO"]) {
        itemElements = [[NSMutableDictionary alloc]init];
        xmlBranch =elementName.mutableCopy;
    }else if ([elementName isEqualToString:@"COUNT"]) {
        NSLog(@"ElementName = %@",elementName);
        //memorizzare il valore di COUNT, per verifica dati a fine sincronizzazione
        
    }else{
        NSLog(@"ElementName = %@",elementName);
    }
    xmlTag = elementName.mutableCopy;
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
    if([xmlBranch isEqualToString:@"ACTIVITYARRAY"]){
        
        if ([xmlTag isEqualToString:@"CUSTOMERCODE"]) {
            //costruisco array nsmanagedobject *customer, da passare successivamente come parametro a core data
            
            //devo splittare la stringa in array
            NSArray *customerCodes = [string componentsSeparatedByString: @","];
            for (NSString *customerCode in customerCodes) {
                NSArray *customers = [_creaDB selectFromTable:@"Customer" Where:[NSString stringWithFormat: @"code = '%@'", customerCode] OrderBy:nil isAscending:NO];
                if (customers && customers.count >= 1) {
//                if (customers) {
                    if (! self.customers) {
                        self.customers = [[NSMutableArray alloc] init];
                    }
                    [self.customers addObject:[customers objectAtIndex:0]];
                }
            }
            
        } else if ([xmlTag isEqualToString:@"TARGETUNID"]) {
            //registro unid target, da passare successivamente come parametro a core data
            self.targetUnid = string;
            
        } else if ([[headerTagToPropertyMapping allKeys] containsObject:xmlTag]) {
            NSString *valoreTag = @"";
            valoreTag = [valoreTag stringByAppendingString:string];

            NSMutableDictionary *tagValueAsDictionary = [[NSMutableDictionary alloc] init];

            if ([[headerElements valueForKey:[[headerTagToPropertyMapping valueForKey:xmlTag] valueForKey:@"attribute"]] valueForKey:@"value"]) {
                NSString *keyValue = [[headerElements valueForKey:[[headerTagToPropertyMapping valueForKey:xmlTag] valueForKey:@"attribute"]] valueForKey:@"value"];
                keyValue = [keyValue stringByAppendingString:valoreTag];
                [tagValueAsDictionary setValue:keyValue forKey:@"value"];
            } else {
                [tagValueAsDictionary setValue:[[headerTagToPropertyMapping valueForKey:xmlTag] valueForKey:@"type"] forKey:@"type"];
                [tagValueAsDictionary setValue:valoreTag forKey:@"value"];
            }
            
            
            [headerElements setValue:tagValueAsDictionary forKey:[[headerTagToPropertyMapping valueForKey:xmlTag] valueForKey:@"attribute"]];
        }
    }else if ([xmlBranch isEqualToString:@"ITEMINFO"]){
        
        if ([xmlTag isEqualToString:@"QUESTIONDEFINITIONUNID"]) {
            
            self.questionDefinitionUnid = string;
            
        } else if ([[itemTagToPropertyMapping allKeys] containsObject:xmlTag]) {
            
            NSString *valoreTag = @"";
            valoreTag = [valoreTag stringByAppendingString:string];
        
            if ([itemElements valueForKey:[[itemTagToPropertyMapping valueForKey:xmlTag] valueForKey:@"attribute"]] != nil) {
                NSString *keyValue = [itemElements valueForKey:[[itemTagToPropertyMapping valueForKey:xmlTag] valueForKey:@"attribute"]];
                keyValue = [keyValue stringByAppendingString:valoreTag];
                [itemElements setValue:keyValue forKey:[[itemTagToPropertyMapping valueForKey:xmlTag] valueForKey:@"attribute"]];
            }else{
                [itemElements setValue:valoreTag forKey:[[itemTagToPropertyMapping valueForKey:xmlTag] valueForKey:@"attribute"]];
            }
  
        }
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
    @try {
    if ([elementName isEqualToString:@"ACTIVITYARRAY"]) {
        BOOL successDelete = NO;
        int updateResult = 0;

        if ([[[headerElements valueForKey:@"status"] valueForKey:@"value"] isEqualToString:@"99"]) {
            //cancello in Core Data L'ATTIVITA' (e di conseguenza i righi questionario in quanto collegati a relationship)
            NSArray *queryResult = [_creaDB selectFromTable:@"ActivityHeader" Where:[NSString stringWithFormat:@"unid = '%@'",[[headerElements valueForKey:@"unid"] valueForKey:@"value"]] OrderBy:nil isAscending:0];
            if (queryResult.count > 0) {

                ActivityHeader *deletedActivity = [queryResult objectAtIndex:0];
                int deletedAnswers = [[deletedActivity answers] count];
                
                successDelete =[_creaDB deleteItem:[queryResult objectAtIndex:0]];
                if (successDelete == YES){
                    _deletedHeaders ++;
                    _deletedRows += deletedAnswers;
                }
                
            }else{
                //se l'elemento con status = 99 corrisponde ad un rigo (= risposta questionario), lo cancello in Core Data
                NSArray *queryResult = [_creaDB selectFromTable:@"ActivityAnswer" Where:[NSString stringWithFormat:@"unid = '%@'",[[headerElements valueForKey:@"unid"] valueForKey:@"value"]] OrderBy:nil isAscending:0];
                if (queryResult.count > 0) {
                    
                    successDelete =[_creaDB deleteItem:[queryResult objectAtIndex:0]];
                    if (successDelete == YES){
                        _deletedRows ++;
                    }
                }
            }
            
        }else{
            //scrivo in core data header e righi corrispondenti
            updateResult = [_creaDB syncActivityWithItemDictionary:headerElements withTargetUnid:self.targetUnid withCustomers:self.customers];
            
            switch (updateResult) {
                case 1:
                    _newHeaders ++;
                    _newRows += [[headerElements valueForKey:@"questions"] count];
                    break;
                case 2:
                    _updatedHeaders ++;
                    _updatedRows += [[headerElements valueForKey:@"questions"] count];
                    break;
                default:
                    break;
            }
            
            
            //temporaneo, per vedere cosa c'è nella tabella ActivityHeader
            NSArray *activityObjects = [_creaDB selectFromTable:@"ActivityHeader" Where:@"status != '9'" OrderBy:@"date" isAscending:YES];
            NSArray *activityObjectsAll = [_creaDB selectFromTable:@"ActivityHeader" Where:@"status != '99999'" OrderBy:@"date" isAscending:YES];
            //temporaneo, per vedere cosa c'è nella tabella Target


            [itemList removeAllObjects];
        }
    }else if ([elementName isEqualToString:@"ITEMINFO"]) {
        //aggiungo rigo completo ad array, che verrà aggiunto più sotto (if successivo) al dictionary che definisce l'attività da salvare in core data
        NSMutableDictionary *itemTagAsDictionary = [[NSMutableDictionary alloc] init];
        [itemTagAsDictionary setValue:itemElements forKey:self.questionDefinitionUnid];
         [itemList addObject:itemTagAsDictionary];
    }else if ([elementName isEqualToString:@"ITEMARRAY"]){
        //aggiungo l'array di righi al dictionary che definisce l'attività da salvare in core data
        NSLog(@"ElementName = %@",elementName);
        [headerElements setValue:itemList forKey:@"questions"];
    }else{
        NSLog(@"ElementName = %@",elementName);
    }

    xmlTag = @"".mutableCopy;
    }
    @catch (NSException *exception) {
        NSLog(@"Error parser didEndElement: %@",exception.description);
        self.syncResultErrorStatus = RDSyncErrorStatusXMLParserError;
    }
    @finally {

    }
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
    NSLog(@"Error: %@", [parseError localizedDescription]);
    self.syncResultErrorStatus = RDSyncErrorStatusXMLParserError;
}

@end
