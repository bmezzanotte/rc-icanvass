//
//  SurveysSyncSend.h
//  iRoyalDays
//
//  Created by Bernardino on 06/01/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebServiceClient.h"


@class DataBaseManager;

@interface ActivitySyncSend : NSObject<NSXMLParserDelegate>{
    NSMutableString *xmlBranch;
    NSMutableString *xmlTag;
    NSMutableDictionary *headerElements;
    NSMutableDictionary *itemElements;
    NSMutableArray *itemList;
    
    NSMutableArray *unidArray;
    DataBaseManager *creaDB;
}

@property (nonatomic, retain) DataBaseManager *creaDB;
@property (nonatomic, strong) NSData *wsResponseData;
@property (nonatomic, weak) NSString *syncSessionStarted;
@property (nonatomic, weak) NSString *syncSessionEnded;
@property (nonatomic) RDSyncErrorStatus syncResultErrorStatus;

@property (nonatomic) int newHeaders;
@property (nonatomic) int updatedHeaders;
@property (nonatomic) int deletedHeaders;
@property (nonatomic) int rejectedHeaders;

@property (nonatomic) int newRows;
@property (nonatomic) int updatedRows;
@property (nonatomic) int deletedRows;
@property (nonatomic) int rejectedRows;

- (id)initWithToken:(NSString *)cookieToken;
@end
