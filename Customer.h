//
//  Customer.h
//  iCanvass
//
//  Created by Bernardino on 10/04/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ActivityHeader;

@interface Customer : NSManagedObject

@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSString * cap;
@property (nonatomic, retain) NSString * city;
@property (nonatomic, retain) NSString * code;
@property (nonatomic, retain) NSString * county;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * fax;
@property (nonatomic, retain) NSDate * lastmodified;
@property (nonatomic, retain) NSString * latitude;
@property (nonatomic, retain) NSString * longitude;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * nameInitial;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSString * pvchallenge;
@property (nonatomic, retain) NSString * pvtop;
@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSString * unid;
@property (nonatomic, retain) NSSet *activity;
@end

@interface Customer (CoreDataGeneratedAccessors)

- (void)addActivityObject:(ActivityHeader *)value;
- (void)removeActivityObject:(ActivityHeader *)value;
- (void)addActivity:(NSSet *)values;
- (void)removeActivity:(NSSet *)values;

@end
