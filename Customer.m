//
//  Customer.m
//  iCanvass
//
//  Created by Bernardino on 10/04/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import "Customer.h"
#import "ActivityHeader.h"


@implementation Customer

@dynamic address;
@dynamic cap;
@dynamic city;
@dynamic code;
@dynamic county;
@dynamic email;
@dynamic fax;
@dynamic lastmodified;
@dynamic latitude;
@dynamic longitude;
@dynamic name;
@dynamic nameInitial;
@dynamic phone;
@dynamic pvchallenge;
@dynamic pvtop;
@dynamic status;
@dynamic unid;
@dynamic activity;

@end
