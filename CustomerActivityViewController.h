//
//  CustomerActivityViewController.h
//  iCanvass
//
//  Created by NAeS Lavoro on 04/03/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CustomerActivityDelegate <NSObject>

- (void)getListCustomerSelected:(NSMutableArray *)arrayCustomers;

@end

@interface CustomerActivityViewController : UIViewController<NSFetchedResultsControllerDelegate, UITableViewDataSource, UITableViewDelegate>{
    __weak id delegate;
}
@property (weak, nonatomic) id<CustomerActivityDelegate> delegate;
@property (nonatomic,strong) NSMutableArray *customerSelectedList;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *saveButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cancelButton;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UIToolbar *toolBar;
- (IBAction)saveAction:(id)sender;
- (IBAction)cancelAction:(id)sender;

@end
