//
//  CustomerActivityViewController.m
//  iCanvass
//
//  Created by NAeS Lavoro on 04/03/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import "CustomerActivityViewController.h"
#import "AppDelegate.h"
#import "DataBaseManager.h"

@interface CustomerActivityViewController ()
@property (nonatomic, strong, readonly) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, retain) NSMutableArray *searchResults;
@end

@implementation CustomerActivityViewController
@synthesize fetchedResultsController = _fetchedResultsController, delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [_saveButton setTitle:NSLocalizedString(@"Conferma", @"Conferma")];
    [_cancelButton setTitle:NSLocalizedString(@"Annulla", @"Annulla")];
//    self.customerSelectedList = [[NSMutableArray alloc]init];
	[NSFetchedResultsController deleteCacheWithName:@"CustomersActivity"];
    
    self.navigationItem.title = NSLocalizedString(@"Elenco Punti Vendita", @"Titolo nella navigation bar");
    NSError *error = nil;
    if (![[self fetchedResultsController] performFetch:&error]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Errore caricamento dati", @"Messagio de errore nel caricamento dati") message:
                              [NSString stringWithFormat:NSLocalizedString(@"L'errore è stato: %@, provocando la chiusura.", @"Error was: %@, quitting."),
                               [error localizedDescription]]
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"Aw, Nuts", @"Aw, Nuts")
                                              otherButtonTitles:nil];
        [alert show];
    }
    self.navigationController.toolbarHidden = NO;
    self.toolbarItems = self.toolBar.items;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    tableView.sectionIndexColor = [UIColor darkGrayColor];
    //    self.tableView.sectionIndexBackgroundColor = [UIColor whiteColor];
    //    self.tableView.sectionIndexTrackingBackgroundColor = [UIColor lightGrayColor];
    tableView.sectionIndexMinimumDisplayRowCount = 20.0;
    
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return 1;
    }else{
        return [[self.fetchedResultsController sections] count];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return [_searchResults count];
    } else {
        id < NSFetchedResultsSectionInfo > sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
        return [sectionInfo numberOfObjects];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"PuntoVenditaIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"PuntoVenditaIdentifier"];
    }
    
    NSManagedObject *puntoVendita;
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        puntoVendita = [_searchResults objectAtIndex:indexPath.row];
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"%K == %@",@"unid", [puntoVendita valueForKey:@"unid"]];
        NSArray *customers = [self.customerSelectedList filteredArrayUsingPredicate:pred];
        if ([customers count] != 0) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            NSLog(@"Trovato!");
        }else{
            cell.accessoryType = UITableViewCellAccessoryNone;
            NSLog(@"Non trovato!");
        }
    } else {
        puntoVendita = [self.fetchedResultsController objectAtIndexPath:indexPath];
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"%K == %@",@"unid", [puntoVendita valueForKey:@"unid"]];
        NSArray *customers = [self.customerSelectedList filteredArrayUsingPredicate:pred];
        if ([customers count] != 0) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            NSLog(@"Trovato!");
        }else{
            cell.accessoryType = UITableViewCellAccessoryNone;
            NSLog(@"Non trovato!");
        }
    }
    
    NSArray *scopeTitles = [self.searchDisplayController.searchBar scopeButtonTitles];
    NSString *scope = [scopeTitles objectAtIndex:self.searchDisplayController.searchBar.selectedScopeButtonIndex];
    
    cell.imageView.image = nil;
    if (tableView != self.searchDisplayController.searchResultsTableView || [scope isEqualToString:@"Tutti"]){
        if ([[puntoVendita valueForKey:@"pvtop"] isEqualToString:@"1"]) {
            if ([[puntoVendita valueForKey:@"pvchallenge"] isEqualToString:@"1"]) {
                cell.imageView.image = [UIImage imageNamed:@"top_challenge_icon.png"];
            }else{
                cell.imageView.image = [UIImage imageNamed:@"challenge_icon.png"];
            }
        }else if([[puntoVendita valueForKey:@"pvtop"] isEqualToString:@"2"]) {
            if ([[puntoVendita valueForKey:@"pvchallenge"] isEqualToString:@"1"]) {
                cell.imageView.image = [UIImage imageNamed:@"club_challenge_icon.png"];
            }else{
                cell.imageView.image = [UIImage imageNamed:@"challenge_icon.png"];
            }
        }else{
            if ([[puntoVendita valueForKey:@"pvchallenge"] isEqualToString:@"1"]) {
                cell.imageView.image = [UIImage imageNamed:@"challenge_icon.png"];
            }
        }
    }
    cell.textLabel.text = [puntoVendita valueForKey:@"name"];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ - %@",[puntoVendita valueForKey:@"city"], [puntoVendita valueForKey:@"address"]];
        
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSManagedObject *selectedCustomer;
    
//    if (!self.customerSelectedList) {                             //with IF commented out, block multiple selection
        self.customerSelectedList = [[NSMutableArray alloc] init];
//    }
    
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        selectedCustomer = [_searchResults objectAtIndex:indexPath.row];
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"%K == %@",@"unid", [selectedCustomer valueForKey:@"unid"]];
        NSArray *customers = [self.customerSelectedList filteredArrayUsingPredicate:pred];
        if ([customers count] == 0) {
            [self.customerSelectedList addObject:selectedCustomer];
        }else{
            [self.customerSelectedList removeObjectsInArray:[self.customerSelectedList filteredArrayUsingPredicate:pred]];
        }
        [tableView reloadData];
    }else{
        selectedCustomer = [self.fetchedResultsController objectAtIndexPath:indexPath];
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"%K == %@",@"unid", [selectedCustomer valueForKey:@"unid"]];
        NSArray *customers = [self.customerSelectedList filteredArrayUsingPredicate:pred];
        if ([customers count] == 0) {
            [self.customerSelectedList addObject:selectedCustomer];
        }else{
            [self.customerSelectedList removeObjectsInArray:[self.customerSelectedList filteredArrayUsingPredicate:pred]];
        }
    }
    [self.tableView reloadData];
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if (tableView == self.searchDisplayController.searchResultsTableView){
        return nil;
    }else{
        return [[self.fetchedResultsController sectionIndexTitles] objectAtIndex:section];
    }
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    if (tableView == self.searchDisplayController.searchResultsTableView){
        return nil;
    }else{
        return [self.fetchedResultsController sectionIndexTitles];
    }
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    if (tableView == self.searchDisplayController.searchResultsTableView){
        return 0;
    }else{
        return [self.fetchedResultsController sectionForSectionIndexTitle:title atIndex:index];
    }
}

#pragma mark - FetchedResultsController Property
- (NSFetchedResultsController *)fetchedResultsController{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = [appDelegate managedObjectContext];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Customer" inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    [fetchRequest setFetchBatchSize:20];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status != '9' AND status != '99'"];
    [fetchRequest setPredicate:predicate];
    NSString *sectionKey = nil;
    NSSortDescriptor *sortDescriptor1 = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor1, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    sectionKey = @"nameInitial";
    _fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:managedObjectContext sectionNameKeyPath:sectionKey cacheName:@"CustomersActivity"];
    _fetchedResultsController.delegate = self;
    return _fetchedResultsController;
}

#pragma mark - NSFetchedResultsControllerDelegate Methods
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}
- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}
- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id < NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type{
    switch(type) {
        case NSFetchedResultsChangeInsert: [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete: [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeMove:
            break;
        case NSFetchedResultsChangeUpdate:
            break;
    }
}
- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath{
    switch(type) {
        case NSFetchedResultsChangeInsert: [self.tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete: [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeUpdate:
        case NSFetchedResultsChangeMove:
            break;
    }
}

#pragma mark - search methods
- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope{
    
    [_searchResults removeAllObjects];
    
    DataBaseManager *dbManager = [[DataBaseManager alloc]init];
    NSString *predicate = nil;
    NSString *trimmedSearchText = [searchText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (trimmedSearchText.length > 0) {
        predicate = [NSString stringWithFormat:@"(name contains[cd] '%@' OR city contains[cd] '%@' OR address contains[cd] '%@' OR cap contains[cd] '%@' OR county contains[cd] '%@')",trimmedSearchText, trimmedSearchText, trimmedSearchText, trimmedSearchText, trimmedSearchText];
    }
    
    if ([scope isEqualToString:@"Top"]){
        if (predicate > 0) {
            predicate = [predicate stringByAppendingString:@" AND (pvtop = '1')"];
        }else{
            predicate = @"(pvtop = '1')";
        }
    }
    
    if ([scope isEqualToString:@"Club"]){
        if (predicate > 0) {
            predicate = [predicate stringByAppendingString:@" AND (pvtop = '2')"];
        }else{
            predicate = @"(pvtop = '2')";
        }
    }
    
    if ([scope isEqualToString:@"Challenge"]){
        if (predicate > 0) {
            predicate = [predicate stringByAppendingString:@" AND (pvchallenge = '1')"];
        }else{
            predicate = @"(pvchallenge = '1')";
        }
    }
    
    if (predicate > 0) {
        predicate = [predicate stringByAppendingString:@" AND (status != '9')"];
    }else{
        predicate = @"(status != '9')";
    }
    
    _searchResults = [dbManager selectFromTable:@"Customer" Where:predicate OrderBy:@"name" isAscending:1];
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString{
    [self filterContentForSearchText:searchString scope:[[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
    return YES;
}


-(void)searchDisplayController:(UISearchDisplayController *)controller didShowSearchResultsTableView:(UITableView *)tableView {
    
    CGRect f = self.tableView.frame;  // The tableView the search replaces
    //CGRect s = self.searchDisplayController.searchBar.frame;
    CGRect newFrame = CGRectMake(f.origin.x,
                                 f.origin.y,
                                 f.size.width,
                                 f.size.height);
    
    tableView.frame = newFrame;
}


- (void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope{
    
    //if aggiunto per risolvere problema mancato completamento ricerca quando stringa ricerca vuota
    if (self.searchDisplayController.searchBar.text.length == 0) {
        self.searchDisplayController.searchBar.text = @" ";
    }
    
    NSArray *scopeTitles = [self.searchDisplayController.searchBar scopeButtonTitles];
    [self filterContentForSearchText:self.searchDisplayController.searchBar.text scope:[scopeTitles objectAtIndex:selectedScope]];
    
}


- (IBAction)saveAction:(id)sender {
    
    if ([self.delegate respondsToSelector:@selector(getListCustomerSelected:)]) {
        [self.delegate getListCustomerSelected:self.customerSelectedList];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)cancelAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
