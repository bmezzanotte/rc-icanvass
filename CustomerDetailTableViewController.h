//
//  CustomerDetailTableViewController.h
//  iCanvass
//
//  Created by NAeS Lavoro on 17/02/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utility.h"
@interface CustomerDetailTableViewController : UITableViewController
@property (nonatomic,strong) NSManagedObject *customer;
@property (nonatomic, retain) Utility *utility;
@end
