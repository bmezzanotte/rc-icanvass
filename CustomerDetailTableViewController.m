//
//  CustomerDetailTableViewController.m
//  iCanvass
//
//  Created by NAeS Lavoro on 17/02/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import "CustomerDetailTableViewController.h"
#import "RDDetailTextCell.h"
#import "RDDetailYesNoCell.h"
#import "Utility.h"

@interface CustomerDetailTableViewController ()
@property (nonatomic,strong) NSArray *sections;
@end

@implementation CustomerDetailTableViewController


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSURL *plistURL = [[NSBundle mainBundle] URLForResource:@"CustomerDetailConfiguration" withExtension:@"plist"];
    NSDictionary *plist = [NSDictionary dictionaryWithContentsOfURL:plistURL];
    self.sections = [plist valueForKey:@"sections"];
    self.utility = [[Utility alloc]init];
    NSLog(@"%@", [self.customer valueForKey:@"unid"]);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *sectionName;
    NSDictionary *plistSection = [self.sections objectAtIndex:section];
    
    sectionName = [plistSection objectForKey:@"header"];
    return sectionName;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger sectionIndex = [indexPath section];
    NSUInteger rowIndex = [indexPath row];
    NSDictionary *section = [self.sections objectAtIndex:sectionIndex];
    NSArray *rows = [section objectForKey:@"rows"];
    NSDictionary *row = [rows objectAtIndex:rowIndex];
    
    NSString *cellClassName;
    if ([row valueForKey:@"class"]!=nil){
        cellClassName = [row valueForKey:@"class"];
    }else{
        cellClassName = @"RDDetailTextCell";
    }
    NSLog(@"Cell class: %@",cellClassName);
    
    RDDetailTextCell *cell = [tableView dequeueReusableCellWithIdentifier:cellClassName];
    if (cell == nil){
        Class cellClass = NSClassFromString(cellClassName);
        cell = [cellClass alloc];
        cell = [cell initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:cellClassName];
    }
    
    NSString *labelPlistValue = [row objectForKey:@"label"];
    cell.label.text = NSLocalizedString(labelPlistValue, labelPlistValue);
    if ([cellClassName isEqualToString:@"RDDetailRadioCell"]) {
        NSArray *options = [[NSArray alloc] initWithArray:[row valueForKey:@"options"]];
        NSString *optionString = [self.customer valueForKey:[row objectForKey:@"key"]];
        int optionValue = [optionString intValue];
        NSLog(@"option value = %d", optionValue);
        NSString *optionLabel = [options objectAtIndex:optionValue];
        cell.fieldValueLabel.text = NSLocalizedString(optionLabel, optionLabel);
    }else if ([cellClassName isEqualToString:@"RDDetailYesNoCell"]) {
        if ([[self.customer valueForKey:[row objectForKey:@"key"]] isEqualToString:@"1"]) {
            cell.fieldValueLabel.text = NSLocalizedString(@"SI", @"SI");
        }else{
            cell.fieldValueLabel.text = NSLocalizedString(@"NO", @"NO");
        }
    }else{
        if ([[self.customer valueForKey:[row objectForKey:@"key"]] isKindOfClass:[NSDate class]]) {
            cell.fieldValueLabel.text = [_utility getStringFromDate:[self.customer valueForKey:[row objectForKey:@"key"]] withPattern:@"dd/MM/yyyy HH.mm.ss"];
        }else{
            cell.fieldValueLabel.text = [self.customer valueForKey:[row objectForKey:@"key"]];
        }
        
        if ([row objectForKey:@"lines"]!=nil){
            cell.fieldValueLabel.lineBreakMode = NSLineBreakByWordWrapping;
            cell.fieldValueLabel.numberOfLines = [[row objectForKey:@"lines"] integerValue];
            [cell.fieldValueLabel sizeToFit];
        }
    }
    
    return cell;
}

@end
