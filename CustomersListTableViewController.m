//
//  CustomersListTableViewController.m
//  iCanvass
//
//  Created by NAeS Lavoro on 14/02/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import "CustomersListTableViewController.h"
#import "AppDelegate.h"
#import "DataBaseManager.h"
#import "CustomerDetailTableViewController.h"

@interface CustomersListTableViewController ()
@property (nonatomic, strong, readonly) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, retain) NSMutableArray *searchResults;
@end

@implementation CustomersListTableViewController
@synthesize fetchedResultsController = _fetchedResultsController, searchResults = _searchResults, tableView = _tableView;

- (void)viewDidLoad
{
    [super viewDidLoad];
    [NSFetchedResultsController deleteCacheWithName:@"Customers"];
    
    self.navigationItem.title = NSLocalizedString(@"Elenco Punti Vendita", @"Titolo nella navigation bar");
    NSError *error = nil;
    if (![[self fetchedResultsController] performFetch:&error]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Errore caricamento dati", @"Messagio de errore nel caricamento dati") message:
                              [NSString stringWithFormat:NSLocalizedString(@"L'errore è stato: %@, provocando la chiusura.", @"Error was: %@, quitting."),
                               [error localizedDescription]]
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"Aw, Nuts", @"Aw, Nuts")
                                              otherButtonTitles:nil];
        [alert show];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    tableView.sectionIndexColor = [UIColor darkGrayColor];
    //    self.tableView.sectionIndexBackgroundColor = [UIColor whiteColor];
    //    self.tableView.sectionIndexTrackingBackgroundColor = [UIColor lightGrayColor];
    tableView.sectionIndexMinimumDisplayRowCount = 20.0;
    
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return 1;
    }else{
        return [[self.fetchedResultsController sections] count];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return [_searchResults count];
    } else {
        id < NSFetchedResultsSectionInfo > sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
        return [sectionInfo numberOfObjects];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"PuntoVenditaIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"PuntoVenditaIdentifier"];
    }
    
    NSManagedObject *puntoVendita;
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        puntoVendita = [_searchResults objectAtIndex:indexPath.row];
    } else {
        puntoVendita = [self.fetchedResultsController objectAtIndexPath:indexPath];
    }
    
    NSArray *scopeTitles = [self.searchDisplayController.searchBar scopeButtonTitles];
    NSString *scope = [scopeTitles objectAtIndex:self.searchDisplayController.searchBar.selectedScopeButtonIndex];
    
    cell.imageView.image = nil;
    if (tableView != self.searchDisplayController.searchResultsTableView || [scope isEqualToString:@"Tutti"]){
        if ([[puntoVendita valueForKey:@"pvtop"] isEqualToString:@"1"]) {
            if ([[puntoVendita valueForKey:@"pvchallenge"] isEqualToString:@"1"]) {
                cell.imageView.image = [UIImage imageNamed:@"top_challenge_icon.png"];
            }else{
                cell.imageView.image = [UIImage imageNamed:@"challenge_icon.png"];
            }
        }else if([[puntoVendita valueForKey:@"pvtop"] isEqualToString:@"2"]) {
            if ([[puntoVendita valueForKey:@"pvchallenge"] isEqualToString:@"1"]) {
                cell.imageView.image = [UIImage imageNamed:@"club_challenge_icon.png"];
            }else{
                cell.imageView.image = [UIImage imageNamed:@"challenge_icon.png"];
            }
        }else{
            if ([[puntoVendita valueForKey:@"pvchallenge"] isEqualToString:@"1"]) {
                cell.imageView.image = [UIImage imageNamed:@"challenge_icon.png"];
            }
        }
    }
    cell.textLabel.text = [puntoVendita valueForKey:@"name"];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ - %@",[puntoVendita valueForKey:@"address"], [puntoVendita valueForKey:@"city"]];
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if (tableView == self.searchDisplayController.searchResultsTableView){
        return nil;
    }else{
        return [[self.fetchedResultsController sectionIndexTitles] objectAtIndex:section];
    }
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    if (tableView == self.searchDisplayController.searchResultsTableView){
        return nil;
    }else{
        return [self.fetchedResultsController sectionIndexTitles];
    }
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    if (tableView == self.searchDisplayController.searchResultsTableView){
        return 0;
    }else{
        return [self.fetchedResultsController sectionForSectionIndexTitle:title atIndex:index];
    }
}

#pragma mark - Navigation
// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"CustomerDetailSegue"]){
        if([sender isKindOfClass:[NSManagedObject class]]){
            CustomerDetailTableViewController *detailController = segue.destinationViewController;
            detailController.customer = sender;
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSManagedObject *selectedCustomer;
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        selectedCustomer = [_searchResults objectAtIndex:indexPath.row];
    } else {
        selectedCustomer = [self.fetchedResultsController objectAtIndexPath:indexPath];
    }
    
    [self performSegueWithIdentifier:@"CustomerDetailSegue" sender:selectedCustomer];
}


#pragma mark - FetchedResultsController Property
- (NSFetchedResultsController *)fetchedResultsController{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = [appDelegate managedObjectContext];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Customer" inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    [fetchRequest setFetchBatchSize:20];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status != '9' AND status != '99'"];
    [fetchRequest setPredicate:predicate];
    NSString *sectionKey = nil;
    NSSortDescriptor *sortDescriptor1 = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor1, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    sectionKey = @"nameInitial";
    _fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:managedObjectContext sectionNameKeyPath:sectionKey cacheName:@"Customers"];
    _fetchedResultsController.delegate = self;
    return _fetchedResultsController;
}

#pragma mark - NSFetchedResultsControllerDelegate Methods
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}
- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}
- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id < NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type{
    switch(type) {
        case NSFetchedResultsChangeInsert: [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete: [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}
- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath{
    switch(type) {
        case NSFetchedResultsChangeInsert: [self.tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete: [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeUpdate:
        case NSFetchedResultsChangeMove:
            break;
    }
}

#pragma mark - search methods
- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope{
    
    [_searchResults removeAllObjects];
    
    DataBaseManager *dbManager = [[DataBaseManager alloc]init];
    NSString *predicate = nil;
    NSString *trimmedSearchText = [searchText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (trimmedSearchText.length > 0) {
        predicate = [NSString stringWithFormat:@"(name contains[cd] '%@' OR city contains[cd] '%@' OR address contains[cd] '%@' OR cap contains[cd] '%@' OR county contains[cd] '%@')",trimmedSearchText, trimmedSearchText, trimmedSearchText, trimmedSearchText, trimmedSearchText];
    }
    
    if ([scope isEqualToString:@"Top"]){
        if (predicate > 0) {
            predicate = [predicate stringByAppendingString:@" AND (pvtop = '1')"];
        }else{
            predicate = @"(pvtop = '1')";
        }
    }
    
    if ([scope isEqualToString:@"Club"]){
        if (predicate > 0) {
            predicate = [predicate stringByAppendingString:@" AND (pvtop = '2')"];
        }else{
            predicate = @"(pvtop = '2')";
        }
    }
    
    if ([scope isEqualToString:@"Challenge"]){
        if (predicate > 0) {
            predicate = [predicate stringByAppendingString:@" AND (pvchallenge = '1')"];
        }else{
            predicate = @"(pvchallenge = '1')";
        }
    }
    
    if (predicate > 0) {
        predicate = [predicate stringByAppendingString:@" AND (status != '9')"];
    }else{
        predicate = @"(status != '9')";
    }
    
    _searchResults = [dbManager selectFromTable:@"Customer" Where:predicate OrderBy:@"name" isAscending:1];
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString{
    [self filterContentForSearchText:searchString scope:[[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
    
    return YES;
}

- (void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope{
    
    //if aggiunto per risolvere problema mancato completamento ricerca quando stringa ricerca vuota
    if (self.searchDisplayController.searchBar.text.length == 0) {
        self.searchDisplayController.searchBar.text = @" ";
    }
    
    NSArray *scopeTitles = [self.searchDisplayController.searchBar scopeButtonTitles];
    [self filterContentForSearchText:self.searchDisplayController.searchBar.text scope:[scopeTitles objectAtIndex:selectedScope]];
    
}

@end
