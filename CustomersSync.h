//
//  CustomersSync.h
//  iRoyalDays
//
//  Created by Bernardino on 19/12/13.
//  Copyright (c) 2013 NAeS Lavoro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebServiceClient.h"

@class DataBaseManager;

@interface CustomersSync : NSObject<NSXMLParserDelegate>{
    NSDictionary *tagToPropertyMapping;
    NSString *xmlTag;
    NSMutableDictionary *oggettoLista;
    NSMutableArray *listaOggetti;
    NSMutableArray *unidArray;
}

@property (nonatomic, retain) DataBaseManager *dbManager;
@property (nonatomic, strong) NSData *wsResponseData;
@property (nonatomic, weak) NSString *syncSessionStarted;
@property (nonatomic, weak) NSString *syncSessionEnded;
@property (nonatomic) RDSyncErrorStatus syncResultErrorStatus;

@property (nonatomic) int newCustomers;
@property (nonatomic) int updatedCustomers;
@property (nonatomic) int deletedCustomers;

- (id)initWithToken:(NSString *)cookieToken;
- (void)syncTable;
- (NSMutableArray *)propertyArray;
@end

