//
//  CustomersSync.m
//  iRoyalDays
//
//  Created by Bernardino on 19/12/13.
//  Copyright (c) 2013 NAeS Lavoro. All rights reserved.
//

#import "CustomersSync.h"
#import "DataBaseManager.h"
#import "Utility.h"

@interface CustomersSync()
@property Utility *util;
@end

@implementation CustomersSync
@synthesize wsResponseData = _wsResponseData, syncSessionStarted = _syncSessionStarted, util = _util, dbManager = _dbManager;
@synthesize syncResultErrorStatus;

- (id)initWithToken:(NSString *)cookieToken{
    if (self = [super init]) {
        _util = [[Utility alloc] init];
        _dbManager = [[DataBaseManager alloc]init];
        _syncSessionStarted = [_util getCurrentDateAsString];
       
        NSString *path = [[NSBundle mainBundle] pathForResource:@"Customer" ofType:@"plist"];
        tagToPropertyMapping = [NSDictionary dictionaryWithContentsOfFile:path];
        listaOggetti = [[NSMutableArray alloc]init];
        
        //costruisco array UNID record esistenti in CoreData, da inviare al WS
        unidArray = [_dbManager selectFromTable:@"Customer" Where:nil OrderBy:nil isAscending:NO];

 //       WebServiceClient *customersGET = [[WebServiceClient alloc] initWithWsName:@"JavaProdSurveyGetSurveyXML" withMethodName:@"GETSURVEYRESULT" withTimeStamp:[self timeStampForEntity:@"lastSyncForCustomers"] withArrayOfUniversalID:unidArray];
        
        WebServiceClient *customersGET = [[WebServiceClient alloc] initWithWsName:@"GetMasterDataXML" withMethodName:@"GETPVLIST" withTimeStamp:[self timeStampForEntity:@"lastSyncForCustomers"] withArrayOfUniversalID:unidArray];
        
        _wsResponseData = [customersGET getWebServiceResponseWithToken:cookieToken];
        
        NSInteger httpResponseStatusCode = [customersGET.httpResponse statusCode];
        NSLog(@"http response status code: %ld",(long)httpResponseStatusCode);
        if (httpResponseStatusCode != 200) {
            NSLog(@"Errore WS HTTP error: %ld",(long)httpResponseStatusCode);
            //devo interrompere completamente la sincronizzazione e mostrare a video un messaggio d'errore
            self.syncResultErrorStatus = RDSyncErrorStatusServerError;
        }else{
            if (_wsResponseData == nil){
                NSLog(@"Errore WS: ResponseData VUOTO");
                //devo interrompere soltanto la sincronizzazione di questa tabella e, a fine sincronizzazione, mostrare KO in riepilogo
                self.syncResultErrorStatus = RDSyncErrorStatusNullStringResult;
            }else{
                //proseguo con il parsing XML della risposta
                self.syncResultErrorStatus = RDSyncErrorStatusNoError;
            }
        }

    }
    return self;
}

- (void) syncTable {
    NSArray *arrayItem = [self propertyArray];
    BOOL deletionSuccess = NO;
    int updateSuccess;
    
    self.deletedCustomers = 0;
    self.updatedCustomers = 0;
    self.newCustomers = 0;
    
    for (int j = 0; j<[arrayItem count]; j++) {
        NSMutableDictionary *item = [arrayItem objectAtIndex:j];
        if ([item valueForKey:@"unid"] != nil) {
            if ([[item valueForKey:@"status"]  isEqualToString: @"99"]){
                NSMutableArray *items = [_dbManager selectFromTable:@"Customer" Where:[NSString stringWithFormat:@"unid == '%@'",[item valueForKey:@"unid"]] OrderBy:nil isAscending:0];
                if (items.count > 0) {
                    deletionSuccess = [_dbManager deleteItem:(NSManagedObject *)[items objectAtIndex:0]];
                    if (deletionSuccess == YES){
                        self.deletedCustomers ++;
                    }
                }
            }else{
                updateSuccess = [_dbManager syncItemForTable:@"Customer" WithItemDictionary:item];
                switch (updateSuccess) {
                    case 1:
                        self.newCustomers ++;
                        break;
                    case 2:
                        self.updatedCustomers ++;
                        break;
                    default:
                        break;
                }
            }
        }
    }
    
    if (self.syncResultErrorStatus == RDSyncErrorStatusNoError) {
        _syncSessionEnded = [_util getCurrentDateAsString];
        [_dbManager updateOrInsertSettingWithPropertyName:@"lastSyncForCustomers" AndPropertyValue:_syncSessionEnded];
    }
}

- (NSMutableArray *)propertyArray {
    NSXMLParser *parser = [[NSXMLParser alloc]initWithData:_wsResponseData];
    [parser setDelegate:self];
    BOOL success = [parser parse];
    if (success == YES) {
        NSLog(@"Its ok");
        return listaOggetti;
    }else{
        NSLog(@"Its not ok");
        NSError *error = [parser parserError];
        NSLog(@"errore:%@",error);
        return listaOggetti;
    }
}

- (NSString *) timeStampForEntity:(NSString *)propertyNameForEntity {
    NSString *timeStamp = [NSString alloc];
    NSArray *settingsCapture = [_dbManager selectFromTable:@"Settings" Where:[NSString stringWithFormat:@"propertyName = '%@'",propertyNameForEntity] OrderBy:nil isAscending:0];
    if ([settingsCapture count]>0){
        timeStamp = [[settingsCapture objectAtIndex:0]  valueForKey:@"propertyValue"];
    }else{
        timeStamp = @"0000/00/00 00.00.000";
    }
    
    return timeStamp;
}




- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    if ([elementName isEqualToString:@"faultstring"]) {
        self.syncResultErrorStatus = RDSyncErrorStatusWSThrowFault;
    }else if ([elementName isEqualToString:@"ITEMARRAY"]) {
        oggettoLista = [[NSMutableDictionary alloc]init];
    }else{
        NSLog(@"ElementName = %@",elementName);
    }
    xmlTag = elementName;
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
    if ([[tagToPropertyMapping allKeys] containsObject:xmlTag]) {
        NSString *valoreTag = @"";
        valoreTag = [valoreTag stringByAppendingString:string];
        
        if ([oggettoLista valueForKey:[tagToPropertyMapping valueForKey:xmlTag]] != nil) {
            NSString *keyValue = [oggettoLista valueForKey:[tagToPropertyMapping valueForKey:xmlTag]];
            keyValue = [keyValue stringByAppendingString:valoreTag];
            [oggettoLista setValue:keyValue forKey:[tagToPropertyMapping valueForKey:xmlTag]];
        }else{
            [oggettoLista setValue:valoreTag forKey:[tagToPropertyMapping valueForKey:xmlTag]];
        }
    }    
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
    if ([elementName isEqualToString:@"ITEMARRAY"]) {
        
        NSString * initial = [[oggettoLista valueForKey:@"name"] substringToIndex:1];
        [oggettoLista setValue:initial forKey:@"nameInitial"];

        [listaOggetti addObject:oggettoLista];
        //[oggettoLista removeAllObjects];
    }
    
    xmlTag = @"";
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
    NSLog(@"Error: %@", [parseError localizedDescription]);
    self.syncResultErrorStatus = RDSyncErrorStatusXMLParserError;
}


@end
