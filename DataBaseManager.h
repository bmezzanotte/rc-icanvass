//
//  DataBaseManager.h
//  iRoyalDays
//
//  Created by NAeS Lavoro on 28/11/13.
//  Copyright (c) 2013 NAeS Lavoro. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Customer;

@interface DataBaseManager : NSObject

- (void)insertNewSettingWithPropertyName:(NSString *)propertyName AndPropertyValue:(NSString *)propertyValue;
- (NSMutableArray *)selectFromTable:(NSString *)nomeTabella Where:(NSString *)condizione OrderBy:(NSString *)ordinamento isAscending:(BOOL)ascending;
- (NSMutableArray *)selectFromTable:(NSString *)nomeTabella withPredicate:(NSPredicate *)predicate OrderBy:(NSString *)sortKey isAscending:(BOOL)ascending;
- (NSMutableArray *)selectPropertyFromTable:(NSString *)entityName Where:(NSString *)filter OrderBy:(NSString*)sortKey isAcending:(BOOL)ascending forProperty:(NSString *)property;
- (BOOL)deleteItem:(NSManagedObject *)item;
- (void)deleteAllItemsForTable:(NSString *)table;
- (NSString *)getValueFromSettingsByPropertyName:(NSString *)properyName;
- (void)updateOrInsertSettingWithPropertyName:(NSString *)propertyName AndPropertyValue:(NSString *)propertyValue;
- (int)syncItemForTable:(NSString *)tableName WithItemDictionary:(NSMutableDictionary *)dataItem;

- (int)syncTargetwithItemDictionary:(NSMutableDictionary *)dataForItem;
- (int)syncActivityWithItemDictionary:(NSMutableDictionary *)dataForItem withTargetUnid:(NSString *)targetUnid withCustomers:(NSArray *)customers;
- (int)syncSurveyWithItemDictionary:(NSMutableDictionary *)dataForItem withCustomerUnid:(NSString *)customerUnid;

- (void)updateItem:(NSManagedObject *)item WithUpdateData:(NSDictionary *)listaAggiornamenti;
@end
