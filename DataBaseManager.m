//
//  DataBaseManager.m
//  iRoyalDays
//
//  Created by NAeS Lavoro on 28/11/13.
//  Copyright (c) 2013 NAeS Lavoro. All rights reserved.
//

#import "DataBaseManager.h"
#import "AppDelegate.h"
#import "Utility.h"

#import "Target.h"
#import "TargetQuestion.h"
#import "ActivityHeader.h"
#import "ActivityAnswer.h"

#import "SurveyHeader.h"
#import "SurveyAnswer.h"

@implementation DataBaseManager

- (void)insertNewSettingWithPropertyName:(NSString *)propertyName AndPropertyValue:(NSString *)propertyValue{
    AppDelegate *appDelegate =[[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *managedObjectContext = [appDelegate managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Settings" inManagedObjectContext:managedObjectContext];
    [request setEntity:entity];
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"propertyName = %@",propertyName];
    [request setPredicate:pred];
    NSManagedObject *theItem = nil;
    NSError *error1;
    NSArray *objects = [managedObjectContext executeFetchRequest:request error:&error1];
    if (objects == nil) {
        NSLog(@"C'è stato un errore!");
    }
    if ([objects count]>0) {
        theItem = [objects objectAtIndex:0];
    }else{
        theItem = [NSEntityDescription insertNewObjectForEntityForName:@"Settings" inManagedObjectContext:managedObjectContext];
        [theItem setValue:propertyName forKey:@"propertyName"];
        [theItem setValue:propertyValue forKey:@"propertyValue"];
    }
    [managedObjectContext save:&error1];
}

- (NSMutableArray *)selectFromTable:(NSString *)nomeTabella Where:(NSString *)condizione OrderBy:(NSString *)ordinamento isAscending:(BOOL)ascending{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = [appDelegate managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:nomeTabella inManagedObjectContext:managedObjectContext];
    [request setEntity:entity];
    
    if (condizione != nil) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:condizione];
       [request setPredicate:predicate];
    }
    if (ordinamento != nil) {
        NSSortDescriptor *sortDescriptor1 = [[NSSortDescriptor alloc] initWithKey:ordinamento ascending:ascending];
        NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor1, nil];
        [request setSortDescriptors:sortDescriptors];
    }
    
    NSError *error1;
    NSArray *objects = [managedObjectContext executeFetchRequest:request error:&error1];
    return [objects mutableCopy];
}


- (NSMutableArray *)selectFromTable:(NSString *)nomeTabella withPredicate:(NSPredicate *)predicate OrderBy:(NSString *)sortKey isAscending:(BOOL)ascending {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = [appDelegate managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:nomeTabella inManagedObjectContext:managedObjectContext];
    [request setEntity:entity];
    
    if (predicate != nil) {
        [request setPredicate:predicate];
    }
    if (sortKey != nil) {
        NSSortDescriptor *sortDescriptor1 = [[NSSortDescriptor alloc] initWithKey:sortKey ascending:ascending];
        NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor1, nil];
        [request setSortDescriptors:sortDescriptors];
    }
    
    NSError *error1;
    NSArray *objects = [managedObjectContext executeFetchRequest:request error:&error1];
    return [objects mutableCopy];

}


- (NSMutableArray *)selectPropertyFromTable:(NSString *)entityName Where:(NSString *)filter OrderBy:(NSString*)sortKey isAcending:(BOOL)ascending forProperty:(NSString *)property {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = [appDelegate managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:managedObjectContext];
    [request setEntity:entity];
    
    if (filter != nil) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:filter];
        [request setPredicate:predicate];
    }
    if (sortKey != nil) {
        NSSortDescriptor *sortDescriptor1 = [[NSSortDescriptor alloc] initWithKey:sortKey ascending:ascending];
        NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor1, nil];
        [request setSortDescriptors:sortDescriptors];
    }
    
    NSError *error1;
    
    request.resultType = NSDictionaryResultType;
    request.propertiesToFetch = [NSArray arrayWithObject:[[entity propertiesByName] objectForKey:property]];
    request.returnsDistinctResults = YES;
    
    // Now it should yield an NSArray of distinct values in dictionaries.
    NSArray *propertyValues = [[managedObjectContext executeFetchRequest:request error:&error1] valueForKey:property];
    NSLog (@"%@: %@",property,propertyValues);
    
    return [propertyValues mutableCopy];
}

- (BOOL)deleteItem:(NSManagedObject *)item {
    AppDelegate *appDelegate =[[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *managedObjectContext = [appDelegate managedObjectContext];
    
    [managedObjectContext deleteObject:item];
    if ([item isDeleted]){
        NSLog(@"Successfully deleted object...");
        NSError *savingError = nil;
        if ([managedObjectContext save:&savingError]){
            NSLog(@"Successfully saved the context.");
            return YES;
        }else{
            NSLog(@"Failed to save the context.");
            return NO;
        }
    } else {
        NSLog(@"Failed to delete object.");
        return NO;
    }
}

- (void)deleteAllItemsForTable:(NSString *)table {
    AppDelegate *appDelegate =[[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *managedObjectContext = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:table inManagedObjectContext:managedObjectContext];
    [request setEntity:entity];
    
    NSError *requestError = nil;
    NSArray *objects = [managedObjectContext executeFetchRequest:request error:&requestError];
    for (NSManagedObject * obj in objects) {
        [managedObjectContext deleteObject:obj];
        if (![obj isDeleted]) {
            NSLog(@"Failed to delete the object.");
        }
    }
    NSError *saveError = nil;
    if ([managedObjectContext save:&saveError]){
        NSLog(@"Successfully saved the context.");
    } else {
        NSLog(@"Failed to save the context.");
    }
}

- (NSString *)getValueFromSettingsByPropertyName:(NSString *)properyName{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = [appDelegate managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Settings" inManagedObjectContext:managedObjectContext];
    [request setEntity:entity];
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"propertyName == '%@'", properyName]];
    [request setPredicate:predicate];
    
    NSError *error1;
    NSArray *objects = [managedObjectContext executeFetchRequest:request error:&error1];
    
    if ([objects count]>0) {
        return [[objects objectAtIndex:0] valueForKey:@"propertyValue"];
    }else{
        return nil;
    }
}

- (void)updateOrInsertSettingWithPropertyName:(NSString *)propertyName AndPropertyValue:(NSString *)propertyValue{
    AppDelegate *appDelegate =[[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *managedObjectContext = [appDelegate managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Settings" inManagedObjectContext:managedObjectContext];
    [request setEntity:entity];
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"propertyName = %@",propertyName];
    [request setPredicate:pred];
    NSManagedObject *theItem = nil;
    NSError *error1;
    NSArray *objects = [managedObjectContext executeFetchRequest:request error:&error1];
    if (objects == nil) {
        NSLog(@"C'è stato un errore!");
    }
    if ([objects count]>0) {
        theItem = [objects objectAtIndex:0];
    }else{
        theItem = [NSEntityDescription insertNewObjectForEntityForName:@"Settings" inManagedObjectContext:managedObjectContext];
        [theItem setValue:propertyName forKey:@"propertyName"];
    }
    
    [theItem setValue:propertyValue forKey:@"propertyValue"];
    [managedObjectContext save:&error1];
}


- (int)syncItemForTable:(NSString *)tableName WithItemDictionary:(NSMutableDictionary *)dataForItem {
    int result = 0;     //0=failure, 1=success/record added, 2 = success / record updated
    
    @try {
        AppDelegate *appDelegate =[[UIApplication sharedApplication]delegate];
        NSManagedObjectContext *managedObjectContext = [appDelegate managedObjectContext];
        NSFetchRequest *request = [[NSFetchRequest alloc]init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:tableName inManagedObjectContext:managedObjectContext];
        [request setEntity:entity];
        NSPredicate *pred = nil;
        if ([tableName isEqualToString:@"Customer"]) {
            pred = [NSPredicate predicateWithFormat:@"code = %@",[dataForItem valueForKey:@"code"]];
        }else if ([tableName isEqualToString:@"Period"]){
            pred = [NSPredicate predicateWithFormat:@"code = %@",[[dataForItem valueForKey:@"code"] valueForKey:@"value"]];
        }else if ([tableName isEqualToString:@"Target"]){
            pred = [NSPredicate predicateWithFormat:@"subjectUnid == %@ AND periodCode = %@",[[dataForItem valueForKey:@"subjectUnid"] valueForKey:@"value"],[[dataForItem valueForKey:@"periodCode"] valueForKey:@"value"]];
        }else if  ([tableName isEqualToString:@"ActivityHeader"]){
            pred = [NSPredicate predicateWithFormat:@"unid == %@",[[dataForItem valueForKey:@"unid"] valueForKey:@"value"]];
        }

        [request setPredicate:pred];
        NSManagedObject *theItem = nil;
        NSError *error1;
        NSArray *objects = [managedObjectContext executeFetchRequest:request error:&error1];
        if (objects == nil) {
            NSLog(@"C'è stato un errore!");
            return 0;
        }
        if ([objects count]>0) {
            theItem = [objects objectAtIndex:0];
            result = 2;
        }else{
            theItem = [NSEntityDescription insertNewObjectForEntityForName:tableName inManagedObjectContext:managedObjectContext];
            result = 1;
        }

        Utility *utility = [[Utility alloc]init];

        //inserisco o aggiorno tutti i campi
        if ([tableName isEqualToString:@"Customer"]) {
            NSArray *listaChiavi = [dataForItem allKeys];
            for (int i = 0; i < [listaChiavi count]; i++) {
                [theItem setValue:[dataForItem valueForKey:[listaChiavi objectAtIndex:i]] forKey:[listaChiavi objectAtIndex:i]];
            }

            //imposto la data di ultima modifica
            NSDate *modifyDate = [utility getCurrentDate];
            [theItem setValue:modifyDate forKey:@"lastmodified"];
        }else{
            //nuovo processo che utilizza nuova configurazione plist per gestire tipo di dato da salvare in CoreData
            NSArray *listaChiavi = [dataForItem allKeys];
            for (int i = 0; i < [listaChiavi count]; i++) {
                NSString *attributeName = [listaChiavi objectAtIndex:i];
                NSString *attributeType = [[dataForItem valueForKey:[listaChiavi objectAtIndex:i]] valueForKey:@"type"];
                NSString *attributeValue = [[dataForItem valueForKey:[listaChiavi objectAtIndex:i]] valueForKey:@"value"];
                
                if ([attributeType isKindOfClass:[NSString class]]) {
                    if ([attributeType isEqualToString:@"Integer"]) {
                        int value = [attributeValue intValue];
                        [theItem setValue:[NSNumber numberWithInteger:value] forKey:attributeName];
                    }else if ([attributeType isEqualToString:@"Double"]){
                        double value = [attributeValue doubleValue];
                        [theItem setValue:[NSNumber numberWithDouble:value] forKey:attributeName];
                    }else if ([attributeType isEqualToString:@"Date"]){
                        NSDate *value = [utility getDateFromString:attributeValue withPattern:@"yyyy/MM/dd HH.mm.ss.SSS"];
                        [theItem setValue:value forKey:attributeName];
                    }else if ([attributeType isEqualToString:@"Boolean"]){
                        if ([attributeValue isEqualToString:@"1"]) {
                            [theItem setValue:[NSNumber numberWithInt:1] forKey:attributeName];
                        }else{
                            [theItem setValue:[NSNumber numberWithInt:0] forKey:attributeName];
                        }
                    }else  if ([attributeType isEqualToString:@"String"]){
                        [theItem setValue:attributeValue forKey:attributeName];
                    }                    
                }
            }
        }
        
        //salvo
        if ([managedObjectContext save:&error1]){
            return result;
        }else{
            return 0;
        }
    }
    @catch (NSException *exception) {
        NSLog(@"Erorr in DatabaseManager - syncItemforTable: %@", exception.description);
        return 0;
    }
}


- (int)syncTargetwithItemDictionary:(NSMutableDictionary *)dataForItem
{
    NSString *entityName = @"Target";
    NSString *childEntityName = @"TargetQuestion";

    int result = 0;     //0=failure, 1=success/record added, 2 = success / record updated
    
    @try {
        AppDelegate *appDelegate =[[UIApplication sharedApplication]delegate];
        NSManagedObjectContext *managedObjectContext = [appDelegate managedObjectContext];
        NSFetchRequest *request = [[NSFetchRequest alloc]init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:managedObjectContext];
        [request setEntity:entity];
        NSPredicate *pred = nil;
        pred = [NSPredicate predicateWithFormat:@"subjectUnid == %@ AND periodCode = %@",[[dataForItem valueForKey:@"subjectUnid"] valueForKey:@"value"],[[dataForItem valueForKey:@"periodCode"] valueForKey:@"value"]];
        
        [request setPredicate:pred];
        Target *theItem = nil;
        NSError *error1;
        NSArray *objects = [managedObjectContext executeFetchRequest:request error:&error1];
        if (objects == nil) {
            NSLog(@"C'è stato un errore!");
            return 0;
        }
        if ([objects count]>0) {
            theItem = [objects objectAtIndex:0];
            result = 2;
        }else{
            theItem = [NSEntityDescription insertNewObjectForEntityForName:entityName inManagedObjectContext:managedObjectContext];
            result = 1;
        }
        
        Utility *utility = [[Utility alloc]init];
        
        //inserisco o aggiorno tutti i campi
        //nuovo processo che utilizza nuova configurazione plist per gestire tipo di dato da salvare in CoreData
        NSArray *listaChiavi = [dataForItem allKeys];
        for (int i = 0; i < [listaChiavi count]; i++) {
            NSString *attributeName = [listaChiavi objectAtIndex:i];
            NSString *attributeType = [[dataForItem valueForKey:[listaChiavi objectAtIndex:i]] valueForKey:@"type"];
            NSString *attributeValue = [[dataForItem valueForKey:[listaChiavi objectAtIndex:i]] valueForKey:@"value"];
            
            if ([attributeType isKindOfClass:[NSString class]]) {
                if ([attributeType isEqualToString:@"Integer"]) {
                    int value = [attributeValue intValue];
                    [theItem setValue:[NSNumber numberWithInteger:value] forKey:attributeName];
                }else if ([attributeType isEqualToString:@"Double"]){
                    double value = [attributeValue doubleValue];
                    [theItem setValue:[NSNumber numberWithDouble:value] forKey:attributeName];
                }else if ([attributeType isEqualToString:@"Date"]){
                    NSDate *value = [utility getDateFromString:attributeValue withPattern:@"yyyy/MM/dd HH.mm.ss.SSS"];
                    [theItem setValue:value forKey:attributeName];
                }else if ([attributeType isEqualToString:@"Boolean"]){
                    if ([attributeValue isEqualToString:@"1"]) {
                        [theItem setValue:[NSNumber numberWithInt:1] forKey:attributeName];
                    }else{
                        [theItem setValue:[NSNumber numberWithInt:0] forKey:attributeName];
                    }
                    
                }else  if ([attributeType isEqualToString:@"String"]){
                    [theItem setValue:attributeValue forKey:attributeName];
                }
            } else {
                
                
                if ([attributeName isEqualToString:@"questions"]) {
                    NSLog(@"Set Relationships");
                    NSArray *childEntities = [[NSArray alloc] init];
                    childEntities = [dataForItem valueForKey:[listaChiavi objectAtIndex:i]];
                    if ([childEntities count] > 0) {
                        for (NSDictionary *dict in childEntities) {
                            NSLog(@"Add relationship");
                            TargetQuestion *childItem = nil;
                            childItem = [NSEntityDescription insertNewObjectForEntityForName:childEntityName inManagedObjectContext:managedObjectContext];
                            
                            NSArray *childKeys = [dict allKeys];
                            for (int j = 0; j < [childKeys count]; j++) {
                                NSString *attributeName = [childKeys objectAtIndex:j];
                                NSString *attributeType = [[dict valueForKey:[childKeys objectAtIndex:j]] valueForKey:@"type"];
                                NSString *attributeValue = [[dict valueForKey:[childKeys objectAtIndex:j]] valueForKey:@"value"];
                                
                                if ([attributeType isEqualToString:@"Integer"]) {
                                    int value = [attributeValue intValue];
                                    [childItem setValue:[NSNumber numberWithInteger:value] forKey:attributeName];
                                }else if ([attributeType isEqualToString:@"Double"]){
                                    double value = [attributeValue doubleValue];
                                    [childItem setValue:[NSNumber numberWithDouble:value] forKey:attributeName];
                                }else if ([attributeType isEqualToString:@"Date"]){
                                    NSDate *value = [utility getDateFromString:attributeValue withPattern:@"yyyy/MM/dd HH.mm.ss.SSS"];
                                    [childItem setValue:value forKey:attributeName];
                                }else if ([attributeType isEqualToString:@"Boolean"]){
                                    if ([attributeValue isEqualToString:@"1"]) {
                                        [childItem setValue:[NSNumber numberWithInt:1] forKey:attributeName];
                                    }else{
                                        [childItem setValue:[NSNumber numberWithInt:0] forKey:attributeName];
                                    }
                                }else  if ([attributeType isEqualToString:@"String"]){
                                    [childItem setValue:attributeValue forKey:attributeName];
                                }
                            }
                            
                            [theItem addQuestionsObject:childItem];
                        }
                    }
                }
            }
        }
        
        
        //salvo
        if ([managedObjectContext save:&error1]){
            return result;
        }else{
            return 0;
        }
    }
    @catch (NSException *exception) {
        NSLog(@"Erorr in DatabaseManager - syncItemforTable: %@", exception.description);
        return 0;
    }
}

- (int)syncActivityWithItemDictionary:(NSMutableDictionary *)dataForItem withTargetUnid:(NSString *)targetUnid withCustomers:(NSArray *)customers
{
    NSString *entityName = @"ActivityHeader";
    NSString *childEntityName = @"ActivityAnswer";
    
    int result = 0;     //0=failure, 1=success/record added, 2 = success / record updated
    
    @try {
        AppDelegate *appDelegate =[[UIApplication sharedApplication]delegate];
        NSManagedObjectContext *managedObjectContext = [appDelegate managedObjectContext];
        NSFetchRequest *request = [[NSFetchRequest alloc]init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:managedObjectContext];
        [request setEntity:entity];
        NSPredicate *pred = nil;
        pred = [NSPredicate predicateWithFormat:@"unid == %@",[[dataForItem valueForKey:@"unid"] valueForKey:@"value"]];
        
        [request setPredicate:pred];
        ActivityHeader *theItem = nil;
        NSError *error1;
        NSArray *objects = [managedObjectContext executeFetchRequest:request error:&error1];
        if (objects == nil) {
            NSLog(@"C'è stato un errore!");
            return 0;
        }
        if ([objects count]>0) {
            theItem = [objects objectAtIndex:0];
            result = 2;
        }else{
            
            /*
            //temporaneo, per vedere cosa c'è nella tabella ActivityHeader
            NSFetchRequest *request = [[NSFetchRequest alloc]init];
            NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:managedObjectContext];
            [request setEntity:entity];
            NSError *error1;
            NSArray *objects = [managedObjectContext executeFetchRequest:request error:&error1];
            //temporaneo, per vedere cosa c'è nella tabella ActivityHeader
            */
            
            
            
            theItem = [NSEntityDescription insertNewObjectForEntityForName:entityName inManagedObjectContext:managedObjectContext];
            result = 1;
        }
        
        Utility *utility = [[Utility alloc]init];
        
        //inserisco o aggiorno tutti i campi
        NSArray *listaChiavi = [dataForItem allKeys];
        for (int i = 0; i < [listaChiavi count]; i++) {
            NSString *attributeName = [listaChiavi objectAtIndex:i];
            NSString *attributeType = [[dataForItem valueForKey:[listaChiavi objectAtIndex:i]] valueForKey:@"type"];
            NSString *attributeValue = [[dataForItem valueForKey:[listaChiavi objectAtIndex:i]] valueForKey:@"value"];
            
            if ([attributeType isKindOfClass:[NSString class]]) {
                
                
                if ([attributeType isEqualToString:@"Integer"]) {
                    int value = [attributeValue intValue];
                    [theItem setValue:[NSNumber numberWithInteger:value] forKey:attributeName];
                }else if ([attributeType isEqualToString:@"Double"]){
                    double value = [attributeValue doubleValue];
                    [theItem setValue:[NSNumber numberWithDouble:value] forKey:attributeName];
                }else if ([attributeType isEqualToString:@"Number"]){
                    double value = [attributeValue doubleValue];
                    [theItem setValue:[NSNumber numberWithDouble:value] forKey:attributeName];
                }else if ([attributeType isEqualToString:@"Date"]){
                    if ([[attributeValue class] isSubclassOfClass:[NSDate class]]) {
                        [theItem setValue:attributeValue forKey:attributeName];
                    } else {
                        NSDate *myDate = [utility getDateFromString:attributeValue withPattern:@"yyyy/MM/dd HH.mm.ss.SSS"];
                        [theItem setValue:myDate forKey:attributeName];
                    }
                }else if ([attributeType isEqualToString:@"Boolean"]){
                    if ([attributeValue isEqualToString:@"1"]) {
                        [theItem setValue:[NSNumber numberWithInt:1] forKey:attributeName];
                    }else{
                        [theItem setValue:[NSNumber numberWithInt:0] forKey:attributeName];
                    }
                    
                }else  if ([attributeType isEqualToString:@"String"]){
                    [theItem setValue:attributeValue forKey:attributeName];
                }
                
                
            } else {
                
                if ([attributeName isEqualToString:@"questions"]) {
                    NSLog(@"Set Answer Relationships");
                    
                    //elimino da CoreData le answers eventualmente salvate in precedenza per questa attività
                    for (ActivityAnswer *answerToDelete in theItem.answers) {
                        [managedObjectContext deleteObject: answerToDelete];
                    }
                    
                    
                    NSArray *childEntities = [[NSArray alloc] init];
                    childEntities = [dataForItem valueForKey:[listaChiavi objectAtIndex:i]];
                    if ([childEntities count] > 0) {
                        for (NSDictionary *dict in childEntities) {
                            NSLog(@"Add Answer relationship");
                            ActivityAnswer *childItem = nil;
                            

                            if (! childItem){
                                //1 devo recuperare da entity TargetQuestion la domanda corrispondente a questionDefinitionUnid
                                NSFetchRequest *request = [[NSFetchRequest alloc]init];
                                NSEntityDescription *entity = [NSEntityDescription entityForName:@"TargetQuestion" inManagedObjectContext:managedObjectContext];
                                [request setEntity:entity];
                                NSPredicate *pred = nil;
                                pred = [NSPredicate predicateWithFormat:@"unid == %@",[[dict allKeys] objectAtIndex:0]];
                                
                                [request setPredicate:pred];
                                TargetQuestion *myQuestion = nil;
                                NSError *error1;
                                NSArray *objects = [managedObjectContext executeFetchRequest:request error:&error1];
                                if (objects == nil) {
                                    NSLog(@"C'è stato un errore!");
                                    return 0;
                                }
                                if ([objects count]>0) {
                                    myQuestion = [objects objectAtIndex:0];
                                }else{
                                    NSLog(@"Question Definition non trovata!");
                                    return 0;
                                }

                                
                                //2 creo l'oggetto in entity ActivityAnswer
                                childItem = [NSEntityDescription insertNewObjectForEntityForName:childEntityName inManagedObjectContext:managedObjectContext];
                                
                                //3 popolo i campi
                                NSArray *childKeys = [dict allKeys];
                                
                                NSString *attributeName = [childKeys objectAtIndex:0];
                                NSString *attributeType = [[dict valueForKey:[childKeys objectAtIndex:0]] valueForKey:@"type"];
                                NSString *attributeValue = [[dict valueForKey:[childKeys objectAtIndex:0]] valueForKey:@"value"];
                                NSString *attributeUnid = [[dict valueForKey:[childKeys objectAtIndex:0]] valueForKey:@"unid"];
                                    
                                [childItem setValue:attributeName forKey:@"questionDefinitionUnid"];
                                [childItem setValue:attributeType forKey:@"type"];
                                [childItem setValue:attributeValue forKey:@"value"];
                                [childItem setValue:attributeUnid forKey:@"unid"];
                            }

                            
                            [theItem addAnswersObject:childItem];
                        }
                    }
                }
                
            }
        }
        
        
        //customers relationship
        //rimuovo precedenti relationship con customers
        [theItem setCustomer:nil];
        
        //aggiungo i clienti selezionati
        if (customers) {
            for (NSManagedObject *customer in customers) {
                NSLog(@"Set Customer Relationship");
                [theItem addCustomerObject:customer];
            }
        }
        
        //taget relationship
        if (targetUnid) {
            NSLog(@"Set Target Relationship");
            Target *myTarget = nil;
            
            //recupero da entity Target l'oggetto corrispondente a targetUnid
            NSFetchRequest *request = [[NSFetchRequest alloc]init];
            NSEntityDescription *entity = [NSEntityDescription entityForName:@"Target" inManagedObjectContext:managedObjectContext];
            [request setEntity:entity];
            NSPredicate *pred = nil;
            pred = [NSPredicate predicateWithFormat:@"subjectUnid == %@ or unid == %@",targetUnid, targetUnid];
            
            [request setPredicate:pred];
            NSError *error1;
            NSArray *objects = [managedObjectContext executeFetchRequest:request error:&error1];
            if (objects == nil) {
                NSLog(@"C'è stato un errore!");
                return 0;
            }
            if ([objects count]>0) {
                myTarget = [objects objectAtIndex:0];
            }else{
                NSLog(@"Target NON TROVATO!");
                
                /*
                //temporaneo, per vedere cosa c'è nella tabella Target
                NSFetchRequest *request = [[NSFetchRequest alloc]init];
                NSEntityDescription *entity = [NSEntityDescription entityForName:@"Target" inManagedObjectContext:managedObjectContext];
                [request setEntity:entity];
                NSError *error1;
                NSArray *objects = [managedObjectContext executeFetchRequest:request error:&error1];
                //temporaneo, per vedere cosa c'è nella tabella Target
                */
                
                return 0;
            }
            
            [theItem setTarget:myTarget];
        }
        
        
        //lastmodified
        NSDate *lastoModifiedDate = [NSDate date];
        [theItem setValue:lastoModifiedDate forKey:@"lastmodified"];

        
        //salvo
        if ([managedObjectContext save:&error1]){
            return result;
        }else{
            NSLog(@"Errore salvataggio managedObjectContext: %@", error1.description);
            return 0;
        }
    }
    @catch (NSException *exception) {
        NSLog(@"Erorr in DatabaseManager - syncItemforTable: %@", exception.description);
        return 0;
    }    
}

- (int)syncSurveyWithItemDictionary:(NSMutableDictionary *)dataForItem withCustomerUnid:(NSString *)customerUnid
{
    NSString *entityName = @"SurveyHeader";
    NSString *childEntityName = @"SurveyAnswer";
    
    int result = 0;     //0=failure, 1=success/record added, 2 = success / record updated
    
    @try {
        AppDelegate *appDelegate =[[UIApplication sharedApplication]delegate];
        NSManagedObjectContext *managedObjectContext = [appDelegate managedObjectContext];
        NSFetchRequest *request = [[NSFetchRequest alloc]init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:managedObjectContext];
        [request setEntity:entity];
        NSPredicate *pred = nil;
        pred = [NSPredicate predicateWithFormat:@"unid == %@",[[dataForItem valueForKey:@"unid"] valueForKey:@"value"]];
        
        [request setPredicate:pred];
        SurveyHeader *theItem = nil;
        NSError *error1;
        NSArray *objects = [managedObjectContext executeFetchRequest:request error:&error1];
        if (objects == nil) {
            NSLog(@"C'è stato un errore!");
            return 0;
        }
        if ([objects count]>0) {
            theItem = [objects objectAtIndex:0];
            result = 2;
        }else{
            
            /*
            //temporaneo, per vedere cosa c'è nella tabella SurveyHeader
            NSFetchRequest *request = [[NSFetchRequest alloc]init];
            NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:managedObjectContext];
            [request setEntity:entity];
            NSError *error1;
            NSArray *objects = [managedObjectContext executeFetchRequest:request error:&error1];
            //temporaneo, per vedere cosa c'è nella tabella ActivityHeader
            */
            
            
            
            theItem = [NSEntityDescription insertNewObjectForEntityForName:entityName inManagedObjectContext:managedObjectContext];
            result = 1;
        }
        
        Utility *utility = [[Utility alloc]init];
        
        //inserisco o aggiorno tutti i campi
        NSArray *listaChiavi = [dataForItem allKeys];
        for (int i = 0; i < [listaChiavi count]; i++) {
            NSString *attributeName = [listaChiavi objectAtIndex:i];
            NSString *attributeType = [[dataForItem valueForKey:[listaChiavi objectAtIndex:i]] valueForKey:@"type"];
            NSString *attributeValue = [[dataForItem valueForKey:[listaChiavi objectAtIndex:i]] valueForKey:@"value"];
            
            if ([attributeType isKindOfClass:[NSString class]]) {
                
                if ([attributeType isEqualToString:@"Integer"]) {
                    int value = [attributeValue intValue];
                    [theItem setValue:[NSNumber numberWithInteger:value] forKey:attributeName];
                }else if ([attributeType isEqualToString:@"Double"]){
                    double value = [attributeValue doubleValue];
                    [theItem setValue:[NSNumber numberWithDouble:value] forKey:attributeName];
                }else if ([attributeType isEqualToString:@"Number"]){
                    double value = [attributeValue doubleValue];
                    [theItem setValue:[NSNumber numberWithDouble:value] forKey:attributeName];
                }else if ([attributeType isEqualToString:@"Date"]){
                    if ([[attributeValue class] isSubclassOfClass:[NSDate class]]) {
                        [theItem setValue:attributeValue forKey:attributeName];
                    } else {
                        NSDate *myDate = [utility getDateFromString:attributeValue withPattern:@"yyyy/MM/dd HH.mm.ss.SSS"];
                        [theItem setValue:myDate forKey:attributeName];
                    }
                }else if ([attributeType isEqualToString:@"Boolean"]){
                    if ([attributeValue isEqualToString:@"1"]) {
                        [theItem setValue:[NSNumber numberWithInt:1] forKey:attributeName];
                    }else{
                        [theItem setValue:[NSNumber numberWithInt:0] forKey:attributeName];
                    }
                    
                }else  if ([attributeType isEqualToString:@"String"]){
                    [theItem setValue:attributeValue forKey:attributeName];
                }
                
                
            } else {
                
                if ([attributeName isEqualToString:@"questions"]) {
                    
                    //elimino da CoreData le answers eventualmente salvate in precedenza per questa attività
                    for (ActivityAnswer *answerToDelete in theItem.answers) {
                        [managedObjectContext deleteObject: answerToDelete];
                    }
                    
                    
                    
                    /*
                    //temporaneo, per vedere cosa c'è nella tabella SurveyAnswer
                    NSFetchRequest *newrequest = [[NSFetchRequest alloc]init];
                    NSEntityDescription *newentity = [NSEntityDescription entityForName:@"SurveyAnswer" inManagedObjectContext:managedObjectContext];
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", @"unid", [NSString stringWithFormat:@"%@_brandposd_r",theItem.unid]];
                    [newrequest setEntity:newentity];
                    [newrequest setPredicate:predicate];
                    NSError *newerror1;
                    NSArray *responseObjects = [managedObjectContext executeFetchRequest:newrequest error:&newerror1];
                    //temporaneo, per vedere cosa c'è nella tabella SurveyAnswer
                    */
                    
 
 
                    NSArray *childEntities = [[NSArray alloc] init];
                    childEntities = [dataForItem valueForKey:[listaChiavi objectAtIndex:i]];
                    if ([childEntities count] > 0) {
                        for (NSDictionary *dict in childEntities) {
                            SurveyAnswer *childItem = nil;
                            
                            
                            if (! childItem){
                                
                                // creo l'oggetto in entity SurveyAnswer
                                childItem = [NSEntityDescription insertNewObjectForEntityForName:childEntityName inManagedObjectContext:managedObjectContext];
                                
                                // popolo i campi			
                                NSString *attributeType = [dict valueForKey:@"type"];
                                NSString *attributeSubject = [[dict valueForKey:@"subject"] lowercaseString];
//                                NSString *attributeSubject = [dict valueForKey:@"subject"];
                                
                                /*
                                if ([attributeSubject isEqualToString:@"visibilitylayoutok"]) {
                                    NSLog(@"QUI");
                                }
                                */
                                
                                NSMutableString *attributeValue;
                                if ([[dict valueForKey:@"value"] isKindOfClass:[NSArray class]]) {
                                    NSArray *values = [dict valueForKey:@"value"];
                                    attributeValue = [[values componentsJoinedByString:@","] mutableCopy];
                                }else{
                                    attributeValue = [dict valueForKey:@"value"];
                                }
                                
                                if (attributeValue && ![attributeValue isEqualToString:@""]) {
                                    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
                                    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"it_IT"];
                                    [formatter setLocale:locale];
                                    
                                    if ([attributeType isEqualToString:@"Integer"]) {
                                        NSNumber *value = [NSNumber numberWithInteger:[attributeValue integerValue]];
                                        [formatter setNumberStyle:NSNumberFormatterRoundFloor];
                                        attributeValue = [[formatter stringFromNumber:value] mutableCopy];
                                    }else if ([attributeType isEqualToString:@"Double"]){
                                        [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
                                        double value = [[formatter numberFromString: attributeValue] doubleValue];
                                        attributeValue = [[formatter stringFromNumber:[NSNumber numberWithDouble:value]] mutableCopy];
                                    }else if ([attributeType isEqualToString:@"Number"]){
                                        [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
                                        double value = [[formatter numberFromString: attributeValue] doubleValue];
                                        attributeValue = [[formatter stringFromNumber:[NSNumber numberWithDouble:value]] mutableCopy];
                                    }else if ([attributeType isEqualToString:@"Date"]){
                                        //already formatted as string from WS
                                    }else if ([attributeType isEqualToString:@"Boolean"]){
                                        //already formatted as string from WS
                                    }else  if ([attributeType isEqualToString:@"String"]){
                                        //already formatted as string from WS
                                    }else{
                                        
                                    }
                                }
                                
                                [childItem setValue:attributeSubject forKey:@"subject"];
                                [childItem setValue:attributeType forKey:@"type"];
                                [childItem setValue:attributeValue forKey:@"value"];
                                [childItem setValue:[NSString stringWithFormat:@"%@_%@", theItem.unid, attributeSubject] forKey:@"unid"];
                            }
                            
                            
                            [theItem addAnswersObject:childItem];
                        }
                    }
                }
                
            }
        }
        
        
        
        
        /*
        //temporaneo, per vedere cosa c'è nella tabella SurveyAnswer
        NSFetchRequest *newrequest = [[NSFetchRequest alloc]init];
        NSEntityDescription *newentity = [NSEntityDescription entityForName:@"SurveyAnswer" inManagedObjectContext:managedObjectContext];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", @"unid", [NSString stringWithFormat:@"%@_brandposd_r",theItem.unid]];
        [newrequest setEntity:newentity];
        [newrequest setPredicate:predicate];
        NSError *newerror1;
        NSArray *responseObjects = [managedObjectContext executeFetchRequest:newrequest error:&newerror1];
        //temporaneo, per vedere cosa c'è nella tabella SurveyAnswer
        */
        
        
        //customers relationship
        [theItem setCustomer:nil];
        
        if (customerUnid) {
            Customer *myCustomer = nil;
            
            //recupero da entity Customer l'oggetto corrispondente a customerUnid
            NSFetchRequest *request = [[NSFetchRequest alloc]init];
            NSEntityDescription *entity = [NSEntityDescription entityForName:@"Customer" inManagedObjectContext:managedObjectContext];
            [request setEntity:entity];
            NSPredicate *pred = nil;
            pred = [NSPredicate predicateWithFormat:@"unid == %@",customerUnid];
            
            [request setPredicate:pred];
            NSError *error1;
            NSArray *objects = [managedObjectContext executeFetchRequest:request error:&error1];
            if (objects == nil) {
                NSLog(@"C'è stato un errore!");
                return 0;
            }
            if ([objects count]>0) {
                myCustomer = [objects objectAtIndex:0];
            }else{
                NSLog(@"Customer NON TROVATO!");
                return 0;
            }

            [theItem setCustomer:myCustomer];
        }
        
        
        
        //lastmodified
        NSDate *lastoModifiedDate = [NSDate date];
        [theItem setValue:lastoModifiedDate forKey:@"lastmodified"];
        
        
        //salvo
        if ([managedObjectContext save:&error1]){
            return result;
        }else{
            NSLog(@"Errore salvataggio managedObjectContext: %@", error1.description);
            return 0;
        }
    }
    @catch (NSException *exception) {
        NSLog(@"Erorr in DatabaseManager - syncItemforTable: %@", exception.description);
        return 0;
    }    
}

- (void)updateItem:(NSManagedObject *)item WithUpdateData:(NSDictionary *)listaAggiornamenti{
    AppDelegate *appDelegate =[[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *managedObjectContext = [appDelegate managedObjectContext];
    
    for (NSString *key in listaAggiornamenti.keyEnumerator){
        [item setValue:[listaAggiornamenti valueForKey:key] forKey:key];
    }
    
    //imposto la data di ultima modifica
    Utility *utility = [[Utility alloc]init];
    NSDate *modifyDate =[utility getCurrentDate];
    [item setValue:modifyDate forKey:@"lastmodified"];
    
    NSError *error1;
    [managedObjectContext save:&error1];
}

@end
