//
//  DetailActivityViewController.h
//  iCanvass
//
//  Created by NAeS Lavoro on 24/02/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomerActivityViewController.h"

@class Utility;
@class DataBaseManager;
@interface DetailActivityViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource, CustomerActivityDelegate, UIActionSheetDelegate, UITextFieldDelegate>{
    NSIndexPath *indexCell;
    NSMutableArray *listDetailActivity;
    
}
@property (nonatomic, retain) Utility *utility;
@property (nonatomic, retain) DataBaseManager *dbManager;
@property (nonatomic, assign) BOOL isNewActivity;
@property (nonatomic, strong) NSMutableArray *pickerViewData;
@property (nonatomic, retain) NSMutableArray *listQuestionTarget;
@property (nonatomic, retain) UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *saveButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cancelButton;
@property (nonatomic, retain) UITextField *textField;
- (IBAction)saveAction:(id)sender;
- (IBAction)cancelAction:(id)sender;
- (IBAction)hidePickerView:(id)sender;

@end