//
//  DetailActivityViewController.m
//  iCanvass
//
//  Created by NAeS Lavoro on 24/02/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import "DetailActivityViewController.h"
#import "Utility.h"
#import "Target.h"
#import "TargetQuestion.h"
#import "CustomerActivityViewController.h"
#import "RDDetailTextCell.h"
#import "RDDetailRadioCell.h"

@interface DetailActivityViewController ()

@end

@implementation DetailActivityViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _utility = [[Utility alloc]init];
    _dbManager = [[DataBaseManager alloc]init];
	listDetailActivity = [[NSMutableArray alloc]initWithObjects:[_utility getFormattedDate:[_utility getCurrentDateAsString] withPattern:@"dd-MM-yyyy"], NSLocalizedString(@"Scegli l'obiettivo", @"Scegli l'obiettivo"), NSLocalizedString(@"Scegli il punto vendita", @"Scegli il punto vendita"), NSLocalizedString(@"Scegli la fascia oraria", @"Scegli la fascia oraria"), nil];
    _listQuestionTarget = [[NSMutableArray alloc]initWithCapacity:5];
    _pickerViewData = [[NSMutableArray alloc]initWithCapacity:5];
    
    _datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 280, 0, 0)];
    _datePicker.datePickerMode = UIDatePickerModeDate;
    [_datePicker addTarget:self action:@selector(setDateActivity) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:_datePicker];
    [_datePicker setAlpha:0.0];
    
    
    NSArray *lista = [_dbManager selectFromTable:@"TargetQuestion" Where:nil OrderBy:nil isAscending:nil];
    for (NSManagedObject *question in lista) {
        NSLog(@"%@, - %@, - %@, - %@, - %@, - %@, - %@",[question valueForKey:@"lastmodified"],[question valueForKey:@"options"],[question valueForKey:@"required"],[question valueForKey:@"status"],[question valueForKey:@"subject"],[question valueForKey:@"type"],[question valueForKey:@"unid"]);
    }
    
    [self.saveButton setTitle:@"Salva"];
    [self.cancelButton setTitle:@"Annulla"];
    [self.pickerView setAlpha:0.0];
}

- (void)done{
    NSLog(@"chi mi chiama?");
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return [listDetailActivity count];
    }else{
        return [_listQuestionTarget count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 0) {
        UITableViewCell *cell;
        NSString *identifier = @"ActivityDetailCellIdentifier";
        
        cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"ActivityDetailCellIdentifier"];
        }
        cell.textLabel.font = [UIFont systemFontOfSize:14];
        cell.textLabel.text = [listDetailActivity objectAtIndex:indexPath.row];
        return cell;
    }else{
        UITableViewCell *cell;
        NSString *identifier = @"ActivityQuestionnaireCellIdentifier";
        
        cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithFrame:CGRectZero];
            //cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:@"ActivityQuestionnaireCellIdentifier"];
        }
        
        CGRect nameLabelRect = CGRectMake(5, 10, 140, 60);
        UILabel *nameLabel = [[UILabel alloc] initWithFrame:nameLabelRect];
        nameLabel.textAlignment = NSTextAlignmentRight;
        nameLabel.numberOfLines = 0;
        nameLabel.text = @"Name:";
        nameLabel.font = [UIFont boldSystemFontOfSize:12];
        [cell.contentView addSubview: nameLabel];
        
        TargetQuestion *question;
        if ([_listQuestionTarget count] != 0) {
            question = [_listQuestionTarget objectAtIndex:indexPath.row];
            nameLabel.text = [question valueForKey:@"subject"];
            NSLog(@"%@",question.type);
            if ([question.type isEqualToString:@"RadioButton"]) {
                UISwitch *mySwitch = [[UISwitch alloc] initWithFrame:CGRectMake(160, 20, 0, 0)];
                [mySwitch addTarget:self action:@selector(changeSwitch:) forControlEvents:UIControlEventValueChanged];
                [cell.contentView addSubview:mySwitch];
            }else if([question.type isEqualToString:@"Number"]){
                _textField = [[UITextField alloc] initWithFrame:CGRectMake(160, 15, 140, 40)];
                [_textField setBorderStyle:UITextBorderStyleBezel];
                _textField.delegate = self;
                [_textField setPlaceholder:@"Inserisci quì"];
                [_textField setKeyboardType:UIKeyboardTypeNumberPad];
                [cell.contentView addSubview:_textField];
            }
        }
        return cell;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if(textField==self.textField){
        [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(keyboardDidShowOrHide:) name: UIKeyboardDidShowNotification object:nil];
    }else{
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    }
}

- (void) keyboardDidShowOrHide : (id) sender {
    // create custom button
    UIButton *doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
    doneButton.frame = CGRectMake(0, 515, 106, 53);
    doneButton.adjustsImageWhenHighlighted = NO;
    [doneButton setBackgroundImage:[UIImage imageNamed:@"doneup.png"] forState:UIControlStateNormal];
    [doneButton setBackgroundImage:[UIImage imageNamed:@"donedown.png"] forState:UIControlStateHighlighted];
    [doneButton setTitle:NSLocalizedString(@"Fatto", @"Fatto") forState:UIControlStateNormal];
    [doneButton setTitle:NSLocalizedString(@"Fatto", @"Fatto") forState:UIControlStateHighlighted];
    [doneButton addTarget:self action:@selector(doneButton:) forControlEvents:UIControlEventTouchUpInside];
    UIWindow* tempWindow = [[[UIApplication sharedApplication] windows] objectAtIndex:1];
    [tempWindow addSubview:doneButton];
}


-(void)doneButton:(id)sender
{
    UIButton *btntmp=sender;
    [btntmp removeFromSuperview];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    //[self setViewMovedUp:NO];
    [self.textField resignFirstResponder];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        if ([_datePicker alpha] == 1.0) {
            [UIView animateWithDuration:0.25 animations:^{
                CGFloat height = 568.0;
                CGRect frame = self.tableView.frame;
                frame.size.height = height;
                self.tableView.frame = frame;
            }];
            [_datePicker setAlpha:0.0];

        }else if ([_pickerView alpha] == 1.0){
            [UIView animateWithDuration:0.25 animations:^{
                CGFloat height = 568.0;
                CGRect frame = self.tableView.frame;
                frame.size.height = height;
                self.tableView.frame = frame;
            }];
            [_pickerView setAlpha:0.0];
        }
        [_pickerViewData removeAllObjects];
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        indexCell = indexPath;
        switch (indexPath.row) {
            case 0:{
                NSLog(@"Ho cliccato la cella 0");
                
                [UIView animateWithDuration:0.25 animations:^{
                    CGFloat height = 300.0;
                    CGRect frame = self.tableView.frame;
                    frame.size.height = height;
                    self.tableView.frame = frame;
                }];
                [_datePicker setAlpha:1.0];
                [_datePicker setUserInteractionEnabled:YES];
            }
                break;
            case 1:{
                [_pickerViewData addObject:NSLocalizedString(@"Scegli l'obiettivo", @"Scegli l'obiettivo")];
                [_pickerViewData addObjectsFromArray:[[_dbManager selectFromTable:@"Target" Where:[NSString stringWithFormat:@"status != '9' AND periodCode == '%@'",[userDefault objectForKey:@"currentPeriodTarget"]] OrderBy:@"subject" isAscending:YES]mutableCopy]];
                
                [self.pickerView reloadAllComponents];
                [UIView animateWithDuration:0.25 animations:^{
                    CGFloat height = 307.0;
                    CGRect frame = self.tableView.frame;
                    frame.size.height = height;
                    self.tableView.frame = frame;
                }];
                [self.pickerView setAlpha:1.0];
                [_pickerView setUserInteractionEnabled:YES];
            }
                break;
            case 2:
                [self performSegueWithIdentifier:@"chooseCustomerIdentifier" sender:self];
                break;
            case 3:{
                [_pickerViewData addObject:@"N/A"];
                [_pickerViewData addObject:@"Tutta la giornata"];
                [_pickerViewData addObject:@"Mattina"];
                [_pickerViewData addObject:@"Pomeriggio"];
                [self.pickerView reloadAllComponents];
                [UIView animateWithDuration:0.25 animations:^{
                    CGFloat height = 307.0;
                    CGRect frame = self.tableView.frame;
                    frame.size.height = height;
                    self.tableView.frame = frame;
                }];
                [self.pickerView setAlpha:1.0];
                [_pickerView setUserInteractionEnabled:YES];
            }
                break;
            default:
                break;
        }
    }
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)setDateActivity{
    [listDetailActivity replaceObjectAtIndex:[indexCell row] withObject:[_utility getStringFromDate:[_datePicker date] withPattern:@"dd-MM-yyyy"]];
    [self.tableView reloadData];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return NSLocalizedString(@"Dettaglio Attività", @"Dettaglio Attività");
    }else{
        return NSLocalizedString(@"Obiettivo", @"Obiettivo");
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1) {
        return 80;
    }else{
        return 44;
    }
}

#pragma mark - UIPickerView DataSource Method
// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent: (NSInteger)component
{
    return [_pickerViewData count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (indexCell.row == 1) {
        if (row == 0) {
            return [_pickerViewData objectAtIndex:row];
        }else{
            return [[_pickerViewData objectAtIndex:row]valueForKey:@"subject"];
        }
    }else{
        return [_pickerViewData objectAtIndex:row];
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (indexCell.row == 1) {
        if (row == 0) {
            [listDetailActivity replaceObjectAtIndex:[indexCell row] withObject:[_pickerViewData objectAtIndex:row]];
        }else{
            [listDetailActivity replaceObjectAtIndex:[indexCell row] withObject:[[_pickerViewData objectAtIndex:row]valueForKey:@"subject"]];
            Target *targetChoosed = [_pickerViewData objectAtIndex:row];
            _listQuestionTarget = [[targetChoosed.questions allObjects]mutableCopy];
        }
    }else{
        [listDetailActivity replaceObjectAtIndex:[indexCell row] withObject:[_pickerViewData objectAtIndex:row]];
    }
    [self.tableView reloadData];
}


- (IBAction)saveAction:(id)sender {
    NSString *actionSheetTitle = NSLocalizedString(@"Salvataggio", @"Salvataggio"); //Action Sheet Title
    NSString *other1 = NSLocalizedString(@"Conferma", @"Conferma");
    NSString *other2 = NSLocalizedString(@"Conferma e apri nuova attività stesso negozio", @"Conferma e apri nuova stesso negozio");
    NSString *other3 = NSLocalizedString(@"Conferma e apri stessa attività su altro negozio", @"Conferma e apri stessa attività su altro negozio");
    NSString *cancelTitle = NSLocalizedString(@"Annulla", @"Annulla");
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:actionSheetTitle
                                  delegate:self
                                  cancelButtonTitle:cancelTitle
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:other1, other2, other3, nil];
    
    [actionSheet showInView:self.view];
}

- (IBAction)cancelAction:(id)sender {
    
    
}

- (void)changeSwitch:(id)sender{
    if([sender isOn]){
        // Execute any code when the switch is ON
        NSLog(@"Switch is ON");
    } else{
        // Execute any code when the switch is OFF
        NSLog(@"Switch is OFF");
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    //TODO:finire l'implementamento
    NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
    if ([buttonTitle isEqualToString:@"Conferma"]) {
        NSLog(@"confermo!");
    }
    if ([buttonTitle isEqualToString:@"Conferma e apri nuova attività stesso negozio"]) {
        NSLog(@"confermo e continuo su stesso negozio!");
    }
    if ([buttonTitle isEqualToString:@"Conferma e apri stessa attività su altro negozio"]) {
        NSLog(@"confermo e continuo su un'altro negozio!");
    }
}

- (IBAction)hidePickerView:(id)sender {
    if ([self.pickerView alpha] == 1.0) {
        [UIView animateWithDuration:0.25 animations:^{
            CGFloat height = 568.0;
            CGRect frame = self.tableView.frame;
            frame.size.height = height;
            self.tableView.frame = frame;
        }];
        [self.pickerView setAlpha:0.0];
        [self.pickerView setUserInteractionEnabled:NO];
    }
    if ([_datePicker alpha] == 1.0) {
        [UIView animateWithDuration:0.25 animations:^{
            CGFloat height = 568.0;
            CGRect frame = self.tableView.frame;
            frame.size.height = height;
            self.tableView.frame = frame;
        }];
        [_datePicker setAlpha:0.0];
        [_datePicker setUserInteractionEnabled:NO];
    }
}

- (BOOL)isDate:(NSDate *)date inRangeFirstDate:(NSDate *)firstDate lastDate:(NSDate *)lastDate {
    return [date compare:firstDate] == NSOrderedDescending && [date compare:lastDate] == NSOrderedAscending;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"chooseCustomerIdentifier"]) {
        UINavigationController *destination = (UINavigationController *)segue.destinationViewController;
    
        CustomerActivityViewController *listCustomerView = [[destination viewControllers]objectAtIndex:0];
        listCustomerView.delegate = self;
    }
}

- (void)getListCustomerSelected:(NSMutableArray *)arrayCustomers{
    NSString *listCustomer = @"";
    for (int i = 0; i<[arrayCustomers count]; i++) {
        NSManagedObject *customer = [arrayCustomers objectAtIndex:i];
        if (i != [arrayCustomers count]-1) {
            listCustomer =[listCustomer stringByAppendingString:[NSString stringWithFormat:@"%@, ",[customer valueForKey:@"name"]]];
        }else{
            listCustomer = [listCustomer stringByAppendingString:[NSString stringWithFormat:@"%@",[customer valueForKey:@"name"]]];
        }
    }
    [listDetailActivity replaceObjectAtIndex:[indexCell row] withObject:listCustomer];
    [self.tableView reloadData];
}

@end
