//
//  HomeViewController.h
//  iCanvass
//
//  Created by NAeS Lavoro on 06/02/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Utility;
@interface HomeViewController : UIViewController{
    Utility *utility;
}
@property (nonatomic, retain) Utility *utility;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *syncButton;
@property (weak, nonatomic) IBOutlet UILabel *welcomeLabel;

@property (weak, nonatomic) IBOutlet UILabel *currentRelease;
@property (weak, nonatomic) IBOutlet UILabel *mainTitle;
@property (weak, nonatomic) IBOutlet UIImageView *helpDeskIcon;

- (IBAction)syncAction:(id)sender;
- (IBAction)swipeLeft:(id)sender;

@end
