//
//  HomeViewController.m
//  iCanvass
//
//  Created by NAeS Lavoro on 06/02/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import "HomeViewController.h"
#import "AppDelegate.h"
#import "Utility.h"
#import "DataBaseManager.h"
#import "CustomersListTableViewController.h"

#import "ActivityHeader.h"
#import "ActivityAnswer.h"
#import "Target.h"

static NSString *helDeskBaseUrlWithLogin = @"https://www.naes.it/names.nsf?Login&Username=%@&Password=%@&redirectTo=desk/xdesk.nsf/home.xsp";
static NSString *helDeskBaseUrl = @"https://www.naes.it/desk/xdesk.nsf/home.xsp";

@interface HomeViewController ()
@property (nonatomic,strong) DataBaseManager *dbManager;
@end

@implementation HomeViewController
@synthesize utility = _utility, dbManager = _dbManager;


- (void)viewDidLoad
{
    [super viewDidLoad];
    UITabBarItem *tabItemHome = [[[self.tabBarController tabBar] items] objectAtIndex:0];
    UITabBarItem *tabItemTarget = [[[self.tabBarController tabBar] items] objectAtIndex:1];
    UITabBarItem *tabItemSurvey = [[[self.tabBarController tabBar] items] objectAtIndex:2];
    UITabBarItem *tabItemActivity = [[[self.tabBarController tabBar] items] objectAtIndex:3];
    UITabBarItem *tabItemResultActivity = [[[self.tabBarController tabBar] items] objectAtIndex:4];

    [tabItemHome setTitle:@"Home"];
    [tabItemTarget setTitle:@"Obiettivi"];
    [tabItemSurvey setTitle:@"Censimenti"];
    [tabItemActivity setTitle:@"Attività"];
    [tabItemResultActivity setTitle:@"Risultati"];

    [self.navigationItem setTitle:NSLocalizedString(@"Home", @"Titolo navigationBar home page")];
    [self.syncButton setTitle:NSLocalizedString(@"Sinc", @"Sinc")];
    
    
    UITapGestureRecognizer* recognizer;
    recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openHelpDesk)];
    self.helpDeskIcon.userInteractionEnabled = YES;
    [self.helpDeskIcon addGestureRecognizer:recognizer];
    

    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *majorVersion = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    NSString *minorVersion = [infoDictionary objectForKey:@"CFBundleVersion"];

    self.currentRelease.text = [NSString stringWithFormat:@"%@ %@ (%@ %@)",NSLocalizedString(@"Versione", @"Versione"),majorVersion, NSLocalizedString(@"build", @"build"), minorVersion];
    
    self.mainTitle.text = NSLocalizedString(@"iCanvass", @"iCanvass");

    _utility = [[Utility alloc]init];
    _dbManager = [[DataBaseManager alloc] init];

    NSString *propertyPassword;
    NSString *propertyUserName;
    //commentando questa riga si forza l'utente ad inserire sempre la password
    propertyPassword = [_dbManager getValueFromSettingsByPropertyName:@"Password"];
    //se si commenta ANCHE la seguente, si forza l'inserimento ANCHE dello user name
    propertyUserName = [_dbManager getValueFromSettingsByPropertyName:@"UserName"];

    if (propertyUserName == nil || propertyPassword == nil) {
        [self performSegueWithIdentifier:@"LoginRoyalDays" sender:self];
    }
    
    //questo pezzo va sistemato utilizzando un valore ("sex") memorizzato nelle preferenze locali e prelevato dal server (da scheda merchandiser) tramite WS "preferenze utente", da scrivere. Questo WS dovrà trasmettere anche opzioni tipo "abilita inserimento quantità in censimenti royal canin / concorrenza"
    NSMutableArray *nameArray = (NSMutableArray *)[propertyUserName componentsSeparatedByString:@" "];
    [nameArray removeObject:@""];
    NSString *lastChar = [[nameArray objectAtIndex:0] substringFromIndex:[[nameArray objectAtIndex:0] length] - 1];
    if([lastChar isEqualToString:@"a"] || [lastChar isEqualToString:@"e"]){
        self.welcomeLabel.text = [NSString stringWithFormat:@"Benvenuta %@",[nameArray objectAtIndex:0]];
    }else{
        self.welcomeLabel.text = [NSString stringWithFormat:@"Benvenuto %@",[nameArray objectAtIndex:0]];
    }
    
}


- (void)viewDidAppear:(BOOL)animated
{
    NSString *username = [_dbManager getValueFromSettingsByPropertyName:@"UserName"];
    NSString *password = [_dbManager getValueFromSettingsByPropertyName:@"Password"];
    if (username == nil || password == nil) {
        [self performSegueWithIdentifier:@"LoginRoyalDays" sender:self];
    }else{
        //questo pezzo va sistemato utilizzando un valore ("sex") memorizzato nelle preferenze locali e prelevato dal server (da scheda merchandiser) tramite WS "preferenze utente", da scrivere. Questo WS dovrà trasmettere anche opzioni tipo "abilita inserimento quantità in censimenti royal canin / concorrenza"
        NSMutableArray *nameArray = (NSMutableArray *)[username componentsSeparatedByString:@" "];
        [nameArray removeObject:@""];
        NSString *lastChar = [[nameArray objectAtIndex:0] substringFromIndex:[[nameArray objectAtIndex:0] length] - 1];
        if([lastChar isEqualToString:@"a"] || [lastChar isEqualToString:@"e"]){
            self.welcomeLabel.text = [NSString stringWithFormat:@"Benvenuta %@",[nameArray objectAtIndex:0]];
        }else{
            self.welcomeLabel.text = [NSString stringWithFormat:@"Benvenuto %@",[nameArray objectAtIndex:0]];
        }
        
        //[self showEntitySummary];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)syncAction:(id)sender {
    [self performSegueWithIdentifier:@"SyncPageIdentifier" sender:sender];
}


- (IBAction)swipeLeft:(id)sender {
    [self performSegueWithIdentifier:@"ElencoPuntiVendita" sender:self];
}


- (void)openHelpDesk{
    
    //se trovo user e password correnti memorizzati nelle preferenze, effettuo login
    NSUserDefaults *userPreference = [[NSUserDefaults alloc] init];
    NSMutableString *currentUser = [userPreference valueForKey:@"currentUser"];
    NSMutableString *currentPassword = [userPreference valueForKey:@"currentPassword"];
    
    if (currentUser != nil && currentPassword != nil) {
        currentUser = [[currentUser stringByReplacingOccurrencesOfString:@" " withString:@"%20"] mutableCopy];
        currentPassword = [[currentPassword stringByReplacingOccurrencesOfString:@" " withString:@"%20"] mutableCopy];
        
        NSLog(@"loginAction - currentUser: %@, currentPassword: %@", currentUser, currentPassword);
        
        NSString *myUrl = [NSString stringWithFormat:helDeskBaseUrlWithLogin, currentUser, currentPassword];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:myUrl]];
    }else{
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:helDeskBaseUrl]];
    }
    
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    UIViewController *destination;
    if ([[segue identifier] isEqualToString:@"LoginRoyalDays"]){
        destination = segue.destinationViewController;
    }else if ([[segue identifier] isEqualToString:@"ElencoPuntiVendita"]) {
        CustomersListTableViewController *destinazione = segue.destinationViewController;
    }
}

@end
