//
//  LoginViewController.h
//  iCanvass
//
//  Created by NAeS Lavoro on 14/02/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DataBaseManager;
@class Utility;
@interface LoginViewController : UIViewController<UIAlertViewDelegate, UITextFieldDelegate>{
    NSString *nomeUtente;
    NSString *password;
    Utility *utility;
    BOOL isOccuredAuthenticationError;
}

@property (nonatomic, copy) NSString *nomeUtente;
@property (nonatomic, copy) NSString *password;
@property (nonatomic, retain)DataBaseManager *databaseMaker;
@property (nonatomic, assign) BOOL isOccuredAuthenticationError;

@property (weak, nonatomic) IBOutlet UILabel *loginLabel;
@property (weak, nonatomic) IBOutlet UITextField *userNameField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UIButton *logInButton;


- (IBAction)logInAction:(id)sender;

@end
