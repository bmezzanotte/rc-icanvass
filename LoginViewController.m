//
//  LoginViewController.m
//  iCanvass
//
//  Created by NAeS Lavoro on 14/02/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import "LoginViewController.h"
#import "DataBaseManager.h"
#import "Utility.h"

@interface LoginViewController ()

@end

@implementation LoginViewController
@synthesize nomeUtente = _nomeUtente, password = _password, userNameField, passwordField;
@synthesize databaseMaker = _databaseMaker, isOccuredAuthenticationError = _isOccuredAuthenticationError;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	_nomeUtente = @"";
    _password = @"";
    
    
    NSUserDefaults *userPreference = [[NSUserDefaults alloc] init];
    NSString *currentUser = [userPreference valueForKey:@"currentUser"];
    if (currentUser != nil) {
        self.userNameField.text = currentUser;
    }
    
    [self userInterfaceSettings];
    
    _databaseMaker = [[DataBaseManager alloc]init];
    
    utility = [[Utility alloc]init];
    if (_isOccuredAuthenticationError) {
        NSUserDefaults *userPreference = [[NSUserDefaults alloc] init];
        [userPreference removeObjectForKey:@"currentUser"];
        [userPreference removeObjectForKey:@"currentPassword"];
        
        [self showWarningAuthenticationMessege];
    }
}

- (void)userInterfaceSettings{
    self.navigationController.title = NSLocalizedString(@"Login", @"Titolo nella navigation bar");
    
    self.loginLabel.text = NSLocalizedString(@"Inserisci nome utente e password", @"Etichetta che compare nella schermata di login");
    
    self.userNameField.delegate = self;
    [self.userNameField setAutocapitalizationType:UITextAutocapitalizationTypeWords];
    self.userNameField.placeholder = NSLocalizedString(@"Nome utente", @"Placeholder del campo username nella schermata di login");
    
    self.passwordField.placeholder = NSLocalizedString(@"Password", @"Placeholder del campo password nella schermata di login");
    self.passwordField.delegate = self;
    
    NSString *loginButtonLabel = NSLocalizedString(@"Log In", @"Etichetta del pulsante della schermata di login");
    [self.logInButton setTitle:loginButtonLabel forState:UIControlStateApplication];
    [self.logInButton setTitle:loginButtonLabel forState:UIControlStateDisabled];
    [self.logInButton setTitle:loginButtonLabel forState:UIControlStateHighlighted];
    [self.logInButton setTitle:loginButtonLabel forState:UIControlStateNormal];
    [self.logInButton setTitle:loginButtonLabel forState:UIControlStateReserved];
    [self.logInButton setTitle:loginButtonLabel forState:UIControlStateSelected];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)logInAction:(id)sender {
    BOOL authenticated = NO;
    NSString *userNameInserito = self.userNameField.text;
    NSString *passwordInserita = self.passwordField.text;
    
    //se trovo user e password correnti memorizzati nelle preferenze, effettuo un login locale
    NSUserDefaults *userPreference = [[NSUserDefaults alloc] init];
    NSString *currentUser = [userPreference valueForKey:@"currentUser"];
    NSString *currentPassword = [userPreference valueForKey:@"currentPassword"];
    NSString *cookie;
    
    NSLog(@"loginAction - currentUser: %@, currentPassword: %@", currentUser, currentPassword);
    if (currentUser != nil && currentPassword != nil) {
        NSLog(@"autenticazione locale");
        NSLog(@"username: %@, password: %@", userNameInserito, passwordInserita);
        if ([userNameInserito isEqualToString:currentUser] && [passwordInserita isEqualToString:currentPassword]) {
            NSLog(@"RIUSCITA");
            authenticated = YES;
        }else{
            NSLog(@"FALLITA");
        }
    }else{
        NSLog(@"autenticazione online");
        cookie = [utility autenticazioneConDominoConNomeUtente:userNameInserito Password:passwordInserita];
        if (cookie != nil) {
            NSLog(@"RIUSCITA");
            authenticated = YES;
            
//            [userPreference setObject:userNameInserito forKey:@"currentUser"];
//            [userPreference setObject:passwordInserita forKey:@"currentPassword"];
        }else{
            NSLog(@"FALLITA");
        }
    }
    
    if (authenticated != YES) {
        UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Attenzione!", @"Titolo dell'alert view quando si sbagli il login") message:NSLocalizedString(@"Errore di autenticazione. Riprova", @"Testo dell'alert view quando si sbaglia il login") delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertView show];
        [self.passwordField setText:@""];
        [self.userNameField setText:@""];
    }else{
        
        /*
        //memorizzo temporaneamente nelle preferenze la chiave che serve per disattivare l'inserimento delle quantità nei censimenti
        //queste preferenze andranno impostate da WS getPReferences - ancora da scrivere ed implementare
        [userPreference setObject:@"0" forKey:@"quantityInSurvey_Royal"];
        [userPreference setObject:@"0" forKey:@"quantityInSurvey_Competitor"];
        
        [userPreference setObject:@"1" forKey:@"quantityInRoyalDay"];
        [userPreference setObject:@"1" forKey:@"quantityInRoyalDay_Hostess"];
        
        [userPreference setObject:@"R14G" forKey:@"customerDataSheetPrefixCat"];
        [userPreference setObject:@"R14C" forKey:@"customerDataSheetPrefixDog"];
        //queste preferenze andranno impostate da WS getPReferences - ancora da scrivere ed implementare
        */
        
        
        [_databaseMaker updateOrInsertSettingWithPropertyName:@"UserName" AndPropertyValue:userNameInserito];
        [_databaseMaker updateOrInsertSettingWithPropertyName:@"Password" AndPropertyValue:passwordInserita];
        if (cookie != nil) {
            [_databaseMaker updateOrInsertSettingWithPropertyName:@"NomeCookie" AndPropertyValue:[utility searchPropertyWithName:@"NomeCookie"]];
            [_databaseMaker updateOrInsertSettingWithPropertyName:@"LoginURL" AndPropertyValue:[utility searchPropertyWithName:@"LoginURL"]];
        }
        
        //memorizzo nelle preferenze utente una copia di nome utente e password, in modo da permettere successivamente il login locale (in caso di logout)
        [userPreference setObject:userNameInserito forKey:@"currentUser"];
        [userPreference setObject:passwordInserita forKey:@"currentPassword"];
        
        [self.passwordField setText:@""];
        [self.userNameField setText:@""];
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

#pragma mark - TextFieldDelegate Method
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    UIToolbar *toolbar = [[UIToolbar alloc] init];
    toolbar.frame = CGRectMake(0, 0, self.view.frame.size.width, 44.0);
    toolbar.barStyle = UIBarStyleDefault;
    
    // creiamo il bottone per terminare l'inserimento
    UIBarButtonItem *prevButton = [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Prec.", @"Etichetta che compare nella tool bar sopra la tastiera") style:UIBarButtonItemStyleBordered target:self action:@selector(prevButtonAction)];
    UIBarButtonItem *nextButton = [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Succ.", @"Etichetta che compare nella tool bar sopra la tastiera") style:UIBarButtonItemStyleBordered target:self action:@selector(nextButtonAction)];
    UIBarButtonItem *spaceButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil]; // spazio per mantenere a dx il bottone Fine
    
    NSMutableArray *items = [[NSMutableArray alloc] initWithObjects:prevButton, nextButton, spaceButton, nil];
    toolbar.items = items;
    self.userNameField.inputAccessoryView = toolbar;
    self.passwordField.inputAccessoryView = toolbar;
    return YES;
}

- (void)prevButtonAction{
    if ([self.passwordField isFirstResponder]) {
        [self.userNameField becomeFirstResponder];
    }
}

- (void)nextButtonAction{
    if ([self.userNameField isFirstResponder]) {
        [self.passwordField becomeFirstResponder];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    //[self logInAction:self];
    return YES;
}

#pragma mark - AlertView Method
- (void)showWarningAuthenticationMessege{
    UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Attenzione!", @"Titolo dell'alert view quando si sbagli il login") message:NSLocalizedString(@"Errore nell'autenticazione. Riprova", @"Testo dell'alert view quando si sbaglia il login") delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    [alertView show];
}

@end
