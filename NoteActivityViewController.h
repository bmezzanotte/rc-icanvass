//
//  NoteActivityViewController.h
//  iCanvass
//
//  Created by NAeS Lavoro on 14/04/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NoteActivityDelegate <NSObject>

- (void)getNoteInput:(NSString *)noteContent;
- (NSString *)getNote;

@end

@interface NoteActivityViewController : UIViewController {
        __weak id delegate;
}

@property (weak, nonatomic) id<NoteActivityDelegate> delegate;

@property (nonatomic) int activityStatus;

@property (weak, nonatomic) IBOutlet UITextView *noteActivity;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *saveButton;
- (IBAction)saveAction:(id)sender;
@end
