//
//  NoteActivityViewController.m
//  iCanvass
//
//  Created by NAeS Lavoro on 14/04/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import "NoteActivityViewController.h"
#import "AppDelegate.h"
#import "Activity.h"

@interface NoteActivityViewController ()

@end

@implementation NoteActivityViewController
@synthesize delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self.noteActivity setText:[self.delegate getNote]];

    if (self.activityStatus > 1) {
        self.navigationItem.title = NSLocalizedString(@"Nota", @"Nota");
        [self.saveButton setTitle:NSLocalizedString(@"", @"")];
        [self.saveButton setEnabled:NO];
        [self.noteActivity setEditable:NO];
    }else{
        self.navigationItem.title = NSLocalizedString(@"Scrivi Nota", @"Scrivi Nota");
        [self.saveButton setTitle:NSLocalizedString(@"Salva", @"Salva")];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)saveAction:(id)sender {
    
    NSString *noteContent = self.noteActivity.text;
    NSLog(@"nota:%@",noteContent);

    if ([self.delegate respondsToSelector:@selector(getNoteInput:)]) {
        [self.delegate getNoteInput:self.noteActivity.text];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
    
}
@end
