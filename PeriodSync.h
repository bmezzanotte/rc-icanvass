//
//  PeriodSync.h
//  iCanvass
//
//  Created by Bernardino on 18/02/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebServiceClient.h"

@class DataBaseManager;

@interface PeriodSync : NSObject<NSXMLParserDelegate>{
    NSDictionary *tagToPropertyMapping;
    NSString *xmlTag;
    NSMutableDictionary *oggettoLista;
    NSMutableArray *listaOggetti;
    NSMutableArray *unidArray;
}

@property (nonatomic, retain) DataBaseManager *dbManager;
@property (nonatomic, strong) NSData *wsResponseData;
@property (nonatomic, weak) NSString *syncSessionStarted;
@property (nonatomic, weak) NSString *syncSessionEnded;
@property (nonatomic) RDSyncErrorStatus syncResultErrorStatus;

@property (nonatomic) int newPeriods;
@property (nonatomic) int updatedPeriods;
@property (nonatomic) int deletedPeriods;

- (id)initWithToken:(NSString *)cookieToken;
- (void)syncTable;
- (NSMutableArray *)propertyArray;


@end
