//
//  PickListViewController.h
//  iCanvass
//
//  Created by Bernardino on 02/04/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PickListDelegate <NSObject>

- (void)getSelectedItems:(NSMutableArray *)selectedItems;

@end

@interface PickListViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) id<PickListDelegate> delegate;
@property (nonatomic, retain) NSMutableArray *selectedItems;

@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, strong) NSString *pickerTitle;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
