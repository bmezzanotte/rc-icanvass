//
//  PickListViewController.m
//  iCanvass
//
//  Created by Bernardino on 02/04/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import "PickListViewController.h"

@interface PickListViewController ()
@property (weak, nonatomic) IBOutlet UIBarButtonItem *saveButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cancelButton;

- (IBAction)saveAction:(id)sender;
- (IBAction)cancelAction:(id)sender;


@end

@implementation PickListViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.saveButton.title = NSLocalizedString(@"Conferma", @"Conferma");
    
    //nascondo il pulsante conferma, perché ho impostato la chiusura automatica della finestra nel momento in cui si seleziona una cella
    self.saveButton.enabled = false;
    self.saveButton.title = @"";
    
    self.cancelButton.title = NSLocalizedString(@"Annulla", @"Annulla");
    
//    NSLog(@"valore selezionato:%@",self.selectedValue);
    
    for (int i = 0; i < [self.dataArray count]; i++) {
        for (NSString *key in self.selectedItems) {
            if ([key isEqualToString:[self.dataArray objectAtIndex:i]]) {
                [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0] animated:YES scrollPosition:UITableViewScrollPositionTop];
            }

        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger numRows = 0;
    numRows = self.dataArray.count;
    
    return numRows;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return self.pickerTitle;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 66;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 66;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"BasicCellIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"BasicCellIdentifier"];
    }
    cell.textLabel.numberOfLines = 2;
    cell.textLabel.font = [UIFont systemFontOfSize:12.0];
    cell.textLabel.text = [self.dataArray objectAtIndex:indexPath.row];
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *selectedItem = [self.dataArray objectAtIndex:indexPath.row];
    self.selectedItems = [[NSMutableArray alloc] init];
    [self.selectedItems addObject:selectedItem];
    
    //con queste righe provoco salvataggio dato e chiusura finestra appena cliccata la cella
    if ([self.delegate respondsToSelector:@selector(getSelectedItems:)]) {
        [self.delegate getSelectedItems:self.selectedItems];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)saveAction:(id)sender {
    if ([self.delegate respondsToSelector:@selector(getSelectedItems:)]) {
        [self.delegate getSelectedItems:self.selectedItems];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)cancelAction:(id)sender {
     [self dismissViewControllerAnimated:YES completion:nil];
}

@end
