//
//  RDDescriptionTextCell.h
//  iCanvass
//
//  Created by Bernardino on 22/02/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RDDescriptionTextCell : UITableViewCell
@property (nonatomic,strong) UILabel *fieldValueLabel;
@property (nonatomic,strong) NSString *key;
@property (nonatomic,strong) id value;
@end
