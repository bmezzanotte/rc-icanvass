//
//  RDDescriptionTextCell.m
//  iCanvass
//
//  Created by Bernardino on 22/02/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import "RDDescriptionTextCell.h"

#define kLabelTextColor [UIColor colorWithRed:0.321569f green:0.4f blue:0.568627f alpha:1.0f]

@implementation RDDescriptionTextCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.fieldValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(12.0, 15.0, 295.0, 88.0)];
        self.fieldValueLabel.backgroundColor = [UIColor clearColor];
        self.fieldValueLabel.font = [UIFont boldSystemFontOfSize:[UIFont systemFontSize]];
        self.fieldValueLabel.textAlignment = NSTextAlignmentLeft;
        self.fieldValueLabel.textColor = kLabelTextColor;
        self.fieldValueLabel.text = @"valueLabel";
        //      self.fieldValueLabel.layer.backgroundColor = [UIColor redColor].CGColor;
        [self.contentView addSubview:self.fieldValueLabel];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
