//
//  RDDetailRadioCell.h
//  iRoyalDays
//
//  Created by Bernardino on 26/02/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RDDetailRadioCell : UITableViewCell
@property (nonatomic,strong) UILabel *label;
@property (nonatomic,strong) UILabel *fieldValueLabel;
//@property (nonatomic,strong) UITextField *textField;
@property (nonatomic,strong) NSString *key;
@property (nonatomic,strong) id value;
@end
