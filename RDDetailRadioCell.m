//
//  RDDetailRadioCell.m
//  iRoyalDays
//
//  Created by Bernardino on 26/02/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import "RDDetailRadioCell.h"

#define kLabelTextColor [UIColor colorWithRed:0.321569f green:0.4f blue:0.568627f alpha:1.0f]

@implementation RDDetailRadioCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.label = [[UILabel alloc] initWithFrame:CGRectMake(12.0, 15.0, 75.0, 15.0)];
        self.label.backgroundColor = [UIColor clearColor];
        self.label.font = [UIFont boldSystemFontOfSize:[UIFont smallSystemFontSize]];
        self.label.textAlignment = NSTextAlignmentLeft;
        self.label.textColor = kLabelTextColor;
        self.label.text = @"label";
        //        self.label.layer.backgroundColor = [UIColor greenColor].CGColor;
        [self.contentView addSubview:self.label];
        
        self.fieldValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(93.0, 13.0, 220.0, 19.0)];
        self.fieldValueLabel.backgroundColor = [UIColor clearColor];
        self.fieldValueLabel.font = [UIFont boldSystemFontOfSize:[UIFont systemFontSize]];
        self.fieldValueLabel.textAlignment = NSTextAlignmentLeft;
        self.fieldValueLabel.textColor = kLabelTextColor;
        self.fieldValueLabel.text = @"valueLabel";
        //      self.fieldValueLabel.layer.backgroundColor = [UIColor redColor].CGColor;
        [self.contentView addSubview:self.fieldValueLabel];
        
        /*
         self.textField = [[UITextField alloc] initWithFrame:CGRectMake(93.0, 13.0, 220.0, 19.0)];
         //        self.textField.borderStyle = UITextBorderStyleLine;
         self.textField.backgroundColor = [UIColor clearColor];
         self.textField.clearButtonMode = UITextFieldViewModeWhileEditing;
         self.textField.enabled = NO;
         self.textField.font = [UIFont boldSystemFontOfSize:[UIFont systemFontSize]];
         self.textField.text = @"Title";
         [self.contentView addSubview:self.textField];
         */
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
/*
 - (void)setEditing:(BOOL)editing animated:(BOOL)animated{
 [super setEditing:editing animated:animated];
 self.textField.enabled = editing;
 }
 */

/*
 #pragma mark - Property Overrides
 - (id)value{
 return self.textField.text;
 }
 
 - (void)setValue:(id)aValue{
 self.textField.text = aValue;
 }
 */
@end
