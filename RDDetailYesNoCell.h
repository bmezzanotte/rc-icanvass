//
//  RDDetailYesNoCell.h
//  iRoyalDays
//
//  Created by Bernardino on 04/01/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RDDetailYesNoCell : UITableViewCell
@property (nonatomic,strong) UILabel *label;
@property (nonatomic,strong) UILabel *fieldValueLabel;
//@property (nonatomic,strong) UITextField *textField;
@property (nonatomic,strong) NSString *key;
@property (nonatomic,strong) id value;
@end
