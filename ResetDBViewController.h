//
//  ResetDBViewController.h
//  iCanvass
//
//  Created by NAeS Lavoro on 14/02/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ResetDbDelegate <NSObject>
- (void)setNotSynchronized;
- (void)firstViewControllerDismissed;
@end

@interface ResetDBViewController : UIViewController<UITableViewDataSource, UITableViewDelegate,UIAlertViewDelegate>{
    __weak id delegate;
}
@property (weak, nonatomic) id<ResetDbDelegate>delegate;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *cancelButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *resetDbButton;
- (IBAction)cancelAction:(id)sender;
- (IBAction)resetDB:(id)sender;
@end
