//
//  ResetDBViewController.m
//  iCanvass
//
//  Created by NAeS Lavoro on 14/02/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import "ResetDBViewController.h"
#import "DataBaseManager.h"
#import "Utility.h"


@interface ResetDBViewController ()
@property NSArray *dbEntities;
@property NSMutableArray *selectedEntities;

@end

@implementation ResetDBViewController
@synthesize delegate = _delegate, dbEntities = _dbEntities, selectedEntities = _selectedEntities;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.cancelButton.title = NSLocalizedString(@"Annulla", @"Annulla");
    self.resetDbButton.title = NSLocalizedString(@"Reset DB", @"Reset DB");
    
    _dbEntities = [NSArray arrayWithObjects:
                   NSLocalizedString(@"Punti Vendita",@"Punti Vendita"),
                   NSLocalizedString(@"Periodi",@"Periodi"),
                   NSLocalizedString(@"Obiettivi",@"Obiettivi"),
                   NSLocalizedString(@"Attività",@"Attività"),
                   NSLocalizedString(@"Censimenti",@"Censimenti"),
                   NSLocalizedString(@"Preferenze utente",@"Preferenze utente"),
                   NSLocalizedString(@"Reset utente",@"Reset utente"),
                   nil];
    
    _selectedEntities = [[NSMutableArray alloc] init];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSouce Property
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _dbEntities.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return @"Seleziona le tabelle da cancellare";
    }else{
        return nil;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.textLabel.text = [_dbEntities objectAtIndex:indexPath.row];
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSUInteger index = [[tableView indexPathsForVisibleRows] indexOfObject:indexPath];
    if (index != NSNotFound) {
        UITableViewCell *cell = [[tableView visibleCells] objectAtIndex:index];
        if ([cell accessoryType] == UITableViewCellAccessoryNone) {
            [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
            [_selectedEntities addObject:[NSString stringWithFormat:@"%d",(int)index]];
        } else {
            [cell setAccessoryType:UITableViewCellAccessoryNone];
            [_selectedEntities removeObject:[NSString stringWithFormat:@"%d",(int)index]];
        }
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSUInteger index = [[tableView indexPathsForVisibleRows] indexOfObject:indexPath];
    if (index != NSNotFound) {
        UITableViewCell *cell = [[tableView visibleCells] objectAtIndex:index];
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        [_selectedEntities removeObject:[NSString stringWithFormat:@"%d",(int)index]];
    }
}

- (IBAction)cancelAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)resetDB:(id)sender {
    
    UIAlertView *alertView = [UIAlertView alloc];
    NSString *alertTitle = NSLocalizedString(@"Attenzione!", @"Attenzione!");
    NSString *alertMessage = NSLocalizedString(@"Vuoi veramente cancellare tutti i dati dalle tabelle selezionate? Eventuali dati non sincronizzati con il server non potranno essere recuperati", @"ResetDB Alert message!") ;
    NSString *cancelButtonTitle = NSLocalizedString(@"No", @"No");
    NSString *confirmButtonTitle = NSLocalizedString(@"Si", @"Si");
    alertView = [alertView initWithTitle:alertTitle message:alertMessage delegate:self cancelButtonTitle:cancelButtonTitle otherButtonTitles:confirmButtonTitle, nil];
    [alertView show];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        //wipe DB Selected Entities
        DataBaseManager *dbManager = [[DataBaseManager alloc]init];
        
        NSString *username = [dbManager getValueFromSettingsByPropertyName:@"UserName"];
        NSString *password = [dbManager getValueFromSettingsByPropertyName:@"Password"];
        
        NSUserDefaults *userPreference = [[NSUserDefaults alloc] init];
        
        if ([_selectedEntities containsObject:@"6"]) {
            _selectedEntities = [[NSMutableArray alloc] initWithObjects:@"0",@"1",@"2",@"3",@"4",@"6",nil];
            [NSUserDefaults resetStandardUserDefaults];
            [userPreference removeObjectForKey:@"currentUser"];
            [userPreference removeObjectForKey:@"currentPassword"];
        }
        
        for (NSString *entity in _selectedEntities){
            NSLog(@"key = %@",entity);
            switch ([entity intValue]) {
                case 0:
                    [dbManager deleteAllItemsForTable:@"Customer"];
                    [dbManager updateOrInsertSettingWithPropertyName:@"lastSyncForCustomers" AndPropertyValue:@"0000/00/00 00.00.00.000"];

                    //se cancello i punti vendita, devo cancellare anche le attività. Altrimenti, viene persa la relationship
                    [dbManager deleteAllItemsForTable:@"ActivityHeader"];
                    [dbManager updateOrInsertSettingWithPropertyName:@"lastSyncForActivity" AndPropertyValue:@"0000/00/00 00.00.00.000"];

                    //se cancello i punti vendita, devo cancellare anche i censimenti. Altrimenti, viene persa la relationship
                    [dbManager deleteAllItemsForTable:@"SurveyHeader"];
                    [dbManager updateOrInsertSettingWithPropertyName:@"lastSyncForSurvey" AndPropertyValue:@"0000/00/00 00.00.00.000"];

                    break;

                case 1:
                    [dbManager deleteAllItemsForTable:@"Period"];
                    [dbManager updateOrInsertSettingWithPropertyName:@"lastSyncForPeriod" AndPropertyValue:@"0000/00/00 00.00.00.000"];
                    break;
                
                case 2:
                    [dbManager deleteAllItemsForTable:@"Target"];
                    [dbManager updateOrInsertSettingWithPropertyName:@"lastSyncForTarget" AndPropertyValue:@"0000/00/00 00.00.00.000"];

                    //se cancello gli obiettivi, devo cancellare anche le attività. Altrimenti, viene persa la relationship
                    [dbManager deleteAllItemsForTable:@"ActivityHeader"];
                    [dbManager updateOrInsertSettingWithPropertyName:@"lastSyncForActivity" AndPropertyValue:@"0000/00/00 00.00.00.000"];

                    break;

                case 3:
                    [dbManager deleteAllItemsForTable:@"ActivityHeader"];
                    [dbManager updateOrInsertSettingWithPropertyName:@"lastSyncForActivity" AndPropertyValue:@"0000/00/00 00.00.00.000"];
                    break;

                case 4:
                    [dbManager deleteAllItemsForTable:@"SurveyHeader"];
                    [dbManager updateOrInsertSettingWithPropertyName:@"lastSyncForSurvey" AndPropertyValue:@"0000/00/00 00.00.00.000"];
                    break;

                case 5:
                    [dbManager deleteAllItemsForTable:@"Settings"];
                    [dbManager insertNewSettingWithPropertyName:@"UserName" AndPropertyValue:username];
                    [dbManager insertNewSettingWithPropertyName:@"Password" AndPropertyValue:password];
                    
                    [self deleteSurveyDefinitionPlist];
                    [dbManager updateOrInsertSettingWithPropertyName:@"lastSyncForSurveyDefinition" AndPropertyValue:@"0000/00/00 00.00.00.000"];
                    break;
                    
                case 6:
                    [dbManager deleteAllItemsForTable:@"Settings"];

                    [self deleteSurveyDefinitionPlist];
                    [dbManager updateOrInsertSettingWithPropertyName:@"lastSyncForSurveyDefinition" AndPropertyValue:@"0000/00/00 00.00.00.000"];
                    break;
                    
                default:
                    break;
            }
        }
        
        [self.delegate setNotSynchronized];
        
        [self dismissViewControllerAnimated:YES completion:nil];
        
        if (([self.delegate respondsToSelector:@selector(firstViewControllerDismissed)])) {
            [self.delegate firstViewControllerDismissed];
        }
    }
}

- (void) deleteSurveyDefinitionPlist{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    BOOL success = false;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString  *documentsDirectory = [paths objectAtIndex:0];
    NSString  *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,@"SurveyDetailTableConfiguration.plist"];
    
    success = [fileManager removeItemAtPath:filePath error:&error];
    if (success) {
        NSLog(@"File successfully deleted");
    }else{
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
}
@end
