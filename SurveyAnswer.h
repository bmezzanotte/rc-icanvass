//
//  SurveyAnswer.h
//  iCanvass
//
//  Created by Bernardino on 26/12/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class SurveyHeader;

@interface SurveyAnswer : NSManagedObject

@property (nonatomic, retain) NSString * subject;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * unid;
@property (nonatomic, retain) NSString * value;
@property (nonatomic, retain) SurveyHeader *header;

@end
