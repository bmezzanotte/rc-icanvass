//
//  SurveyAnswer.m
//  iCanvass
//
//  Created by Bernardino on 26/12/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import "SurveyAnswer.h"
#import "SurveyHeader.h"


@implementation SurveyAnswer

@dynamic subject;
@dynamic type;
@dynamic unid;
@dynamic value;
@dynamic header;

@end
