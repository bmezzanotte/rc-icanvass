//
//  SurveyDefinitionSync.h
//  iCanvass
//
//  Created by Bernardino on 09/01/15.
//  Copyright (c) 2015 NAeS Lavoro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebServiceClient.h"

@class DataBaseManager;

@interface SurveyDefinitionSync : NSObject<NSXMLParserDelegate>{
    NSMutableArray *unidArray;
    
    NSMutableString *xmlBranch;
    NSMutableString *xmlTag;
    NSMutableDictionary *headerElements;
}


@property (nonatomic, retain) DataBaseManager *creaDB;
@property (nonatomic, strong) NSData *wsResponseData;
@property (nonatomic, weak) NSString *syncSessionStarted;
@property (nonatomic, weak) NSString *syncSessionEnded;
@property (nonatomic) RDSyncErrorStatus syncResultErrorStatus;

- (id)initWithToken:(NSString *)cookieToken;
- (void)syncTable;
@end
