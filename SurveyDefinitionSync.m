//
//  SurveyDefinitionSync.m
//  iCanvass
//
//  Created by Bernardino on 09/01/15.
//  Copyright (c) 2015 NAeS Lavoro. All rights reserved.
//

#import "SurveyDefinitionSync.h"
#import "DataBaseManager.h"
#import "Utility.h"

static NSString *TIME_STAMP_FOR_ENTITY = @"lastSyncForSurveyDefinition";
static NSString *WS_NAME = @"GetMasterDataXML";
static NSString *METHOD_NAME = @"GETSURVEYDEF";

@interface SurveyDefinitionSync()
@property Utility *util;
@property (nonatomic, strong) NSString *urlForFileDownload;

@property (nonatomic, strong) NSString *token;
@end

@implementation SurveyDefinitionSync

- (id)initWithToken:(NSString *)cookieToken{
    if (self = [super init]) {
        
        
        self.token = cookieToken;
        
        
        _util = [[Utility alloc] init];
        _syncSessionStarted = [_util getCurrentDateAsString];
        _creaDB = [[DataBaseManager alloc]init];
        
        unidArray = [[NSMutableArray alloc] init];
        
        WebServiceClient *surveyDefinitionGET = [[WebServiceClient alloc] initWithWsName:WS_NAME withMethodName:METHOD_NAME withTimeStamp:[self timeStampForEntity:TIME_STAMP_FOR_ENTITY] withArrayOfUniversalID:unidArray];
        
        _wsResponseData = [surveyDefinitionGET getWebServiceResponseWithToken:cookieToken];
        
        NSInteger httpResponseStatusCode = [surveyDefinitionGET.httpResponse statusCode];
        NSLog(@"http response status code: %ld",(long)httpResponseStatusCode);
        if (httpResponseStatusCode != 200) {
            NSLog(@"Errore WS HTTP error: %ld",(long)httpResponseStatusCode);
            //devo interrompere completamente la sincronizzazione e mostrare a video un messaggio d'errore
            self.syncResultErrorStatus = RDSyncErrorStatusServerError;
        }else{
            if (_wsResponseData == nil){
                NSLog(@"Errore WS: ResponseData VUOTO");
                //devo interrompere soltanto la sincronizzazione di questa tabella e, a fine sincronizzazione, mostrare KO in riepilogo
                self.syncResultErrorStatus = RDSyncErrorStatusNullStringResult;
            }else{
                //proseguo con il parsing XML della risposta
                self.syncResultErrorStatus = RDSyncErrorStatusNoError;
            }
        }
    }
    return self;
}

- (void) syncTable {

    
    NSXMLParser *parser = [[NSXMLParser alloc]initWithData:_wsResponseData];
    [parser setDelegate:self];
    BOOL success = [parser parse];
    
    if (success == YES) {
        NSLog(@"XML parsing ok - URL = %@", self.urlForFileDownload);

        if (self.urlForFileDownload){
            //download plist from url
            NSString * stringURL = self.urlForFileDownload;
            
            NSHTTPURLResponse * response;
            NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:stringURL] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:120.0];
            [theRequest setHTTPMethod:@"GET"];
            [theRequest setValue:self.token forHTTPHeaderField:[_util searchPropertyWithName:@"NomeCookie"]];
            NSError *error;
            NSData *responseData = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:&response error:&error];
            NSLog(@"%@", [[NSString alloc] initWithData:responseData encoding:NSASCIIStringEncoding]);
            
            
            if ( responseData ) {
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString  *documentsDirectory = [paths objectAtIndex:0];
                
                NSString  *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,@"SurveyDetailTableConfiguration.plist"];
                [self deleteSurveyDefinitionPlist:filePath];
                [responseData writeToFile:filePath atomically:YES];
                
                //TODO - save last sync timestamp!
                _syncSessionEnded = [_util getCurrentDateAsString];
                [_creaDB updateOrInsertSettingWithPropertyName:TIME_STAMP_FOR_ENTITY AndPropertyValue:_syncSessionEnded];
                
            }
        }

    }else{
        NSError *error = [parser parserError];
        NSLog(@"error parsing XML:%@",error);
    }
}


- (void) deleteSurveyDefinitionPlist:(NSString *)filePath{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    BOOL success = false;
    
    success = [fileManager removeItemAtPath:filePath error:&error];
    if (success) {
        NSLog(@"File successfully deleted");
    }else{
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
}


- (NSString *) timeStampForEntity:(NSString *)propertyNameForEntity {
    NSString *timeStamp = [NSString alloc];
    NSArray *settingsCapture = [_creaDB selectFromTable:@"Settings" Where:[NSString stringWithFormat:@"propertyName = '%@'",propertyNameForEntity] OrderBy:nil isAscending:0];
    if ([settingsCapture count]>0){
        timeStamp = [[settingsCapture objectAtIndex:0]  valueForKey:@"propertyValue"];
    }else{
        timeStamp = @"0000/00/00 00.00.00.000";
    }
    
    return timeStamp;
}



#pragma mark - parser delegate methods
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    if ([elementName isEqualToString:@"faultstring"]) {
        self.syncResultErrorStatus = RDSyncErrorStatusWSThrowFault;
    }else if ([elementName isEqualToString:@"ITEMARRAY"]) {
        headerElements = [[NSMutableDictionary alloc]init];
        
        xmlBranch = elementName.mutableCopy;
        
    }else if ([elementName isEqualToString:@"COUNT"]) {
        NSLog(@"ElementName = %@",elementName);
        //memorizzare il valore di COUNT, per verifica dati a fine sincronizzazione
        
    }else{
        NSLog(@"ElementName = %@",elementName);
    }
    xmlTag = elementName.mutableCopy;
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
    if([xmlBranch isEqualToString:@"ITEMARRAY"]){
        
        if ([xmlTag isEqualToString:@"SURVEYURL"]) {
            
            //Download plist file from URL
            self.urlForFileDownload = string;
            
        }
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
    @try {
        if ([elementName isEqualToString:@"ITEMARRAY"]) {

            //in questo caso non c'è niente da fare
            
        }else{
//            NSLog(@"ElementName = %@",elementName);
        }
        
        xmlTag = [@"" mutableCopy];
    }
    @catch (NSException *exception) {
        NSLog(@"Error parser didEndElement: %@",exception.description);
        self.syncResultErrorStatus = RDSyncErrorStatusXMLParserError;
    }
    @finally {
        
    }
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
    NSLog(@"Error: %@", [parseError localizedDescription]);
    self.syncResultErrorStatus = RDSyncErrorStatusXMLParserError;
}


#pragma mark - connection delegate methods
- (NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse {
    return nil;
}
@end
