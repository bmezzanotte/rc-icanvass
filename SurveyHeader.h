//
//  SurveyHeader.h
//  iCanvass
//
//  Created by Bernardino on 26/12/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class SurveyAnswer, Customer;

@interface SurveyHeader : NSManagedObject

@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSDate * lastmodified;
@property (nonatomic, retain) NSString * note;
@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSString * unid;
@property (nonatomic, retain) Customer *customer;
@property (nonatomic, retain) NSSet *answers;
@end

@interface SurveyHeader (CoreDataGeneratedAccessors)

- (void)addAnswersObject:(SurveyAnswer *)value;
- (void)removeAnswersObject:(SurveyAnswer *)value;
- (void)addAnswers:(NSSet *)values;
- (void)removeAnswers:(NSSet *)values;

@end
