//
//  SurveyHeader.m
//  iCanvass
//
//  Created by Bernardino on 26/12/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import "SurveyHeader.h"
#import "Customer.h"


@implementation SurveyHeader

@dynamic date;
@dynamic lastmodified;
@dynamic note;
@dynamic status;
@dynamic unid;
@dynamic customer;
@dynamic answers;

@end
