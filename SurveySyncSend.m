//
//  SurveySyncSend.m
//  iCanvass
//
//  Created by Bernardino on 05/01/15.
//  Copyright (c) 2015 NAeS Lavoro. All rights reserved.
//

#import "SurveySyncSend.h"
#import "DataBaseManager.h"
#import "TCMXMLWriter.h"
#import "Utility.h"

#import "SurveyHeader.h"
#import "SurveyAnswer.h"
#import "Customer.h"

static NSString *PLIST_PATH_HEADER = @"SurveyHeader";
static NSString *PLIST_PATH_ANSWER = @"SurveyAnswer";
static NSString *ENTITY_NAME_HEADER = @"SurveyHeader";
static NSString *TIME_STAMP_FOR_ENTITY = @"lastSyncForSurvey";
static NSString *WS_NAME = @"SendPosReviewXML";
static NSString *METHOD_NAME = @"SENDLIST";

static NSString *FETCH_CACHE_NAME = @"Survey";

@interface SurveySyncSend()
@property Utility *util;
@property NSArray *resultsSummaryTags;

@property (nonatomic,strong) NSDictionary *headerTagToPropertyMapping;
@property (nonatomic,strong) NSDictionary *itemTagToPropertyMapping;
@end

@implementation SurveySyncSend

- (id)initWithToken:(NSString *)cookieToken{
    if (self = [super init]) {
        _util = [[Utility alloc] init];
        _syncSessionStarted = [_util getCurrentDateAsString];
        
        //elenco tags risposta soap, da analizzare per ottenere conteggi documenti recepiti dal server
        _resultsSummaryTags = [[NSArray alloc] initWithObjects:@"COUNT",@"NEWHEADERS",@"UPDATEDHEADERS",@"DELETEDHEADERS",@"REJECTEDHEADERS",@"NEWROWS",@"UPDATEDROWS",@"DELETEDROWS",@"REJECTEDROWS", nil];
        
        
        NSString *path = [[NSBundle mainBundle] pathForResource:PLIST_PATH_HEADER ofType:@"plist"];
        self.headerTagToPropertyMapping = [NSDictionary dictionaryWithContentsOfFile:path];
        
        path = [[NSBundle mainBundle] pathForResource:PLIST_PATH_ANSWER ofType:@"plist"];
        self.itemTagToPropertyMapping = [NSDictionary dictionaryWithContentsOfFile:path];
        
        itemList = [[NSMutableArray alloc]init];
        _creaDB = [[DataBaseManager alloc]init];
        
        //costruisco array degli oggetti ActivityHeader esistenti in CoreData, da inviare al WS
        unidArray = [_creaDB selectFromTable:ENTITY_NAME_HEADER Where:nil OrderBy:nil isAscending:NO];
        
        if (unidArray.count >0) {
            NSString *timeStamp = [self timeStampForEntity:TIME_STAMP_FOR_ENTITY];
            WebServiceClient *surveySEND = [[WebServiceClient alloc] initWithWsName:WS_NAME withMethodName:METHOD_NAME withTimeStamp:timeStamp withArrayOfUniversalID:unidArray];
            
            
            NSString *xmlbody = [self writeHTTPrequestForSendingSurveys:unidArray withMethodName:METHOD_NAME withObjectsArray:[unidArray copy] withTimeStamp:timeStamp];
            
            //DECOMMENTARE PER CHIAMARE IL WS
            _wsResponseData = [surveySEND getWebServiceResponseWithToken:cookieToken withXMLbody:xmlbody];
            
            NSInteger httpResponseStatusCode = [surveySEND.httpResponse statusCode];
            NSLog(@"http response status code: %ld",(long)httpResponseStatusCode);
            
            if (httpResponseStatusCode != 200) {
                NSLog(@"Errore WS HTTP error: %ld",(long)httpResponseStatusCode);
                self.syncResultErrorStatus = RDSyncErrorStatusServerError;
            }else{
                if (_wsResponseData == nil){
                    NSLog(@"Errore WS: ResponseData VUOTO");
                    self.syncResultErrorStatus = RDSyncErrorStatusNullStringResult;
                }else{
                    //verifico la risposta SOAP del server
                    NSXMLParser *parser = [[NSXMLParser alloc]initWithData:_wsResponseData];
                    [parser setDelegate:self];
                    BOOL success = [parser parse];
                    
                    if (success == YES) {
                        switch (self.syncResultErrorStatus) {
                            case RDSyncErrorStatusXMLParserError:
                                break;
                                
                            case RDSyncErrorStatusWSThrowFault:
                                break;
                                
                            default:
                                self.syncResultErrorStatus = RDSyncErrorStatusNoError;
                                
                                NSLog(@"XML parsing ok");
                                for (id obj in unidArray){
                                    [_creaDB deleteItem:obj];
                                }
                                
                                _syncSessionEnded = [_util getCurrentDateAsString];
                                break;
                        }
                        
                    }else{
                        NSError *error = [parser parserError];
                        NSLog(@"error parsing XML:%@",error);
                        self.syncResultErrorStatus = RDSyncErrorStatusXMLParserError;
                    }
                }
            }
        }else{
            self.syncResultErrorStatus = RDSyncErrorStatusNoError;
            _syncSessionEnded = [_util getCurrentDateAsString];
        }
    }
    return self;
}


- (NSString *)writeHTTPrequestForSendingSurveys:(NSArray *)activities withMethodName:(NSString *)method withObjectsArray:(NSArray *)objectsToSend withTimeStamp:(NSString *)lastSynchro
{
    TCMXMLWriter *xmlWriter = [[TCMXMLWriter alloc]initWithOptions:TCMXMLWriterOptionPrettyPrinted];
    Utility *util = [[Utility alloc] init];
        
    NSString *xmlMethod = [NSString stringWithFormat:@"n0:%@",method];
    
    //verifico il numero effettivo di Censimenti da inviare (per popolare correttamente il tag RESULTSARRAY)
    NSDate *lastSynchroDate = [util getDateFromString:lastSynchro withPattern:@"yyyy/MM/dd HH.mm.ss.SSS"];
    NSTimeZone* local = [NSTimeZone localTimeZone];
    NSInteger gmtOffset = [local secondsFromGMTForDate:[NSDate date]];
    NSDate *lastSynchroDateGMT = [lastSynchroDate dateByAddingTimeInterval:-gmtOffset];
    
    long headerCount = 0;
    for (SurveyHeader *obj in objectsToSend){
        NSDate *lastModifiedDate = [obj valueForKey:@"lastmodified"];
        
        if (lastSynchroDate == nil || [lastModifiedDate compare:lastSynchroDateGMT] == NSOrderedDescending){
            headerCount++;
        }
    }
    
    
    [xmlWriter instructXML];
    [xmlWriter tag:@"v:Envelope" attributes:@{@"xmlns:i" : @"http://www.w3.org/2001/XMLSchema-instance", @"xmlns:d" : @"http://www.w3.org/2001/XMLSchema", @"xmlns:c" : @"http://schemas.xmlsoap.org/soap/encoding/", @"xmlns:v" : @"http://schemas.xmlsoap.org/soap/envelope/"} contentBlock:^{
        [xmlWriter tag:@"v:Header" attributes:nil];
        [xmlWriter tag:@"v:Body" attributes:nil contentBlock:^{
            [xmlWriter tag:xmlMethod attributes:@{@"id": @"o0", @"c:root" : @"1", @"xmlns:n0" : @"urn:DefaultNamespace"} contentBlock:^{
                [xmlWriter tag:@"RESULTSARRAY" attributes:@{@"i:type": @"c:Array", @"c:arrayType" : [NSString stringWithFormat:@"d:anyType[%lu]", headerCount]} contentBlock:^{
                    
                    for (SurveyHeader *obj in objectsToSend){
                        @try {
                            NSDate *lastModifiedDate = [obj valueForKey:@"lastmodified"];
                            
                            if (lastSynchroDate == nil || [lastModifiedDate compare:lastSynchroDateGMT] == NSOrderedDescending) {
                                NSSet *itemSet = obj.answers;
                                
                                NSString *contentCDATA = @"<survey";
                                
                                //campi standard usando plist
                                for (id key in self.headerTagToPropertyMapping){
                                    NSLog(@"key=%@ value=%@", key, [self.headerTagToPropertyMapping objectForKey:key]);
                                    NSString *fieldName = [[self.headerTagToPropertyMapping objectForKey:key] valueForKey:@"attribute"];
                                    if ([obj valueForKey:fieldName] != nil) {
                                        NSString *fieldType = [[self.headerTagToPropertyMapping objectForKey:key] valueForKey:@"type"];
                                        if ([fieldType isEqualToString:@"String"]) {
                                            NSString *fieldValue = [obj valueForKey:fieldName];
                                            contentCDATA = [contentCDATA stringByAppendingString:[NSString stringWithFormat:@" %@='%@'",fieldName,[_util xmlEscapeString:fieldValue]]];
                                        }
                                        if ([fieldType isEqualToString:@"Date"]) {
                                            NSDate *myDate = [obj valueForKey:fieldName];
                                            NSString *fieldValueAsString = [self.util getStringFromDate:myDate withPattern:@"yyyy/MM/dd HH.mm.ss.SSS"];
                                            contentCDATA = [contentCDATA stringByAppendingString:[NSString stringWithFormat:@" %@='%@'",fieldName,fieldValueAsString]];
                                        }
                                        if ([fieldType isEqualToString:@"Number"]) {
                                            NSString *fieldValue = [NSString stringWithFormat:@"%@",[obj valueForKey:fieldName]];
                                            contentCDATA = [contentCDATA stringByAppendingString:[NSString stringWithFormat:@" %@='%@'",fieldName,[_util xmlEscapeString:fieldValue]]];
                                        }
                                        
                                    }else{
                                        contentCDATA = [contentCDATA stringByAppendingString:[NSString stringWithFormat:@" %@=''",fieldName]];
                                    }
                                }
                                
                                //aggiungo cliente usando relationship customer
                                NSString *customerCode = obj.customer.unid;
                                contentCDATA = [contentCDATA stringByAppendingString:[NSString stringWithFormat:@" customercode='%@'", customerCode]];
                                
                                //chiudo tag survey
                                contentCDATA = [contentCDATA stringByAppendingString:@">"];
                                
                                //aggiungo risposte questionario usando relatioship answers
                                for (SurveyAnswer *itm in itemSet){
                                    contentCDATA = [contentCDATA stringByAppendingString:@"<item"];
                                    
                                    
                                    for (id key in self.itemTagToPropertyMapping){
                                        NSLog(@"key=%@ value=%@", key, [self.itemTagToPropertyMapping objectForKey:key]);
                                        NSString *fieldName = [[self.itemTagToPropertyMapping objectForKey:key] valueForKey:@"attribute"];
                                        if ([itm valueForKey:fieldName] != nil) {
                                            NSString *fieldType = [[self.itemTagToPropertyMapping objectForKey:key] valueForKey:@"type"];
                                            if ([fieldType isEqualToString:@"String"]) {
                                                NSString *fieldValue = [itm valueForKey:fieldName];
                                                contentCDATA = [contentCDATA stringByAppendingString:[NSString stringWithFormat:@" %@='%@'",fieldName,[_util xmlEscapeString:fieldValue]]];
                                            }
                                            if ([fieldType isEqualToString:@"Date"]) {
                                                NSDate *myDate = [itm valueForKey:fieldName];
                                                NSString *fieldValueAsString = [self.util getStringFromDate:myDate withPattern:@"yyyy/MM/dd HH.mm.ss.SSS"];
                                                contentCDATA = [contentCDATA stringByAppendingString:[NSString stringWithFormat:@" %@='%@'",fieldName,fieldValueAsString]];
                                            }
                                            if ([fieldType isEqualToString:@"Number"]) {
                                                NSString *fieldValue = [NSString stringWithFormat:@"%@",[itm valueForKey:fieldName]];
                                                contentCDATA = [contentCDATA stringByAppendingString:[NSString stringWithFormat:@" %@='%@'",fieldName,[_util xmlEscapeString:fieldValue]]];
                                            }
                                            
                                        }else{
                                            contentCDATA = [contentCDATA stringByAppendingString:[NSString stringWithFormat:@" %@=''",fieldName]];
                                        }
                                    }
                                    
                                    contentCDATA = [contentCDATA stringByAppendingString:@">"];
                                    contentCDATA = [contentCDATA stringByAppendingString:@"</item>"];
                                }
                                
                                contentCDATA = [contentCDATA stringByAppendingString:@"</survey>"];
                                
                                [xmlWriter tag:@"ITEM" attributes:@{@"i:type": @"d:string"} contentCDATA:[contentCDATA copy]];
                            }else{
                                //rimuovo da unidArray l'oggetto non trasmesso, per evitare di cancellarlo dal db
                                [unidArray removeObject:obj];
                            }
                        }
                        @catch (NSException *exception) {
                            NSLog(@"Errore scrittura XML: %@",exception.description);
                            [unidArray removeAllObjects];
                        }
                        
                    }
                }];
                
                [xmlWriter tag:@"DOCTYPE" attributes:@{@"i:type": @"d:string"} contentText:@"survey"];
                [xmlWriter tag:@"TIMESTAMP" attributes:@{@"i:type": @"d:string"} contentText:lastSynchro];
            }];
        }];
    }];
    
    NSString *xmlString = [xmlWriter XMLString];
    NSLog(@"xmlString = %@",xmlString);
    return xmlString;
}



- (NSString *) timeStampForEntity:(NSString *)propertyNameForEntity {
    NSString *timeStamp = [NSString alloc];
    NSArray *settingsCapture = [_creaDB selectFromTable:@"Settings" Where:[NSString stringWithFormat:@"propertyName = '%@'",propertyNameForEntity] OrderBy:nil isAscending:0];
    if ([settingsCapture count]>0){
        timeStamp = [[settingsCapture objectAtIndex:0]  valueForKey:@"propertyValue"];
    }else{
        timeStamp = @"0000/00/00 00.00.00.000";
    }
    
    return timeStamp;
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    if ([elementName isEqualToString:@"faultstring"]) {
        self.syncResultErrorStatus = RDSyncErrorStatusWSThrowFault;
    }else{
        NSLog(@"ElementName start = %@",elementName);
    }
    xmlTag = elementName.mutableCopy;
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
    NSString *valoreTag = @"";
    //    NSLog(@"posizione tag: %lu",(unsigned long)[_resultsSummaryTags indexOfObject:xmlTag]);
    switch ([_resultsSummaryTags indexOfObject:xmlTag]) {
        case 0:
            valoreTag = [valoreTag stringByAppendingString:string];
            break;
        case 1:
            valoreTag = [valoreTag stringByAppendingString:string];
            _newHeaders = [valoreTag intValue];
            break;
        case 2:
            valoreTag = [valoreTag stringByAppendingString:string];
            _updatedHeaders = [valoreTag intValue];
            break;
        case 3:
            valoreTag = [valoreTag stringByAppendingString:string];
            _deletedHeaders = [valoreTag intValue];
            break;
        case 4:
            valoreTag = [valoreTag stringByAppendingString:string];
            _rejectedHeaders = [valoreTag intValue];
            break;
        case 5:
            valoreTag = [valoreTag stringByAppendingString:string];
            _newRows = [valoreTag intValue];
            break;
        case 6:
            valoreTag = [valoreTag stringByAppendingString:string];
            _updatedRows = [valoreTag intValue];
            break;
        case 7:
            valoreTag = [valoreTag stringByAppendingString:string];
            _deletedRows = [valoreTag intValue];
            break;
        case 8:
            valoreTag = [valoreTag stringByAppendingString:string];
            _rejectedRows = [valoreTag intValue];
            break;
        default:
            break;
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
    
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
    NSLog(@"Error: %@", [parseError localizedDescription]);
    self.syncResultErrorStatus = RDSyncErrorStatusXMLParserError;
}

@end
