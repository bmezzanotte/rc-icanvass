//
//  SyncViewController.h
//  iCanvass
//
//  Created by NAeS Lavoro on 06/02/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "ResetDBViewController.h"

@class DataBaseManager;
@class Utility;
@interface SyncViewController : UIViewController<UITableViewDataSource, UITableViewDelegate,MBProgressHUDDelegate, ResetDbDelegate>{
    NSMutableArray *arrayItem;
    MBProgressHUD *progressView;
    int valueToScore;
    int scoredValue;
}
@property (weak, nonatomic) IBOutlet UIButton *resetDataBaseButton;
@property (weak, nonatomic) IBOutlet UILabel *syncSummaryTitle;
@property (weak, nonatomic) IBOutlet UIButton *syncButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *logoutButton;
@property (weak, nonatomic) NSTimer *timer;
@property (weak, nonatomic) IBOutlet UITableView *summaryTable;

@property (nonatomic,strong) DataBaseManager *dbManager;
@property (nonatomic,strong) Utility *util;


- (IBAction)syncAction:(id)sender;
- (IBAction)logout:(id)sender;


@end
