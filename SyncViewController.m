//
//  SyncViewController.m
//  iCanvass
//
//  Created by NAeS Lavoro on 06/02/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import "SyncViewController.h"

#import "DataBaseManager.h"
#import "Utility.h"
#import "LoginViewController.h"
#import "AppDelegate.h"
#import "ResetDBViewController.h"

#import "CustomersSync.h"
#import "PeriodSync.h"
#import "TargetSync.h"
#import "ActivitySyncGet.h"
#import "ActivitySyncSend.h"
#import "SurveySyncGet.h"
#import "SurveySyncSend.h"
#import "SurveyDefinitionSync.h"

@interface SyncViewController ()
@property (nonatomic,strong) NSString *customersSynchroResultMessage;
@property (nonatomic,strong) NSString *periodSynchroResultMessage;
@property (nonatomic,strong) NSString *targetSynchroResultMessage;
@property (nonatomic,strong) NSString *activitySynchroResultMessage;
@property (nonatomic,strong) NSString *surveySynchroResultMessage;
@property (nonatomic,strong) NSString *surveySynchroDefinitionResultMessage;

@property (nonatomic) BOOL isSynchronized;
@property (nonatomic) BOOL synchroEndedSuccessfully;

@property (nonatomic,strong) CustomersSync *customersSyncTable;
@property (nonatomic,strong) PeriodSync *periodSyncTable;
@property (nonatomic,strong) TargetSync *targetSyncTable;
@property (nonatomic,strong) ActivitySyncSend *activitySendResults;
@property (nonatomic,strong) ActivitySyncGet *activityGetResults;

@property (nonatomic,strong) SurveySyncSend *surveySendResults;
@property (nonatomic,strong) SurveySyncGet *surveyGetResults;
@property (nonatomic,strong) SurveyDefinitionSync *surveyDefinitionSync;

@property (nonatomic,strong) NSMutableArray *syncResultSummary;
@end

@implementation SyncViewController

#pragma mark - Life Cycle Methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.dbManager = [[DataBaseManager alloc] init];
    self.util = [[Utility alloc] init];
    arrayItem = [[NSMutableArray alloc]init];
    valueToScore = 0;
    scoredValue = 0;
    self.syncResultSummary = [[NSMutableArray alloc] init];

    [self.navigationItem setTitle:NSLocalizedString(@"Sincronizza", @"Sincronizza")];
    [self.logoutButton setTitle:NSLocalizedString(@"Logout", @"Logout")];

    [self.resetDataBaseButton setTitle:NSLocalizedString(@"Reset DB", @"Reset DB") forState:UIControlStateNormal];
    [self.syncButton setTitle:NSLocalizedString(@"Sincronizza", @"Sincronizza") forState:UIControlStateNormal];
    [self.syncSummaryTitle setText:NSLocalizedString(@"Riepilogo Tabelle Database", @"Riepilogo Tabelle Database")];
	
    
    //timer = [[NSTimer alloc]init];
    
    if (self.isSynchronized == NO) {
        self.customersSynchroResultMessage = @"";
    }
    self.synchroEndedSuccessfully = YES;
    //    [self showSynchronisationResults];

    
    
    
    /*
    //temporaneo, per vedere cosa c'è nella tabella Target
    NSArray *targetObjects = [self.dbManager selectFromTable:@"Target" Where:@"status != '9'" OrderBy:@"subject" isAscending:YES];
    //temporaneo, per vedere cosa c'è nella tabella Target
    
    //temporaneo, per vedere cosa c'è nella tabella Activity
    NSArray *activityObjects = [self.dbManager selectFromTable:@"ActivityHeader" Where:@"status != '9'" OrderBy:@"date" isAscending:YES];
    NSArray *activityObjectsAll = [self.dbManager selectFromTable:@"ActivityHeader" Where:@"status != '99999'" OrderBy:@"date" isAscending:YES];
    //temporaneo, per vedere cosa c'è nella tabella Activity

    //temporaneo, per vedere cosa c'è nella tabella Survey
    NSArray *surveyObjects = [self.dbManager selectFromTable:@"SurveyHeader" Where:@"status != '9'" OrderBy:@"date" isAscending:YES];
    NSArray *surveyObjectsAll = [self.dbManager selectFromTable:@"SurveyHeader" Where:@"status != '99999'" OrderBy:@"date" isAscending:YES];
    //temporaneo, per vedere cosa c'è nella tabella Survey
     */

    
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [[[[self.tabBarController tabBar] items] objectAtIndex:0] setEnabled:YES];
    [[[[self.tabBarController tabBar] items] objectAtIndex:1] setEnabled:YES];
}

- (void)viewDidAppear:(BOOL)animated
{
    if (self.isSynchronized == NO) {
        self.customersSynchroResultMessage = @"";
    }
    [self showSynchronisationResults];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSouce Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.syncResultSummary.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return @" ";
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *simpleTableIdentifier = @"SummaryItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    UILabel *tableTitle = (UILabel *)[cell viewWithTag:1];
    UILabel *syncSummary = (UILabel *)[cell viewWithTag:2];
    UILabel *tableRecords = (UILabel *)[cell viewWithTag:3];
    
    tableTitle.text = [[self.syncResultSummary objectAtIndex:indexPath.row] valueForKey:@"title"];
    tableRecords.text = [[self.syncResultSummary objectAtIndex:indexPath.row] valueForKey:@"total"];
    
    if (self.isSynchronized == YES || self.synchroEndedSuccessfully == NO) {
        NSString *result = [[self.syncResultSummary objectAtIndex:indexPath.row] valueForKey:@"result"];
        NSLog(@"result: %@",result);
        if ([result isEqualToString:@""]) {
            syncSummary.text = NSLocalizedString(@"sincronizzazione interrotta", @"sincronizzazione interrotta");
        }else{
            if (indexPath.row <= 2) {
                syncSummary.text = [NSString stringWithFormat:@"%@ - ricevuti: %@/%@/%@",
                                    [[self.syncResultSummary objectAtIndex:indexPath.row] valueForKey:@"result"],
                                    [[self.syncResultSummary objectAtIndex:indexPath.row] valueForKey:@"getAdd"],
                                    [[self.syncResultSummary objectAtIndex:indexPath.row] valueForKey:@"getModify"],
                                    [[self.syncResultSummary objectAtIndex:indexPath.row] valueForKey:@"getDelete"]];
            }else{
                syncSummary.text = [NSString stringWithFormat:@"%@ - ricevuti: %@/%@/%@ - inviati: %@/%@/%@",
                                    [[self.syncResultSummary objectAtIndex:indexPath.row] valueForKey:@"result"],
                                    [[self.syncResultSummary objectAtIndex:indexPath.row] valueForKey:@"getAdd"],
                                    [[self.syncResultSummary objectAtIndex:indexPath.row] valueForKey:@"getModify"],
                                    [[self.syncResultSummary objectAtIndex:indexPath.row] valueForKey:@"getDelete"],
                                    [[self.syncResultSummary objectAtIndex:indexPath.row] valueForKey:@"sendAdd"],
                                    [[self.syncResultSummary objectAtIndex:indexPath.row] valueForKey:@"sendModify"],
                                    [[self.syncResultSummary objectAtIndex:indexPath.row] valueForKey:@"sendDelete"]];
            }
        }
    }else{
        NSArray *lastSyncLabels = [[NSArray alloc] initWithObjects:@"lastSyncForCustomers",@"lastSyncForPeriod",@"lastSyncForTarget",@"lastSyncForActivity",@"lastSyncForActivity",
                                   @"lastSyncForSurvey",@"lastSyncForSurvey", nil];
        NSString *timeStamp = [self timeStampForEntity:[lastSyncLabels objectAtIndex:indexPath.row]];
        syncSummary.text = [NSString stringWithFormat:@"ultima sincronizzazione: %@", [self.util getFormattedDate:timeStamp withPattern:@"dd/MM/yy HH.mm.ss"]];
    }
    
    return cell;
}

#pragma mark - Actions

- (IBAction)syncAction:(id)sender {
    //progressView = [[MBProgressHUD alloc]initWithView:self.navigationController.view];
    progressView = [[MBProgressHUD alloc]initWithView:self.tabBarController.view];
    [self.tabBarController.view addSubview:progressView];
    progressView.delegate = self;
	progressView.labelText = @"Loading";
	progressView.detailsLabelText = @"updating data";
    progressView.dimBackground = YES;
	progressView.square = YES;
    
    [progressView showWhileExecuting:@selector(sincronizzazione) onTarget:self withObject:nil animated:YES];
}

- (IBAction)logout:(id)sender {
    DataBaseManager *dbManager = [[DataBaseManager alloc]init];
    NSMutableArray *password = [dbManager selectFromTable:@"Settings" Where:@"propertyName == 'Password'" OrderBy:nil isAscending:0];
    if (password.count > 0) {
        [dbManager deleteItem:[password objectAtIndex:0]];
    }
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)sincronizzazione{
    progressView.detailsLabelText = [NSString stringWithFormat:@"Sto effettuando\n l'autenticazione"];
    
    NSString *username = [self.dbManager getValueFromSettingsByPropertyName:@"UserName"];
    NSString *password = [self.dbManager getValueFromSettingsByPropertyName:@"Password"];
    
    NSString *dialogBoxPrefixSend = NSLocalizedString(@"Sto inviando la tabella", @"Sto inviando la tabella");
    NSString *dialogBoxPrefixGet = NSLocalizedString(@"Sto ricevendo la tabella", @"Sto ricevendo la tabella");
    
    NSString *cookieToken = [self.util autenticazioneConDominoConNomeUtente:username Password:password];
    if (cookieToken == nil) {
        __block UIImageView *imageView;
        dispatch_sync(dispatch_get_main_queue(), ^{
            UIImage *image = [UIImage imageNamed:@"37x-Xmark.png"];
            imageView = [[UIImageView alloc] initWithImage:image];
        });
        progressView.customView = imageView;
        progressView.mode = MBProgressHUDModeCustomView;
        progressView.labelText = @"Sincronizzazione\n non Riuscita";
        progressView.detailsLabelText = nil;
        sleep(2);
        [self performSegueWithIdentifier:@"AuthenticationErrorIdentifier" sender:self];
    }else{
        self.synchroEndedSuccessfully = YES;
        
        //sincronizzazione punti vendita
        NSLog(@"Sto ricevendo la tabella Punti Vendita");
        progressView.detailsLabelText = [NSString stringWithFormat:@"%@:\n\n %@",dialogBoxPrefixGet,NSLocalizedString(@"Punti Vendita", @"Punti Vendita")];
        self.customersSyncTable = [[CustomersSync alloc] initWithToken:cookieToken];
        
        if (self.customersSyncTable.syncResultErrorStatus == RDSyncErrorStatusNoError) {
            [self.customersSyncTable syncTable];
        }
        
        switch (self.customersSyncTable.syncResultErrorStatus) {
            case RDSyncErrorStatusNoError:
                self.customersSynchroResultMessage = @"OK";
                break;
            case RDSyncErrorStatusServerError:
                //devo interrompere l'intera sessione di sincronizzazione
                NSLog(@"Errore risposta server != 200 - customersSync");
                self.customersSynchroResultMessage = @"KO";
                self.synchroEndedSuccessfully = NO;
                [self showSynchronisationGlobalErrorAlert];
                return;
                break;
            case RDSyncErrorStatusXMLParserError:
                NSLog(@"Errore parsing XML - customersSync");
                self.customersSynchroResultMessage = @"KO";
                self.synchroEndedSuccessfully = NO;
                break;
            case RDSyncErrorStatusWSThrowFault:
                NSLog(@"WS ThrowFault - customersSync");
                self.customersSynchroResultMessage = @"KO";
                self.synchroEndedSuccessfully = NO;
                break;
            default:
                NSLog(@"Errore sincronizzazione - customersSync");
                self.customersSynchroResultMessage = @"KO";
                self.synchroEndedSuccessfully = NO;
                break;
        }
        //END sincronizzazione punti vendita

        //sincronizzazione periodi
        NSLog(@"Sto ricevendo la tabella Periodi");
        progressView.detailsLabelText = [NSString stringWithFormat:@"%@:\n\n %@",dialogBoxPrefixGet,NSLocalizedString(@"Periodi", @"Periodi")];
        self.periodSyncTable = [[PeriodSync alloc] initWithToken:cookieToken];
        
        if (self.periodSyncTable.syncResultErrorStatus == RDSyncErrorStatusNoError) {
            [self.periodSyncTable syncTable];
        }
        
        switch (self.periodSyncTable.syncResultErrorStatus) {
            case RDSyncErrorStatusNoError:
                self.periodSynchroResultMessage = @"OK";
                break;
            case RDSyncErrorStatusServerError:
                //devo interrompere l'intera sessione di sincronizzazione
                NSLog(@"Errore risposta server != 200 - periodSync");
                self.periodSynchroResultMessage = @"KO";
                self.synchroEndedSuccessfully = NO;
                [self showSynchronisationGlobalErrorAlert];
                return;
                break;
            case RDSyncErrorStatusXMLParserError:
                NSLog(@"Errore parsing XML - periodSync");
                self.periodSynchroResultMessage = @"KO";
                self.synchroEndedSuccessfully = NO;
                break;
            case RDSyncErrorStatusWSThrowFault:
                NSLog(@"WS ThrowFault - periodSync");
                self.periodSynchroResultMessage = @"KO";
                self.synchroEndedSuccessfully = NO;
                break;
            default:
                NSLog(@"Errore sincronizzazione - periodSync");
                self.periodSynchroResultMessage = @"KO";
                self.synchroEndedSuccessfully = NO;
                break;
        }
        //END sincronizzazione periodi

        //sincronizzazione obiettivi
        NSLog(@"Sto ricevendo la tabella Obiettivi");
        progressView.detailsLabelText = [NSString stringWithFormat:@"%@:\n\n %@",dialogBoxPrefixGet,NSLocalizedString(@"Obiettivi", @"Obiettivi")];
        self.targetSyncTable = [[TargetSync alloc] initWithToken:cookieToken];
        
        if (self.targetSyncTable.syncResultErrorStatus == RDSyncErrorStatusNoError) {
            [self.targetSyncTable syncTable];
        }
        
        switch (self.targetSyncTable.syncResultErrorStatus) {
            case RDSyncErrorStatusNoError:
                self.targetSynchroResultMessage = @"OK";
                break;
            case RDSyncErrorStatusServerError:
                //devo interrompere l'intera sessione di sincronizzazione
                NSLog(@"Errore risposta server != 200 - targetSync");
                self.targetSynchroResultMessage = @"KO";
                self.synchroEndedSuccessfully = NO;
                [self showSynchronisationGlobalErrorAlert];
                return;
                break;
            case RDSyncErrorStatusXMLParserError:
                NSLog(@"Errore parsing XML - targetSync");
                self.targetSynchroResultMessage = @"KO";
                self.synchroEndedSuccessfully = NO;
                break;
            case RDSyncErrorStatusWSThrowFault:
                NSLog(@"WS ThrowFault - targetSync");
                self.targetSynchroResultMessage = @"KO";
                self.synchroEndedSuccessfully = NO;
                break;
            default:
                NSLog(@"Errore sincronizzazione - targetSync");
                self.targetSynchroResultMessage = @"KO";
                self.synchroEndedSuccessfully = NO;
                break;
        }
        //END sincronizzazione obiettivi

        //sincronizzazione attività SEND
        NSLog(@"Sto inviando la tabella Attività");
        progressView.detailsLabelText = [NSString stringWithFormat:@"%@:\n\n %@",dialogBoxPrefixGet,NSLocalizedString(@"Attività", @"Attività")];
        self.activitySendResults = [[ActivitySyncSend alloc] initWithToken:cookieToken];
        
        switch (self.activitySendResults.syncResultErrorStatus) {
            case RDSyncErrorStatusNoError:
                self.activitySynchroResultMessage = @"OK";
                break;
            case RDSyncErrorStatusServerError:
                //devo interrompere l'intera sessione di sincronizzazione
                NSLog(@"Errore risposta server != 200 - activityResultSend");
                self.activitySynchroResultMessage = @"KO";
                self.synchroEndedSuccessfully = NO;
                [self showSynchronisationGlobalErrorAlert];
                return;
                break;
            default:
                NSLog(@"Errore sincronizzazione - activityResultSend");
                self.activitySynchroResultMessage = @"KO";
                self.synchroEndedSuccessfully = NO;
                break;
        }
        //END sincronizzazione attività SEND

        
        //sincronizzazione attività GET
        //eseguo solo se la send è andata a buon fine
        if ([self.activitySynchroResultMessage isEqualToString:@"OK"]) {
            NSLog(@"Sto ricevendo la tabella Attività");
            
            progressView.detailsLabelText = [NSString stringWithFormat:@"%@:\n\n %@",dialogBoxPrefixGet,NSLocalizedString(@"Attività", @"Attività")];
            self.activityGetResults = [[ActivitySyncGet alloc] initWithToken:cookieToken];

            if (self.activityGetResults.syncResultErrorStatus == RDSyncErrorStatusNoError) {
                [self.activityGetResults syncTable];
            }
            
            switch (self.activityGetResults.syncResultErrorStatus) {
                case RDSyncErrorStatusNoError:
                    self.activitySynchroResultMessage = @"OK";
                    break;
                case RDSyncErrorStatusServerError:
                    //devo interrompere l'intera sessione di sincronizzazione
                    NSLog(@"Errore risposta server != 200 - activityResultGet");
                    self.activitySynchroResultMessage = @"KO";
                    self.synchroEndedSuccessfully = NO;
                    [self showSynchronisationGlobalErrorAlert];
                    return;
                    break;
                case RDSyncErrorStatusXMLParserError:
                    NSLog(@"Errore parsing XML - activityResultGet");
                    self.activitySynchroResultMessage = @"KO";
                    self.synchroEndedSuccessfully = NO;
                    break;
                case RDSyncErrorStatusWSThrowFault:
                    NSLog(@"WS ThrowFault - activityResultGet");
                    self.activitySynchroResultMessage = @"KO";
                    self.synchroEndedSuccessfully = NO;
                    break;
                default:
                    NSLog(@"Errore sincronizzazione - activityResultGet");
                    self.activitySynchroResultMessage = @"KO";
                    self.synchroEndedSuccessfully = NO;
                    break;
            }
            
            if (self.activityGetResults.syncResultErrorStatus == RDSyncErrorStatusNoError ) {
                NSString *syncSessionEnded = [_util getCurrentDateAsString];
                [self.dbManager updateOrInsertSettingWithPropertyName:@"lastSyncForActivity" AndPropertyValue:syncSessionEnded];
            }

        }
        //END sincronizzazione attività GET
 
        

        
        
        //sincronizzazione definizione survey
        NSLog(@"Sto ricevendo la struttura del questionario censimenti");
        progressView.detailsLabelText = [NSString stringWithFormat:@"%@:\n\n %@",dialogBoxPrefixGet,NSLocalizedString(@"Definizione questionario", @"Definizione questionario")];
        self.surveyDefinitionSync = [[SurveyDefinitionSync alloc] initWithToken:cookieToken];
        
        if (self.surveyDefinitionSync.syncResultErrorStatus == RDSyncErrorStatusNoError) {
            [self.surveyDefinitionSync syncTable];
        }
        
        switch (self.surveyDefinitionSync.syncResultErrorStatus) {
            case RDSyncErrorStatusNoError:
                self.surveySynchroDefinitionResultMessage = @"OK";
                break;
            case RDSyncErrorStatusServerError:
                //devo interrompere l'intera sessione di sincronizzazione
                NSLog(@"Errore risposta server != 200 - surveyDefinitionSync");
                self.surveySynchroDefinitionResultMessage = @"KO";
                self.synchroEndedSuccessfully = NO;
                [self showSynchronisationGlobalErrorAlert];
                return;
                break;
            case RDSyncErrorStatusXMLParserError:
                NSLog(@"Errore parsing XML - surveyDefinitionSync");
                self.surveySynchroDefinitionResultMessage = @"KO";
                self.synchroEndedSuccessfully = NO;
                break;
            case RDSyncErrorStatusWSThrowFault:
                NSLog(@"WS ThrowFault - surveyDefinitionSync");
                self.surveySynchroDefinitionResultMessage = @"KO";
                self.synchroEndedSuccessfully = NO;
                break;
            default:
                NSLog(@"Errore sincronizzazione - surveyDefinitionSync");
                self.surveySynchroDefinitionResultMessage = @"KO";
                self.synchroEndedSuccessfully = NO;
                break;
        }
        //END sincronizzazione definizione survey
        
        
        
        
        
        //sincronizzazione survey SEND
        NSLog(@"Sto inviando la tabella Censimenti");
        progressView.detailsLabelText = [NSString stringWithFormat:@"%@:\n\n %@",dialogBoxPrefixGet,NSLocalizedString(@"Censimenti", @"Censimenti")];
        self.surveySendResults = [[SurveySyncSend alloc] initWithToken:cookieToken];
        
        switch (self.surveySendResults.syncResultErrorStatus) {
            case RDSyncErrorStatusNoError:
                self.surveySynchroResultMessage = @"OK";
                break;
            case RDSyncErrorStatusServerError:
                //devo interrompere l'intera sessione di sincronizzazione
                NSLog(@"Errore risposta server != 200 - surveyResultSend");
                self.surveySynchroResultMessage = @"KO";
                self.synchroEndedSuccessfully = NO;
                [self showSynchronisationGlobalErrorAlert];
                return;
                break;
            default:
                NSLog(@"Errore sincronizzazione - surveyResultSend");
                self.surveySynchroResultMessage = @"KO";
                self.synchroEndedSuccessfully = NO;
                break;
        }
        //END sincronizzazione survey SEND
        
        
        //sincronizzazione survey GET
        //eseguo solo se la send è andata a buon fine
        if ([self.surveySynchroResultMessage isEqualToString:@"OK"]) {
            NSLog(@"Sto ricevendo la tabella Censimenti");
            
            progressView.detailsLabelText = [NSString stringWithFormat:@"%@:\n\n %@",dialogBoxPrefixGet,NSLocalizedString(@"Censimenti", @"Censimenti")];
            self.surveyGetResults = [[SurveySyncGet alloc] initWithToken:cookieToken];
            
            if (self.surveyGetResults.syncResultErrorStatus == RDSyncErrorStatusNoError) {
                [self.surveyGetResults syncTable];
            }
            
            switch (self.surveyGetResults.syncResultErrorStatus) {
                case RDSyncErrorStatusNoError:
                    self.surveySynchroResultMessage = @"OK";
                    break;
                case RDSyncErrorStatusServerError:
                    //devo interrompere l'intera sessione di sincronizzazione
                    NSLog(@"Errore risposta server != 200 - surveyResultGet");
                    self.surveySynchroResultMessage = @"KO";
                    self.synchroEndedSuccessfully = NO;
                    [self showSynchronisationGlobalErrorAlert];
                    return;
                    break;
                case RDSyncErrorStatusXMLParserError:
                    NSLog(@"Errore parsing XML - surveyResultGet");
                    self.surveySynchroResultMessage = @"KO";
                    self.synchroEndedSuccessfully = NO;
                    break;
                case RDSyncErrorStatusWSThrowFault:
                    NSLog(@"WS ThrowFault - surveyResultGet");
                    self.surveySynchroResultMessage = @"KO";
                    self.synchroEndedSuccessfully = NO;
                    break;
                default:
                    NSLog(@"Errore sincronizzazione - surveyResultGet");
                    self.surveySynchroResultMessage = @"KO";
                    self.synchroEndedSuccessfully = NO;
                    break;
            }
            
            if (self.surveyGetResults.syncResultErrorStatus == RDSyncErrorStatusNoError ) {
                NSString *syncSessionEnded = [_util getCurrentDateAsString];
                [self.dbManager updateOrInsertSettingWithPropertyName:@"lastSyncForSurvey" AndPropertyValue:syncSessionEnded];
            }
            
        }
        //END sincronizzazione survey GET

        
        __block UIImageView *imageView;
        dispatch_sync(dispatch_get_main_queue(), ^{
            UIImage *image = [UIImage imageNamed:@"37x-Checkmark.png"];
            imageView = [[UIImageView alloc] initWithImage:image];
        });
        progressView.customView = imageView;
        progressView.mode = MBProgressHUDModeCustomView;
        progressView.labelText = @"Sincronizzazione";
        
        if (self.synchroEndedSuccessfully == YES) {
            self.isSynchronized = YES;
            progressView.detailsLabelText = @"completata";
            sleep(2);
        }else{
            progressView.detailsLabelText = @"completata con errori";
            sleep(3);
        }
        
        [self showSynchronisationResults];
    }
}

- (void) showSynchronisationGlobalErrorAlert {
    __block UIImageView *imageView;
    dispatch_sync(dispatch_get_main_queue(), ^{
        UIImage *image = [UIImage imageNamed:@"37x-Checkmark.png"];
        imageView = [[UIImageView alloc] initWithImage:image];
    });
    progressView.customView = imageView;
    progressView.mode = MBProgressHUDModeCustomView;
    progressView.labelText = @"Sincronizzazione";
    progressView.detailsLabelText = @"completata con errori";
    
    sleep(3);
    
    [self showSynchronisationResults];
}

- (void)showSynchronisationResults{
    DataBaseManager *dbManager = [[DataBaseManager alloc]init];
    
    NSMutableArray *shopList = [dbManager selectFromTable:@"Customer" Where:@"status != '9'" OrderBy:nil isAscending:0];
    NSMutableArray *periodList = [dbManager selectFromTable:@"Period" Where:@"status != '9'" OrderBy:nil isAscending:0];
    NSMutableArray *targetList = [dbManager selectFromTable:@"Target" Where:@"status != '9'" OrderBy:nil isAscending:0];
    NSMutableArray *activityHeaderList = [dbManager selectFromTable:@"ActivityHeader" Where:@"status != '9'" OrderBy:nil isAscending:0];
    NSMutableArray *activityAnswerList = [dbManager selectFromTable:@"ActivityAnswer" Where:nil OrderBy:nil isAscending:0];
    NSMutableArray *surveyHeaderList = [dbManager selectFromTable:@"SurveyHeader" Where:@"status != '9'" OrderBy:nil isAscending:0];
    NSMutableArray *surveyAnswerList = [dbManager selectFromTable:@"SurveyAnswer" Where:nil OrderBy:nil isAscending:0];
    
    
    [self.syncResultSummary removeAllObjects];
    
    NSMutableDictionary *shopResult = [[NSMutableDictionary alloc] init];
    [shopResult setObject:@"getShops" forKey:@"operation"];
    [shopResult setObject:@"Punti Vendita" forKey:@"title"];

    NSMutableDictionary *periodResult = [[NSMutableDictionary alloc] init];
    [periodResult setObject:@"getPeriods" forKey:@"operation"];
    [periodResult setObject:@"Periodi" forKey:@"title"];

    NSMutableDictionary *targetResult = [[NSMutableDictionary alloc] init];
    [targetResult setObject:@"getTarget" forKey:@"operation"];
    [targetResult setObject:@"Obiettivi" forKey:@"title"];

    NSMutableDictionary *activityHeaderResult = [[NSMutableDictionary alloc] init];
    [activityHeaderResult setObject:@"activityHeaders" forKey:@"operation"];
    [activityHeaderResult setObject:@"Attività" forKey:@"title"];
    
    NSMutableDictionary *activityAnswerResult = [[NSMutableDictionary alloc] init];
    [activityAnswerResult setObject:@"activityAnswers" forKey:@"operation"];
    [activityAnswerResult setObject:@"Righe questionario attività" forKey:@"title"];

    NSMutableDictionary *surveyHeaderResult = [[NSMutableDictionary alloc] init];
    [surveyHeaderResult setObject:@"surveyHeaders" forKey:@"operation"];
    [surveyHeaderResult setObject:@"Censimenti" forKey:@"title"];

    NSMutableDictionary *surveyAnswerResult = [[NSMutableDictionary alloc] init];
    [surveyAnswerResult setObject:@"surveyAnswers" forKey:@"operation"];
    [surveyAnswerResult setObject:@"Risposte questionario censimenti" forKey:@"title"];



    if (self.isSynchronized == YES || self.synchroEndedSuccessfully == NO) {
        self.syncSummaryTitle.text = NSLocalizedString(@"Riepilogo ultima sincronizzazione", @"Riepilogo ultima sincronizzazione");
        
        //clienti
        [shopResult setObject:self.customersSynchroResultMessage forKey:@"result"];
        [shopResult setObject:[NSString stringWithFormat:@"%lu",(unsigned long)[shopList count]] forKey:@"total"];
        [shopResult setObject:[NSString stringWithFormat:@"%d",(int)self.customersSyncTable.newCustomers] forKey:@"getAdd"];
        [shopResult setObject:[NSString stringWithFormat:@"%d",(int)self.customersSyncTable.updatedCustomers] forKey:@"getModify"];
        [shopResult setObject:[NSString stringWithFormat:@"%d",(int)self.customersSyncTable.deletedCustomers] forKey:@"getDelete"];
        [self.syncResultSummary addObject:shopResult];

        //periodi
        [periodResult setObject:self.periodSynchroResultMessage forKey:@"result"];
        [periodResult setObject:[NSString stringWithFormat:@"%lu",(unsigned long)[periodList count]] forKey:@"total"];
        [periodResult setObject:[NSString stringWithFormat:@"%d",(int)self.periodSyncTable.newPeriods] forKey:@"getAdd"];
        [periodResult setObject:[NSString stringWithFormat:@"%d",(int)self.periodSyncTable.updatedPeriods] forKey:@"getModify"];
        [periodResult setObject:[NSString stringWithFormat:@"%d",(int)self.periodSyncTable.deletedPeriods] forKey:@"getDelete"];
        [self.syncResultSummary addObject:periodResult];

        //obiettivi
        [targetResult setObject:self.periodSynchroResultMessage forKey:@"result"];
        [targetResult setObject:[NSString stringWithFormat:@"%lu",(unsigned long)[targetList count]] forKey:@"total"];
        [targetResult setObject:[NSString stringWithFormat:@"%d",(int)self.targetSyncTable.newTargets] forKey:@"getAdd"];
        [targetResult setObject:[NSString stringWithFormat:@"%d",(int)self.targetSyncTable.updatedTargets] forKey:@"getModify"];
        [targetResult setObject:[NSString stringWithFormat:@"%d",(int)self.targetSyncTable.deletedTargets] forKey:@"getDelete"];
        [self.syncResultSummary addObject:targetResult];

        //attività (headers)
        /*
        int newReceivedActivityHeaders = self.activityGetResults.newHeaders - (self.activitySendResults.newHeaders + self.activitySendResults.updatedHeaders);
        int newReceivedActivityRows = self.activityGetResults.newRows - (self.activitySendResults.newRows + self.activitySendResults.updatedRows);
        */

        [activityHeaderResult setObject:self.activitySynchroResultMessage forKey:@"result"];
        [activityHeaderResult setObject:[NSString stringWithFormat:@"%lu",(unsigned long)[activityHeaderList count]] forKey:@"total"];
        [activityHeaderResult setObject:[NSString stringWithFormat:@"%d",(int)self.activitySendResults.newHeaders] forKey:@"sendAdd"];
        [activityHeaderResult setObject:[NSString stringWithFormat:@"%d",(int)self.activitySendResults.updatedHeaders] forKey:@"sendModify"];
        [activityHeaderResult setObject:[NSString stringWithFormat:@"%d",(int)self.activitySendResults.deletedHeaders] forKey:@"sendDelete"];
        [activityHeaderResult setObject:[NSString stringWithFormat:@"%d",(int)self.activityGetResults.newHeaders - ((int)self.activitySendResults.newHeaders + (int)self.activitySendResults.updatedHeaders)] forKey:@"getAdd"];
        [activityHeaderResult setObject:[NSString stringWithFormat:@"%d",(int)self.activityGetResults.updatedHeaders] forKey:@"getModify"];
        [activityHeaderResult setObject:[NSString stringWithFormat:@"%d",(int)self.activityGetResults.deletedHeaders] forKey:@"getDelete"];
        [self.syncResultSummary addObject:activityHeaderResult];
        
        
        //attività (answers)
        [activityAnswerResult setObject:self.activitySynchroResultMessage forKey:@"result"];
        [activityAnswerResult setObject:[NSString stringWithFormat:@"%lu",(unsigned long)[activityAnswerList count]] forKey:@"total"];
        [activityAnswerResult setObject:[NSString stringWithFormat:@"%d",(int)self.activitySendResults.newRows] forKey:@"sendAdd"];
        [activityAnswerResult setObject:[NSString stringWithFormat:@"%d",(int)self.activitySendResults.updatedRows] forKey:@"sendModify"];
        [activityAnswerResult setObject:[NSString stringWithFormat:@"%d",(int)self.activitySendResults.deletedRows] forKey:@"sendDelete"];
        [activityAnswerResult setObject:[NSString stringWithFormat:@"%d",(int)self.activityGetResults.newRows - ((int)self.activitySendResults.newRows + (int)self.activitySendResults.updatedRows)] forKey:@"getAdd"];
        [activityAnswerResult setObject:[NSString stringWithFormat:@"%d",(int)self.activityGetResults.updatedRows] forKey:@"getModify"];
        [activityAnswerResult setObject:[NSString stringWithFormat:@"%d",(int)self.activityGetResults.deletedRows] forKey:@"getDelete"];
        [self.syncResultSummary addObject:activityAnswerResult];

        
        //surveys (headers)
        [surveyHeaderResult setObject:self.surveySynchroResultMessage forKey:@"result"];
        [surveyHeaderResult setObject:[NSString stringWithFormat:@"%lu",(unsigned long)[surveyHeaderList count]] forKey:@"total"];
        [surveyHeaderResult setObject:[NSString stringWithFormat:@"%d",(int)self.surveySendResults.newHeaders] forKey:@"sendAdd"];
        [surveyHeaderResult setObject:[NSString stringWithFormat:@"%d",(int)self.surveySendResults.updatedHeaders] forKey:@"sendModify"];
        [surveyHeaderResult setObject:[NSString stringWithFormat:@"%d",(int)self.surveySendResults.deletedHeaders] forKey:@"sendDelete"];
        [surveyHeaderResult setObject:[NSString stringWithFormat:@"%d",(int)self.surveyGetResults.newHeaders - ((int)self.surveySendResults.newHeaders + (int)self.surveySendResults.updatedHeaders)] forKey:@"getAdd"];
        [surveyHeaderResult setObject:[NSString stringWithFormat:@"%d",(int)self.surveyGetResults.updatedHeaders] forKey:@"getModify"];
        [surveyHeaderResult setObject:[NSString stringWithFormat:@"%d",(int)self.surveyGetResults.deletedHeaders] forKey:@"getDelete"];
        [self.syncResultSummary addObject:surveyHeaderResult];
        
        
        //surveys (answers)
        [surveyAnswerResult setObject:self.surveySynchroResultMessage forKey:@"result"];
        [surveyAnswerResult setObject:[NSString stringWithFormat:@"%lu",(unsigned long)[surveyAnswerList count]] forKey:@"total"];
        [surveyAnswerResult setObject:[NSString stringWithFormat:@"%d",(int)self.surveySendResults.newRows] forKey:@"sendAdd"];
        [surveyAnswerResult setObject:[NSString stringWithFormat:@"%d",(int)self.surveySendResults.updatedRows] forKey:@"sendModify"];
        [surveyAnswerResult setObject:[NSString stringWithFormat:@"%d",(int)self.surveySendResults.deletedRows] forKey:@"sendDelete"];
        [surveyAnswerResult setObject:[NSString stringWithFormat:@"%d",(int)self.surveyGetResults.newRows - ((int)self.surveySendResults.newRows + (int)self.surveySendResults.updatedRows)] forKey:@"getAdd"];
        [surveyAnswerResult setObject:[NSString stringWithFormat:@"%d",(int)self.surveyGetResults.updatedRows] forKey:@"getModify"];
        [surveyAnswerResult setObject:[NSString stringWithFormat:@"%d",(int)self.surveyGetResults.deletedRows] forKey:@"getDelete"];
        [self.syncResultSummary addObject:surveyAnswerResult];
        

        
    }else{
        self.syncSummaryTitle.text = NSLocalizedString(@"Riepilogo tabelle database", @"Riepilogo tabelle database");
        
        [shopResult setObject:[NSString stringWithFormat:@"%lu",(unsigned long)[shopList count]] forKey:@"total"];
        [self.syncResultSummary addObject:shopResult];

        [periodResult setObject:[NSString stringWithFormat:@"%lu",(unsigned long)[periodList count]] forKey:@"total"];
        [self.syncResultSummary addObject:periodResult];

        [targetResult setObject:[NSString stringWithFormat:@"%lu",(unsigned long)[targetList count]] forKey:@"total"];
        [self.syncResultSummary addObject:targetResult];

        [activityHeaderResult setObject:[NSString stringWithFormat:@"%lu",(unsigned long)[activityHeaderList count]] forKey:@"total"];
        [self.syncResultSummary addObject:activityHeaderResult];

        [activityAnswerResult setObject:[NSString stringWithFormat:@"%lu",(unsigned long)[activityAnswerList count]] forKey:@"total"];
        [self.syncResultSummary addObject:activityAnswerResult];

        [surveyHeaderResult setObject:[NSString stringWithFormat:@"%lu",(unsigned long)[surveyHeaderList count]] forKey:@"total"];
        [self.syncResultSummary addObject:surveyHeaderResult];
        
        [surveyAnswerResult setObject:[NSString stringWithFormat:@"%lu",(unsigned long)[surveyAnswerList count]] forKey:@"total"];
        [self.syncResultSummary addObject:surveyAnswerResult];

    }
    [self.summaryTable reloadData];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([[segue identifier] isEqualToString:@"AuthenticationErrorIdentifier"]){
        LoginViewController *destination = segue.destinationViewController;
        destination.isOccuredAuthenticationError = YES;
    }else if ([[segue identifier] isEqualToString:@"ResetDB"]){
        ResetDBViewController *destination = (ResetDBViewController *)[segue destinationViewController];
        destination.delegate = self;
    }
}

- (void)setNotSynchronized{
    self.isSynchronized = NO;
}

/*
- (void)aggiorna{
    valueToScore --;
    scoredValue ++;
    //[self.numPuntiVendita setText:[NSString stringWithFormat:@"%i",scoredValue]];
    if (valueToScore == 0) {
        [timer invalidate];
    }
    
}
*/

- (NSString *) timeStampForEntity:(NSString *)propertyNameForEntity {
    NSString *timeStamp = [NSString alloc];
    NSArray *settingsCapture = [self.dbManager selectFromTable:@"Settings" Where:[NSString stringWithFormat:@"propertyName = '%@'",propertyNameForEntity] OrderBy:nil isAscending:0];
    if ([settingsCapture count]>0){
        timeStamp = [[settingsCapture objectAtIndex:0]  valueForKey:@"propertyValue"];
    }else{
        timeStamp = @"0000/00/00 00.00.000";
    }
    
    return timeStamp;
}

- (void)firstViewControllerDismissed {
    NSString *username = [self.dbManager getValueFromSettingsByPropertyName:@"UserName"];
    NSString *password = [self.dbManager getValueFromSettingsByPropertyName:@"Password"];
    
    if (username == nil || password == nil) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}


@end
