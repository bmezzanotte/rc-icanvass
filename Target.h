//
//  Target.h
//  iCanvass
//
//  Created by Bernardino on 07/05/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ActivityHeader, TargetQuestion;

@interface Target : NSManagedObject

@property (nonatomic, retain) NSString * allowMultipleInstances;
@property (nonatomic, retain) NSString * applyToCustomerCategory;
@property (nonatomic, retain) NSDecimalNumber * bonus;
@property (nonatomic, retain) NSString * category;
@property (nonatomic, retain) NSString * details;
@property (nonatomic, retain) NSNumber * disabledForInput;
@property (nonatomic, retain) NSDecimalNumber * goalQuantity;
@property (nonatomic, retain) NSString * goalType;
@property (nonatomic, retain) NSNumber * goalTypeCode;
@property (nonatomic, retain) id guideLines;
@property (nonatomic, retain) NSDate * lastmodified;
@property (nonatomic, retain) NSString * periodCode;
@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSString * subject;
@property (nonatomic, retain) NSString * subjectUnid;
@property (nonatomic, retain) NSDecimalNumber * targetQuantity;
@property (nonatomic, retain) NSString * unid;
@property (nonatomic, retain) NSSet *activity;
@property (nonatomic, retain) NSSet *questions;
@end

@interface Target (CoreDataGeneratedAccessors)

- (void)addActivityObject:(ActivityHeader *)value;
- (void)removeActivityObject:(ActivityHeader *)value;
- (void)addActivity:(NSSet *)values;
- (void)removeActivity:(NSSet *)values;

- (void)addQuestionsObject:(TargetQuestion *)value;
- (void)removeQuestionsObject:(TargetQuestion *)value;
- (void)addQuestions:(NSSet *)values;
- (void)removeQuestions:(NSSet *)values;


//added by BM
- (NSString *)calculateResult;
- (NSArray *)numericQuestionsResult;
- (int) activityCount;
@end
