//
//  Target.m
//  iCanvass
//
//  Created by Bernardino on 07/05/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import "Target.h"
#import "ActivityHeader.h"
#import "TargetQuestion.h"

#import "DataBaseManager.h"
#import "ActivityAnswer.h"


@implementation Target

@dynamic allowMultipleInstances;
@dynamic applyToCustomerCategory;
@dynamic bonus;
@dynamic category;
@dynamic details;
@dynamic disabledForInput;
@dynamic goalQuantity;
@dynamic goalType;
@dynamic goalTypeCode;
@dynamic guideLines;
@dynamic lastmodified;
@dynamic periodCode;
@dynamic status;
@dynamic subject;
@dynamic subjectUnid;
@dynamic targetQuantity;
@dynamic unid;
@dynamic activity;
@dynamic questions;


#pragma mark - code added by BM
- (NSString *)calculateResult{
    DataBaseManager *dbManager = [[DataBaseManager alloc] init];
    NSString *result;
    NSLog(@"goalTypeCode = %@",[self valueForKey:@"goalTypeCode"]);
    switch ([[self valueForKey:@"goalTypeCode"]intValue]) {
        case 0:
             result = NSLocalizedString(@"n/a", @"n/a");
            break;
        case 1:
            result = [NSString stringWithFormat:@"%lu/%d",(unsigned long)[self.activity count],[[self valueForKey:@"goalQuantity"]intValue]];
            break;
        case 2:{
            NSSet *questions = self.questions;
            int numAnswer = 0;
            for (TargetQuestion *question in questions) {
                if ([question valueForKey:@"keyForGoal"]) {
                    NSArray *listaTEMP = [dbManager selectFromTable:@"ActivityAnswer" Where:[NSString stringWithFormat:@"questionDefinitionUnid = '%@'",[question unid]] OrderBy:nil isAscending:0];
                    for (ActivityAnswer *answer in listaTEMP) {
                        numAnswer += [[answer value]intValue];
                    }
                }
            }
            result = [NSString stringWithFormat:@"%d/%d",numAnswer,[[self valueForKey:@"goalQuantity"]intValue]];
        }
            break;
        default:
            break;
    }
    return result;
}

- (int) activityCount{
    return self.activity.count;
}

- (NSArray *)numericQuestionsResult{
    DataBaseManager *dbManager = [[DataBaseManager alloc] init];
    NSMutableArray *result = [[NSMutableArray alloc] init];
    
    NSSet *questions = self.questions;
    for (TargetQuestion *question in questions) {
        NSLog(@"tipo risposta: %@", [question valueForKey:@"type"]);
        
        if ([[question valueForKey:@"type"] isEqualToString:@"Number"]) {
            NSMutableDictionary *questionResult = [[NSMutableDictionary alloc] init];
            int tot = 0;
            NSArray *listaTEMP = [dbManager selectFromTable:@"ActivityAnswer" Where:[NSString stringWithFormat:@"questionDefinitionUnid = '%@'",[question unid]] OrderBy:nil isAscending:0];
            for (ActivityAnswer *answer in listaTEMP) {
                tot +=  [[answer value] intValue];
            }
            
            [questionResult setValue:[NSNumber numberWithInt:tot] forKey:[question valueForKey:@"subject"]];
            [result addObject:questionResult];
        }
    }

    if (result.count == 0) {
        NSMutableDictionary *questionResult = [[NSMutableDictionary alloc] init];
        [questionResult setValue:@"" forKey:@"- nessuna risposta numerica -"];
        [result addObject:questionResult];
    }
    
    return [result copy];
}
@end
