//
//  TargetSync.h
//  iCanvass
//
//  Created by Bernardino on 18/02/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebServiceClient.h"

@class DataBaseManager;

@interface TargetSync : NSObject<NSXMLParserDelegate>{
    NSDictionary *tagToPropertyMapping_Target;
    NSDictionary *tagToPropertyMapping_Question;
    
    NSMutableDictionary *targetObject;
    NSMutableArray *targetList;
    NSMutableDictionary *questionObject;
    NSMutableArray *questionList;
    NSMutableArray *unidArray;
}

@property (nonatomic, retain) DataBaseManager *dbManager;
@property (nonatomic, strong) NSData *wsResponseData;
@property (nonatomic, weak) NSString *syncSessionStarted;
@property (nonatomic, weak) NSString *syncSessionEnded;
@property (nonatomic) RDSyncErrorStatus syncResultErrorStatus;

@property (nonatomic) int newTargets;
@property (nonatomic) int updatedTargets;
@property (nonatomic) int deletedTargets;

- (id)initWithToken:(NSString *)cookieToken;
- (void)syncTable;
- (NSMutableArray *)propertyArray;

@end
