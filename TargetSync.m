//
//  TargetSync.m
//  iCanvass
//
//  Created by Bernardino on 18/02/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import "TargetSync.h"
#import "DataBaseManager.h"
#import "Utility.h"

@interface TargetSync()
@property Utility *util;
@property (nonatomic,strong)NSString *xmlTag;
@property (nonatomic,strong)NSString *xmlListTag;
@end

@implementation TargetSync
@synthesize wsResponseData = _wsResponseData, syncSessionStarted = _syncSessionStarted, util = _util, dbManager = _dbManager;
@synthesize syncResultErrorStatus, xmlTag = _xmlTag, xmlListTag = _xmlListTag;

- (id)initWithToken:(NSString *)cookieToken{
    if (self = [super init]) {
        _util = [[Utility alloc] init];
        _dbManager = [[DataBaseManager alloc]init];
        _syncSessionStarted = [_util getCurrentDateAsString];
        
        NSString *path1 = [[NSBundle mainBundle] pathForResource:@"Target" ofType:@"plist"];
        tagToPropertyMapping_Target = [NSDictionary dictionaryWithContentsOfFile:path1];
        NSString *path2 = [[NSBundle mainBundle] pathForResource:@"TargetQuestion" ofType:@"plist"];
        tagToPropertyMapping_Question = [NSDictionary dictionaryWithContentsOfFile:path2];

        targetList = [[NSMutableArray alloc]init];
        questionList = [[NSMutableArray alloc]init];
        
        //costruisco array UNID record esistenti in CoreData, da inviare al WS
        unidArray = [_dbManager selectFromTable:@"Target" Where:nil OrderBy:nil isAscending:NO];
        
        WebServiceClient *periodsGET = [[WebServiceClient alloc] initWithWsName:@"GetCanvassDataXML2" withMethodName:@"GETTARGETLIST" withTimeStamp:[self timeStampForEntity:@"lastSyncForTarget"] withArrayOfUniversalID:unidArray];
        
        _wsResponseData = [periodsGET getWebServiceResponseWithToken:cookieToken];
        
        NSInteger httpResponseStatusCode = [periodsGET.httpResponse statusCode];
        NSLog(@"http response status code: %ld",(long)httpResponseStatusCode);
        if (httpResponseStatusCode != 200) {
            NSLog(@"Errore WS HTTP error: %ld",(long)httpResponseStatusCode);
            //devo interrompere completamente la sincronizzazione e mostrare a video un messaggio d'errore
            self.syncResultErrorStatus = RDSyncErrorStatusServerError;
        }else{
            if (_wsResponseData == nil){
                NSLog(@"Errore WS: ResponseData VUOTO");
                //devo interrompere soltanto la sincronizzazione di questa tabella e, a fine sincronizzazione, mostrare KO in riepilogo
                self.syncResultErrorStatus = RDSyncErrorStatusNullStringResult;
            }else{
                //proseguo con il parsing XML della risposta
                self.syncResultErrorStatus = RDSyncErrorStatusNoError;
            }
        }
        
    }
    return self;
}

- (void) syncTable {
    NSArray *arrayItem = [self propertyArray];
    BOOL deletionSuccess = NO;
    int updateSuccess;
    
    self.deletedTargets = 0;
    self.updatedTargets = 0;
    self.newTargets = 0;
    
    for (int j = 0; j<[arrayItem count]; j++) {
        NSMutableDictionary *item = [arrayItem objectAtIndex:j];
        
        if ([[item valueForKey:@"unid"] valueForKey:@"value"] != nil) {
            if ([[[item valueForKey:@"status"] valueForKey:@"value"]  isEqualToString: @"99"]){
                NSMutableArray *items = [_dbManager selectFromTable:@"Target" Where:[NSString stringWithFormat:@"unid == '%@'",[item valueForKey:@"unid"]] OrderBy:nil isAscending:0];
                if (items.count > 0) {
                    deletionSuccess = [_dbManager deleteItem:(NSManagedObject *)[items objectAtIndex:0]];
                    if (deletionSuccess == YES){
                        self.deletedTargets ++;
                    }
                }
            }else{
                //updateSuccess = [_dbManager syncItemForTable:@"Target" WithItemDictionary:item];
                //updateSuccess = [_dbManager syncTargetWithItemDictionary:item];
                updateSuccess = [_dbManager syncTargetwithItemDictionary:item];
                switch (updateSuccess) {
                    case 1:
                        self.newTargets ++;
                        break;
                    case 2:
                        self.updatedTargets ++;
                        break;
                    default:
                        break;
                }
            }
        }
    }
    
    if (self.syncResultErrorStatus == RDSyncErrorStatusNoError) {
        _syncSessionEnded = [_util getCurrentDateAsString];
        [_dbManager updateOrInsertSettingWithPropertyName:@"lastSyncForTarget" AndPropertyValue:_syncSessionEnded];
    }
}

- (NSMutableArray *)propertyArray {
    NSXMLParser *parser = [[NSXMLParser alloc]initWithData:_wsResponseData];
    [parser setDelegate:self];
    BOOL success = [parser parse];
    if (success == YES) {
        NSLog(@"Its ok");
        return targetList;
    }else{
        NSLog(@"Its not ok");
        NSError *error = [parser parserError];
        NSLog(@"errore:%@",error);
        return targetList;
    }
}

- (NSString *) timeStampForEntity:(NSString *)propertyNameForEntity {
    NSString *timeStamp = [NSString alloc];
    NSArray *settingsCapture = [_dbManager selectFromTable:@"Settings" Where:[NSString stringWithFormat:@"propertyName = '%@'",propertyNameForEntity] OrderBy:nil isAscending:0];
    if ([settingsCapture count]>0){
        timeStamp = [[settingsCapture objectAtIndex:0]  valueForKey:@"propertyValue"];
    }else{
        timeStamp = @"0000/00/00 00.00.000";
    }
    
    return timeStamp;
}



#pragma Mark - XML Parser

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    if ([elementName isEqualToString:@"faultstring"]) {
        self.syncResultErrorStatus = RDSyncErrorStatusWSThrowFault;
    }else if ([elementName isEqualToString:@"ITEMARRAY"]) {
        targetObject = [[NSMutableDictionary alloc]init];
        _xmlListTag = @"target";
    }else if ([elementName isEqualToString:@"ITEMARRAYQUESTION"]){
        questionObject = [[NSMutableDictionary alloc]init];
        _xmlListTag = @"question";
    }else{
        NSLog(@"ElementName = %@",elementName);
    }
    _xmlTag = elementName;
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)tagValue{
    if ([_xmlListTag isEqualToString:@"target"]) {
        if ([[tagToPropertyMapping_Target allKeys] containsObject:_xmlTag]) {

            NSMutableDictionary *tagDictionary = [[NSMutableDictionary alloc] init];

            if ([targetObject valueForKey:[[tagToPropertyMapping_Target valueForKey:_xmlTag] valueForKey:@"attribute"]] != nil) {
                NSString *keyValue = @"";
                keyValue = [[targetObject valueForKey:[[tagToPropertyMapping_Target valueForKey:_xmlTag] valueForKey:@"attribute"]] valueForKey:@"value"];
                NSLog(@"Element value = %@",keyValue);
                keyValue = [keyValue stringByAppendingString:tagValue];
                
                [tagDictionary setValue:keyValue forKey:@"value"];
            } else {
                [tagDictionary setValue:tagValue forKey:@"value"];
            }

            
            [tagDictionary setValue:[[tagToPropertyMapping_Target valueForKey:_xmlTag] valueForKey:@"type"] forKey:@"type"];
            [targetObject setValue:tagDictionary forKey:[[tagToPropertyMapping_Target valueForKey:_xmlTag] valueForKey:@"attribute"]];

        }
    }else if ([_xmlListTag isEqualToString:@"question"]){
        if ([[tagToPropertyMapping_Question allKeys] containsObject:_xmlTag]){
            
            NSMutableDictionary *tagDictionary = [[NSMutableDictionary alloc] init];

            if ([questionObject valueForKey:[[tagToPropertyMapping_Question valueForKey:_xmlTag] valueForKey:@"attribute"]] != nil) {
                NSString *keyValue = @"";
                keyValue = [[questionObject valueForKey:[[tagToPropertyMapping_Question valueForKey:_xmlTag] valueForKey:@"attribute"]] valueForKey:@"value"];
                NSLog(@"Element value = %@",keyValue);
                keyValue = [keyValue stringByAppendingString:tagValue];
                
                [tagDictionary setValue:keyValue forKey:@"value"];
            } else {
                [tagDictionary setValue:tagValue forKey:@"value"];
            }

            
            [tagDictionary setValue:[[tagToPropertyMapping_Question valueForKey:_xmlTag] valueForKey:@"type"] forKey:@"type"];
            [questionObject setValue:tagDictionary forKey:[[tagToPropertyMapping_Question valueForKey:_xmlTag] valueForKey:@"attribute"]];
            
        }
    }else{
        NSLog(@"xmlListTag = %@ - tagValue = %@",_xmlListTag, tagValue);
    }

}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
    if ([elementName isEqualToString:@"ITEMARRAY"]) {
        [targetObject setValue:[questionList copy] forKey:@"questions"];
        [targetList addObject:targetObject];
        [questionList removeAllObjects];
    }else if ([elementName isEqualToString:@"ITEMARRAYQUESTION"]){
        [questionList addObject:questionObject];
    }
    
    _xmlTag = @"";
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
    NSLog(@"Error: %@", [parseError localizedDescription]);
    self.syncResultErrorStatus = RDSyncErrorStatusXMLParserError;
}

@end
