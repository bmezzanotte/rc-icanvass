//
//  Utility.h
//  iRoyalDays
//
//  Created by NAeS Lavoro on 20/11/13.
//  Copyright (c) 2013 NAeS Lavoro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataBaseManager.h"
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>

@interface Utility : NSObject{
    DataBaseManager *dbMaker;
}

@property (nonatomic, strong) AVAudioPlayer *audioPlayer;
@property (nonatomic, retain) DataBaseManager *dbMaker;

- (NSString *)stringByReplacingOccurrencesOfNull:(NSString *)string;
- (NSString *)autenticazioneConDominoConNomeUtente:(NSString *)username Password:(NSString *)password;
- (NSString *)searchPropertyWithName:(NSString *)name;
- (void)scanSoundAndViberWithSuccess:(BOOL)scansione;
- (NSDate *)getCurrentDate;
//- (NSString *)getStringCurrentData;
- (NSString *)getCurrentDateAsString;
-(NSDate *) dateFromDate:(NSDate *)myDate withHour:(NSInteger)hour minute:(NSInteger)minute second:(NSInteger)second;
- (NSString *)getFormattedDate:(NSString *)source withPattern:(NSString *)pattern;
- (NSString *)getStringFromDate:(NSDate *)date withPattern:(NSString *)pattern;
- (NSDate *)getDateFromString:(NSString *)date withPattern:(NSString *)pattern;
- (BOOL)isDate:(NSDate *)dateToCheck BetweenDate:(NSDate *)earlierDate andDate:(NSDate *)laterDate;
- (NSString *)pseudoID;
- (NSString *)xmlEscapeString:(NSString *)source;
@end
