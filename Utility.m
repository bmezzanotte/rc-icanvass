//
//  Utility.m
//  iRoyalDays
//
//  Created by NAeS Lavoro on 20/11/13.
//  Copyright (c) 2013 NAeS Lavoro. All rights reserved.
//

#import "Utility.h"
#import <sqlite3.h>
#import "DataBaseManager.h"
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>


@implementation Utility

- (NSDate *)getCurrentDate{
    NSDate *currentDateTime = [NSDate date];
    return currentDateTime;
}

-(NSDate *) dateFromDate:(NSDate *)myDate withHour:(NSInteger)hour minute:(NSInteger)minute second:(NSInteger)second
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];

    NSDateComponents *components = [calendar components: NSYearCalendarUnit|
                                    NSMonthCalendarUnit|
                                    NSDayCalendarUnit
                                               fromDate:myDate];
    [components setHour:hour];
    [components setMinute:minute];
    [components setSecond:second];
    NSDate *newDate = [calendar dateFromComponents:components];
    return newDate;
}

- (NSDate *)getDateFromString:(NSString *)date withPattern:(NSString *)pattern{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:pattern];
    NSDate *localDate = [dateFormatter dateFromString:date];
    
    NSTimeInterval timeZoneOffset = [[NSTimeZone defaultTimeZone] secondsFromGMT]; // You could also use the systemTimeZone method
    
    NSTimeInterval gmtTimeInterval = [localDate timeIntervalSinceReferenceDate] + timeZoneOffset;
    NSDate *gmtDate = [NSDate dateWithTimeIntervalSinceReferenceDate:gmtTimeInterval];

    return gmtDate;
}

- (NSString *)getFormattedDate:(NSString *)source withPattern:(NSString *)pattern{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"yyyy/MM/dd HH.mm.ss.SSS"];
    NSDate *myDate = [dateFormatter dateFromString:source];
    [dateFormatter setDateFormat:pattern];
    NSString *formattedDateString = [dateFormatter stringFromDate:myDate];
    NSLog(@"formattedDateString: %@", formattedDateString);
    
    return formattedDateString;
}

- (NSString *)getStringFromDate:(NSDate *)date withPattern:(NSString *)pattern{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"yyyy/MM/dd HH.mm.ss.SSS"];
    [dateFormatter setDateFormat:pattern];
    NSString *formattedDateString = [dateFormatter stringFromDate:date];
    NSLog(@"formattedDateString: %@", formattedDateString);
    
    return formattedDateString;
}

- (NSString *)getCurrentDateAsString{
    // Get current date time
    NSDate *currentDateTime = [NSDate date];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy/MM/dd HH.mm.ss.SSS"];
    NSString *dateInStringFormatted = [dateFormatter stringFromDate:currentDateTime];
    
    return dateInStringFormatted;
}

- (BOOL)isDate:(NSDate *)dateToCheck BetweenDate:(NSDate *)earlierDate andDate:(NSDate *)laterDate
{
    // first check that we are later than the earlierDate.
    if ([dateToCheck compare:earlierDate] == NSOrderedDescending) {
        
        // next check that we are earlier than the laterData
        if ( [dateToCheck compare:laterDate] == NSOrderedAscending ) {
            return YES;
        }
    }
    
    // otherwise we are not
    return NO;
}

- (NSString *)pseudoID{
    NSDate *currentDateTime = [NSDate date];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"'-'ddMMyyyy'T'HHmmssSSS"];
    
    return [NSString stringWithFormat:@"PSEUDO%@", [dateFormatter stringFromDate:currentDateTime]];
}

- (NSString *)dataFilePath{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return [documentsDirectory stringByAppendingPathComponent:@"iCanvass.sqlite"];
}

- (NSString *)stringByReplacingOccurrencesOfNull:(NSString *)string{
    if (string == nil) {
        return @"";
    }else{
        return string;
    }

}

- (NSString *)autenticazioneConDominoConNomeUtente:(NSString *)username Password:(NSString *)password{

    if (username == nil) {
        return nil;
//        username = [self searchPropertyValueByPropertyName:@"UserName"];
    }
    
    if (password == nil) {
        return nil;
//        password = [self searchPropertyValueByPropertyName:@"Password"];
    }

    @try {
        NSHTTPURLResponse *response;
        NSError *error;
        NSMutableURLRequest *request;
        NSString *nomeUtente = [username stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        
        NSString *url = [NSString stringWithFormat:[self searchPropertyWithName:@"LoginURL"],nomeUtente,password];
        NSLog(@"url autenticazione: %@",url);
        request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:60];
        [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
//        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        //NSLog(@"%@", [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding]);
        
        NSHTTPCookieStorage* cookies = [NSHTTPCookieStorage sharedHTTPCookieStorage];
        NSArray* dominoCookies = [cookies cookiesForURL:[NSURL URLWithString:url]];
        //NSLog(@"%d", dominoCookies.count);
        
        NSString *LtpaToken = nil;
        for (NSHTTPCookie *cookie in dominoCookies) {
            NSLog(@"Name: %@ : Value: %@", cookie.name, cookie.value);
            if ([cookie.name isEqualToString:@"LtpaToken"]){
//            if ([cookie.name isEqualToString:[self searchPropertyWithName:@"NomeCookie"]]){
                LtpaToken=cookie.value;
            }
        }
        return LtpaToken;
    }
    @catch (NSException *exception) {
        return nil;
    }
}

- (NSString *)searchPropertyWithName:(NSString *)name{
    dbMaker = [[DataBaseManager alloc]init];
    if ([dbMaker getValueFromSettingsByPropertyName:name] == nil) {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"DefaultSettings" ofType:@"plist"];
        NSDictionary *plistDictionary = [NSDictionary dictionaryWithContentsOfFile:path];
        NSString *propertyValue = [plistDictionary valueForKey:name];
        [dbMaker insertNewSettingWithPropertyName:name AndPropertyValue:propertyValue];
        return propertyValue;
    }else{
        return [dbMaker getValueFromSettingsByPropertyName:name];
    }
}

- (void)scanSoundAndViberWithSuccess:(BOOL)scansione{
    if (!_audioPlayer) {
        NSString *audioPath = nil;
        NSURL *audioURL = nil;
        if (scansione == YES) {
            audioPath = [[NSBundle mainBundle]pathForResource:@"Tick-beep" ofType:@"wav"];
            audioURL = [NSURL fileURLWithPath:audioPath];
        }else{
            audioPath = [[NSBundle mainBundle]pathForResource:@"Error-beep" ofType:@"wav"];
            audioURL = [NSURL fileURLWithPath:audioPath];
        }
        _audioPlayer = [[AVAudioPlayer alloc]initWithContentsOfURL:audioURL error:nil];
    }
    
    [_audioPlayer play];
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
}


- (NSString *)xmlEscapeString:(NSString *)source
{
    NSMutableString *escapeStr = [NSMutableString stringWithString:source];
    
    [escapeStr replaceOccurrencesOfString:@"&"  withString:@"&amp;"  options:NSLiteralSearch range:NSMakeRange(0, [escapeStr length])];
    [escapeStr replaceOccurrencesOfString:@"\"" withString:@"&quot;" options:NSLiteralSearch range:NSMakeRange(0, [escapeStr length])];
    [escapeStr replaceOccurrencesOfString:@"'"  withString:@"&#x27;" options:NSLiteralSearch range:NSMakeRange(0, [escapeStr length])];
    [escapeStr replaceOccurrencesOfString:@">"  withString:@"&gt;"   options:NSLiteralSearch range:NSMakeRange(0, [escapeStr length])];
    [escapeStr replaceOccurrencesOfString:@"<"  withString:@"&lt;"   options:NSLiteralSearch range:NSMakeRange(0, [escapeStr length])];
    
    return [escapeStr copy];
}

@end
