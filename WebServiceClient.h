//
//  WebServiceClient.h
//  iRoyalDays
//
//  Created by NAeS Lavoro on 26/11/13.
//  Copyright (c) 2013 NAeS Lavoro. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, RDSyncErrorStatus) {
    RDSyncErrorStatusNoError,
    RDSyncErrorStatusConnectionError,
    RDSyncErrorStatusNullResponse,
    RDSyncErrorStatusInvalidStatus,
    RDSyncErrorStatusNullStringResult,
    RDSyncErrorStatusNullHttpEntity,
    RDSyncErrorStatusNullInputStream,
    RDSyncErrorStatusWithException,
    RDSyncErrorStatusInvalidAuthentication,
    RDSyncErrorStatusServerError,
    RDSyncErrorStatusXMLParserError,
    RDSyncErrorStatusWSThrowFault
};

@interface WebServiceClient : NSObject{
    NSString *getWsName;
    NSString *sendWsName;
    NSString *getMethodName;
    NSString *sendMethodName;
    NSString *timeStamp;
    NSArray *unidArray;
}

@property (nonatomic, copy) NSString *getWsName;
@property (nonatomic, copy) NSString *sendWsName;
@property (nonatomic, copy) NSString *getMethodName;
@property (nonatomic, copy) NSString *sendMethodName;
@property (nonatomic, copy) NSString *timeStamp;
@property (nonatomic, copy) NSArray *unidArray;

@property (nonatomic,strong) NSHTTPURLResponse *httpResponse;

- (id) initWithWsName:(NSString *)wsName withMethodName:(NSString *)methodName withTimeStamp:(NSString *)lastSynchro withArrayOfUniversalID:(NSArray *)universalIDs;
- (NSData *)getWebServiceResponseWithToken:(NSString *)token;
- (NSData *)getWebServiceResponseWithToken:(NSString *)token withXMLbody:(NSString *)sendxml;
@end
