//
//  WebServiceClient.m
//  iRoyalDays
//
//  Created by NAeS Lavoro on 26/11/13.
//  Copyright (c) 2013 NAeS Lavoro. All rights reserved.
//

#import "WebServiceClient.h"
#import "Utility.h"
#import "TCMXMLWriter.h"

static float wstimeout = 360.0;

@implementation WebServiceClient
@synthesize getWsName,sendWsName,getMethodName,sendMethodName,timeStamp,unidArray;
@synthesize httpResponse = _httpResponse;


- (id) initWithWsName:(NSString *)wsName withMethodName:(NSString *)methodName withTimeStamp:(NSString *)lastSynchro withArrayOfUniversalID:(NSMutableArray *)universalIDs{
    if (self = [super init]) {
        getWsName = wsName;
        getMethodName = methodName;
        timeStamp = lastSynchro;
        unidArray = universalIDs;
    }
    return self;
}


- (NSData *)getWebServiceResponseWithToken:(NSString *)token{
    @try {
        Utility *utility =  [[Utility alloc]init];
        NSString *wsurl = [NSString stringWithFormat:[utility searchPropertyWithName:@"WsURL"],self.getWsName];
        NSString *sendxml = [self writeHTTPrequest];
        
        NSHTTPURLResponse * response;
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:wsurl] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:wstimeout];
        NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[sendxml length]];
        
        [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
        [theRequest setHTTPMethod:@"POST"];
        [theRequest setHTTPBody: [sendxml dataUsingEncoding:NSUTF8StringEncoding]];
        [theRequest setValue:token forHTTPHeaderField:[utility searchPropertyWithName:@"NomeCookie"]];
        NSError *error;
        NSData *responseData = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:&response error:&error];
        NSLog(@"%@", [[NSString alloc] initWithData:responseData encoding:NSASCIIStringEncoding]);
        
        self.httpResponse = response;
        return responseData;
    }
    @catch (NSException *exception) {
        return nil;
    }
}

- (NSData *)getWebServiceResponseWithToken:(NSString *)token withXMLbody:(NSString *)sendxml{
    @try {
        Utility *utility =  [[Utility alloc]init];
        NSString *wsurl = [NSString stringWithFormat:[utility searchPropertyWithName:@"WsURL"],self.getWsName];
        NSHTTPURLResponse * response;
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:wsurl] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:wstimeout];

        NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[sendxml length]];
        
        [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
        [theRequest setHTTPMethod:@"POST"];
        [theRequest setHTTPBody: [sendxml dataUsingEncoding:NSUTF8StringEncoding]];
        [theRequest setValue:token forHTTPHeaderField:[utility searchPropertyWithName:@"NomeCookie"]];
        NSError *error;
        NSData *responseData = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:&response error:&error];
        NSLog(@"%@", [[NSString alloc] initWithData:responseData encoding:NSASCIIStringEncoding]);
        
        self.httpResponse = response;
        return responseData;
    }
    @catch (NSException *exception) {
        return nil;
    }
}


- (NSString *)writeHTTPrequest{
    TCMXMLWriter *xmlWriter = [[TCMXMLWriter alloc]initWithOptions:TCMXMLWriterOptionPrettyPrinted];
    NSString *metodo = [NSString stringWithFormat:@"n0:%@",self.getMethodName];

    [xmlWriter instructXML];
    [xmlWriter tag:@"v:Envelope" attributes:@{@"xmlns:i" : @"http://www.w3.org/2001/XMLSchema-instance", @"xmlns:d" : @"http://www.w3.org/2001/XMLSchema", @"xmlns:c" : @"http://schemas.xmlsoap.org/soap/encoding/", @"xmlns:v" : @"http://schemas.xmlsoap.org/soap/envelope/"} contentBlock:^{
        [xmlWriter tag:@"v:Header" attributes:nil];
        [xmlWriter tag:@"v:Body" attributes:nil contentBlock:^{
            [xmlWriter tag:metodo attributes:@{@"id": @"o0", @"c:root" : @"1", @"xmlns:n0" : @"urn:DefaultNamespace"} contentBlock:^{
                [xmlWriter tag:@"UNIDARRAY" attributes:@{@"i:type": @"c:Array", @"c:arrayType" : [NSString stringWithFormat:@"d:anyType[%lu]", (unsigned long)[unidArray count]]} contentBlock:^{
                    for (NSManagedObject *obj in unidArray){
                        [xmlWriter tag:@"ITEM" attributes:@{@"i:type": @"d:string"} contentText:[obj valueForKey:@"unid"]];
                        /*
                        @try {
                            [xmlWriter tag:@"ITEM" attributes:@{@"i:type": @"d:string"} contentText:[obj valueForKey:@"noteid"]];
                        }
                        @catch (NSException *exception) {
                            [xmlWriter tag:@"ITEM" attributes:@{@"i:type": @"d:string"} contentText:[obj valueForKey:@"noteID"]];
                        }
                         */
                    }
                }];
                [xmlWriter tag:@"TIMESTAMP" attributes:@{@"i:type": @"d:string"} contentText:timeStamp];
            }];
        }];
    }];
    
    NSString *xmlString = [xmlWriter XMLString];
    NSLog(@"xmlString = %@",xmlString);
    return xmlString;
}

@end
