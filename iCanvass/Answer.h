//
//  Answer.h
//  iCanvass
//
//  Created by Bernardino on 06/04/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Answer : NSObject

@property (nonatomic, strong) NSString *unid;
@property (nonatomic, strong) id response;

@property (nonatomic, strong) NSString *subject;
@property (nonatomic, strong) NSString *questionUnid;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *subType;
@property (nonatomic, strong) NSNumber *required;

@end
