//
//  GuideLinesViewController.m
//  iCanvass
//
//  Created by NAeS Lavoro on 18/02/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import "GuideLinesViewController.h"

@interface GuideLinesViewController ()

@end

@implementation GuideLinesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
