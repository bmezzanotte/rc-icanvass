//
//  Activity.h
//  iCanvass
//
//  Created by Bernardino on 12/03/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Answer.h"

@interface Activity : NSObject
@property (nonatomic, strong) NSString *unid;
//@property (nonatomic, strong) NSString *canvass;
//@property (nonatomic, strong) NSString *category;
@property (nonatomic, strong) NSDate *date;
@property (nonatomic, strong) NSString *timeSlot;
@property (nonatomic, strong) NSString *note;
@property (nonatomic, strong) NSString *targetUnid;
@property (nonatomic, strong) NSString *targetSubject;
@property (nonatomic, strong) NSString *targetAllowMultipleInstances;
@property (nonatomic, strong) NSString *status;

@property (nonatomic, strong) NSArray *customers;

@property (nonatomic, strong) NSString *periodCode;

@property (nonatomic, strong) NSMutableArray *answers;

@property (nonatomic, getter = isNewActivity) BOOL newActivity;
@property (nonatomic, strong) NSString *saveErrorMessage;

- (int) save;
- (void) setPeriodCodeWithDate:(NSDate *)date;
@end
