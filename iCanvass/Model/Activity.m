//
//  Activity.m
//  iCanvass
//
//  Created by Bernardino on 12/03/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import "Activity.h"
#import "DataBaseManager.h"
#import "Utility.h"

#import "ActivityHeader.h"
#import "customer.h"

@interface Activity ()

@property (nonatomic, strong) DataBaseManager *dbManager;
@property (nonatomic, strong) Utility *util;

@end

@implementation Activity


- (id) init
{
    self = [super init];
  
    self.dbManager = [[DataBaseManager alloc] init];
    self.util = [[Utility alloc] init];
    
    return self;
}

- (int) save
{
    int result = 0;     //0=failure, 1=success/record added, 2 = success/record updated
    
    //verifico che per il periodo in corso non sia già stata registrata un'attività con stesso negozio e stesso targetUnid
    
    //Il tipo di obiettivo consente la registrazione di più attività sullo stesso PV nello stesso periodo?
    if (![self.targetAllowMultipleInstances isEqualToString:@"1"]) {
        NSString *predicate = @"status != '9'";
        NSArray *activities = [self.dbManager selectFromTable:@"ActivityHeader" Where:predicate OrderBy:nil isAscending:NO];
        if ([activities count]>0) {
            NSLog(@"%@", self.targetSubject);
            NSLog(@"%@", self.targetUnid);
            for (ActivityHeader *activityHeader in activities) {
                NSLog(@"%@",[[activityHeader valueForKey:@"target"] valueForKey:@"subjectUnid"]);
                NSLog(@"%@",[[activityHeader valueForKey:@"target"] valueForKey:@"subject"]);
                if ([self.targetUnid isEqualToString:[[activityHeader valueForKey:@"target"] valueForKey:@"subjectUnid"]] && [self.periodCode isEqualToString:[[activityHeader valueForKey:@"target"] valueForKey:@"periodCode"]]) {
                    //trovato lo stesso tipo di attività, verifico se è stata eseguita sullo stesso PV
                    if (activityHeader.customer) {
                        for (Customer *customer in activityHeader.customer) {
                            NSLog(@"foreign customer: %@", [customer valueForKey:@"code"]);
                            for (Customer *myCustomer in self.customers) {
                                NSLog(@"local customer: %@", [myCustomer valueForKey:@"code"]);
                                if ([[myCustomer valueForKey:@"unid"] isEqualToString:[customer valueForKey:@"unid"] ]) {
                                    if ([activityHeader.unid isEqualToString:self.unid]) {
                                        //ho trovato me stesso
                                        NSLog(@"me stesso");
                                    }else{
                                        //ho trovato un'attività identica (tipo e PV) già registrata
                                        self.saveErrorMessage = [NSString stringWithFormat:@"%@ %@!",
                                                                 NSLocalizedString(@"Hai già registrato questa attività sul Punto Vendita", @"Hai già registrato questa attività sul Punto Vendita"),
                                                                 [myCustomer valueForKey:@"name"]];
                                        return 0;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    //END verifico che per il periodo in corso non sia già stata registrata un'attività con stesso negozio e stesso targetUnid
    
    
    //costrurire dictionary da passare al metodo in DatabaseManager
    NSMutableDictionary *itemData = [[NSMutableDictionary alloc] init];
    /*
    date
    note
    timeSlot
    unid
    */
    NSMutableDictionary *tagDictionary = [[NSMutableDictionary alloc] init];

    [tagDictionary setValue:self.note forKey:@"value"];
    [tagDictionary setValue:@"String" forKey:@"type"];
    [itemData setValue:[tagDictionary copy] forKey:@"note"];

    [tagDictionary setValue:self.timeSlot forKey:@"value"];
    [tagDictionary setValue:@"String" forKey:@"type"];
    [itemData setValue:[tagDictionary copy] forKey:@"timeSlot"];

    if (self.unid) {
        [tagDictionary setValue:self.unid forKey:@"value"];
    }else{
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"ddMMyyyy-hh:mm:ss.sss"];
        [tagDictionary setValue:[NSString stringWithFormat:@"PSEUDO-%@",[dateFormatter stringFromDate:[NSDate date]]] forKey:@"value"];
    }
    [tagDictionary setValue:@"String" forKey:@"type"];
    [itemData setValue:[tagDictionary copy] forKey:@"unid"];

    [tagDictionary setValue:self.date forKey:@"value"];
    [tagDictionary setValue:@"Date" forKey:@"type"];
    [itemData setValue:[tagDictionary copy] forKey:@"date"];

    [tagDictionary setValue:@"1" forKey:@"value"];
    [tagDictionary setValue:@"String" forKey:@"type"];
    [itemData setValue:[tagDictionary copy] forKey:@"status"];
    
    //answers
    NSMutableArray *questions = [[NSMutableArray alloc] init];
    for (Answer *myAnswer in self.answers) {
        //verify if mandatory question has response
        if (myAnswer.required.integerValue == 1) {
            if (! myAnswer.response || [myAnswer.response length] == 0) {
                self.saveErrorMessage = NSLocalizedString(@"La risposta alle domande evidenziate in rosso è obbligatoria", @"La risposta alle domande evidenziate in rosso è obbligatoria");
                return 0;
            }
        }
        
        NSMutableDictionary *answerData = [[NSMutableDictionary alloc] init];
        [answerData setValue:myAnswer.response forKey:@"value"];
        [answerData setValue:myAnswer.type forKey:@"type"];
        
        NSMutableDictionary *questionDictionary = [[NSMutableDictionary alloc] init];
        
        [questionDictionary setValue:[answerData copy] forKey:myAnswer.questionUnid];
        [questions addObject:[questionDictionary copy]];
        
        NSLog(@"response %@", myAnswer.response);
        NSLog(@"tagDictionary value %@", [tagDictionary valueForKey:@"value"]);
        
    }
    [itemData setValue:questions forKey:@"questions"];

    
    [tagDictionary setValue:[NSDate date] forKey:@"value"];
    [tagDictionary setValue:@"Date" forKey:@"type"];
    [itemData setValue:[tagDictionary copy] forKey:@"lastmodified"];

    
    result = [self.dbManager syncActivityWithItemDictionary:itemData withTargetUnid:self.targetUnid withCustomers:self.customers];
    if (result == 0) {
        self.saveErrorMessage = NSLocalizedString(@"Il salvataggio dell'attività non è riuscito. Verificare i dati inseriti e riprovare.", @"Il salvataggio dell'attività non è riuscito. Verificare i dati inseriti e riprovare.");
    }
    return result;
}

- (void) setPeriodCodeWithDate:(NSDate *)date
{
    self.periodCode = nil;
    NSArray *periods = [self.dbManager selectFromTable:@"Period" Where:nil OrderBy:@"dateIn" isAscending:false];

    
    //vietato selezionare date nel futuro
    NSDate *now = [NSDate date];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:now];
    [components setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    [components setHour:23];
    [components setMinute:59];
    [components setSecond:59];
    NSDate *today12pm = [calendar dateFromComponents:components];
    
    if ([date compare:today12pm] == NSOrderedDescending){
        self.periodCode = nil;
        return;
    }
    
    
    /*
     deve esssere possibile selezionare solo date del periodo corrente,
     oppure una data del periodo immediatamente precedente, solo nei primi dieci giorni del periodo corrente
     impossibile selezionare date appartenenti ad altri periodi
     */
    double dayLimit = 10;
    NSDate *myCurrentDate = [self.util getCurrentDate];
    NSDate *currentPeriodStartDate;
    NSDate *cutoffDate;
    NSString *currentPeriodCode = nil;
    NSString *previousPeriodCode = nil;
    
    //in quale periodo siamo oggi?
    for (NSManagedObject *period in periods) {
        NSDate *startDate = [period valueForKey:@"dateIn"];
        NSDate *endDate = [self.util dateFromDate:[period valueForKey:@"dateOut"] withHour:23 minute:59 second:59];
        
        if ([startDate compare:myCurrentDate] == NSOrderedAscending && [endDate compare:myCurrentDate] == NSOrderedDescending) {
            currentPeriodCode = [period valueForKey:@"code"];
            currentPeriodStartDate = startDate;
    
            NSTimeInterval timeInterval = 60 * 60 * 24 * dayLimit;
            cutoffDate = [currentPeriodStartDate dateByAddingTimeInterval:timeInterval];
            
        }
    }
    
    
    //codice del periodo precedente
    //se la data odierna è oltre il 10° giorno del periodo, imposto il codice periodo precedente = codice periodo attuale
    //in modo da impedire registrazioni NON appartenenti al periodo corrente
    if ([myCurrentDate compare:cutoffDate] == NSOrderedDescending) {
        previousPeriodCode = [NSString stringWithString:currentPeriodCode];
    }else{
        NSTimeInterval timeInterval = 60 * 60 * 24 * 2;
        NSDate* lastDayOfPrecedingPeriod = [currentPeriodStartDate dateByAddingTimeInterval: - timeInterval];
        for (NSManagedObject *period in periods) {
            NSDate *startDate = [period valueForKey:@"dateIn"];
            NSDate *endDate = [self.util dateFromDate:[period valueForKey:@"dateOut"] withHour:23 minute:59 second:59];
            
            if ([startDate compare:lastDayOfPrecedingPeriod] == NSOrderedAscending && [endDate compare:lastDayOfPrecedingPeriod] == NSOrderedDescending) {
                previousPeriodCode = [period valueForKey:@"code"];
            }
        }
    }
    
    
    
    //a quale periodo appartiene la data richiesta?
    for (NSManagedObject *period in periods) {
        NSDate *startDate = [period valueForKey:@"dateIn"];
        NSDate *endDate = [self.util dateFromDate:[period valueForKey:@"dateOut"] withHour:23 minute:59 second:59];
        
        if ([startDate compare:date] == NSOrderedAscending && [endDate compare:date] == NSOrderedDescending) {
            //se la data richiesta appartiene al periodo corrente o precedente OK
            if ([[period valueForKey:@"code"] isEqualToString:currentPeriodCode] || [[period valueForKey:@"code"] isEqualToString:previousPeriodCode]) {
                self.periodCode = [period valueForKey:@"code"];
            }else{
                self.periodCode = nil;
            }
            
            return;
        }
    }
}

@end
