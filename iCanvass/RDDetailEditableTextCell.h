//
//  RDDetailEditableTextCell.h
//  iCanvass
//
//  Created by Bernardino on 18/04/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RDDetailSimpleCell.h"

static const float KEYBOARD_HEIGHT = 135.0;
static const float EXTRA_KEYBOARD_HEIGHT = 70.0;

@interface RDDetailEditableTextCell : RDDetailSimpleCell<UITextFieldDelegate>

@property (nonatomic,strong) UITextView *textField;
@property (nonatomic,strong) NSString *key;
@property (nonatomic,strong) id value;

@end
