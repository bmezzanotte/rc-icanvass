//
//  RDDetailNumberCell.m
//  iCanvass
//
//  Created by Bernardino on 05/04/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import "RDDetailNumberCell.h"

#define kLabelTextColor [UIColor colorWithRed:0.321569f green:0.4f blue:0.568627f alpha:1.0f]
#define kBorderTextColor [UIColor colorWithRed:0.8f green:0.8f blue:0.8f alpha:1.0f]
#define kFieldTextColor [UIColor colorWithRed:0.4f green:0.4f blue:0.4f alpha:1.0f]


@implementation RDDetailNumberCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self.label removeFromSuperview];
        
        self.label = [[UILabel alloc] initWithFrame:CGRectMake(12.0, 5.0, 220.0, 42.0)];
        self.label.backgroundColor = [UIColor clearColor];
        self.label.font = [UIFont boldSystemFontOfSize:[UIFont smallSystemFontSize]];
        self.label.textAlignment = NSTextAlignmentLeft;
        self.label.textColor = kLabelTextColor;
        self.label.text = @"label";
        //self.label.layer.backgroundColor = [UIColor greenColor].CGColor;
        [self.contentView addSubview:self.label];
        self.label.numberOfLines = 2;

        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] init];
        [self.label addGestureRecognizer:singleTap];

        
        self.textField = [[UITextField alloc] initWithFrame:CGRectMake(230.0, 5.0, 80.0, 34.0)];
        //        self.textField.borderStyle = UITextBorderStyleLine;
        self.textField.backgroundColor = [UIColor clearColor];
        self.textField.layer.cornerRadius = 5;
        self.textField.layer.borderColor = kBorderTextColor.CGColor;
        self.textField.layer.borderWidth = 1;
        self.textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        self.textField.enabled = YES;
        self.textField.delegate = self;
        self.textField.font = [UIFont boldSystemFontOfSize:[UIFont systemFontSize]];
        self.textField.textColor = kFieldTextColor;
        self.textField.text = @"0";
        self.textField.textAlignment = NSTextAlignmentCenter;
        
        [self.textField setKeyboardType:UIKeyboardTypeDecimalPad];

                
        [self.contentView addSubview:self.textField];
        
    }
    return self;
}

#pragma mark - Gesture Recognizer Method
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.textField resignFirstResponder];
    
}

#pragma mark - UITextFieldViewDelegate Method
- (BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    //Scroll tabella per portare la cella che si sta modificando subito sopra la tastiera
    UITableView *tabella = (UITableView *)self.superview.superview;

    CGRect frame = tabella.frame;
    
    // Start animation
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.3f];
    
    // Reduce size of the Table view
    float myKeyboardHeight = KEYBOARD_HEIGHT;
    
    //correzione per gestire diverso comportamento Activity vs Survey
    //non ho trovato un sistema migliore!
    if (self.maxFrame > 500) {
        myKeyboardHeight += EXTRA_KEYBOARD_HEIGHT;
    }
    frame.size.height -= myKeyboardHeight;
    
    // Apply new size of table view
    tabella.frame = frame;
    
    // Scroll the table view to see the TextField just above the keyboard

        CGRect textFieldRect = [tabella convertRect:textField.bounds fromView:textField];
        [tabella scrollRectToVisible:textFieldRect animated:NO];
    
    
    [UIView commitAnimations];
    
    
    
    /*
    CGPoint pointInTable = [textField.superview convertPoint:textField.frame.origin toView:tabella.superview];
    
    pointInTable.y = pointInTable.y + textField.frame.size.height;
    CGPoint contentOffset = tabella.contentOffset;

    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenHeight = screenRect.size.height;
    
    contentOffset.y = (pointInTable.y - ((float)screenHeight - KEYBOARD_HEIGHT));

    [tabella setContentOffset:contentOffset animated:YES];
    */

    
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    
    UITableView *tabella = (UITableView *)self.superview.superview;
    CGRect frame = tabella.frame;
    
    // Start animation
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.3f];
    
    // Extend size of the Table view
    float myKeyboardHeight = KEYBOARD_HEIGHT;
    
    //correzione per gestire diverso comportamento Activity vs Survey
    //non ho trovato un sistema migliore!
    if (self.maxFrame > 500) {
        myKeyboardHeight += EXTRA_KEYBOARD_HEIGHT;
    }
    frame.size.height += myKeyboardHeight;
    if (frame.size.height > self.maxFrame) {
        frame.size.height = self.maxFrame;
    }
    
    // Apply new size of table view
    tabella.frame = frame;
    
    [UIView commitAnimations];
    
    
    
    self.value = textField.text;
    [self.delegate setQuestion:self.key withValue:textField.text];
    
    [textField resignFirstResponder];

    return YES;
}


- (void)setFieldValueWithString:(NSString *)value
{
    self.textField.text = value;
}

@end
