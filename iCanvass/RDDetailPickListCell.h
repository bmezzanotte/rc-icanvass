//
//  RDDetailPickListCell.h
//  iCanvass
//
//  Created by Bernardino on 05/04/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RDDetailSimpleCell.h"

@interface RDDetailPickListCell : RDDetailSimpleCell

@property (nonatomic,strong) UILabel *fieldValueLabel;
@property (nonatomic,strong) NSString *key;
@property (nonatomic,strong) id value;

- (void)setFieldValueWithString:(NSString *)value;
@end
