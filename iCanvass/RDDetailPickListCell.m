//
//  RDDetailPickListCell.m
//  iCanvass
//
//  Created by Bernardino on 05/04/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import "RDDetailPickListCell.h"

#define kLabelTextColor [UIColor colorWithRed:0.321569f green:0.4f blue:0.568627f alpha:1.0f]

@implementation RDDetailPickListCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
      
        [self.label removeFromSuperview];
        
        self.label = [[UILabel alloc] initWithFrame:CGRectMake(12.0, 5.0, 270.0, 42.0)];
        self.label.backgroundColor = [UIColor clearColor];
        self.label.font = [UIFont boldSystemFontOfSize:[UIFont smallSystemFontSize]];
        self.label.textAlignment = NSTextAlignmentLeft;
        self.label.textColor = kLabelTextColor;
        self.label.text = @"label";
        //        self.label.layer.backgroundColor = [UIColor greenColor].CGColor;
        [self.contentView addSubview:self.label];
        self.label.numberOfLines = 2;

        self.fieldValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(54.0, 5.0, 228.0, 42.0)];
        self.fieldValueLabel.backgroundColor = [UIColor clearColor];
        self.fieldValueLabel.font = [UIFont boldSystemFontOfSize:[UIFont systemFontSize]];
        self.fieldValueLabel.textAlignment = NSTextAlignmentLeft;
        self.fieldValueLabel.textColor = [UIColor lightGrayColor];
        if (! self.value){
            self.fieldValueLabel.textAlignment = NSTextAlignmentCenter;
            self.fieldValueLabel.text = NSLocalizedString(@"- seleziona risposta -", @"- seleziona risposta -");
        }
        //      self.fieldValueLabel.layer.backgroundColor = [UIColor redColor].CGColor;
        [self.contentView addSubview:self.fieldValueLabel];
        
        /*
         self.textField = [[UITextField alloc] initWithFrame:CGRectMake(93.0, 13.0, 220.0, 19.0)];
         //        self.textField.borderStyle = UITextBorderStyleLine;
         self.textField.backgroundColor = [UIColor clearColor];
         self.textField.clearButtonMode = UITextFieldViewModeWhileEditing;
         self.textField.enabled = NO;
         self.textField.font = [UIFont boldSystemFontOfSize:[UIFont systemFontSize]];
         self.textField.text = @"Title";
         [self.contentView addSubview:self.textField];
         */
        
    }
    return self;
}

- (void)setFieldValueWithString:(NSString *)value
{
    self.fieldValueLabel.text = value;
}

@end
