//
//  RDDetailRadioCell.h
//  iCanvass
//
//  Created by Bernardino on 01/05/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RDDetailSimpleCell.h"

@interface RDDetailRadioCell : RDDetailSimpleCell

@property (nonatomic,strong) UILabel *fieldValueLabel;
//@property (nonatomic,strong) UITextField *textField;

@property (nonatomic,strong) NSString *key;
@property (nonatomic,strong) id value;
@end
