//
//  RDDetailSegmentedControlCell.h
//  iCanvass
//
//  Created by Bernardino on 11/01/15.
//  Copyright (c) 2015 NAeS Lavoro. All rights reserved.
//

#import "RDDetailSimpleCell.h"

@interface RDDetailSegmentedControlCell : RDDetailSimpleCell

@property (nonatomic, strong) NSString *key;
@property (nonatomic, strong) id value;

@property (nonatomic, strong) NSArray *controlLabels;
@property (nonatomic, strong) NSArray *controlValues;

@property (nonatomic) NSInteger selectedIndex;


//- (void)appendItems:(NSArray *)items;

@end
