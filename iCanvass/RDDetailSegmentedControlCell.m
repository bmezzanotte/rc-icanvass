//
//  RDDetailSegmentedControllCell.m
//  iCanvass
//
//  Created by Bernardino on 11/01/15.
//  Copyright (c) 2015 NAeS Lavoro. All rights reserved.
//

#import "RDDetailSegmentedControlCell.h"

#define kLabelTextColor [UIColor colorWithRed:0.321569f green:0.4f blue:0.568627f alpha:1.0f]

@interface RDDetailSegmentedControlCell ()
@property (nonatomic,strong) UISegmentedControl *myControl;
@end
@implementation RDDetailSegmentedControlCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self.label removeFromSuperview];
        
        self.label = [[UILabel alloc] initWithFrame:CGRectMake(12.0, 5.0, 120.0, 36.0)];
        self.label.backgroundColor = [UIColor clearColor];
        self.label.font = [UIFont boldSystemFontOfSize:[UIFont smallSystemFontSize]];
        self.label.textAlignment = NSTextAlignmentLeft;
        self.label.textColor = kLabelTextColor;
        self.label.text = @"label";
        //        self.label.layer.backgroundColor = [UIColor greenColor].CGColor;
        [self.contentView addSubview:self.label];
        self.label.numberOfLines = 2;
        
        CGRect controlFrame = CGRectMake(130.0, 5.0, 180.0, 40.0);
        self.myControl = [[UISegmentedControl alloc] initWithFrame:controlFrame];
        
        [self.myControl addTarget:self action:@selector(whichButton:) forControlEvents:UIControlEventValueChanged];
        
        [self.contentView addSubview:self.myControl];
    }
    
    return self;
}

- (void)whichButton:(UISegmentedControl *)sender{
    
    NSInteger selectedIndex = [sender selectedSegmentIndex];
    self.value = [self.controlValues objectAtIndex:selectedIndex];
    
    [self.delegate setQuestion:self.key withValue:self.value];

}

- (void)appendItems:(NSArray *)items{

    if ([self.myControl numberOfSegments] == 0) {
        for (int i=0; i < items.count; i++) {
            [self.myControl insertSegmentWithTitle:[items objectAtIndex:i] atIndex:i animated:true];
        }
    }
    
    
    [self.myControl setSelectedSegmentIndex:self.selectedIndex];

}

@end
