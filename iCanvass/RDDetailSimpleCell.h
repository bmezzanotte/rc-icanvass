//
//  RDDetailSimpleCell.h
//  iCanvass
//
//  Created by Bernardino on 05/04/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RDDetailCellDelegate <NSObject>

- (void)setQuestion:(NSString *)questionUnid withValue:(NSString *)fieldValue;

@end

@interface RDDetailSimpleCell : UITableViewCell

@property (weak, nonatomic) id<RDDetailCellDelegate> delegate;

@property (nonatomic,strong) UILabel *label;
@property (nonatomic,strong) NSString *key;
@property (nonatomic,strong) id value;

//properties an methods for RDDetailSegmentedControllCell (and RDDetailYesNoMultiCell)
@property (nonatomic, strong) NSArray *controlLabels;
@property (nonatomic, strong) NSArray *controlValues;
@property (nonatomic) NSInteger selectedIndex;
- (void)appendItems:(NSArray *)items;

//properties and methods for RDDetailYesNoCell
@property (nonatomic,strong) NSString *controlOnValue;
@property (nonatomic,strong) NSString *controlOffValue;
- (void)setSwitchStatus:(BOOL)status;

//properties and methods for RDDetailYesNoMultiCell
- (void)setSwitchStatus:(BOOL)status atIndex:(int)index;
- (void)appendItems:(NSArray *)labls withValues:(NSArray *)values;


- (void)setFieldValueWithString:(NSString *)value;
- (void)setRedColorForLabel;
- (void)setDefaultColorForLabel;

@property (nonatomic) float maxFrame;

@end
