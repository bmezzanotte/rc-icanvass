//
//  RDDetailSimpleCell.m
//  iCanvass
//
//  Created by Bernardino on 05/04/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import "RDDetailSimpleCell.h"

#define kLabelTextColor [UIColor colorWithRed:0.321569f green:0.4f blue:0.568627f alpha:1.0f]
#define kRedLabelTextColor [UIColor colorWithRed:0.9f green:0.2f blue:0.2f alpha:1.0f]

@implementation RDDetailSimpleCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.label = [[UILabel alloc] initWithFrame:CGRectMake(12.0, 15.0, 280.0, 19.0)];
        self.label.backgroundColor = [UIColor clearColor];
        self.label.font = [UIFont boldSystemFontOfSize:[UIFont smallSystemFontSize]];
        self.label.textAlignment = NSTextAlignmentLeft;
        self.label.textColor = kLabelTextColor;
        self.label.text = @"label";
        //        self.label.layer.backgroundColor = [UIColor greenColor].CGColor;
        [self.contentView addSubview:self.label];
    }
    
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
/*
 - (void)setEditing:(BOOL)editing animated:(BOOL)animated{
 [super setEditing:editing animated:animated];
 self.textField.enabled = editing;
 }
 */

/*
 #pragma mark - Property Overrides
 - (id)value{
 return self.textField.text;
 }
 
 - (void)setValue:(id)aValue{
 self.textField.text = aValue;
 }
 */


- (void)setFieldValueWithString:(NSString *)value
{

}

- (void)setRedColorForLabel
{
    self.label.textColor = kRedLabelTextColor;
}

- (void)setDefaultColorForLabel
{
    self.label.textColor = kLabelTextColor;
}
@end
