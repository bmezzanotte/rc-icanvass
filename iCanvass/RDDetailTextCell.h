//
//  RDDetailTextCell.h
//  iRoyalDays
//
//  Created by Bernardino on 03/01/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RDDetailSimpleCell.h"

@interface RDDetailTextCell : RDDetailSimpleCell

@property (nonatomic,strong) UILabel *fieldValueLabel;
//@property (nonatomic,strong) UITextField *textField;

@property (nonatomic,strong) NSString *key;
@property (nonatomic,strong) id value;
@end
