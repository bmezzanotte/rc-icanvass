//
//  RDDetailTextCell.m
//  iRoyalDays
//
//  Created by Bernardino on 03/01/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import "RDDetailTextCell.h"

#define kLabelTextColor [UIColor colorWithRed:0.321569f green:0.4f blue:0.568627f alpha:1.0f]

@implementation RDDetailTextCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.selectionStyle = UITableViewCellSelectionStyleNone;

        self.fieldValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(95.0, 13.0, 215.0, 19.0)];
        self.fieldValueLabel.backgroundColor = [UIColor clearColor];
        self.fieldValueLabel.font = [UIFont boldSystemFontOfSize:[UIFont systemFontSize]];
        self.fieldValueLabel.textAlignment = NSTextAlignmentLeft;
        self.fieldValueLabel.textColor = kLabelTextColor;
        self.fieldValueLabel.text = @"valueLabel";
        //      self.fieldValueLabel.layer.backgroundColor = [UIColor redColor].CGColor;
        [self.contentView addSubview:self.fieldValueLabel];

        /*
        self.textField = [[UITextField alloc] initWithFrame:CGRectMake(93.0, 13.0, 220.0, 19.0)];
//        self.textField.borderStyle = UITextBorderStyleLine;
        self.textField.backgroundColor = [UIColor clearColor];
        self.textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        self.textField.enabled = NO;
        self.textField.font = [UIFont boldSystemFontOfSize:[UIFont systemFontSize]];
        self.textField.text = @"Title";
        [self.contentView addSubview:self.textField];
         */
    }
    return self;
}

@end
