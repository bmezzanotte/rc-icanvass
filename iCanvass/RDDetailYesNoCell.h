//
//  RDDetailYesNoCell.h
//  iRoyalDays
//
//  Created by Bernardino on 04/01/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RDDetailSimpleCell.h"

@interface RDDetailYesNoCell : RDDetailSimpleCell

@property (nonatomic,strong) NSString *key;
@property (nonatomic,strong) id value;

@property(nonatomic, retain) UIImage *offImage;
@property(nonatomic, retain) UIImage *onImage;

@end
