//
//  RDDetailYesNoCell.m
//  iRoyalDays
//
//  Created by Bernardino on 04/01/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import "RDDetailYesNoCell.h"

#define kLabelTextColor [UIColor colorWithRed:0.321569f green:0.4f blue:0.568627f alpha:1.0f]

@interface RDDetailYesNoCell ()
@property (nonatomic,strong) UISwitch *onoff;
@end
@implementation RDDetailYesNoCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.selectionStyle = UITableViewCellSelectionStyleNone;
       
        [self.label removeFromSuperview];
        
        self.label = [[UILabel alloc] initWithFrame:CGRectMake(12.0, 5.0, 240.0, 36.0)];
        self.label.backgroundColor = [UIColor clearColor];
        self.label.font = [UIFont boldSystemFontOfSize:[UIFont smallSystemFontSize]];
        self.label.textAlignment = NSTextAlignmentLeft;
        self.label.textColor = kLabelTextColor;
        self.label.text = @"label";
        //        self.label.layer.backgroundColor = [UIColor greenColor].CGColor;
        [self.contentView addSubview:self.label];
        self.label.numberOfLines = 2;

        self.onoff = [[UISwitch alloc] initWithFrame: CGRectMake(250.0, 5.0, 0.0, 0.0)];
        
        [self.onoff addTarget: self action: @selector(flip:) forControlEvents:UIControlEventValueChanged];
        [self addSubview: self.onoff];
        
        self.controlOnValue = @"1";
        self.controlOffValue = @"";
        
    }
    
    return self;
}

- (void)flip:(id)sender{
    if([sender isOn]){
        NSLog(@"Switch is ON");
        self.value = self.controlOnValue;
    } else{
        NSLog(@"Switch is OFF");
        self.value = self.controlOffValue;
    }

    [self.delegate setQuestion:self.key withValue:self.value];

}

- (void)setSwitchStatus:(BOOL)status {
    [self.onoff setOn:status];
}
@end
