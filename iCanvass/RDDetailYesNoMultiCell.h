//
//  RDDetailYesNoMultiCell.h
//  iCanvass
//
//  Created by Bernardino on 12/01/15.
//  Copyright (c) 2015 NAeS Lavoro. All rights reserved.
//

#import "RDDetailSimpleCell.h"

@interface RDDetailYesNoMultiCell : RDDetailSimpleCell

@property (nonatomic,strong) NSString *key;
@property (nonatomic,strong) id value;

@property(nonatomic, retain) NSArray *offImages;   //array of UIImage
@property(nonatomic, retain) NSArray *onImages;   //array of UIImage


@end
