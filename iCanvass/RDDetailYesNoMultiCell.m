//
//  RDDetailYesNoMultiCell.m
//  iCanvass
//
//  Created by Bernardino on 12/01/15.
//  Copyright (c) 2015 NAeS Lavoro. All rights reserved.
//

#import "RDDetailYesNoMultiCell.h"

#define kLabelTextColor [UIColor colorWithRed:0.321569f green:0.4f blue:0.568627f alpha:1.0f]

static const float START_Y = 5.0;
static const float DELTA_Y = 44.0;

@interface RDDetailYesNoMultiCell ()
@property (nonatomic,strong) NSMutableArray *onoffs; //Array of UISWitch
@end

@implementation RDDetailYesNoMultiCell

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        // Initialization code
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self.label removeFromSuperview];
        
    }
    
    return self;
}

- (void)appendItems:(NSArray *)labls withValues:(NSArray *)values {
    
    if (!self.onoffs) {
        self.onoffs = [[NSMutableArray alloc] init];
        
        self.controlLabels = labls;
        self.controlValues = values;
        
        float myY = START_Y;
        for (int i=0; i < self.controlLabels.count; i++) {
            
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(12.0, myY, 240.0, 36.0)];
            label.backgroundColor = [UIColor clearColor];
            label.font = [UIFont boldSystemFontOfSize:[UIFont smallSystemFontSize]];
            label.textAlignment = NSTextAlignmentLeft;
            label.textColor = kLabelTextColor;
            label.text = [self.controlLabels objectAtIndex:i];
            //self.label.layer.backgroundColor = [UIColor greenColor].CGColor;
            [self.contentView addSubview:label];
            label.numberOfLines = 1;
            
            
            
            UISwitch *onoff = [[UISwitch alloc] initWithFrame: CGRectMake(250.0, myY, 0.0, 0.0)];
            [self.onoffs addObject:onoff];
            
            [onoff addTarget: self action: @selector(flip:) forControlEvents:UIControlEventValueChanged];
            
            NSString *senderVal = [self.controlValues objectAtIndex:i];
            if ([self.value containsObject:senderVal]) {
                [onoff setOn:true];
//                [self setSwitchStatus:true atIndex:i];
            }
            
            [self addSubview: onoff];
            
            myY += DELTA_Y;
        
        }
    }
}

- (void)flip:(id)sender{

    //which switch?
    int pos = [self.onoffs indexOfObject:sender];
    
    if([sender isOn]){
        NSLog(@"Switch is ON");
        
        
        if (!self.value) {
            NSArray *val = [NSArray arrayWithObject:[self.controlValues objectAtIndex:pos]];
            self.value = [val mutableCopy];
        }else{
            [self.value addObject:[self.controlValues objectAtIndex:pos]];
        }

    } else{
        NSLog(@"Switch is OFF");
        if (self.value) {
            NSString *senderVal = [self.controlValues objectAtIndex:pos];
            int i = [self.value indexOfObject:senderVal];
            [self.value removeObjectAtIndex:i];
        }
    
    }
    
    NSLog(@"FieldValue = %@", self.value);
    [self.delegate setQuestion:self.key withValue:self.value];
    
}

- (void)setSwitchStatus:(BOOL)status atIndex:(int)index{

    UISwitch *onoff = [self.onoffs objectAtIndex:index];
    [onoff setOn:status];
    
}


@end
