//
//  ResultsDetailViewController.h
//  iCanvass
//
//  Created by NAeS Lavoro on 08/05/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Target.h"

@interface ResultsDetailViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, retain) Target *target;
@property (nonatomic, retain) NSString *targetResult;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end
