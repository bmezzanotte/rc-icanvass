//
//  ResultsDetailViewController.m
//  iCanvass
//
//  Created by NAeS Lavoro on 08/05/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import "ResultsDetailViewController.h"
#import "ActivityDetailViewController.h"
#import "Target.h"
#import "TargetQuestion.h"
#import "ActivityHeader.h"
#import "ActivityAnswer.h"
#import "Activity.h"
#import "Utility.h"


@interface ResultsDetailViewController ()

@property (nonatomic, strong) NSArray *activityList;
@property (nonatomic, strong) NSArray *numericQuestions;
@property (nonatomic,strong) Utility *util;
@property (nonatomic, retain) DataBaseManager *dbManager;
@end

@implementation ResultsDetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.util = [[Utility alloc] init];
    self.dbManager = [[DataBaseManager alloc]init];
    
    
    NSPredicate *myPredicate = [NSPredicate predicateWithFormat:@"status != '9'"];
    _activityList = [[_target.activity filteredSetUsingPredicate:myPredicate] allObjects];

//    _numericQuestions = [_target numericQuestionsResult];
}

- (void)viewWillAppear:(BOOL)animated{
    _numericQuestions = [_target numericQuestionsResult];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    }else if (section == 1){
        return _numericQuestions.count;
    }else{
        NSLog(@"righe = %lu",(unsigned long)[_activityList count]);
        return [_activityList count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell;
    if (indexPath.section == 0) {
        NSString *identifier = @"TargetCellIdentifier";
        cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"TargetCellIdentifier"];
        }
        UILabel *targetLabel = (UILabel *)[cell viewWithTag:11];
        targetLabel.text = [_target valueForKey:@"subject"];
        UILabel *targetResultLabel = (UILabel *)[cell viewWithTag:12];
        targetResultLabel.text = _targetResult;
      
    } else if (indexPath.section == 1){
        NSString *identifier = @"TargetCellIdentifier";
        cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"TargetCellIdentifier"];
        }
        UILabel *targetLabel = (UILabel *)[cell viewWithTag:11];
        
        NSDictionary *dict = [_numericQuestions objectAtIndex:[indexPath row]];
        targetLabel.text = [[dict allKeys] objectAtIndex:0];
        
        UILabel *targetResultLabel = (UILabel *)[cell viewWithTag:12];
        targetResultLabel.text = [NSString stringWithFormat:@"%@", [dict valueForKey:targetLabel.text]];

    
    }else{
        NSString *identifier = @"ActivityCellIdentifier";
        cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"ActivityCellIdentifier"];
        }
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        ActivityHeader *activity = [_activityList objectAtIndex:indexPath.row];
        
        UILabel *dateActivityLabel = (UILabel *)[cell viewWithTag:1];
        UILabel *targetLabel = (UILabel *)[cell viewWithTag:2];
        UILabel *customersLabel = (UILabel *)[cell viewWithTag:3];
        UILabel *addressCustomerLabel = (UILabel *)[cell viewWithTag:4];
        
        NSSet *activityCustomerSet = activity.customer;
        
        dateActivityLabel.text = [self.util getStringFromDate:activity.date withPattern:@"dd/MM/yyyy"];
        targetLabel.text = _target.subject;
        
        if ([activityCustomerSet count] == 1) {
            NSManagedObject *customer = [[activityCustomerSet allObjects]objectAtIndex:0];
            customersLabel.text = [customer valueForKey:@"name"];
            addressCustomerLabel.text = [NSString stringWithFormat:@"%@ - %@",[customer valueForKey:@"city"],[customer valueForKey:@"address"]];
        }else{
            [addressCustomerLabel setAlpha:0.0];
            [customersLabel setFrame:CGRectMake(customersLabel.frame.origin.x, customersLabel.frame.origin.y, customersLabel.frame.size.width, 40)];
            customersLabel.numberOfLines = 0;
            NSArray *listCustomer = [[activityCustomerSet allObjects] valueForKey:@"name"];
            //    customersLabel.text = [[listCustomer objectAtIndex:0] valueForKey:@"name"];
            customersLabel.text =[listCustomer componentsJoinedByString:@", "];
        }
    }
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return NSLocalizedString(@"Target", @"Target");
    }else if (section == 1) {
        return NSLocalizedString(@"Conteggio risposte numeriche", @"Conteggio risposte numeriche");
    }else{
        return NSLocalizedString(@"Dettaglio attività svolte", @"Dettaglio attività svolte");
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 2) {
        return 110;
    }else{
        return 60;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0 || indexPath.section==1){
        return NO;
    }else{
        // Return NO if you do not want the specified item to be editable.
        ActivityHeader *activity = [_activityList objectAtIndex:indexPath.row];
        if ([activity.status integerValue] > 1) {
            return NO;
        }else{
            return YES;
        }
    }
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        ActivityHeader *activity = [_activityList objectAtIndex:indexPath.row];
        [_dbManager updateItem:activity WithUpdateData:[NSDictionary dictionaryWithObject:@"9" forKey:@"status"]];
        
        NSPredicate *myPredicate = [NSPredicate predicateWithFormat:@"status != '9'"];
        _activityList = [[_target.activity filteredSetUsingPredicate:myPredicate] allObjects];
        
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"DetailActivitySegue"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        
        ActivityHeader *myActivityHeader = [_activityList objectAtIndex:indexPath.row];
        
        Activity *myActivity = [[Activity alloc] init];
        myActivity.unid = myActivityHeader.unid;
        myActivity.date = myActivityHeader.date;
        [myActivity setPeriodCodeWithDate:myActivity.date];
        myActivity.timeSlot = myActivityHeader.timeSlot;
        myActivity.note = myActivityHeader.note;
        myActivity.targetUnid = myActivityHeader.target.subjectUnid;
        myActivity.targetSubject = myActivityHeader.target.subject;
        myActivity.status = myActivityHeader.status;
        
        NSSet *myCustomers = myActivityHeader.customer;
        NSMutableArray *customerArray = [[NSMutableArray alloc] init];
        for (NSManagedObject *myCustomer in myCustomers) {
            [customerArray addObject:myCustomer];
        }
        myActivity.customers = [customerArray copy];
        
        //compilazione questionario
        NSSet *myAnswers = myActivityHeader.answers;
        //        NSSortDescriptor *sortDescriptor;
        //        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"subject" ascending:YES];
        //        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        NSArray *answersArray = [myAnswers allObjects];
        
        myActivity.answers = [[NSMutableArray alloc] init];
        for (ActivityAnswer *myAnswer in answersArray) {
            NSLog(@"answer value: %@", myAnswer.value);
            NSLog(@"answer type: %@", myAnswer.type);
            
            Answer *answerObject = [[Answer alloc] init];
            answerObject.questionUnid = myAnswer.questionDefinitionUnid;
            
            NSArray *myQuestions = [self.dbManager selectFromTable:@"TargetQuestion"
                                                             Where:[NSString stringWithFormat:@"unid == '%@'",myAnswer.questionDefinitionUnid]
                                                           OrderBy:@"subject"
                                                       isAscending:YES];
            
            if (myQuestions) {
                TargetQuestion *myQuestion =  [myQuestions objectAtIndex:0];
                
                answerObject.subject = myQuestion.subject ;
                answerObject.required = myQuestion.required;
                answerObject.type = myAnswer.type;
                
                answerObject.response = myAnswer.value;
                
                [myActivity.answers addObject:answerObject];
            }
        }
        
        NSSortDescriptor *sortDescriptor;
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"subject" ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        myActivity.answers = [[myActivity.answers sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
        
        ActivityDetailViewController *destination = [segue destinationViewController];
        destination.activity = myActivity;
        
        destination.activity.newActivity = NO;
    }
}


@end
