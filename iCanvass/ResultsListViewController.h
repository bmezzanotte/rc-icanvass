//
//  ResultsListViewController.h
//  iCanvass
//
//  Created by NAeS Lavoro on 07/05/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResultsListViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, UIPickerViewDataSource,UIPickerViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIPickerView *periodPicker;
- (IBAction)doubleTapGesture:(id)sender;
@end
