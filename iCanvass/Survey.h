//
//  Survey.h
//  iCanvass
//
//  Created by Bernardino on 26/12/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Answer.h"

@interface Survey : NSObject
@property (nonatomic, strong) NSString *unid;
@property (nonatomic, strong) NSDate *date;
@property (nonatomic, strong) NSString *note;
@property (nonatomic, strong) NSString *status;

@property (nonatomic, strong) NSArray *customers;

@property (nonatomic, strong) NSMutableArray *answers;

@property (nonatomic, getter = isNewSurveyy) BOOL newSurvey;
@property (nonatomic, strong) NSString *saveErrorMessage;

- (int) save;
@end
