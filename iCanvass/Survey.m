//
//  Survey.m
//  iCanvass
//
//  Created by Bernardino on 26/12/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import "Survey.h"
#import "DataBaseManager.h"
#import "Utility.h"

#import "SurveyHeader.h"
#import "customer.h"

static NSString *ENTITY_NAME_HEADER = @"SurveyHeader";

@interface Survey ()

@property (nonatomic, strong) DataBaseManager *dbManager;
@property (nonatomic, strong) Utility *util;

@end
@implementation Survey
- (id) init
{
    self = [super init];
    
    self.dbManager = [[DataBaseManager alloc] init];
    self.util = [[Utility alloc] init];
    
    return self;
}



- (int) save
{
    int result = 0;     //0=failure, 1=success/record added, 2 = success/record updated, 3=success/form incomplete (required fields missing)
    bool mandatoryFieldsFailure = false;
    
    //Verify if a survey on same customer and same date already exists
    Customer *myCustomer = [self.customers objectAtIndex:0];
    NSString *myCustomerUnid = myCustomer.unid;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@ and %K != %@", @"customer.unid", myCustomerUnid, @"status", @"9"];
    NSArray *activities = [self.dbManager selectFromTable:ENTITY_NAME_HEADER withPredicate:predicate OrderBy:nil isAscending:NO];
    if ([activities count]>0) {
        if (![[[activities objectAtIndex:0] unid] isEqualToString:self.unid]) {
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"dd/MM/yyyy"];
            
            NSString *oldSurveyDate = [dateFormatter stringFromDate:[[activities objectAtIndex:0] date]];
            NSString *newSurveyDate = [dateFormatter stringFromDate:self.date];

            if ([oldSurveyDate isEqualToString:newSurveyDate]) {
                NSLog(@"Survey for customer %@ in date %@ already exists!", myCustomer.name, self.date);
                self.saveErrorMessage = NSLocalizedString(@"E' già stato registrato un censimento in questa data, su questo Punto Vendita. Impossibile Proseguire", @"messaggio errore censimento doppio");
                result = 0;
                return result;
            }
            
        }
    }
    
    
    //costrurire dictionary da passare al metodo in DatabaseManager
    NSMutableDictionary *itemData = [[NSMutableDictionary alloc] init];
    /*
     date
     note
     unid
     */
    NSMutableDictionary *tagDictionary = [[NSMutableDictionary alloc] init];
    
    [tagDictionary setValue:self.note forKey:@"value"];
    [tagDictionary setValue:@"String" forKey:@"type"];
    [itemData setValue:[tagDictionary copy] forKey:@"note"];
    

    if (self.unid) {
        [tagDictionary setValue:self.unid forKey:@"value"];
    }else{
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"ddMMyyyy-hh:mm:ss.sss"];
        NSString *unid = [NSString stringWithFormat:@"PSEUDO-%@",[dateFormatter stringFromDate:[NSDate date]]];
        [tagDictionary setValue:unid forKey:@"value"];
        self.unid = unid;
    }
    [tagDictionary setValue:@"String" forKey:@"type"];
    [itemData setValue:[tagDictionary copy] forKey:@"unid"];
    
    [tagDictionary setValue:self.date forKey:@"value"];
    [tagDictionary setValue:@"Date" forKey:@"type"];
    [itemData setValue:[tagDictionary copy] forKey:@"date"];
    
    [tagDictionary setValue:self.status forKey:@"value"];
    [tagDictionary setValue:@"String" forKey:@"type"];
    [itemData setValue:[tagDictionary copy] forKey:@"status"];
    
    //TODO - answers
    NSMutableArray *questions = [[NSMutableArray alloc] init];
    for (Answer *myAnswer in self.answers) {
        //verify if mandatory question has response
        if (myAnswer.required.integerValue == 1) {
            if (! myAnswer.response || [myAnswer.response length] == 0) {
                self.saveErrorMessage = NSLocalizedString(@"La risposta alle domande evidenziate in rosso è obbligatoria. Impossibile inviare.", @"La risposta alle domande evidenziate in rosso è obbligatoria");
                
                //reset status to "DRAFT (BOZZA)"
                [tagDictionary setValue:@"0" forKey:@"value"];
                [tagDictionary setValue:@"String" forKey:@"type"];
                [itemData setValue:[tagDictionary copy] forKey:@"status"];
                
                mandatoryFieldsFailure = true;
            }
        }
        
        NSMutableDictionary *answerData = [[NSMutableDictionary alloc] init];
        [answerData setValue:myAnswer.subject forKey:@"subject"];
        [answerData setValue:myAnswer.response forKey:@"value"];
        [answerData setValue:myAnswer.type forKey:@"type"];
        
        /*
        if ([myAnswer.subject isEqualToString:@"visibilitylayoutok"]) {
            NSLog(@"QUI");
        }
        */
        
        [answerData setValue:myAnswer.required forKey:@"required"];
        [answerData setValue:myAnswer.unid forKey:@"unid"];
        

        [questions addObject:[answerData copy]];
        NSLog(@"response %@", myAnswer.response);
        
    }
    [itemData setValue:questions forKey:@"questions"];
    
    
    [tagDictionary setValue:[NSDate date] forKey:@"value"];
    [tagDictionary setValue:@"Date" forKey:@"type"];
    [itemData setValue:[tagDictionary copy] forKey:@"lastmodified"];
    
    NSString *customerUnid = [[self.customers objectAtIndex:0] unid];
    
    result = [self.dbManager syncSurveyWithItemDictionary:itemData withCustomerUnid:customerUnid];
    if (result == 0) {
        self.saveErrorMessage = NSLocalizedString(@"Il salvataggio dell'attività non è riuscito. Verificare i dati inseriti e riprovare.", @"Il salvataggio dell'attività non è riuscito. Verificare i dati inseriti e riprovare.");
        return result;
    }

    if (mandatoryFieldsFailure) {
        result = 3;
    }
    
    return result;
}



@end
