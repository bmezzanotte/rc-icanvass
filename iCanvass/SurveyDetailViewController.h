//
//  SurveyDetailViewController.h
//  iCanvass
//
//  Created by Bernardino on 28/12/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Survey.h"
#import "Answer.h"

#import "CustomerActivityViewController.h"
#import "PickListViewController.h"
#import "NoteActivityViewController.h"

#import "RDDetailSimpleCell.h"

@interface SurveyDetailViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate, CustomerActivityDelegate, NoteActivityDelegate>{
    
}

@property (nonatomic,strong) Survey *survey;
@property (nonatomic) bool saveFailure;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *editButton;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)editAction:(id)sender;

@end
