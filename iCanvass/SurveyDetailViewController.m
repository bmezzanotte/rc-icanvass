//
//  SurveyDetailViewController.m
//  iCanvass
//
//  Created by Bernardino on 28/12/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import "SurveyDetailViewController.h"
#import "SurveyQuestionsPageViewController.h"
#import "DataBaseManager.h"
#import "Utility.h"

#define kLabelTextColor [UIColor colorWithRed:0.321569f green:0.4f blue:0.568627f alpha:1.0f]
#define kRedLabelTextColor [UIColor colorWithRed:0.9f green:0.2f blue:0.2f alpha:1.0f]

#define kPickerAnimationDuration    0.40   // duration for the animation to slide the date picker into view
#define kDatePickerTag              99     // view tag identifiying the date picker view

#define kTitleKey       @"label"   // key for obtaining the data source item's title
#define kDateKey        @"date"    // key for obtaining the data source item's date value
#define kValueKey       @"value"
#define kHeightRow      @"heightRow"
#define kNumberOfLines  @"numberOfLines"

// keep track of which rows have date cells
#define kDateStartRow   0
#define kDateEndRow     0

static NSString *kCustomerSegue = @"chooseCustomerSegue";
static NSString *kQuestionInputSegue = @"questionInputSegue";
static NSString *kNoteActivitySegue = @"NoteActivityIdentifier";

static NSString *kDateCellID = @"dateCell";     // the cells with date
static NSString *kDatePickerID = @"datePicker"; // the cell containing the date picker
static NSString *kOtherCell = @"otherCell";     // the remaining cells at the end
static NSString *kNumberCell = @"numberCell";     // the remaining cells at the end



@interface SurveyDetailViewController ()
@property (nonatomic, retain) NSString *pickerListValueChoosed;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *saveButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cancelButton;

- (IBAction)saveAction:(id)sender;
- (IBAction)cancelAction:(id)sender;

@property (nonatomic, strong) NSMutableArray *headerDataArray;
@property (nonatomic, strong) NSMutableArray *questionGroupDataArray;
@property (nonatomic, strong) NSDateFormatter *dateFormatter;

// keep track which indexPath points to the cell with UIDatePicker
@property (nonatomic, strong) NSIndexPath *datePickerIndexPath;
@property (assign) NSInteger pickerCellRowHeight;


@property (nonatomic, strong) NSArray *sections;
@property (nonatomic, strong) NSArray *headerPattern;
@property (nonatomic, strong) NSArray *questionGroupPattern;

@property (nonatomic, strong) DataBaseManager *dbManager;
@property (nonatomic, strong) Utility *util;

@end



@implementation SurveyDetailViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    //hide back button
    self.navigationItem.hidesBackButton = YES;
    [self.navigationItem setTitle:NSLocalizedString(@"Dettaglio Censimento", @"Titolo navigationBar nel dettaglio censimento")];
    [self.editButton setTitle:NSLocalizedString(@"Note", @"Note")];
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString  *documentsDirectory = [paths objectAtIndex:0];
    NSString  *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,@"SurveyDetailTableConfiguration.plist"];
    NSDictionary *plist = [NSDictionary dictionaryWithContentsOfFile:filePath];
    /*
    NSURL *plistURL = [[NSBundle mainBundle] URLForResource:@"SurveyDetailTableConfiguration" withExtension:@"plist"];
    NSDictionary *plist = [NSDictionary dictionaryWithContentsOfURL:plistURL];
    */
    
    self.sections = [plist valueForKey:@"sections"];
    
    NSLog(@"num sezioni:%lu",(unsigned long)[self.sections count]);
    
    self.dbManager = [[DataBaseManager alloc] init];
    self.util = [[Utility alloc] init];
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setDateStyle:NSDateFormatterShortStyle];    // show short-style date format
    [self.dateFormatter setTimeStyle:NSDateFormatterNoStyle];
    
    self.headerPattern = [[self.sections objectAtIndex:0] valueForKey:@"rows"];
    self.questionGroupPattern = [[self.sections objectAtIndex:1] valueForKey:@"rows"];

    self.saveButton.title = NSLocalizedString(@"Salva", @"Salva");
    
    int statusActivity = [[_survey valueForKey:@"status"] intValue];
    if (statusActivity < 1) {
        [self.saveButton setEnabled:YES];
        
        if (self.survey.isNewSurveyy) {
            [self.cancelButton setTitle:NSLocalizedString(@"Annulla", @"Annulla")];
        }else{
            [self.cancelButton setTitle:NSLocalizedString(@"Chiudi", @"Chiudi")];
        }
        
    }else{
        [self.saveButton setEnabled:NO];
        [self.cancelButton setTitle:NSLocalizedString(@"Chiudi", @"Chiudi")];
    }
    
    // obtain the picker view cell's height, works because the cell was pre-defined in our storyboard
    UITableViewCell *pickerViewCellToCheck = [self.tableView dequeueReusableCellWithIdentifier:kDatePickerID];
    self.pickerCellRowHeight = pickerViewCellToCheck.frame.size.height;
    
    // if the local changes while in the background, we need to be notified so we can update the date
    // format in the table view cells
    //
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(localeChanged:)
                                                 name:NSCurrentLocaleDidChangeNotification
                                               object:nil];
    
    
    [self prepareDataForDisplay];

}

- (void) prepareDataForDisplay
{
    // setup Survey Header data source
    self.headerDataArray = [[NSMutableArray alloc] init];
    for (NSDictionary *elem in self.headerPattern) {
        if ([[elem valueForKey:@"key"] isEqualToString:@"date"]) {
            NSDate *activityDate = self.survey.date;
            if (activityDate) {
                [self.headerDataArray addObject:[@{@"label":[elem valueForKey:@"label"],@"date":activityDate} mutableCopy]];
            }else{
                [self.headerDataArray addObject:[@{@"label":[elem valueForKey:@"label"],@"date":[NSDate date]} mutableCopy]];
            }
            
        }else{
            if ([self.survey respondsToSelector:(SEL)NSSelectorFromString([elem valueForKey:@"key"])]) {
                NSString *testValue = [self.survey performSelector:(SEL)NSSelectorFromString([elem valueForKey:@"key"])];
                if (testValue) {
                    if ([[self.survey performSelector:(SEL)NSSelectorFromString([elem valueForKey:@"key"])] isKindOfClass:[NSArray class]]) {
                        NSArray *testValueArray = [self.survey performSelector:(SEL)NSSelectorFromString([elem valueForKey:@"key"])];
                        testValue = @"";
                        for (NSManagedObject *customer in testValueArray) {
                            if ([testValue isEqualToString:@""]) {
                                testValue = [testValue stringByAppendingString:[customer valueForKey:@"name"]];
                            }else{
                                testValue = [testValue stringByAppendingString:[NSString stringWithFormat:@", %@",[customer valueForKey:@"name"]]];
                            }
                        }
                        
                        NSLog(@"%@",testValue);
                        [self.headerDataArray addObject:[@{@"label":[elem valueForKey:@"label"],@"value":testValue} mutableCopy]];
                    }else{
                        [self.headerDataArray addObject:[@{@"label":[elem valueForKey:@"label"],@"value":testValue} mutableCopy]];
                    }
                    
                }else{
                    if ([elem valueForKey:@"rowHeight"] != nil) {
                        if ([elem valueForKey:@"numberOfLines"] != nil) {
                            [self.headerDataArray addObject:[@{@"label":[elem valueForKey:@"label"],@"value":[elem valueForKey:@"placeholder"], @"rowHeight":[elem valueForKey:@"rowHeight"],@"numberOfLines":[elem valueForKey:@"numberOfLines"]} mutableCopy]];
                        }else{
                            [self.headerDataArray addObject:[@{@"label":[elem valueForKey:@"label"],@"value":[elem valueForKey:@"placeholder"], @"rowHeight":[elem valueForKey:@"rowHeight"]} mutableCopy]];
                        }
                    }else{
                        if ([elem valueForKey:@"numberOfLines"] != nil) {
                            [self.headerDataArray addObject:[@{@"label":[elem valueForKey:@"label"],@"value":[elem valueForKey:@"placeholder"],@"numberOfLines":[elem valueForKey:@"numberOfLines"]} mutableCopy]];
                        }else{
                            [self.headerDataArray addObject:[@{@"label":[elem valueForKey:@"label"],@"value":[elem valueForKey:@"placeholder"]} mutableCopy]];
                        }
                    }
                }
            }else{
                [self.headerDataArray addObject:[@{@"label":[elem valueForKey:@"label"],@"value":[elem valueForKey:@"placeholder"]} mutableCopy]];
            }
        }
    }
    
    
    
    //Init answers is survey is new
    if (!self.survey.answers) {
        self.survey.answers = [[NSMutableArray alloc] init];
        
        NSArray *questionGroups = [[[self.sections objectAtIndex:1] valueForKey:@"rows"] valueForKey:@"groups"];
                                    
        for (int i=0; i < questionGroups.count; i++) {
            NSArray *questionGroup = [[questionGroups objectAtIndex:i] valueForKey:@"questions"];
            if (questionGroup != [NSNull null]) {
                for (int j=0; j < questionGroup.count; j++) {
                    NSArray *questionSubjects = [[questionGroup objectAtIndex:j] valueForKey:@"fieldName"];
                    NSArray *questionTypes = [[questionGroup objectAtIndex:j] valueForKey:@"type"];
                    NSArray *questionRequired = [[questionGroup objectAtIndex:j] valueForKey:@"required"];

                    int x = 0;
                    if (questionSubjects != [NSNull null]) {
                        for (NSString *subject in questionSubjects) {
                            NSLog(@"subject = %@", subject);
                            
                            Answer *myAnswer = [[Answer alloc] init];
                            myAnswer.subject = subject;
                            
                            
                            if ([myAnswer.subject isEqualToString:@"visibilityframesdate"]) {
                                NSLog(@"QUI");
                            }
                            

                            if ([questionTypes objectAtIndex:x] != [NSNull null]) {
                                myAnswer.type = [questionTypes objectAtIndex:x];
                            }else{
                                myAnswer.type = @"String";
                            }

                            if ([questionRequired objectAtIndex:x] != [NSNull null]) {
                                myAnswer.required = [questionRequired objectAtIndex:x];
                            }else{
                                myAnswer.required = 0;
                            }

                            
                            if ([myAnswer.type isEqualToString:@"Number"] || [myAnswer.type isEqualToString:@"Integer"]) {
                                myAnswer.response = 0;
                            }
                            
                            myAnswer.unid = [NSString stringWithFormat:@"%@_%@", self.survey.unid, myAnswer.subject];
                            
                            
                            
                            myAnswer.questionUnid = @"";
                            myAnswer.subType = @"";
                            
                            [self.survey.answers addObject:myAnswer];
                            x++;
                        }
                    }
                    

                    
                }
            }
        }
    }


    
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:NSCurrentLocaleDidChangeNotification
                                                  object:nil];
}


#pragma mark - Locale

/*! Responds to region format or locale changes.
 */
- (void)localeChanged:(NSNotification *)notif
{
    // the user changed the locale (region format) in Settings, so we are notified here to
    // update the date format in the table view cells
    //
    [self.tableView reloadData];
}


#pragma mark - inline DatePicker Utilities

/*! Determines if the given indexPath has a cell below it with a UIDatePicker.
 
 @param indexPath The indexPath to check if its cell has a UIDatePicker below it.
 */
- (BOOL)hasPickerForIndexPath:(NSIndexPath *)indexPath
{
    BOOL hasDatePicker = NO;
    
    NSInteger targetedRow = indexPath.row;
    targetedRow++;
    
    UITableViewCell *checkDatePickerCell =
    [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:targetedRow inSection:0]];
    UIDatePicker *checkDatePicker = (UIDatePicker *)[checkDatePickerCell viewWithTag:kDatePickerTag];
    
    hasDatePicker = (checkDatePicker != nil);
    return hasDatePicker;
}

/*! Updates the UIDatePicker's value to match with the date of the cell above it.
 */
- (void)updateDatePicker
{
    if (self.datePickerIndexPath != nil)
    {
        UITableViewCell *associatedDatePickerCell = [self.tableView cellForRowAtIndexPath:self.datePickerIndexPath];
        
        UIDatePicker *targetedDatePicker = (UIDatePicker *)[associatedDatePickerCell viewWithTag:kDatePickerTag];
        if (targetedDatePicker != nil)
        {
            // we found a UIDatePicker in this cell, so update it's date value
            //
            NSDictionary *itemData = self.headerDataArray[self.datePickerIndexPath.row - 1];
            [targetedDatePicker setDate:[itemData valueForKey:kDateKey] animated:NO];
        }
    }
}

/*! Determines if the UITableViewController has a UIDatePicker in any of its cells.
 */
- (BOOL)hasInlineDatePicker
{
    if (self.datePickerIndexPath != nil) {
        NSLog(@"inline date picker in section:%ld at row:%ld",(long)self.datePickerIndexPath.section, (long)self.datePickerIndexPath.row);
    }
    
    return (self.datePickerIndexPath != nil);
}

/*! Determines if the given indexPath points to a cell that contains the UIDatePicker.
 
 @param indexPath The indexPath to check if it represents a cell with the UIDatePicker.
 */
- (BOOL)indexPathHasPicker:(NSIndexPath *)indexPath
{
    return ([self hasInlineDatePicker] && self.datePickerIndexPath.row == indexPath.row);
}

/*! Determines if the given indexPath points to a cell that contains the start/end dates.
 
 @param indexPath The indexPath to check if it represents start/end date cell.
 */
- (BOOL)indexPathHasDate:(NSIndexPath *)indexPath
{
    BOOL hasDate = NO;
    
    if ((indexPath.row == kDateStartRow) ||
        (indexPath.row == kDateEndRow || ([self hasInlineDatePicker] && (indexPath.row == kDateEndRow + 1))))
    {
        hasDate = YES;
    }
    
    return hasDate;
}



#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.sections count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return NSLocalizedString(@"Intestazione", @"Intestazione");
    }else{
        return NSLocalizedString(@"Questionario", @"Questionario");
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    CGFloat rowH = 44.0;

    if (indexPath.section == 0){
        if ([self indexPathHasPicker:indexPath]) {
            rowH = self.pickerCellRowHeight;
        }else{
            NSInteger modelRow = indexPath.row;
            if (self.datePickerIndexPath != nil && self.datePickerIndexPath.row < indexPath.row) {
                modelRow--;
            }
            
            if ([[self.headerPattern objectAtIndex:modelRow] valueForKey:@"rowHeight"]) {
                NSString *rowHeightString = [[self.headerPattern objectAtIndex:modelRow] valueForKey:@"rowHeight"];
                rowH = [rowHeightString floatValue];
            }else{
                rowH = self.tableView.rowHeight;
            }
        }
//        rowH = ([self indexPathHasPicker:indexPath] ? self.pickerCellRowHeight : self.tableView.rowHeight);
    }else{
//        NSDictionary *itemData;
//        itemData = self.questionsDataArray[indexPath.row];
//        NSString *rowHeightString = [self.rowHeight valueForKey:[itemData valueForKey:@"type"]];
//        rowH =[rowHeightString floatValue];
    }
    
    return rowH;

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger numRows = 0;
    if (section == 0) {
        numRows = self.headerPattern.count;
    }else{
        numRows = self.questionGroupPattern.count;
    }
    
    if ([self hasInlineDatePicker]){
        if (self.datePickerIndexPath.section == section) {
            // we have a date picker, so allow for it in the number of rows in this section
            return ++numRows;
        }
    }
    
    return numRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    NSString *cellID = nil;
    
    NSDictionary *itemData;
    if (indexPath.section == 0) {
        
        
        if ([self indexPathHasPicker:indexPath])
        {
            // the indexPath is the one containing the inline date picker
            cellID = kDatePickerID;     // the current/opened date picker cell
        }
        /*
         else if ([self indexPathHasDate:indexPath])
         {
         // the indexPath is one that contains the date information
         cellID = kDateCellID;       // the start/end date cells
         }
         */
        else
        {
            cellID = [[self.headerPattern objectAtIndex:indexPath.row] valueForKey:@"cellID"];
        }
        
        cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        
        
        // if we have a date picker open whose cell is above the cell we want to update,
        // then we have one more cell than the model allows
        NSInteger modelRow = indexPath.row;
        if (self.datePickerIndexPath != nil && self.datePickerIndexPath.row < indexPath.row)
        {
            modelRow--;
        }
        
        
        itemData = self.headerDataArray[modelRow];
        
        // proceed to configure our cell
        if ([cellID isEqualToString:kDateCellID])
        {
            // we have a date cells, populate label and date field
            cell.textLabel.text = [itemData valueForKey:kTitleKey];
            cell.detailTextLabel.text = [self.dateFormatter stringFromDate:[itemData valueForKey:kDateKey]];
        }
        else if ([cellID isEqualToString:kOtherCell])
        {
            // this cell is a non-date cell, just assign it's text label and value
            cell.textLabel.text = [itemData valueForKey:kTitleKey];
            
            if ([[self.headerPattern objectAtIndex:indexPath.row] valueForKey:@"valuesForKey"]) {
                NSArray *valuesForKey = [[self.headerPattern objectAtIndex:indexPath.row] valueForKey:@"valuesForKey"];
                NSString *textValue = [itemData valueForKey:kValueKey];
                cell.detailTextLabel.text = [valuesForKey objectAtIndex: [textValue intValue]];
            }else{
                cell.detailTextLabel.text = [itemData valueForKey:kValueKey];
            }
            
        }
        
        int statusActivity = [[_survey valueForKey:@"status"] intValue];
        //int statusActivity = 1;
        if (statusActivity > 0) {
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }else{

            if ([[self.headerPattern objectAtIndex:indexPath.row] valueForKey:@"type"]) {
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                cell.selectionStyle = UITableViewCellSelectionStyleDefault;
            }else{
                cell.accessoryType = UITableViewCellAccessoryNone;
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }

        }
        
        if ([itemData valueForKey:kNumberOfLines] != nil) {
            cell.detailTextLabel.numberOfLines = [[itemData valueForKey:kNumberOfLines]intValue];
        }
        NSLog(@"cellID = %@", cellID);
        return cell;
        
    }else{
        
        cellID = [[self.questionGroupPattern objectAtIndex:indexPath.row] valueForKey:@"cellID"];
        cell = [tableView dequeueReusableCellWithIdentifier:cellID];

        cell.textLabel.text = [[self.questionGroupPattern objectAtIndex:indexPath.row] valueForKey:@"label"];
        cell.detailTextLabel.text = @"";
        
        
        //Set section title color RED if Survey has saveFailure and section has required empty questions
        if (self.saveFailure) {
            NSArray *questionsForGroup = [[[self.questionGroupPattern objectAtIndex:indexPath.row] valueForKey:@"groups"] valueForKey:@"questions"];
            for (int i = 0; i < questionsForGroup.count; i++) {
                NSArray *questions = [questionsForGroup objectAtIndex:i];
                if (questions != [NSNull null]) {
                    for (int j = 0; j < questions.count; j++) {
                        if ([[questions objectAtIndex:j] valueForKey:@"required"]) {
                            NSString *questionSubject = [[[questions objectAtIndex:j] valueForKey:@"fieldName"] lowercaseString];
//                            NSString *questionSubject = [[questions objectAtIndex:j] valueForKey:@"fieldName"];
                            
                            NSPredicate *pred = [NSPredicate predicateWithFormat:@"%K == %@",@"subject", questionSubject];
                            NSArray *answers = [self.survey.answers filteredArrayUsingPredicate:pred];
                            if ([answers count] != 0) {
                                if (![[answers objectAtIndex:0] valueForKey:@"response"] || [[[answers objectAtIndex:0] valueForKey:@"response"] isEqualToString:@""]) {
                                    cell.textLabel.textColor = kRedLabelTextColor;
                                }
                            }
                        }
                    }
                }
            }

            
            
            
        }

        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
        
        return cell;
    }
    
    
}

/*! Adds or removes a UIDatePicker cell below the given indexPath.
 
 @param indexPath The indexPath to reveal the UIDatePicker.
 */
- (void)toggleDatePickerForSelectedIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView beginUpdates];
    
    NSArray *indexPaths = @[[NSIndexPath indexPathForRow:indexPath.row + 1 inSection:0]];
    
    // check if 'indexPath' has an attached date picker below it
    if ([self hasPickerForIndexPath:indexPath])
    {
        // found a picker below it, so remove it
        [self.tableView deleteRowsAtIndexPaths:indexPaths
                              withRowAnimation:UITableViewRowAnimationFade];
    }
    else
    {
        // didn't find a picker below it, so we should insert it
        [self.tableView insertRowsAtIndexPaths:indexPaths
                              withRowAnimation:UITableViewRowAnimationFade];
    }
    
    [self.tableView endUpdates];
}

/*! Reveals the date picker inline for the given indexPath, called by "didSelectRowAtIndexPath".
 
 @param indexPath The indexPath to reveal the UIDatePicker.
 */
- (void)displayInlineDatePickerForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // display the date picker inline with the table content
    [self.tableView beginUpdates];
    
    BOOL before = NO;   // indicates if the date picker is below "indexPath", help us determine which row to reveal
    if ([self hasInlineDatePicker])
    {
        before = self.datePickerIndexPath.row < indexPath.row;
    }
    
    BOOL sameCellClicked = (self.datePickerIndexPath.row - 1 == indexPath.row);
    
    // remove any date picker cell if it exists
    if ([self hasInlineDatePicker])
    {
        [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:self.datePickerIndexPath.row inSection:0]]
                              withRowAnimation:UITableViewRowAnimationFade];
        self.datePickerIndexPath = nil;
    }
    
    if (!sameCellClicked)
    {
        // hide the old date picker and display the new one
        NSInteger rowToReveal = (before ? indexPath.row - 1 : indexPath.row);
        NSIndexPath *indexPathToReveal = [NSIndexPath indexPathForRow:rowToReveal inSection:0];
        
        [self toggleDatePickerForSelectedIndexPath:indexPathToReveal];
        self.datePickerIndexPath = [NSIndexPath indexPathForRow:indexPathToReveal.row + 1 inSection:0];
    }
    
    // always deselect the row containing the start or end date
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self.tableView endUpdates];
    
    // inform our date picker of the current date to match the current cell
    [self updateDatePicker];
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    int statusActivity = [[_survey valueForKey:@"status"] intValue];
    //int statusActivity = 1;
    
    if (indexPath.section == 0) {
        if (statusActivity < 1) {
            if ([cell.reuseIdentifier isEqualToString:@"dateCell"]){
                [self displayInlineDatePickerForRowAtIndexPath:indexPath];
            }else{
                if ([self hasInlineDatePicker]) {
                    NSLog(@"C'è un date picker aperto!");
                    // hide the old date picker
                    NSInteger rowToReveal = self.datePickerIndexPath.row -1;
                    NSIndexPath *indexPathToReveal = [NSIndexPath indexPathForRow:rowToReveal inSection:0];
                    
                    self.datePickerIndexPath = nil;
                    [self toggleDatePickerForSelectedIndexPath:indexPathToReveal];
                    
                    if (rowToReveal < indexPath.row) {
                        indexPath =[NSIndexPath indexPathForRow:indexPath.row -1  inSection:indexPath.section];
                    }
                }
            }
            
            if ([[[self.headerPattern objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"customerPicker"]) {
                [self performSegueWithIdentifier:kCustomerSegue sender:self];
            }else{
                [tableView deselectRowAtIndexPath:indexPath animated:YES];
            }
        }
        
    }else{
        if ([cell.reuseIdentifier isEqualToString:@"otherCell"]) {
            [self performSegueWithIdentifier:kQuestionInputSegue sender:self];
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
        }else{
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
        }
    }
    
}

/*
 - (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
 
 RDDetailSimpleCell *myCell = (RDDetailSimpleCell *)[self.tableView cellForRowAtIndexPath: indexPath];
 NSLog(@"cella %@",myCell.class);
 
 if ([myCell respondsToSelector:@selector(setValue:)]) {
 NSLog(@"Cell value = %@", myCell.value);
 //        [[self.activity.answers objectAtIndex:indexPath.row] setResponse:myCell.value];
 }
 
 }
 */

#pragma mark - DatePicker Actions

/*! User chose to change the date by changing the values inside the UIDatePicker.
 
 @param sender The sender for this action: UIDatePicker.
 */
- (IBAction)dateAction:(id)sender
{
    NSIndexPath *targetedCellIndexPath = nil;
    
    if ([self hasInlineDatePicker])
    {
        // inline date picker: update the cell's date "above" the date picker cell
        //
        targetedCellIndexPath = [NSIndexPath indexPathForRow:self.datePickerIndexPath.row - 1 inSection:0];
    }
    else
    {
        // external date picker: update the current "selected" cell's date
        targetedCellIndexPath = [self.tableView indexPathForSelectedRow];
    }
    
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:targetedCellIndexPath];
    UIDatePicker *targetedDatePicker = sender;
    
    
    NSDate *now = [NSDate date];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:now];
    [components setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    [components setHour:23];
    [components setMinute:59];
    [components setSecond:59];
    NSDate *today12pm = [calendar dateFromComponents:components];

    if ([targetedDatePicker.date compare:today12pm] == NSOrderedDescending){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Errore" message:@"Data non valida" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }else{
        // update our data model
        self.survey.date = targetedDatePicker.date;
        
        NSMutableDictionary *itemData = self.headerDataArray[targetedCellIndexPath.row];
        [itemData setValue:targetedDatePicker.date forKey:@"date"];
        
        // update the cell's date string
        cell.detailTextLabel.text = [self.dateFormatter stringFromDate:targetedDatePicker.date];
    }
        
    
}


#pragma mark - Several Delegate Methods
- (void)setQuestion:(NSString *)questionUnid withValue:(NSString *)fieldValue {
    NSLog(@"question unid: %@",questionUnid);
    NSLog(@"Field value: %@",fieldValue);
    
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"questionUnid == %@",questionUnid];
//    NSArray *result = [self.survey.answers filteredArrayUsingPredicate:predicate];
    
//    [[result objectAtIndex:0] setResponse:fieldValue];
    
    
}

- (void)getListCustomerSelected:(NSMutableArray *)arrayCustomers{
    
    self.survey.customers = arrayCustomers;
    [self prepareDataForDisplay];
    [self.tableView reloadData];
    
}

-(void) getNoteInput:(NSString *)noteContent {
    
    self.survey.note = noteContent;
    
}

-(NSString *)getNote {
    
    return self.survey.note;
    
}

- (void)getSelectedItems:(NSMutableArray *)selectedItems{
    NSIndexPath *currentIndexPath = self.tableView.indexPathForSelectedRow;
    
    if (currentIndexPath.section == 0){
        NSString *currentCellKey = [[self.headerPattern objectAtIndex:currentIndexPath.row] valueForKey:@"key"];
        

        NSString *currentSelector = [NSString stringWithFormat:@"set%@:",[currentCellKey stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[currentCellKey substringToIndex:1] uppercaseString]]];
        
        if ([self.survey respondsToSelector:(SEL)NSSelectorFromString(currentSelector)]) {
            [self.survey performSelector:(SEL)NSSelectorFromString(currentSelector) withObject:[selectedItems objectAtIndex:0]];
        }
    }else{
        //TODO open detail window (PageViewController?) at the right section
//        RDDetailSimpleCell *cell = (RDDetailSimpleCell *)[self.tableView cellForRowAtIndexPath:currentIndexPath];

        
    }
    
    [self prepareDataForDisplay];
    [self.tableView reloadData];
    
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:kNoteActivitySegue]) {
        NoteActivityViewController *destination = segue.destinationViewController;
        destination.activityStatus = [self.survey.status intValue];
        destination.delegate = self;
    }
    
    if ([[segue identifier] isEqualToString:kCustomerSegue]) {
        UINavigationController *destination = (UINavigationController *)segue.destinationViewController;
        
        CustomerActivityViewController *listCustomerView = [[destination viewControllers]objectAtIndex:0];
        
        listCustomerView.customerSelectedList = [self.survey.customers mutableCopy];
        listCustomerView.delegate = self;
        
    }else if ([[segue identifier] isEqualToString:kQuestionInputSegue]) {

        SurveyQuestionsPageViewController *destination = [segue destinationViewController];
        destination.startingIndex = [[self.tableView indexPathForSelectedRow] row];
        
        NSMutableArray *sectionTitles = [[NSMutableArray alloc] init];
        for (int i=0; i < self.questionGroupPattern.count; i++) {
            [sectionTitles addObject:[[self.questionGroupPattern objectAtIndex:i] valueForKey:@"label"]];
        }
        destination.sectionTitles = [sectionTitles copy];
        destination.survey = self.survey;

        
    }
}

- (IBAction)saveAction:(id)sender {
    
    //Check if customer is selected
    if (!self.survey.customers) {
        //error alert
        NSString *alertTitle = NSLocalizedString(@"Errore", @"Errore");
        NSString *alertMessage = NSLocalizedString(@"Selezionare Punto Vendita", @"Selezionare Punto Vendita");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertTitle message:alertMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    

    NSString *saveAndClose = NSLocalizedString(@"Salva e invia", @"SurveyActionSheet0");
    NSString *saveAsDraft = NSLocalizedString(@"Salva bozza", @"SurveyActionSheet1");
    NSString *cancelTitle = NSLocalizedString(@"Annulla", @"ActivityActionSheet2");
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:nil
                                  delegate:self
                                  cancelButtonTitle:cancelTitle
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:saveAndClose, saveAsDraft, nil];
    
    [actionSheet showInView:self.view];
    
    
}

- (IBAction)cancelAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)editAction:(id)sender {
    [self performSegueWithIdentifier:@"NoteActivityIdentifier" sender:self];
}


#pragma mark Action Sheet Delegate Methods
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    
    switch (buttonIndex) {
        case 0:
        {
            self.survey.status = @"1";

            
            NSString *alertTitle;
            NSString *alertMessage;
            UIAlertView *alert;
            
            switch ([self.survey save]) {
                case 0:
                {
                    self.saveFailure = true;
                    [self.tableView reloadData];
                    
                    //error alert
                    alertTitle = NSLocalizedString(@"Censimento", @"Censimento");
                    alert = [[UIAlertView alloc] initWithTitle:alertTitle message:self.survey.saveErrorMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                    
                    break;
                }
                case 3:
                {
                    self.saveFailure = true;
                    //error alert
                    alertTitle = NSLocalizedString(@"Censimento", @"Censimento");
                    alertMessage = NSLocalizedString(@"Impossibile inviare. Questionario incompleto", @"Salvataggio bozza eseguito. Quesitonario incompleto.");
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertTitle message:alertMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    self.survey.status = @"0";
                    [alert show];
                    
                    break;
                }
                default:
                    
                    self.saveFailure = false;
                    [self.navigationController popViewControllerAnimated:YES];
                    
                    break;
                    
            }
            
            break;
        }

        case 1:
        {
            self.survey.status = @"0";
            self.survey.newSurvey = NO;
            [self.cancelButton setTitle:NSLocalizedString(@"Chiudi", @"Chiudi")];

            NSString *alertTitle;
            NSString *alertMessage;
            UIAlertView *alert;
        
            switch ([self.survey save]) {
                case 0:
                {
                    alertTitle = NSLocalizedString(@"Censimento", @"Censimento");
                    alert = [[UIAlertView alloc] initWithTitle:alertTitle message:self.survey.saveErrorMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];

                    break;
                }
                case 3:
                {
                    self.saveFailure = true;
                    //error alert
                    alertTitle = NSLocalizedString(@"Censimento", @"Censimento");
                    alertMessage = NSLocalizedString(@"Salvataggio bozza eseguito. Questionario incompleto", @"Salvataggio bozza eseguito. Quesitonario incompleto.");
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertTitle message:alertMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                    
                    break;
                }
                default:
                
                    self.saveFailure = false;
                    alertTitle = NSLocalizedString(@"Censimento", @"Censimento");
                    alertMessage = NSLocalizedString(@"Salvataggio bozza eseguito", @"Salvataggio bozza eseguito");
                    alert = [[UIAlertView alloc] initWithTitle:alertTitle message:alertMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];

                    break;
                
            }
        

            
            [self.tableView reloadData];

            break;
        }
        default:
            
            break;
    }
}





@end
