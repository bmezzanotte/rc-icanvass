//
//  SurveyListViewController.h
//  iCanvass
//
//  Created by Bernardino on 24/12/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SurveyListViewController : UITableViewController<NSFetchedResultsControllerDelegate>


@end
