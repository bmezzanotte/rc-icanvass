//
//  SurveyListViewController.m
//  iCanvass
//
//  Created by Bernardino on 24/12/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import "SurveyListViewController.h"
#import "AppDelegate.h"
#import "SurveyHeader.h"
#import "SurveyAnswer.h"
#import "Customer.h"

#import "SurveyDetailViewController.h"

#import "Utility.h"
#import "DataBaseManager.h"

static NSString *ENTITY_NAME_HEADER = @"SurveyHeader";
static NSString *FETCH_CACHE_NAME = @"Survey";

@interface SurveyListViewController ()
@property (nonatomic, strong, readonly) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic,strong) Utility *util;
@property (nonatomic, strong) DataBaseManager *dbManager;
@end

@implementation SurveyListViewController
@synthesize fetchedResultsController = _fetchedResultsController;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.util = [[Utility alloc] init];
    self.dbManager = [[DataBaseManager alloc] init];
    
    self.navigationItem.title = NSLocalizedString(@"Censimenti", @"Titolo nella navigation bar");
    
}

- (void)viewWillAppear:(BOOL)animated
{
    
    /*
    //temporaneo, per vedere cosa c'è nella tabella SurveyHeader
    AppDelegate *appDelegate =[[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *managedObjectContext = [appDelegate managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:ENTITY_NAME_HEADER inManagedObjectContext:managedObjectContext];
    [request setEntity:entity];
    NSError *error1;
    NSArray *objects = [managedObjectContext executeFetchRequest:request error:&error1];
    //temporaneo, per vedere cosa c'è nella tabella ActivityHeader
    */
    
    
    [NSFetchedResultsController deleteCacheWithName:FETCH_CACHE_NAME];
    NSError *error = nil;
    if (![[self fetchedResultsController] performFetch:&error]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Errore caricamento dati", @"Messagio de errore nel caricamento dati") message:
                              [NSString stringWithFormat:NSLocalizedString(@"L'errore è stato: %@, provocando la chiusura.", @"Error was: %@, quitting."),
                               [error localizedDescription]]
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"Aw, Nuts", @"Aw, Nuts")
                                              otherButtonTitles:nil];
        [alert show];
    }
    
    [self.tableView reloadData];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    tableView.sectionIndexColor = [UIColor darkGrayColor];
    //    self.tableView.sectionIndexBackgroundColor = [UIColor whiteColor];
    //    self.tableView.sectionIndexTrackingBackgroundColor = [UIColor lightGrayColor];
    tableView.sectionIndexMinimumDisplayRowCount = 20.0;
    
    return [[self.fetchedResultsController sections] count];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id < NSFetchedResultsSectionInfo > sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"SurveyCellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    UILabel *dateActivityLabel = (UILabel *)[cell viewWithTag:1];
    UILabel *customersLabel = (UILabel *)[cell viewWithTag:2];
    UILabel *addressCustomerLabel = (UILabel *)[cell viewWithTag:3];
    UILabel *statusLabel = (UILabel *)[cell viewWithTag:4];
    
    SurveyHeader *survey = [self.fetchedResultsController objectAtIndexPath:indexPath];
    Customer *myCustomer = survey.customer;
    
    dateActivityLabel.text = [self.util getStringFromDate:survey.date withPattern:@"dd/MM/yyyy"];
    customersLabel.text = myCustomer.name;
    addressCustomerLabel.text = [NSString stringWithFormat:@"%@ - %@",myCustomer.city,myCustomer.address];
    
    if ([survey.status isEqualToString:@"0"]) {
        statusLabel.text = NSLocalizedString(@"B", @"Bozza");
    }else if ([survey.status isEqualToString:@"1"]) {
        statusLabel.text = NSLocalizedString(@"C", @"Completo");
    }else{
        statusLabel.text = survey.status;
    }
    
    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    SurveyHeader *survey = [self.fetchedResultsController objectAtIndexPath:indexPath];
    if ([survey.status integerValue] > 1) {
        return NO;
    }else{
        return YES;
    }
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [_dbManager updateItem:[self.fetchedResultsController objectAtIndexPath:indexPath] WithUpdateData:[NSDictionary dictionaryWithObject:@"9" forKey:@"status"]];
        //[tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
}



#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"AddNewSurveySegue"]) {
        Survey *newSurvey = [[Survey alloc] init];
        newSurvey.date = [NSDate date];
        newSurvey.status = @"0";
        
        SurveyDetailViewController *destination = [segue destinationViewController];
        destination.survey = newSurvey;
        destination.survey.newSurvey = YES;
        
    }
    
    
    if ([[segue identifier] isEqualToString:@"EditSurveySegue"]) {
        SurveyDetailViewController *destination = [segue destinationViewController];

        SurveyHeader *mySurveyHeader = [self.fetchedResultsController objectAtIndexPath:[self.tableView indexPathForSelectedRow]];
        
        Survey *mySurvey = [[Survey alloc] init];
        mySurvey.unid = mySurveyHeader.unid;
        mySurvey.date = mySurveyHeader.date;
        mySurvey.note = mySurveyHeader.note;
        mySurvey.status = mySurveyHeader.status;
        
        
        Customer *myCustomer = mySurveyHeader.customer;
        NSMutableArray *customerArray = [[NSMutableArray alloc] init];
        [customerArray addObject:myCustomer];
        mySurvey.customers = [customerArray copy];
        
        
        //Compilazione questionario
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString  *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,@"SurveyDetailTableConfiguration.plist"];
        NSDictionary *plist = [NSDictionary dictionaryWithContentsOfFile:filePath];

//        NSURL *plistURL = [[NSBundle mainBundle] URLForResource:@"SurveyDetailTableConfiguration" withExtension:@"plist"];
//        NSDictionary *plist = [NSDictionary dictionaryWithContentsOfURL:plistURL];
  
        NSArray *questionGroupPattern = [[[plist valueForKey:@"sections"] objectAtIndex:1] valueForKey:@"rows"];
        NSArray *questions = [[questionGroupPattern valueForKey:@"groups"] valueForKey:@"questions"];
        
        mySurvey.answers = [[NSMutableArray alloc] init];
        for (SurveyAnswer *surveyAnswer in [mySurveyHeader.answers allObjects]) {
            Answer *myAnswer = [[Answer alloc] init];
            myAnswer.subject = surveyAnswer.subject;
            myAnswer.unid = surveyAnswer.unid;
            myAnswer.type = surveyAnswer.type;

            NSLog(@"myAnswer.subject = %@", myAnswer.subject);
            NSLog(@"myAnswer.type = %@", myAnswer.type);
            if ([myAnswer.type isEqualToString:@"CheckBoxMulti"]) {
                NSLog(@"QUI");
                NSArray *values = [surveyAnswer.value componentsSeparatedByString:@","];
                myAnswer.response = [values mutableCopy];
            }else{
                myAnswer.response = surveyAnswer.value;
            }
            
            NSPredicate *pred = [NSPredicate predicateWithFormat:@"%K == %@",@"fieldName", myAnswer.subject];
            for (int i = 0; i < questions.count; i++) {
                if ([questions objectAtIndex:i] != [NSNull null]) {
                    for (int j = 0 ; j < [[questions objectAtIndex:i] count]; j++) {
                        if ([[questions objectAtIndex:i] objectAtIndex:j] != [NSNull null]) {
                            NSArray *test1 = [[[questions objectAtIndex:i] objectAtIndex:j] filteredArrayUsingPredicate:pred];
                            if ([test1 count]) {
                                if ([[test1 objectAtIndex:0] valueForKey:@"required"]) {
                                    NSLog(@"REQUIRED");
                                    myAnswer.required = [NSNumber numberWithInt:1];
                                    destination.saveFailure = true;
                                }
                                goto NEXTFIELD;
                            }
                        }
                    }
                }
            }
        NEXTFIELD:
            
            
            [mySurvey.answers addObject:myAnswer];
        }

        
        
        destination.survey = mySurvey;
        destination.survey.newSurvey = NO;

    }
}

#pragma mark - FetchedResultsController Property
- (NSFetchedResultsController *)fetchedResultsController{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = [appDelegate managedObjectContext];
    NSEntityDescription *entity = [NSEntityDescription entityForName:ENTITY_NAME_HEADER inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    [fetchRequest setFetchBatchSize:20];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status != '9'"];
    [fetchRequest setPredicate:predicate];
    NSSortDescriptor *sortDescriptor1 = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:NO];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor1, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    //    sectionKey = @"nameInitial";
    _fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:managedObjectContext sectionNameKeyPath:nil cacheName:FETCH_CACHE_NAME];
    _fetchedResultsController.delegate = self;
    return _fetchedResultsController;
}

#pragma mark - NSFetchedResultsControllerDelegate Methods
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}
- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}
- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id < NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type{
    switch(type) {
        case NSFetchedResultsChangeInsert: [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete: [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeMove:[self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeUpdate:[self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}
- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath{
    switch(type) {
        case NSFetchedResultsChangeInsert: [self.tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete: [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeUpdate:
        case NSFetchedResultsChangeMove:
            break;
    }
}

@end
