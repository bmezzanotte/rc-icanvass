//
//  SurveyQuestionGroupViewController.h
//  iCanvass
//
//  Created by Bernardino on 31/12/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RDDetailSimpleCell.h"
#import "PickListViewController.h"

@protocol RDQuestionGroupDelegate <NSObject>

- (void)setQuestion:(NSString *)questionUnid withValue:(NSString *)fieldValue;
- (id)questionValue:(NSString *)questionSubject;

@end


@interface SurveyQuestionGroupViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, RDDetailCellDelegate,PickListDelegate>

@property (weak, nonatomic) id<RDQuestionGroupDelegate> delegate;

@property (nonatomic, strong) NSString *titleText;
@property (nonatomic) NSInteger sectionIndex;

@property (weak, nonatomic) IBOutlet UINavigationItem *pageTitle;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *closeButton;
- (IBAction)close:(id)sender;



@end
