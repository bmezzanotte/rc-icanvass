//
//  SurveyQuestionGroupViewController.m
//  iCanvass
//
//  Created by Bernardino on 31/12/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import "SurveyQuestionGroupViewController.h"
#import "AppDelegate.h"
#import "DataBaseManager.h"
#import "Utility.h"

#import "SurveyQuestionsPageViewController.h"

#define kLabelTextColor [UIColor colorWithRed:0.321569f green:0.4f blue:0.568627f alpha:1.0f]
#define kRedLabelTextColor [UIColor colorWithRed:0.9f green:0.2f blue:0.2f alpha:1.0f]

#define kPickerAnimationDuration    0.40   // duration for the animation to slide the date picker into view
#define kDatePickerTag              99     // view tag identifiying the date picker view

// keep track of which rows have date cells
#define kDateStartRow   0
#define kDateEndRow     0

static const float CELL_TABLE_MAX_FRAME = 416.0;
static NSString *ENTITY_NAME_HEADER = @"SurveyHeader";
static NSString *FETCH_CACHE_NAME = @"Survey";

static NSString *kPickListSegue = @"pickListSegue";

static NSString *kDateCellID = @"dateCell";     // the cells with date
static NSString *kDatePickerID = @"datePicker"; // the cell containing the date picker

@interface SurveyQuestionGroupViewController ()
@property (nonatomic, strong) NSArray *sections;
@property (nonatomic, strong) NSDateFormatter *dateFormatter;

// keep track which indexPath points to the cell with UIDatePicker
@property (nonatomic, strong) NSIndexPath *datePickerIndexPath;
@property (assign) NSInteger pickerCellRowHeight;

@property (nonatomic, strong) DataBaseManager *dbManager;
@property (nonatomic, strong) Utility *util;

@end

@implementation SurveyQuestionGroupViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view.
    self.dbManager = [[DataBaseManager alloc] init];
    self.util = [[Utility alloc] init];
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setDateFormat:@"dd/MM/yyyy"];
//    [self.dateFormatter setDateStyle:NSDateFormatterShortStyle];    // show short-style date format
//    [self.dateFormatter setTimeStyle:NSDateFormatterNoStyle];

    self.pageTitle.title = self.titleText;
    self.closeButton.title = NSLocalizedString(@"Indietro", @"Indietro");
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString  *documentsDirectory = [paths objectAtIndex:0];
    NSString  *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,@"SurveyDetailTableConfiguration.plist"];
    NSDictionary *plist = [NSDictionary dictionaryWithContentsOfFile:filePath];
    /*
    NSURL *plistURL = [[NSBundle mainBundle] URLForResource:@"SurveyDetailTableConfiguration" withExtension:@"plist"];
    NSDictionary *plist = [NSDictionary dictionaryWithContentsOfURL:plistURL];
    */
    
    self.sections = [[[[[plist valueForKey:@"sections"] objectAtIndex:1] valueForKey:@"rows"] objectAtIndex:self.sectionIndex] valueForKey:@"groups"];
    
    
    // obtain the picker view cell's height, works because the cell was pre-defined in our storyboard
    UITableViewCell *pickerViewCellToCheck = [self.tableView dequeueReusableCellWithIdentifier:kDatePickerID];
    self.pickerCellRowHeight = pickerViewCellToCheck.frame.size.height;
    if (!self.pickerCellRowHeight){
        self.pickerCellRowHeight = 162.0;
    }
    
    // if the local changes while in the background, we need to be notified so we can update the date
    // format in the table view cells
    //
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(localeChanged:)
                                                 name:NSCurrentLocaleDidChangeNotification
                                               object:nil];


}

- (void)viewWillAppear:(BOOL)animated {
    
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:NSCurrentLocaleDidChangeNotification
                                                  object:nil];
}


#pragma mark - Locale

/*! Responds to region format or locale changes.
 */
- (void)localeChanged:(NSNotification *)notif
{
    // the user changed the locale (region format) in Settings, so we are notified here to
    // update the date format in the table view cells
    //
    [self.tableView reloadData];
}


#pragma mark - inline DatePicker Utilities

/*! Determines if the given indexPath has a cell below it with a UIDatePicker.
 
 @param indexPath The indexPath to check if its cell has a UIDatePicker below it.
 */
- (BOOL)hasPickerForIndexPath:(NSIndexPath *)indexPath
{
    BOOL hasDatePicker = NO;
    
    NSInteger targetedRow = indexPath.row;
    targetedRow++;
    
    UITableViewCell *checkDatePickerCell =
    [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:targetedRow inSection:indexPath.section]];
    UIDatePicker *checkDatePicker = (UIDatePicker *)[checkDatePickerCell viewWithTag:kDatePickerTag];
    
    hasDatePicker = (checkDatePicker != nil);
    return hasDatePicker;
}

/*! Updates the UIDatePicker's value to match with the date of the cell above it.
 */
- (void)updateDatePicker
{
    if (self.datePickerIndexPath != nil)
    {
        UITableViewCell *associatedDatePickerCell = [self.tableView cellForRowAtIndexPath:self.datePickerIndexPath];
        
        UIDatePicker *targetedDatePicker = (UIDatePicker *)[associatedDatePickerCell viewWithTag:kDatePickerTag];
        if (targetedDatePicker != nil)
        {
            // we found a UIDatePicker in this cell, so update it's date value
            NSInteger dateRowIndex = self.datePickerIndexPath.row - 1;
            NSInteger dateSectionIndex = self.datePickerIndexPath.section;
            
            NSLog(@"dateSectionIndex = %d", dateSectionIndex);
            NSLog(@"dateRowIndex = %d", dateRowIndex);
            
            NSIndexPath *targetedCellIndexPath = [NSIndexPath indexPathForRow:self.datePickerIndexPath.row - 1 inSection:self.datePickerIndexPath.section];
            NSArray *questions = [[self.sections objectAtIndex:targetedCellIndexPath.section] valueForKey:@"questions"];
            
            NSDictionary *itemData;
            itemData = questions[targetedCellIndexPath.row];
            NSString *key = [[itemData valueForKey:@"fieldName"] lowercaseString];
            NSString *keyValue = [self.delegate questionValue:key];
            NSDate *keyDate;
            if (keyValue && ![keyValue isEqualToString:@""]) {
                keyDate = [_util getDateFromString:keyValue withPattern:@"dd/MM/yyyy hh.mm.ss.SSS"];
            }else{
                keyDate = [_util getCurrentDate];
            }

            [targetedDatePicker setDate:keyDate animated:NO];
            
        }
    }
}

/*! Determines if the UITableViewController has a UIDatePicker in any of its cells.
 */
- (BOOL)hasInlineDatePicker
{
    if (self.datePickerIndexPath != nil) {
        NSLog(@"inline date picker in section:%ld at row:%ld",(long)self.datePickerIndexPath.section, (long)self.datePickerIndexPath.row);
    }
    
    return (self.datePickerIndexPath != nil);
}

/*! Determines if the given indexPath points to a cell that contains the UIDatePicker.
 
 @param indexPath The indexPath to check if it represents a cell with the UIDatePicker.
 */
- (BOOL)indexPathHasPicker:(NSIndexPath *)indexPath
{
    return ([self hasInlineDatePicker] && self.datePickerIndexPath.row == indexPath.row);
}

/*! Determines if the given indexPath points to a cell that contains the start/end dates.
 
 @param indexPath The indexPath to check if it represents start/end date cell.
 */
- (BOOL)indexPathHasDate:(NSIndexPath *)indexPath
{
    BOOL hasDate = NO;
    
    if ((indexPath.row == kDateStartRow) || (indexPath.row == kDateEndRow || ([self hasInlineDatePicker] && (indexPath.row == kDateEndRow + 1))))
    {
        hasDate = YES;
    }
    
    return hasDate;
}

/*! Adds or removes a UIDatePicker cell below the given indexPath.
 
 @param indexPath The indexPath to reveal the UIDatePicker.
 */
- (void)toggleDatePickerForSelectedIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView beginUpdates];
    
    NSArray *indexPaths = @[[NSIndexPath indexPathForRow:indexPath.row + 1 inSection:indexPath.section]];
    
    // check if 'indexPath' has an attached date picker below it
    if ([self hasPickerForIndexPath:indexPath])
    {
        // found a picker below it, so remove it
        [self.tableView deleteRowsAtIndexPaths:indexPaths
                              withRowAnimation:UITableViewRowAnimationFade];
    }
    else
    {
        // didn't find a picker below it, so we should insert it
        [self.tableView insertRowsAtIndexPaths:indexPaths
                              withRowAnimation:UITableViewRowAnimationFade];
    }
    
    [self.tableView endUpdates];
}

/*! Reveals the date picker inline for the given indexPath, called by "didSelectRowAtIndexPath".
 
 @param indexPath The indexPath to reveal the UIDatePicker.
 */
- (void)displayInlineDatePickerForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // display the date picker inline with the table content
    [self.tableView beginUpdates];
    
    BOOL before = NO;   // indicates if the date picker is below "indexPath", help us determine which row to reveal
    if ([self hasInlineDatePicker])
    {
        before = self.datePickerIndexPath.row < indexPath.row;
    }
    
    BOOL sameCellClicked = (self.datePickerIndexPath.row - 1 == indexPath.row && self.datePickerIndexPath.section == indexPath.section);
    
    // remove any date picker cell if it exists
    if ([self hasInlineDatePicker])
    {
        [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:self.datePickerIndexPath.row inSection:self.datePickerIndexPath.section]]
                              withRowAnimation:UITableViewRowAnimationFade];
        
//        [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:self.datePickerIndexPath.row inSection:indexPath.section]] withRowAnimation:UITableViewRowAnimationFade];

        
        
        self.datePickerIndexPath = nil;
    }
    
    if (!sameCellClicked)
    {
        // hide the old date picker and display the new one
        NSInteger rowToReveal = (before ? indexPath.row - 1 : indexPath.row);
        NSIndexPath *indexPathToReveal = [NSIndexPath indexPathForRow:rowToReveal inSection:indexPath.section];
        
        [self toggleDatePickerForSelectedIndexPath:indexPathToReveal];
        self.datePickerIndexPath = [NSIndexPath indexPathForRow:indexPathToReveal.row + 1 inSection:indexPath.section];
    }
    
    // always deselect the row containing the start or end date
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self.tableView endUpdates];
    
    // inform our date picker of the current date to match the current cell
    [self updateDatePicker];
}


#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.sections count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [[self.sections objectAtIndex:section] valueForKey:@"label"];

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *questions = [[self.sections objectAtIndex:section] valueForKey:@"questions"];
    NSInteger numRows = [questions count];
    
    if ([self hasInlineDatePicker]){
        if (self.datePickerIndexPath.section == section) {
            // we have a date picker, so allow for it in the number of rows in this section
            return ++numRows;
        }
    }
    
    return numRows;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    CGFloat rowH = 44.0;

    if ([self indexPathHasPicker:indexPath]) {
        rowH = self.pickerCellRowHeight;
    }else{
        NSInteger modelRow = indexPath.row;
        if (self.datePickerIndexPath != nil && self.datePickerIndexPath.row < indexPath.row) {
            modelRow--;
        }

        NSArray *questions = [[self.sections objectAtIndex:indexPath.section] valueForKey:@"questions"];
        
        NSDictionary *itemData;
        itemData = questions[modelRow];
        NSString *rowHeightString = [itemData valueForKey:@"rowHeight"];
        rowH =[rowHeightString floatValue];
    }
    
    return rowH;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellID = nil;
    
    if ([self indexPathHasPicker:indexPath])
    {
        // the indexPath is the one containing the inline date picker
        cellID = kDatePickerID;     // the current/opened date picker cell
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
        
        return cell;
    }
    
    
    // if we have a date picker open whose cell is above the cell we want to update,
    // then we have one more cell than the model allows
    NSInteger modelRow = indexPath.row;
    if (self.datePickerIndexPath != nil && self.datePickerIndexPath.row < indexPath.row)
    {
        modelRow--;
    }
    
    
    
    NSArray *questions = [[self.sections objectAtIndex:indexPath.section] valueForKey:@"questions"];
    
    NSString *cellClassName;
    NSDictionary *itemData;
    itemData = questions[modelRow];
    cellClassName = [itemData valueForKey:@"cellClass"];

    
    if (cellClassName == nil) {
        cellClassName = @"RDDetailSimpleCell";
    }
    
    
    if ([cellClassName isEqualToString:@"RDDetailDateCell"]) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"dateCell"];

        cell.textLabel.text = [itemData valueForKey:@"label"];
        cell.textLabel.font = [UIFont boldSystemFontOfSize:[UIFont smallSystemFontSize]];
        cell.textLabel.textAlignment = NSTextAlignmentLeft;
        cell.textLabel.textColor = kLabelTextColor;

        NSString *key = [[itemData valueForKey:@"fieldName"] lowercaseString];
        NSString *questionValue = [self.delegate questionValue:key];
        cell.detailTextLabel.font = [UIFont boldSystemFontOfSize:[UIFont smallSystemFontSize]];
        if (!questionValue || [questionValue isEqualToString:@""]) {
            cell.detailTextLabel.text = NSLocalizedString(@"- seleziona la data -", @"label for cell as date selector");
        }else{
            NSDate *questionDate = [self.util getDateFromString:questionValue withPattern:@"dd/MM/yyyy HH.mm.ss.SSS"];
            cell.detailTextLabel.text = [self.dateFormatter stringFromDate:questionDate];
        }
        
        return cell;
    }else{
    
    
    
    
    RDDetailSimpleCell *cell = [tableView dequeueReusableCellWithIdentifier:cellClassName];
    if (cell == nil){
        Class cellClass = NSClassFromString(cellClassName);
        cell = [cellClass alloc];
        
        cell = [cell initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:cellClassName];
    }
    
    
    cell.delegate = self;
    cell.label.text = [itemData valueForKey:@"label"];
    cell.key = [[itemData valueForKey:@"fieldName"] lowercaseString];
//    cell.key = [itemData valueForKey:@"fieldName"];
    
    cell.maxFrame = CELL_TABLE_MAX_FRAME;
    
    cell.value = [self.delegate questionValue:cell.key];

    if (![[(SurveyQuestionsPageViewController *)self.delegate survey] isNewSurveyy]) {
        if ([itemData valueForKey:@"required"]) {
            if (!cell.value) {
                [cell setRedColorForLabel];
            }else{
                if ([cell.value isKindOfClass:[NSArray class]]) {
                    if ([cell.value count] == 0) {
                        [cell setRedColorForLabel];
                    }else{
                        [cell setDefaultColorForLabel];
                    }
                }else{
                    if ([cell.value isEqualToString:@""]) {
                        [cell setRedColorForLabel];
                    }else{
                        [cell setDefaultColorForLabel];
                    }
                }
            }
        }else{
            [cell setDefaultColorForLabel];
        }
    }
    
    
    if ([cellClassName isEqualToString:@"RDDetailPickListCell"]) {
        if (cell.value) {
            [cell setFieldValueWithString:cell.value];
        }else{
            [cell setFieldValueWithString:NSLocalizedString(@"- seleziona risposta -", @"- seleziona risposta -")];
        }
    } else if ([cellClassName isEqualToString:@"RDDetailNumberCell"]) {
        if ([cell.value length] == 0) {
            if ([[itemData valueForKey:@"required"] integerValue] == 1) {
                [cell setFieldValueWithString:@""];
                cell.value = nil;
            }else{
                [cell setFieldValueWithString:@""];
                cell.value = nil;
//                [cell setFieldValueWithString:@"0"];
//                cell.value = @"0";
            }
        }else{
            [cell setFieldValueWithString:cell.value];
        }
        
    } else if ([cellClassName isEqualToString:@"RDDetailEditableTextCell"]) {
        
        if (cell.value) {
            [cell setFieldValueWithString:cell.value];
        }
        
    } else if ([cellClassName isEqualToString:@"RDDetailYesNoCell"]) {
        
        NSString *valueON = @"1";
        NSString *valueOff = @"";
        
        if ([itemData valueForKey:@"valueWhenON"]) {
            valueON = [itemData valueForKey:@"valueWhenON"];
        }
        if ([itemData valueForKey:@"valueWhenOFF"]) {
            valueOff = [itemData valueForKey:@"valueWhenOFF"];
        }

        cell.controlOnValue = valueON;
        cell.controlOffValue = valueOff;
        
        [cell setSwitchStatus:false];
        if (cell.value) {
            if ([cell.value isKindOfClass:[NSString class]]) {
                NSString *currValue = (NSString *)cell.value;
                if ([currValue isEqualToString:valueON]) {
                    [cell setSwitchStatus:true];
                }
            }
        }
        
    } else if ([cellClassName isEqualToString:@"RDDetailYesNoMultiCell"]) {
        
        NSString *valueString = [itemData valueForKey:@"values"];

        NSArray *values;
        
        NSArray *labls = [[itemData valueForKey:@"labels"] componentsSeparatedByString:@","];

        if (valueString && ![valueString isEqualToString:@""]) {
            values = [valueString componentsSeparatedByString:@","];
        }else{
            values = @[@"1",@"1",@"1",@"1"];
        }



        cell.controlLabels = labls;
        cell.controlValues = values;
        
        [cell appendItems:labls withValues:values];
        
    } else if ([cellClassName isEqualToString:@"RDDetailSegmentedControlCell"]) {

        NSString *labels = [itemData valueForKey:@"segmentedControlLabels"];
        NSString *values = [itemData valueForKey:@"segmentedControlValues"];
        NSArray *segmentedControlLabels = [labels componentsSeparatedByString:@","];
        NSArray *segmentedControlValues = [values componentsSeparatedByString:@","];
        
        cell.controlLabels = segmentedControlLabels;
        cell.controlValues = segmentedControlValues;

        if (cell.value) {
            NSInteger selectedIndex = [segmentedControlValues indexOfObject:cell.value];
            if (selectedIndex != NSNotFound) {
                cell.selectedIndex = selectedIndex;
            }
        }
        
        [cell appendItems:segmentedControlLabels];
        
    }
    
    
    
    NSLog(@"Row: %ld",(long)modelRow);
    NSLog(@"Cell class: %@",cellClassName);
    NSLog(@"Cell key: %@",cell.key);
    NSLog(@"cellID = %@", cellID);
    return cell;
    
    }

}


#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
 
    if ([cell.reuseIdentifier isEqualToString:@"RDDetailPickListCell"]) {        
        [self performSegueWithIdentifier:kPickListSegue sender:self];
//        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }else{
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    
    if ([cell.reuseIdentifier isEqualToString:@"dateCell"]){
        [self displayInlineDatePickerForRowAtIndexPath:indexPath];
    }else{
        if ([self hasInlineDatePicker]) {
            NSLog(@"C'è un date picker aperto!");
            // hide the old date picker
            NSInteger rowToReveal = self.datePickerIndexPath.row -1;
            NSIndexPath *indexPathToReveal = [NSIndexPath indexPathForRow:rowToReveal inSection:indexPath.section];
            
            self.datePickerIndexPath = nil;
            [self toggleDatePickerForSelectedIndexPath:indexPathToReveal];
            
            if (rowToReveal < indexPath.row) {
                indexPath =[NSIndexPath indexPathForRow:indexPath.row -1  inSection:indexPath.section];
            }
        }
    }
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:kPickListSegue]) {
        PickListViewController *destination =  [segue destinationViewController];
        
        int index = (int)[[self.tableView indexPathForSelectedRow] row];
        NSArray *optionsArray = [[[[self.sections valueForKey:@"questions"] objectAtIndex:0] objectAtIndex:index] valueForKey:@"valuesForKey"];
        
        destination.dataArray = [optionsArray mutableCopy];
        destination.pickerTitle = @"Scegli risposta";
        
        
        /*
         if ([[self.activity.answers objectAtIndex:index] response]) {
         destination.selectedItems = [@[[[self.activity.answers objectAtIndex:index] response]] mutableCopy];
         }
         */
        
        
        destination.delegate = self;
        
    }

}

#pragma mark - PikListDelegate methods
- (void)getSelectedItems:(NSMutableArray *)selectedItems{
    NSIndexPath *currentIndexPath = self.tableView.indexPathForSelectedRow;
    

    RDDetailSimpleCell *cell = (RDDetailSimpleCell *)[self.tableView cellForRowAtIndexPath:currentIndexPath];
    cell.value = [selectedItems objectAtIndex:0];
    [self.delegate setQuestion:cell.key withValue:cell.value];
    
    [self.tableView deselectRowAtIndexPath:currentIndexPath animated:YES];
    [self.tableView reloadData];
    
}

#pragma mark - RDDetailCellDelegate methods
- (void) setQuestion:(NSString *)questionUnid withValue:(NSString *)fieldValue {
    NSLog(@"question unid: %@",questionUnid);
    NSLog(@"Field value: %@",fieldValue);
    
    [self.delegate setQuestion:questionUnid withValue:fieldValue];
    
}

#pragma mark - DatePicker Actions

/*! User chose to change the date by changing the values inside the UIDatePicker.
 
 @param sender The sender for this action: UIDatePicker.
 */
- (IBAction)dateAction:(id)sender
{
    NSIndexPath *targetedCellIndexPath = nil;
    
    if ([self hasInlineDatePicker])
    {
        // inline date picker: update the cell's date "above" the date picker cell
        //
        targetedCellIndexPath = [NSIndexPath indexPathForRow:self.datePickerIndexPath.row - 1 inSection:self.datePickerIndexPath.section];
    }
    else
    {
        // external date picker: update the current "selected" cell's date
        targetedCellIndexPath = [self.tableView indexPathForSelectedRow];
    }
    
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:targetedCellIndexPath];
    UIDatePicker *targetedDatePicker = sender;
    
    NSDate *now = [NSDate date];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:now];
    [components setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    [components setHour:23];
    [components setMinute:59];
    [components setSecond:59];
    NSDate *today12pm = [calendar dateFromComponents:components];
    
    if ([targetedDatePicker.date compare:today12pm] == NSOrderedDescending){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Errore" message:@"Data non valida" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }else{
        // update the cell's date string
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd/MM/yyyy"];
        cell.detailTextLabel.text = [dateFormatter stringFromDate:targetedDatePicker.date];
        
        // update our data model
        [dateFormatter setDateFormat:@"dd/MM/yyyy"];
        NSString *fullDate = [NSString stringWithFormat:@"%@ 00.00.00.000",[dateFormatter stringFromDate:targetedDatePicker.date]];
        
        NSArray *questions = [[self.sections objectAtIndex:targetedCellIndexPath.section] valueForKey:@"questions"];

        NSDictionary *itemData;
        itemData = questions[targetedCellIndexPath.row];
        NSString *key = [[itemData valueForKey:@"fieldName"] lowercaseString];

        [self.delegate setQuestion:key withValue:fullDate];
    }

    
    
}


#pragma mark - Actions
- (IBAction)close:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
