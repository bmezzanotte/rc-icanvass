//
//  SurveyQuestionsPageViewController.h
//  iCanvass
//
//  Created by Bernardino on 31/12/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SurveyQuestionGroupViewController.h"
#import "Survey.h"


@interface SurveyQuestionsPageViewController : UIPageViewController<UIPageViewControllerDataSource, UIPageViewControllerDelegate, RDQuestionGroupDelegate>

@property (nonatomic, strong) NSArray *sectionTitles;
//@property (nonatomic) NSInteger numberOfPages;
@property (nonatomic) NSInteger startingIndex;

@property (nonatomic,strong) Survey *survey;

@end
