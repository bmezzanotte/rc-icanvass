//
//  SurveyQuestionsPageViewController.m
//  iCanvass
//
//  Created by Bernardino on 31/12/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import "SurveyQuestionsPageViewController.h"

@interface SurveyQuestionsPageViewController ()
@property (nonatomic, retain) SurveyQuestionGroupViewController *questionGroupViewController;

@property (nonatomic) NSInteger currentPageIndex;
@property (nonatomic, strong) NSArray *sectionViewControllers;
@end

@implementation SurveyQuestionsPageViewController


//override getters

- (SurveyQuestionGroupViewController *)questionGroupViewController{
    if (!_questionGroupViewController){
        UIStoryboard *storyboard = self.storyboard;
        _questionGroupViewController = [storyboard instantiateViewControllerWithIdentifier:@"SurveyQuestionGroupViewController"];
        _questionGroupViewController.titleText = self.sectionTitles[self.startingIndex];
        _questionGroupViewController.sectionIndex = self.startingIndex;

    }
    
    _questionGroupViewController.delegate = self;
    return _questionGroupViewController;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Data source, Delegate & Initial ViewController
//    self.sectionViewControllers = [NSArray arrayWithObjects:self.sectionOneViewController, self.sectionTwoViewController, nil];
    NSURL *plistURL = [[NSBundle mainBundle] URLForResource:@"SurveyDetailTableConfiguration" withExtension:@"plist"];
    NSDictionary *plist = [NSDictionary dictionaryWithContentsOfURL:plistURL];
    
    self.sectionTitles = [[[[plist valueForKey:@"sections"] objectAtIndex:1] valueForKey:@"rows"] valueForKey:@"label"];
    
//    self.numberOfPages = 2;
    self.currentPageIndex = self.startingIndex;
    
    self.dataSource = self;
    self.delegate = self;

    
    [self setViewControllers:@[self.questionGroupViewController]
                   direction:UIPageViewControllerNavigationDirectionForward
                    animated:YES
                  completion:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - RDQuestionGroupDelegate methods
- (void) setQuestion:(NSString *)questionUnid withValue:(NSString *)fieldValue {
    NSLog(@"question unid: %@",questionUnid);
    NSLog(@"Field value: %@",fieldValue);
    
    
    NSString *unid = [NSString stringWithFormat:@"%@_%@",self.survey.unid, questionUnid];
     NSPredicate *predicate = [NSPredicate predicateWithFormat:@"unid = %@",unid];
     NSArray *result = [self.survey.answers filteredArrayUsingPredicate:predicate];
     
     [[result objectAtIndex:0] setResponse:fieldValue];
     
}

- (id)questionValue:(NSString *)questionSubject {
    int pos = [[self.survey.answers valueForKey:@"subject"] indexOfObject:[questionSubject lowercaseString]];
//    int pos = [[self.survey.answers valueForKey:@"subject"] indexOfObject:questionSubject];
    if (pos > [self.survey.answers count]) {
        return nil;
    }else{
        NSString *value = [[self.survey.answers objectAtIndex:pos] valueForKey:@"response"];
        return value;        
    }
}


#pragma mark - UIPageViewControllerDelegate methods
- (void) pageViewController:(UIPageViewController *)pageViewController willTransitionToViewControllers:(NSArray *)pendingViewControllers {
    
    NSLog(@"transition to page %ld",(long)self.currentPageIndex);
}

#pragma mark - UIPageViewControllerDataSource methods
- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    
    NSUInteger index = ((SurveyQuestionGroupViewController*) viewController).sectionIndex;
    
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    if (index == [self.sectionTitles count]) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
    
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    
    NSUInteger index = ((SurveyQuestionGroupViewController*) viewController).sectionIndex;
    
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index];

}



#pragma mark - utility methods for UIPageViewControllerDataSource
- (SurveyQuestionGroupViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if (([self.sectionTitles count] == 0) || (index >= [self.sectionTitles count])) {
        return nil;
    }
    
    // Create a new view controller and pass suitable data.
    SurveyQuestionGroupViewController *pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SurveyQuestionGroupViewController"];
    pageContentViewController.titleText = self.sectionTitles[index];
    pageContentViewController.sectionIndex = index;
    
    self.currentPageIndex = index;
    
    pageContentViewController.delegate = self;
    return pageContentViewController;
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return [self.sectionTitles count];
//    return self.numberOfPages;
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return self.startingIndex;
}

@end
