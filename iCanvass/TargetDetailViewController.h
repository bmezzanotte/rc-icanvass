//
//  TargetDetailViewController.h
//  iCanvass
//
//  Created by NAeS Lavoro on 18/02/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utility.h"

@interface TargetDetailViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *questionDetailButton;
@property (nonatomic, retain) NSManagedObject *target;
@property (nonatomic, retain) NSArray *dataForTable;
@property (nonatomic, retain) Utility *utility;

- (IBAction)questionDetailAction:(id)sender;

@end