//
//  TargetDetailViewController.m
//  iCanvass
//
//  Created by NAeS Lavoro on 18/02/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import "TargetDetailViewController.h"
#import "TargetQuestionsViewController.h"
#import "RDDetailTextCell.h"


@interface TargetDetailViewController ()
@property (nonatomic,strong) NSArray *sections;
@end

@implementation TargetDetailViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    NSURL *plistURL = [[NSBundle mainBundle] URLForResource:@"TargetDetailConfiguration" withExtension:@"plist"];
    NSDictionary *plist = [NSDictionary dictionaryWithContentsOfURL:plistURL];
    self.sections = [plist valueForKey:@"sections"];
    self.utility = [[Utility alloc]init];
    self.questionDetailButton.title = NSLocalizedString(@"Dettagli", @"Dettagli");    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.sections count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSDictionary *detailSection = [self.sections objectAtIndex:section];
    NSArray *rows = [detailSection objectForKey:@"rows"];
    return [rows count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger sectionIndex = [indexPath section];
    NSUInteger rowIndex = [indexPath row];
    NSDictionary *section = [self.sections objectAtIndex:sectionIndex];
    NSArray *rows = [section objectForKey:@"rows"];
    NSDictionary *row = [rows objectAtIndex:rowIndex];
    
    NSString *cellClassName;
    if ([row valueForKey:@"class"]!=nil){
        cellClassName = [row valueForKey:@"class"];
    }else{
        cellClassName = @"RDDetailTextCell";
    }
    NSLog(@"Cell class: %@",cellClassName);
    
    RDDetailTextCell *cell = [tableView dequeueReusableCellWithIdentifier:cellClassName];
    if (cell == nil){
        Class cellClass = NSClassFromString(cellClassName);
        cell = [cellClass alloc];
        cell = [cell initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:cellClassName];
    }
    
    NSString *labelPlistValue = [row objectForKey:@"label"];
    cell.label.text = NSLocalizedString(labelPlistValue, labelPlistValue);
    
    if ([cellClassName isEqualToString:@"RDDetailYesNoCell"]) {
        if ([[self.target valueForKey:[row objectForKey:@"key"]] isEqualToString:@"1"]) {
            cell.fieldValueLabel.text = NSLocalizedString(@"SI", @"SI");
        }else{
            cell.fieldValueLabel.text = NSLocalizedString(@"NO", @"NO");
        }
    }else{
        
        if ([[row objectForKey:@"key"]isEqualToString:@"lastmodified"]) {
            cell.fieldValueLabel.text = [self.utility getStringFromDate:[self.target valueForKey:[row objectForKey:@"key"]] withPattern:@"yyyy/MM/dd HH.mm.ss.SSS"];
        }else if ([[row objectForKey:@"key"]isEqualToString:@"bonus"]||[[row objectForKey:@"key"]isEqualToString:@"goalQuantity"]||[[row objectForKey:@"key"]isEqualToString:@"targetQuantity"]){
            cell.fieldValueLabel.text = [[self.target valueForKey:[row objectForKey:@"key"]]stringValue];
        }else{
            cell.fieldValueLabel.text = [self.target valueForKey:[row objectForKey:@"key"]];
        }
        
        if ([row objectForKey:@"lines"]!=nil){
            cell.fieldValueLabel.lineBreakMode = NSLineBreakByWordWrapping;
            cell.fieldValueLabel.numberOfLines = [[row objectForKey:@"lines"] integerValue];
            [cell.fieldValueLabel sizeToFit];
        }
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0 && indexPath.row == 0) {
        return 70;
    }else{
        return 44;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *sectionName;
    NSDictionary *plistSection = [self.sections objectAtIndex:section];
    
    sectionName = [plistSection objectForKey:@"header"];
    return sectionName;
}

- (IBAction)questionDetailAction:(id)sender {
    [self performSegueWithIdentifier:@"QuestionsDetailIdentifier" sender:sender];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([[segue identifier] isEqualToString:@"QuestionsDetailIdentifier"]){
        TargetQuestionsViewController *destination = segue.destinationViewController;
        destination.target = self.target;
    }
}

@end
