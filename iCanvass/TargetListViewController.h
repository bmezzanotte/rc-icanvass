//
//  TargetListViewController.h
//  iCanvass
//
//  Created by NAeS Lavoro on 18/02/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Utility;
@class DataBaseManager;
@interface TargetListViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, UIPickerViewDataSource,UIPickerViewDelegate>{
    
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIPickerView *timePicker;

- (IBAction)doubleTapGesture:(id)sender;
@end
