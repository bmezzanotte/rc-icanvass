//
//  TargetListViewController.m
//  iCanvass
//
//  Created by NAeS Lavoro on 18/02/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import "TargetListViewController.h"
#import "TargetDetailViewController.h"
#import "DataBaseManager.h"
#import "AppDelegate.h"
#import "Target.h"

@interface TargetListViewController ()
@property (nonatomic, retain) Utility *utility;
@property (nonatomic, retain) DataBaseManager *dbManager;
@property (nonatomic, strong) NSArray *listPeriod;
@property (nonatomic, strong) NSArray *listTarget;
@property (nonatomic, strong) NSString *selectedPeriod;
@end

@implementation TargetListViewController
@synthesize listPeriod = _listPeriod, listTarget = _listTarget, selectedPeriod = _selectedPeriod;
- (void)viewDidLoad
{
    [super viewDidLoad];

    [self.navigationItem setTitle:NSLocalizedString(@"Obiettivi", @"Obiettivi")];
    [self.timePicker setAlpha:0.0];
    self.timePicker.delegate = self;
    
}

- (void)viewWillAppear:(BOOL)animated{
    self.dbManager = [[DataBaseManager alloc]init];

    _listPeriod = [self.dbManager selectFromTable:@"Period" Where:@"status != '9'" OrderBy:@"dateIn" isAscending:NO];

    if ([_listPeriod count]>0) {
        if (! _selectedPeriod) {
            _selectedPeriod = [[_listPeriod objectAtIndex:0]valueForKey:@"code"];
        }
    }else {
        _selectedPeriod = nil;
    }
    
    _listTarget = [self.dbManager selectFromTable:@"Target" Where:[NSString stringWithFormat:@"status != '9' AND periodCode == '%@'",_selectedPeriod] OrderBy:@"subject" isAscending:YES];
    for (Target *item in _listTarget) {
        NSLog(@"codice Periodo:%@",[item valueForKey:@"periodCode"]);
        NSLog(@"subject:%@",[item valueForKey:@"subject"]);
        NSLog(@"disabled fo input:%@",[item valueForKey:@"disabledForInput"]);
//        NSLog(@"goal Type:%@",item.goalType);
//        NSLog(@"goal Type Code:%d",[item.goalTypeCode intValue]);
    }

    [self.tableView reloadData];
}

- (void)viewDidDisappear:(BOOL)animated{
    if ([self.timePicker alpha] == 1.0) {
        [UIView animateWithDuration:0.25 animations:^{
            CGFloat height = 568.0;
            CGRect frame = self.tableView.frame;
            frame.size.height = height;
            self.tableView.frame = frame;
        }];
        [self.timePicker setAlpha:0.0];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    }else{
        NSLog(@"righe = %lu",(unsigned long)[_listTarget count]);
        return [_listTarget count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    if (indexPath.section == 0) {
        NSString *identifier = @"PeriodCellIdentifier";
        cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"PeriodCellIdentifier"];
        }
        
        if ([_selectedPeriod isEqualToString:@"R"]) {
           cell.textLabel.text = NSLocalizedString(@"Attività di routine", @"Attività di routine");
        }else{
            cell.textLabel.text = _selectedPeriod;
        }
    }else{
        NSString *identifier = @"TargetCellIdentifier";
        cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"TargetCellIdentifier"];
        }
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        NSManagedObject *target = [_listTarget objectAtIndex:indexPath.row];
        cell.textLabel.font = [UIFont boldSystemFontOfSize:[UIFont smallSystemFontSize]];
        cell.textLabel.numberOfLines = 0;
        cell.textLabel.text = [target valueForKey:@"subject"];
    }
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return NSLocalizedString(@"Periodo", @"Periodo");
    }else{
        return NSLocalizedString(@"Obiettivo", @"Obiettivo");
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1) {
        return 60;
    }else{
        return 44;
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        [UIView animateWithDuration:0.25 animations:^{
            CGFloat height = 356.0;
            CGRect frame = self.tableView.frame;
            frame.size.height = height;
            self.tableView.frame = frame;
        }];
        
        if ([self.timePicker numberOfRowsInComponent:0] != [_listPeriod count]) {
            [self.timePicker reloadAllComponents];
        }
        
        [self.timePicker setAlpha:1.0];
    }else{
        [UIView animateWithDuration:0.25 animations:^{
            CGFloat height = 568.0;
            CGRect frame = self.tableView.frame;
            frame.size.height = height;
            self.tableView.frame = frame;
        }];
        [self.timePicker setAlpha:0.0];
        //UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        //[self performSegueWithIdentifier:@"detailTargetIdentifier" sender:cell];
        
    }
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (IBAction)doubleTapGesture:(id)sender {
    if ([self.timePicker alpha] == 1.0) {
        [UIView animateWithDuration:0.25 animations:^{
            CGFloat height = 568.0;
            CGRect frame = self.tableView.frame;
            frame.size.height = height;
            self.tableView.frame = frame;
        }];
        [self.timePicker setAlpha:0.0];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([[segue identifier] isEqualToString:@"detailTargetIdentifier"]){
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        TargetDetailViewController *destination = segue.destinationViewController;
        destination.target = [_listTarget objectAtIndex:indexPath.row];
    }
}

#pragma mark - UIPickerView DataSource Method
// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent: (NSInteger)component
{
    return [_listPeriod count] +1;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (row >= [_listPeriod count]) {
        return @"Attività di routine";
    }else{
        NSManagedObject *period = [_listPeriod objectAtIndex:row];
        return [period valueForKey:@"code"];
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if ([_listPeriod count] > 0) {
        NSUserDefaults *userPreference = [[NSUserDefaults alloc] init];
        [userPreference removeObjectForKey:@"currentPeriodTarget"];
        
        if (row >= [_listPeriod count]) {
            _selectedPeriod = @"R";
        }else{
            _selectedPeriod = [[_listPeriod objectAtIndex:row]valueForKey:@"code"];
        }
        _listTarget = [self.dbManager selectFromTable:@"Target" Where:[NSString stringWithFormat:@"status != '9' AND periodCode == '%@'",_selectedPeriod] OrderBy:@"subject" isAscending:YES];
        
        //    NSString *currentUser = [userPreference valueForKey:@"currentPeriodTarget"];
        [self.tableView reloadData];
    }
}

@end