//
//  TargetQuestion.h
//  iCanvass
//
//  Created by Bernardino on 21/02/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Target;

@interface TargetQuestion : NSManagedObject

@property (nonatomic, retain) NSString * options;
@property (nonatomic, retain) NSString * subject;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * unid;
@property (nonatomic, retain) NSNumber * required;
@property (nonatomic, retain) NSDate * lastmodified;
@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) Target *header;

@end
