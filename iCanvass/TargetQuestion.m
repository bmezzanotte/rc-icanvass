//
//  TargetQuestion.m
//  iCanvass
//
//  Created by Bernardino on 21/02/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import "TargetQuestion.h"
#import "Target.h"


@implementation TargetQuestion

@dynamic options;
@dynamic subject;
@dynamic type;
@dynamic unid;
@dynamic required;
@dynamic lastmodified;
@dynamic status;
@dynamic header;

@end
