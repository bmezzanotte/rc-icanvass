//
//  TargetQuestionsViewController.h
//  iCanvass
//
//  Created by Bernardino on 22/02/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TargetQuestionsViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, retain) NSManagedObject *target;

@end
