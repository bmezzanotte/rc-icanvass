//
//  TargetQuestionsViewController.m
//  iCanvass
//
//  Created by Bernardino on 22/02/14.
//  Copyright (c) 2014 NAeS Lavoro. All rights reserved.
//

#import "TargetQuestionsViewController.h"
#import "RDDetailSimpleCell.h"
#import "Target.h"
#import "TargetQuestion.h"

@interface TargetQuestionsViewController ()
@property (nonatomic,strong) Target *header;
@property (nonatomic,strong) NSArray *questions;
@end

@implementation TargetQuestionsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.header = self.target;
//    self.questions = [self.header.questions allObjects];
    
    
    NSLog(@"Category: %@", self.header.category);
    int countAnswers = (int)[self.header.questions count];
    NSLog(@"answers: %d", countAnswers);
    NSSet *myAnswers = self.header.questions;
//    NSArray *answersArray = [myAnswers allObjects];
    
    
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"subject" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *answersArray = [[myAnswers allObjects] sortedArrayUsingDescriptors:sortDescriptors];

    for (TargetQuestion *myAnswer in answersArray) {
        NSLog(@"answer subject: %@", myAnswer.subject);
        NSLog(@"answer type: %@", myAnswer.type);
    }
    
    self.questions = answersArray;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    }else{
        return [self.header.questions count];
//        return [[self.target valueForKey:@"questions"] count];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return 105.0;
    }else{
        return 52.0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *cellClassName;
    cellClassName = @"RDDetailSimpleCell";
    NSLog(@"Cell class: %@",cellClassName);
    
    RDDetailSimpleCell *cell = [tableView dequeueReusableCellWithIdentifier:cellClassName];
    if (cell == nil){
        Class cellClass = NSClassFromString(cellClassName);
        cell = [cellClass alloc];
        cell = [cell initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:cellClassName];
    }
    

    
    if (indexPath.section == 0) {
        cell.label.text = [self.target valueForKey:@"details"];
        cell.label.numberOfLines = 5;
    }else{
        NSString *subject = [[self.questions objectAtIndex:indexPath.row] valueForKey:@"subject"];
        NSString *type = [[self.questions objectAtIndex:indexPath.row] valueForKey:@"type"];
//        NSString *options = [[self.questions objectAtIndex:indexPath.row] valueForKey:@"options"];
        cell.label.text = [NSString stringWithFormat:@"%@ (%@)",subject,type];
        cell.label.numberOfLines = 2;
    }
    
    cell.label.lineBreakMode = NSLineBreakByWordWrapping;
    [cell.label sizeToFit];

    return cell;

}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return NSLocalizedString(@"DESCRIZIONE", @"DESCRIZIONE");
    }else{
        return NSLocalizedString(@"QUESTIONARIO", @"QUESTIONARIO");
    }
}


@end
